<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|



if(strpos(Request::server('HTTP_HOST'),'crm.')!==false){

    $namespace = 'Modules\Crm\Http\Controller';
    Route::group(['domain' => 'crm.'.env('APP_URL')],function($e) {

        Route::get('brm','HomeController@index');
        Route::get('/', function () {
            return view('crm::home');
        })->name('crm.home');

        Auth::routes();

        Route::get('/home', 'HomeController@index')->name('home');

        $developerZones = \App\Models\Developer\Developer::all()->toArray();
        $controllers = \App\Models\Developer\Navigation::where('type','=','controller')->join('controllers','controllers.id','=','navigations.controller_id')->get()->toArray();

        Route::get('/', 'HomeController@index')->name('controllers');

        if(isset($controllers) && !empty($controllers)){
            foreach($controllers as $controller){
                if($controller['navigation']=='crm'){
                    Route::get('/'.$controller['name'], $controller['controller'].'@'.$controller['function'])->prefix('crm')->name($controller['name']);
                }
                else{
                    Route::get('/'.$controller['name'], $controller['controller'].'@'.$controller['function'])->prefix('loyalty')->name($controller['name']);
                }
            }
        }

        Route::prefix('loyalty')->group(function () {
            Route::resource('information','Loyalty\InformationController');
            Route::resource('sales','Loyalty\SalesController');
            Route::resource('loyaltyfronts','Loyalty\LoyaltyfrontController');
            Route::resource('benefits','Loyalty\BenefitController');
            Route::resource('offers','Loyalty\OffersController');
            Route::resource('categoryoffers','Loyalty\CategoryofferController');
            Route::resource('loyaltynavigations','Loyalty\LoyaltyNavigationController');
            Route::resource('subpages','Loyalty\SubpageController');
            Route::resource('galleries','Loyalty\GalleryController');
            Route::resource('images','Loyalty\ImageController');
        });

        Route::prefix('crm')->group(function(){
            Route::resource('roles','Crm\RoleController');
            Route::resource('users','Crm\UserController');
            Route::resource('customers','Crm\CustomerController');
            Route::resource('properties','Crm\PropertyController');
            Route::resource('propertycategories','Crm\PropertyCategoryController');
            Route::resource('customergroups','Crm\CustomerGroupController');
            Route::resource('loyaltyaccountsettings','Crm\LoyaltyAccountSettingController');
            Route::resource('logs','Crm\LogController');
        });

        Route::prefix('developer')->group(function () {
            Route::resource('permission','Developer\PermissionController');
            Route::resource('apideveloper','Developer\ApiDeveloperController');
        });

        if(isset($developerZones) && !empty($developerZones)){
            foreach($developerZones as $developerZone){
                Route::get('/'.$developerZone['name'], $developerZone['controller'].'@'.$developerZone['function'])->prefix('developer')->name($developerZone['name']);
            }
        }


        Route::post('/store','Developer\NavigationController@store')->name('store');
        Route::post('/handleChangeOrder','Developer\NavigationController@handleChangeOrder')->name('handleChangeOrder');
        Route::post('/removeMenuItem','Developer\NavigationController@removeMenuItem')->name('removeMenuItem');
        Route::post('/editItem','Developer\NavigationController@editItem')->name('editItem');
        Route::post('/editProccess','Developer\NavigationController@editProccess')->name('editProccess');


        Route::post('galleries.imagestore/{galleryID}','Loyalty\GalleryController@imagestore')->name('galleries.imagestore');
        Route::post('galleryChangeOrder','Loyalty\GalleryController@galleryChangeOrder')->name('galleryChangeOrder');
        Route::post('galleries.removeImage','Loyalty\GalleryController@removeImage')->name('galleries.removeImage');
        Route::post('galleries.changeActivity','Loyalty\GalleryController@changeActivity')->name('galleries.changeActivity');
        Route::post('galleries.changeActivityGallery','Loyalty\GalleryController@changeActivityGallery')->name('galleries.changeActivityGallery');
    //navigation routes end

    //controller routes start
        Route::post('/storeController','Developer\ControllersController@storeController')->name('storeController');
        Route::post('/removeController','Developer\ControllersController@removeController')->name('removeController');
        Route::post('/editController','Developer\ControllersController@editController')->name('editController');
        Route::post('/editItemController','Developer\ControllersController@editItemController')->name('editItemController');


        Route::post('/changeLang','BaseController@changeLang')->name('changeLang');
    //controller routes end
    //ajax
        Route::post('/handleChangeApiStatus','Developer\ApiDeveloperController@handleChangeApiStatus')->name('handleChangeApiStatus');
    //endajax
        Route::post('/addPoints','Crm\CustomerController@addPoints')->name('addPoints');
        Route::post('/removePoints','Crm\CustomerController@removePoints')->name('removePoints');

        Route::post('/changeActivitySale','Loyalty\SalesController@changeActivitySale')->name('changeActivitySale');

        Route::post('/changeActivityCategoryoffer','Loyalty\CategoryofferController@changeActivityCategoryoffer')->name('changeActivityCategoryoffer');
        Route::post('/changeActivityoffer','Loyalty\OffersController@changeActivityoffer')->name('changeActivityoffer');
        Route::post('/changeActivityProperty','Crm\PropertyController@changeActivityProperty')->name('changeActivityProperty');

        Route::post('/changeActivityProfile','Crm\LoyaltyAccountSettingController@changeActivityProfile')->name('changeActivityProfile');

        Route::post('/customer_change_group','Crm\CustomerController@customer_change_group')->name('customer_change_group');

        Route::post('/change_default_group','Crm\CustomerGroupController@change_default_group')->name('change_default_group');

        Route::post('/navigationChangeOrder','Loyalty\LoyaltyNavigationController@navigationChangeOrder')->name('navigationChangeOrder');
        Route::post('/changeActivityNavigationLoyalty','Loyalty\LoyaltyNavigationController@changeActivityNavigationLoyalty')->name('changeActivityNavigationLoyalty');
    });
}
else{
    dd(Route::post('/change_default_group','Crm\CustomerGroupController@change_default_group')->name('change_default_group'));
}
