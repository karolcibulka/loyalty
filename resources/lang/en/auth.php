<?php
return [
    'validation.required' => 'This field is required',
    'validation.min.string' => 'Minimal length is 8, must contain small letter , large letter and number',
    'validation.regex' => 'Minimal length is 8, must contain small letter , large letter and number',
    'validation.confirmed' => 'Passwords doesnt match',
];
