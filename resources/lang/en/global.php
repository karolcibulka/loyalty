<?php
return [
    'lang.sk' => 'Slovenčina',
    'lang.cz' => 'Čeština',
    'lang.en' => 'Angličtina',
    'lang.de' => 'Nemčina',
];