<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'experience' => 'experience',
    'category' => 'category',
    'property' => 'property',
    'basket' => 'basket',
    'discounts' => 'discounts',
    'home' => 'home',
    'history' => 'history',
    'history_transactions' => 'transactions-history',
    'my_account' => 'my-account',
    'summary' => 'summary',
    'loyalty' => 'loyalty-zone',
    'benefit' => 'benefit',
];
