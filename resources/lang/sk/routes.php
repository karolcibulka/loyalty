<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'experience' => 'zazitok',
    'category' => 'kategoria',
    'property' => 'zariadenie',
    'basket' => 'kosik',
    'home' => 'domov',
    'discounts' => 'aktualne-zlavy',
    'history' => 'historia',
    'history_transactions' => 'historia-transakcii',
    'my_account' => 'moj-ucet',
    'summary' => 'zhrnutie-objednavky',
    'loyalty' => 'vernostna-zona',
    'benefit' => 'vyhoda'
];
