<?php
return [
    'validation.required' => 'Toto pole je povinné',
    'validation.min.string' => 'Minimálna dĺžka je 8 znakov, musí obsahovať veľké aj malé znaky s číslom',
    'validation.regex' => 'Minimálna dĺžka je 8 znakov, musí obsahovať veľké aj malé znaky s číslom',
    'validation.confirmed' => 'Heslá sa nezhodujú',
];
