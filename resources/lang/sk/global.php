<?php
return [
    'lang.sk' => 'Slovenčina',
    'lang.cz' => 'Čeština',
    'lang.en' => 'Angličtina',
    'lang.de' => 'Nemčina',

    'month.january' => 'Január',
    'month.february' => 'Február',
    'month.march' => 'Marec',
    'month.april' => 'Apríl',
    'month.may' => 'Máj',
    'month.june' => 'Jún',
    'month.july' => 'Júl',
    'month.august' => 'August',
    'month.september' => 'September',
    'month.october' => 'Október',
    'month.november' => 'November',
    'month.december' => 'December',
];
