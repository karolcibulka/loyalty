<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([],function(){
    Route::post('/createCustomer','Api\UserController@createCustomer');
    Route::post('/register','Api\UserController@createUser');
    Route::post('/login','Api\UserController@loginUser');
    Route::post('/getUserData','Api\UserController@getUserData');
    Route::post('/updateExternalID','Api\UserController@updateExternalID');
    Route::post('/updateUser','Api\UserController@updateUser');
    Route::post('/changePassword','Api\UserController@changePassword');
    Route::post('/logout','Api\UserController@logoutUser');
    Route::post('/storeExternalReservation','Api\ReservationController@storeExternalReservation');
    Route::post('/storePointsForLoggedUser','Api\PointController@storePointsForLoggedUser');
    Route::post('/storePointsForNonLoggedUser','Api\PointController@storePointsForNonLoggedUser');
    Route::post('/getPossiblePoints','Api\PointController@getPossiblePoints');
});
