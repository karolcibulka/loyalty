<?php

namespace Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class DocsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $documentation = $this->getDocs();
        return view('api::docs.show')->with(array('documentation'=>$documentation));
    }

    private function getDocs(){
        $response = array(
            '0' => array(
                'method' => 'createCustomer',
                'description' => 'Metóda slúžiaca na získanie zákazníka. Ak užívateľ podľa emailu a priezviska existuje vráti ID existujúceho zákazníka, ak neexistuje, vytvorí sa zákazník a vráti ID zákazníka',
                'type' => 'POST',
                'request' => array(
                    'email'=>'<i>required,string</i>',
                    'first_name'=>'<i>required,string</i>',
                    'last_name'=>'<i>required,string</i>',
                    'city'=>'<i>string</i>',
                    'zip'=>'<i>string</i>',
                    'address'=>'<i>string</i>',
                    'state'=>'<i>string</i>',
                ),
                'requestExample' => json_encode(
                    array(
                        'email'=>'testovaci@email.sk',
                        'first_name'=>'test',
                        'last_name'=>'testovaci',
                        'city'=>'Bratislava',
                        'zip'=>'00000',
                        'address'=>'Testovacia',
                        'state'=>'Slovensko',
                    ),JSON_PRETTY_PRINT
                ),
                'response' => array(
                    'id'=>'<i>int</i>',
                ),
                'responseExample' => json_encode(
                    array(
                        'status' => true,
                        'data' => array(
                            'id' => '1738392'
                        )
                    ),JSON_PRETTY_PRINT
                )
            ),
            '1' => array(
                'method' => 'register',
                'description' => 'Metóda slúži na vytvorenie užívateľa, ktorý sa môže následne prihlásiť do loyalty zóny, pri registrácii sa kontroluje existencia zákaznickeho profilu, ak existuje, novovytvorený účet sa prepojí s existujúcim zákazníckym profilom,
                                    ak nie, vytvorí sa nový zákaznícky profil a prepojí sa s novovytvoreným účtom. Po úspešnej registrácii príde užívateľovi aktivačný email.',
                'type' => 'POST',
                'request' => array(
                    'name'=> '<i>required, string</i>',
                    'surname'=>'<i>required, string</i>',
                    'city' => '<i>required, string</i>',
                    'street' => '<i>required, string</i>',
                    'zip' => '<i>required, string</i>',
                    'phone' => '<i>required, string</i>',
                    'email' => '<i>required, string</i>',
                    'country' => '<i>required, string</i>',
                    'password' => '<i>required, string, min:8, regex</i> , musí obsahovať malé písmeno,veľké písmeno, číslo a špeciálny znak',
                    'password_confirmation' => '</i>password == password_confirmation</i>',
                    'newsletter' => '<i>0/1</i>',
                    'language' => '<i>sk/cz/en/de/pl/hu/ru</i> , inak sa použije defaultný jazyk loyalty zóny'
                ),
                'requestExample' => json_encode(
                    array(
                        'name'=> 'test',
                        'surname'=>'testovaci',
                        'city' => 'Test',
                        'street' => 'Test',
                        'zip' => '00000',
                        'phone' => '+421000000000',
                        'email' => 'test@test.sk',
                        'country' => 'SVK',
                        'password' => 'Password100_',
                        'password_confirmation' => 'Password100_',
                        'newsletter' => '1',
                        'language' => 'sk'
                    ),JSON_PRETTY_PRINT
                ),
                'responseExample' => json_encode(
                    array(
                        'status' => true,
                        'message' => 'success'
                    ),JSON_PRETTY_PRINT
                ),
                'responseIsMessage' => true,
            ),
            '2' => array(
                'method' => 'login',
                'description' => 'Metóda slúžiaca na získanie ID užívateľa a TOKEN užívateľa. Prihlásenie je úšpešné ak email a heslo sú správne a učet je aktivovaný. ',
                'type' => 'POST',
                'request' => array(
                    'email'=>'<i>required,string</i>',
                    'password'=>'<i>required , string</i>'
                ),
                'requestExample' => json_encode(
                    array(
                        'email'=>'test@test.sk',
                        'password'=>'Password100_'
                    ),JSON_PRETTY_PRINT

                ),
                'responseExample' => json_encode(
                    array(
                        'status' => true,
                        'data' => array(
                            'idcont' => '12123773',
                            'user_token' => 'K23Jjasdka2323sndjk76128712bajxy8128sd'
                        )

                    ),JSON_PRETTY_PRINT
                ),
                'response' => array(
                    'idcont'=>'<i>ID zákazníka</i>',
                    'user_token'=>'<i>TOKEN zákazníka</i>'),
            ),
            '3' => array(
                'method' => 'getUserData',
                'description' => 'Metóda slúžiaca na získanie aktuálnych dát o zákazníkovi.',
                'type' => 'POST',
                'request' => array(
                    'user_id'=>'<i>required</i> , je potrebné použiť idcont, vrátený z metódy login',
                    'user_token'=>'<i>required</i> , je potrebné použiť user_token, vrátený z metódy login',
                ),
                'requestExample' => json_encode(
                    array(
                        'user_id' => '12123773',
                        'user_token' => 'K23Jjasdka2323sndjk76128712bajxy8128sd',
                    ),JSON_PRETTY_PRINT
                ),
                'response' => array(
                    'firstname' => 'string',
                    'lastname' => 'string',
                    'email' => 'string',
                    'phone' => 'string',
                    'idcont' => 'string',
                    'external_id' => 'string',
                    'master_idcont' => 'string',
                    'register_date' => 'Y-m-d H:i:s',
                    'city' => 'string',
                    'zip' => 'string',
                    'groupID' => 'int',
                    'country' => 'string',
                    'address' => 'string',
                    'points' => 'int',
                    'credit' => 'int',
                    'lang' => 'string',
                    'profile_picture' => 'string',
                    'cardnumber' => 'string',
                ),
                'responseExample' => json_encode(
                    array(
                        'status' => true,
                        'data' => array(
                            'firstname' => 'test',
                            'lastname' => 'test',
                            'email' => 'test@test.sk',
                            'phone' => '+421000000000',
                            'idcont' => '1283791893',
                            'external_id' => '12782781',
                            'master_idcont' => '1283791893',
                            'register_date' => date('Y-m-d H:i:s'),
                            'city' => 'Test',
                            'zip' => '000000',
                            'groupID' => '12',
                            'country' => 'SVK',
                            'address' => 'Test',
                            'points' => '264638',
                            'credit' => '0',
                            'lang' => 'sk',
                            'profile_picture' => 'http://link.com/profile_picture.png',
                            'cardnumber' => '20202828200',
                        ),
                    ),JSON_PRETTY_PRINT
                )
            ),
            '4' => array(
                'method' => 'updateUser',
                'description' => 'Metóda slúži na úpravu profilu zákazníka',
                'type' => 'POST',
                'request' => array(
                    'email' => '<i>required,string</i>',
                    'firstname' => '<i>required,string</i>',
                    'lastname' => '<i>required,string</i>',
                    'city' => '<i>required,string</i>',
                    'address' => '<i>required,string</i>',
                    'zip' => '<i>required,string</i>',
                    'country' => '<i>required,string</i>',
                    'phone' => '<i>required,string</i>',
                    'idcont' => '<i>required,int</i>',
                ),
                'requestExample' => json_encode(
                    array(
                        'email' => 'test@test.sk',
                        'firstname' => 'test',
                        'lastname' => 'test',
                        'city' => 'Test',
                        'address' => 'Test',
                        'zip' => '00000',
                        'country' => 'SVK',
                        'phone' => '+421000000000',
                        'idcont' => '1224242',
                    ),JSON_PRETTY_PRINT
                ),
                'responseExample' => json_encode(
                    array(
                        'status' => true,
                        'message' => 'success'
                    ),JSON_PRETTY_PRINT
                ),
                'responseIsMessage' => true
            ),
            '5' => array(
                'method' => 'changePassword',
                'description' => 'Metóda slúžiaca na zmenu hesla',
                'type' => 'POST',
                'request' => array(
                    'idcont'=>'co to je',
                    'old_password'=>'<i>required,string</i>',
                    'password' => '<i>required, string, min:8, regex</i> , musí obsahovať malé písmeno,veľké písmeno, číslo a špeciálny znak, musí byť iné ako nové heslo',
                    'password_confirmation' => '</i>password == password_confirmation</i>',
                ),
                'requestExample' => json_encode(
                    array(
                        'idcont'=>'1273773',
                        'old_password'=>'Password100_',
                        'password' => 'Password1000_',
                        'password_confirmation' => 'Password1000_',
                    ),JSON_PRETTY_PRINT
                ),
                'responseExample' => json_encode(
                    array(
                        'status' => true,
                        'message' => 'success'
                    ),JSON_PRETTY_PRINT
                ),
                'responseIsMessage' => true
            ),
            '6' => array(
                'method' => 'storeExternalReservation',
                'description' => 'Metóda slúži na uloženie rezervácie pre zákaznika, ak existuje zistí ,či užívateľ má profil na pripočítanie bodov a následne mu pridá body,  ak neexistuje, zákazník sa vytvorí a priradia sa mu body ak existuje defaultný profil pre pripočítavanie bodov',
                'type' => 'POST',
                'request' => array(
                    'last_name' => '<i>required,string</i>',
                    'email' => '<i>required,string</i>',
                    'external_id' => '<i>required,int</i>',
                    'price' => '<i>required,floor</i>',
                ),
                'requestExample' => json_encode(
                    array(
                        'last_name' => 'test',
                        'email' => 'test@test.sk',
                        'external_id' => '712818',
                        'price' => '1250.50',
                    ),JSON_PRETTY_PRINT
                ),
                'responseIsMessage' => true,
                'responseExample' => json_encode(
                    array(
                        'status' => true,
                        'message' => 'success'
                    ),JSON_PRETTY_PRINT
                ),
            ),
            '7' => array(
                'method' => 'storePointsForLoggedUser',
                'description' => 'Zápis bodov pre prihláseného zákazníka z extérneho systému',
                'type' => 'POST',
                'request' => array(
                    'customer_id' => '<i>required,int</i> , ID zákazníka - idcont',
                    'price' => '<i>required,floor</i>',
                    'external_id' => '<i>required,int</i>',
                ),
                'requestExample' => json_encode(
                    array(
                        'customer_id' => '1272181',
                        'external_id' => '712818',
                        'price' => '1250.50',
                    ),JSON_PRETTY_PRINT
                ),
                'responseIsMessage' => true,
                'responseExample' => json_encode(
                    array(
                        'status' => true,
                        'message' => 'success'
                    ),JSON_PRETTY_PRINT
                ),
            ),
            '8' => array(
                'method' => 'storePointsForNonLoggedUser',
                'description' => 'Zápis bodov pre neprihláseného zákazníka z extérneho systému',
                'type' => 'POST',
                'request' => array(
                    'last_name' => '<i>required,string</i>',
                    'email' => '<i>required,string</i>',
                    'price' => '<i>required,floor</i>',
                    'external_id' => '<i>required,int</i>',
                ),
                'requestExample' => json_encode(
                    array(
                        'last_name' => 'test',
                        'email' => 'test@test.sk',
                        'external_id' => '712818',
                        'price' => '1250.50',
                    ),JSON_PRETTY_PRINT
                ),
                'responseIsMessage' => true,
                'responseExample' => json_encode(
                    array(
                        'status' => true,
                        'message' => 'success'
                    ),JSON_PRETTY_PRINT
                ),
            ),
            '9' => array(
                'method' => 'getPossiblePoints',
                'description' => 'Metóda slúži na získanie informácie, koľko zákazník môže získať bodov za nákup , pri prepočte z defaultného profilu',
                'type' => 'POST',
                'request' => array(
                    'price' => '<i>required</i> , suma rezervácie',
                ),
                'requestExample' => json_encode(
                    array(
                        'price' => '1250.50',
                    ),JSON_PRETTY_PRINT
                ),
                'response' => array(
                    'points'=>'<i>int</i> , vráti počet bodov, ktoré môže neregistrovaný užívateľ získať, podľa defaultného profilu',
                ),
                'responseExample' => json_encode(
                    array(
                        'status' => true,
                        'data' => array(
                            'points' => '125',
                        ),
                    ),JSON_PRETTY_PRINT
                ),
            ),
        );

        return $response;
    }
}
