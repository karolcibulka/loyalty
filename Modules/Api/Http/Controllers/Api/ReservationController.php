<?php

namespace Modules\Api\Http\Controllers\Api;


use App\Libraries\LogLibrary;
use App\Models\Crm\Customer;
use App\Models\Crm\CustomerGroup;
use App\Models\Crm\Point;
use Ejarnutowski\LaravelApiKey\Models\ApiKey;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Modules\Api\Http\Controllers\ApiController;
use Modules\Crm\Libraries\PointLibrary;

class ReservationController extends ApiController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function storeExternalReservation(Request $request){

        $this->storeLog($request,'ReservationController','storeExternalReservation');

        $validator = Validator::make($request->all(), [
            'last_name' => 'required',
            'email' => 'required',
            'external_id' => 'required',
            'price' => 'required',
        ]);

        if($validator->fails()){
            return response(array(
                'status'=>false,
                'message'=> $validator->messages(),
            ));
        }

        $data = $request->all();


        $customer = Customer::where('email','=',$data['email'])->where('encrypted_last_name','=',Str::slug($data['last_name'],""))->first();
        if(!is_null($customer)){
            $pointLibrary = new PointLibrary($customer['id'],$data['price'],$data['external_id']);
            if($pointLibrary->checkCustomerAndHisProfile()){
                if($pointLibrary->searchGroupAndAddPoints('ADD')){
                    return response(array(
                        'status'=>true,
                        'message'=> 'Úspešne pridané body',
                    ));
                }
                else{
                    return response(array(
                        'status'=>false,
                        'message'=> 'Nie je možné priradiť body',
                    ));
                }
            }
            else{
                return response(array(
                    'status'=>false,
                    'message'=> 'Nie je možné priradiť body',
                ));
            }
        }
        else{

            $group_id = '';
            $default_group = CustomerGroup::where('is_default','=','1')->first();
            if(!is_null($default_group)){
                $group_id = $default_group['id'];
            }

            $insertNewCustomer = array(
                'encrypted_last_name' => Str::slug($data['last_name'],""),
                'email' => $data['email'],
                'created_at' => date('Y-m-d H:i:s'),
                'group_id' => $group_id,
            );

            $customerID = Customer::insertGetId($insertNewCustomer);

            Point::insert(array('customer_id'=>$customerID,'created_at'=>date('Y-m-d H:i:s'),'points' => '0'));

            $pointLibrary = new PointLibrary($customerID,$data['price'],$data['external_id']);
            if($pointLibrary->checkCustomerAndHisProfile()){
                if($pointLibrary->searchGroupAndAddPoints('ADD')){
                    return response(array(
                        'status'=>true,
                        'message'=> 'Úspešne pridané body',
                    ));
                }
                else{
                    return response(array(
                        'status'=>false,
                        'message'=> 'Nie je možné priradiť body',
                    ));
                }
            }
            else{
                return response(array(
                    'status'=>false,
                    'message'=> 'Nie je možné priradiť body',
                ));
            }
        }
    }

    private function storeLog($request,$controller,$action){
        $api_key = $request->header('x-authorization');
        $api = ApiKey::where('key','=',$api_key)->first();
        $api['id'];

        $log = new LogLibrary($controller,$action,null,'1', $api['id'],serialize($request->input()));
        $log->storeLog();
    }
}
