<?php

namespace Modules\Api\Http\Controllers\Api;


use App\Libraries\CustomerLibrary;
use App\Libraries\LogLibrary;

use App\Libraries\PointLibraryApi;
use App\Models\Crm\Point;
use Ejarnutowski\LaravelApiKey\Models\ApiKey;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Modules\Api\Http\Controllers\ApiController;

class PointController extends ApiController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function storePointsForLoggedUser(Request $request){

        $this->storeLog($request,'PointController','storePointsForLoggedUser');


        $validator = Validator::make($request->all(), [
            'customer_id' => 'required',
            'price' => 'required',
            'external_id' => 'required',
        ]);

        if($validator->fails()){
            return response()->json(array(
                'status'=>false,
                'message'=> $validator->messages(),
            ));
        }

        $data = $request->all();
        $pointLibrary = new PointLibraryApi($data['customer_id'],$data['price'],$data['external_id']);
        if($pointLibrary->checkCustomerAndHisProfile()){
            if($pointLibrary->searchGroupAndAddPoints('ADD')){
                return response()->json(array(
                    'status'=>true,
                    'message'=> 'Úspešne pridané body',
                ));
            }
            else{
                return response()->json(array(
                    'status'=>false,
                    'message'=> 'Nepodarilo sa pridať body',
                ));
            }
        }
        else{
            return response()->json(array(
                'status'=>false,
                'message'=> 'Zákazník neexistuje',
            ));
        }
    }

    public function storePointsForNonLoggedUser(Request $request){
        $this->storeLog($request,'PointController','storePointsForNonLoggedUser');

        $validator = Validator::make($request->all(), [
            'last_name' => 'required',
            'email' => 'required',
            'price' => 'required',
            'external_id' => 'required',
        ]);

        if($validator->fails()){
            return response()->json(array(
                'status'=>false,
                'message'=> $validator->messages(),
            ));
        }


        $data = $request->all();
        $customerLibrary = new CustomerLibrary();
        $customerID = $customerLibrary->getOrCreateCustomer($data['email'],$data['last_name']);
        $pointLibrary = new PointLibraryApi($customerID,$data['price'],$data['external_id']);
        if($pointLibrary->checkCustomerAndHisProfile()){
            if($pointLibrary->searchGroupAndAddPoints('ADD')){
                return response()->json(array(
                    'status'=>true,
                    'message'=> 'Úspešne pridané body',
                ));
            }
            else{
                return response()->json(array(
                    'status'=>false,
                    'message'=> 'Nepodarilo sa pridať body',
                ));
            }
        }
        else{
            return response()->json(array(
                'status'=>false,
                'message'=> 'Zákazník neexistuje',
            ));
        }
    }

    public function getPossiblePoints(Request $request){
        $this->storeLog($request,'PointController','getPossiblePoints');

        $validator = Validator::make($request->all(), [
            'price' => 'required',
        ]);

        if($validator->fails()){
            return response()->json(array(
                'status'=>false,
                'message'=> $validator->messages(),
            ));
        }


        $data = $request->all();
        $customerLibrary = new CustomerLibrary();
        $defaultGroup = $customerLibrary->getDefaultCustomerGroup();
        if(!is_null($defaultGroup)){
            $pointLibrary = new PointLibraryApi(null,$data['price'],null,$defaultGroup);
            $profiles = $pointLibrary->getProfiles();
            if(!empty($profiles)){
                $points = $pointLibrary->countPoints($profiles);

                return response()->json(array(
                    'status' => true,
                    'data' => array(
                        'points' => $points,
                    ),
                ));
            }
            else{
                return response()->json(array(
                    'status' => false,
                    'message' => 'Pre danú skupinu nie sú dostupné žiadne aktívne profily!'
                ));
            }
        }
        else{
            return response()->json(array(
                'status' => false,
                'message' => 'Nie je definovaná defaultná skupina!'
            ));
        }
    }

    private function storeLog($request,$controller,$action){
        $api_key = $request->header('x-authorization');
        $api = ApiKey::where('key','=',$api_key)->first();

        $log = new LogLibrary($controller,$action,null,'1', $api['id'],serialize($request->input()),null);
        $log->storeLog();
    }
}
