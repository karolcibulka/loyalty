<?php

namespace Modules\Api\Http\Controllers\Api;

use App\Libraries\CustomerLibrary;
use App\Libraries\LogLibrary;
use App\Libraries\MarketingLibrary;
use App\Mail\ActivationMail;
use App\Models\Crm\ConnectionCustomer;
use App\Models\Crm\Customer;
use App\Models\Crm\CustomerGroup;
use App\Models\Crm\CustomerUser;
use App\Models\Crm\Point;
use App\Models\Loyalty\LoyaltySetting;
use Ejarnutowski\LaravelApiKey\Models\ApiKey;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Modules\Api\Http\Controllers\ApiController;

class UserController extends ApiController
{

    public $response;
    public $loyaltyPath;

    public function __construct()
    {
        parent::__construct();
        $this->response    = 'id'; //  id || token
        $this->loyaltyPath = Config::has('theme.loyaltyPath') ? Config::get('theme.loyaltyPath') : '';
    }

    public function createCustomer(Request $request)
    {

        $this->storeLog($request, 'UserController', 'createCustomer');

        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'last_name' => 'required',
            'first_name' => 'required',
        ]);

        if ($validator->fails()) {
            return response(array(
                'status' => false,
                'message' => $validator->messages(),
            ));
        } else {
            $data                        = $request->all();
            $data['encrypted_last_name'] = $sluggedLastName = Str::slug($data['last_name'], '');
            $customer                    = Customer::select('*')->where('email', '=', $data['email'])->where('encrypted_last_name', '=', $sluggedLastName)->get()->first();

            if ($customer) {
                $cstmr = $customer->toArray();

                return response(array(
                    'status' => true,
                    'data' => array(
                        $this->response => $cstmr[$this->response]
                    )
                ));
            } else {

                $customer_group         = null;
                $customer_group_default = CustomerGroup::where('is_default', '=', '1')->first();
                if (!empty($customer_group_default)) {
                    $customer_group = $customer_group_default['id'];
                }

                $customerLibrary = new CustomerLibrary();
                $cardNumberFormat = $customerLibrary->getCardNumberFormat();
                $cardNumber = $customerLibrary->getCardNumber($cardNumberFormat);

                $insertData = array(
                    'email' => $data['email'], //required
                    'encrypted_last_name' => $data['encrypted_last_name'], //auto generated
                    'last_name' => $data['last_name'], //required
                    'first_name' => $data['first_name'], //required
                    'token' => Str::random(40), // auto_generated [40]
                    'city' => (isset($data['city']) && !empty($data['city'])) ? $data['city'] : '',  //isset&&!empty
                    'zip' => (isset($data['zip']) && !empty($data['zip'])) ? $data['zip'] : '', //isset&&!empty
                    'adress' => (isset($data['address']) && !empty($data['address'])) ? $data['address'] : '', //isset&&!empty
                    'state' => (isset($data['state']) && !empty($data['state'])) ? $data['state'] : '', //isset&&!empty
                    'group_id' => $customer_group,
                    'card_number' => $cardNumber,
                    'created_at' => date('Y-m-d H:i:s') //auto_generated
                );
                $id         = Customer::insertGetId($insertData);

                Point::insert(array('customer_id' => $id, 'points' => '0', 'created_at' => 'Y-m-d H:i:s'));

                $customer = Customer::where('id', '=', $id)->first()->toArray();

                return response(array(
                    'status' => true,
                    'data' => array(
                        $this->response => $customer[$this->response]
                    ),
                ));
            }
        }
    }

    public function createUser(Request $request)
    {

        $this->storeLog($request, 'UserController', 'createUser');

        $data      = $request->all();
        $validator = $this->validator($data);

        if ($validator->fails()) {
            return response(array(
                'status' => false,
                'message' => $this->generateMessage($validator->errors()),
            ));
        } else {
            $token = Str::random('40');

            $ls      = LoyaltySetting::first();
            $actived = !is_null($ls) && isset($ls['activation_token_durability']) && !empty($ls['activation_token_durability']) ? $ls['activation_token_durability'] : 60;

            $createCustomerUser = array(
                'first_name' => $data['name'],
                'last_name' => $data['surname'],
                'encrypted_last_name' => Str::slug($data['surname'], ''),
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'token' => $token,
                'phone' => $data['phone'],
                'city' => $data['city'],
                'language' => isset($data['language']) && !empty($data['language']) ? $data['language'] : '',
                'address' => $data['street'],
                'zip' => $data['zip'],
                'country' => $data['country'],
                'newsletter' => isset($data['newsletter']) && !empty($data['newsletter']) ? $data['newsletter'] : '0',
                'created_at' => date('Y-m-d H:i:s'),
                'activated' => '0',
                'token_active_to' => date('Y-m-d H:i:s', strtotime('+ ' . $actived . ' minutes')),
                'last_password_changed' => date('Y-m-d H:i:s')
            );



            $link = route('activation', ['token' => $token]);
            $mail = Mail::to($data['email'])->send(new ActivationMail($link));

            $marketingLibrary = new MarketingLibrary();
            if ($data['newsletter'] == '1') {
                $marketingLibrary->sendNewsletterActivationEmail($createCustomerUser);
                $updateDataCustomerUser['newsletter_active'] = '0';
            }

            $customer = Customer::where('email', '=', $data['email'])->where('encrypted_last_name', '=', $createCustomerUser['encrypted_last_name'])->first();

            if ( !is_null($customer) && isset($customer) && !empty($customer) ) {

                $createCustomerUser['customer_id'] = $customer['id'];
                $customerUserID = CustomerUser::insertGetID($createCustomerUser);


            } else {
                $group          = CustomerGroup::where('is_default', '=', '1')->first();

                $customerLibrary = new CustomerLibrary();
                $cardNumberFormat = $customerLibrary->getCardNumberFormat();
                $cardNumber = $customerLibrary->getCardNumber($cardNumberFormat);

                $createCustomer = array(
                    'first_name' => $data['name'],
                    'last_name' => $data['surname'],
                    'encrypted_last_name' => Str::slug($data['surname'], ''),
                    'email' => $data['email'],
                    'phone' => $data['phone'],
                    'city' => $data['city'],
                    'adress' => $data['street'],
                    'zip' => $data['zip'],
                    'state' => $data['country'],
                    'token' => Str::random('40'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'is_registered' => '1',
                    'card_number' => $cardNumber,
                    'group_id' => $group ? $group['id'] : null,
                );

                $customerID = Customer::insertGetId($createCustomer);

                $createCustomerUser['customer_id'] = $customerID;
                $customerUserID = CustomerUser::insertGetID($createCustomerUser);

                $point = Point::insert(array('customer_id' => $customerID, 'points' => '0', 'created_at' => date('Y-m-d H:i:s')));

            }

            return response(array(
                    'status' => true,
                    'message' => 'Na email Vám bol odoslaný aktivačný email'
                )
            );

        }
    }

    protected function generateMessage($errors)
    {
        $message = '';
        $message .= 'Heslo musí obsahovať malé písmeno,veľké písmeno,číslo a mať min. 8 znakov<br>';
        $message .= 'Heslo sa musí zhodovať!<br>';

        $message .= 'Všetky polia su povinné!<br>';
        return $message;
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'surname' => ['required', 'string', 'max:255'],
            'city' => ['required', 'string', 'max:255'],
            'street' => ['required', 'string', 'max:255'],
            'zip' => ['required', 'string', 'max:255'],
            'country' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:customer_users'],
            'password' => ['required', 'string', 'min:8', 'confirmed', 'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$/'],
        ]);
    }

    public function loginUser(Request $request)
    {

        $this->storeLog($request, 'UserController', 'loginUser');

        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response(array(
                'status' => false,
                'message' => $validator->messages(),
            ));
        } else {
            $data       = $request->all();

            $loggedU = CustomerUser::where('email', '=', $data['email']);
            $loggedUser = $loggedU->first();
            if (is_null($loggedUser)) {
                return response(array(
                    'status' => false,
                    'message' => 'Užívateľ s takýmto emailom neexistuje!'
                ));
            } else {
                $customer = Customer::where('email', '=', $loggedUser['email'])->where('encrypted_last_name', '=', $loggedUser['encrypted_last_name'])->leftJoin('points', 'points.customer_id', '=', 'customers.id')->first();
                if (Hash::check($data['password'], $loggedUser['password'])) {
                    if($loggedUser['activated'] == '1'){
                        return response(array(
                            'status' => true,
                            'data' => array(
                                'idcont' => $customer['id'],
                                'user_token' => $customer['token'],
                            )
                        ));
                    }
                    else{

                        $token = Str::random(40);
                        $loyalty = LoyaltySetting::first();
                        $minutes = $loyalty && isset($loyalty['activation_token_durability']) && !empty($loyalty['activation_token_durability']) ? $loyalty['activation_token_durability'] : 60;

                        $link = route('activation', ['token' => $token]);
                        $mail = Mail::to($loggedUser['email'])->send(new ActivationMail($link));

                        $loggedU->update(array('token'=>$token,'token_active_to'=>date('Y-m-d H:i:s',strtotime('+ '.$minutes.' minutes'))));

                        return response(array(
                            'status' => false,
                            'message' => 'Účet nie je aktivovaný! Na email Vám prišiel nový aktivačný kód',
                        ));
                    }
                } else {
                    return response(array(
                        'status' => false,
                        'message' => 'Nesprávne heslo!',
                    ));
                }
            }
        }
    }

    public function getUserData(Request $request)
    {

        //$this->storeLog($request, 'UserController', 'getUserData');

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'user_token' => 'required',
        ]);
        if ($validator->fails()) {
            return response(array(
                'status' => false,
                'message' => $validator->messages(),
            ));
        }
        $data     = $request->all();
        $customer = Customer::select('customers.*', 'points.points', 'cu.external_id as externalID')
            ->leftJoin('customer_users as cu', 'cu.customer_id', '=', 'customers.id')
            ->where('customers.id', '=', $data['user_id'])
            ->where('customers.token', '=', $data['user_token'])
            ->leftJoin('points', 'points.customer_id', '=', 'customers.id')
            ->first();

        if (!is_null($customer)) {
            return response(array(
                'status' => true,
                'data' => array(
                    'firstname' => $customer['first_name'],
                    'lastname' => $customer['last_name'],
                    'email' => $customer['email'],
                    'phone' => $customer['phone'],
                    'idcont' => $customer['id'],
                    'external_id' => $customer['externalID'],
                    'master_idcont' => $customer['id'],
                    'register_date' => date('Y-m-d H:i:s', strtotime($customer['created_at'])),
                    'city' => $customer['city'],
                    'zip' => $customer['zip'],
                    'groupID' => $customer['group_id'],
                    'country' => $customer['state'],
                    'address' => $customer['adress'],
                    'points' => $customer['points'],
                    'credit' => '0',
                    'lang' => 'sk',
                    'profile_picture' => isset($customer['profile_image']) && !empty($customer['profile_image']) && is_file(asset('profile_images/' . $customer['profile_image'])) ? asset('profile_images/' . $customer['profile_image']) : 'https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg',
                    'cardnumber' => $customer['card_number']
                )
            ));
        } else {
            return response(array(
                'status' => false,
                'message' => 'Zákazník s týmto id alebo tokenom neexistuje'
            ));
        }
    }

    public function updateExternalID(Request $request)
    {
        $this->storeLog($request, 'UserController', 'updateExternalID');

        $validator = Validator::make($request->all(), [
            'idcont' => 'required',
            'external_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response(array(
                'status' => false,
                'message' => 'Nesplnená validácia pri updanutí extérneho id!'
            ));
        } else {
            $data         = $request->all();
            $customer     = CustomerUser::where('customer_id', '=', $data['idcont']);
            $customerData = $customer->first();
            if ($customerData) {
                $customer->update(array('external_id' => $data['external_id']));

                return response(array(
                    'status' => true,
                    'message' => 'Užívateľ bol úspešne updatnutý!',
                ));
            } else {
                return response(array(
                    'status' => false,
                    'message' => 'Užívateľ s daným id neexistuje',
                ));
            }
        }
    }

    public function logoutUser(Request $request)
    {

        $this->storeLog($request, 'UserController', 'logoutUser');


        $validator = Validator::make($request->all(), [
            $this->response => 'required',
        ]);

        if ($validator->fails()) {
            return response(array(
                'status' => false,
                'message' => 'Nesplnená validácia pri odhlásení!'
            ));
        } else {
            $data     = $request->all();
            $customer = CustomerUser::where($this->response, '=', $data[$this->response])->get()->first();
            if ($customer) {
                return response(array(
                    'status' => true,
                    'message' => 'Užívateľ bol úspešne odhlasený!',
                ));
            } else {
                return response(array(
                    'status' => false,
                    'message' => 'Užívateľ s daným ' . $this->response . ' neexistuje',
                ));
            }
        }
    }

    public function updateUser(Request $request)
    {
        $this->storeLog($request, 'UserController', 'updateUser');

        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'firstname' => 'required',
            'lastname' => 'required',
            'city' => 'required',
            'address' => 'required',
            'zip' => 'required',
            'country' => 'required',
            'phone' => 'required',
            'idcont' => 'required',
        ]);
        if ($validator->fails()) {
            return response(array(
                'status' => false,
                'message' => $validator->messages(),
            ));
        } else {
            $data = $request->all();

            $updateData = array(
                'first_name' => $data['firstname'],
                'phone' => $data['phone'],
                'city' => $data['city'],
                'zip' => $data['zip'],
                'adress' => $data['address'],
                'state' => $data['country'],
            );

            $customer = Customer::where('id', '=', $data['idcont'])->update($updateData);

            return response(array(
                'status' => true,
                'message' => 'Úspešne updatnuté',
            ));
        }
    }

    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password' => ['required'],
            'idcont' => ['required'],
            'password' => ['required', 'string', 'min:8', 'confirmed', 'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$/'],
        ]);

        if ($validator->fails()) {
            return response(array(
                'status' => false,
                'message' => '<ul><li>Všetky vstupy sú <strong>povinné</strong></li><li>Heslo musí obsahovať <strong>malé písmeno, veľké písmeno, číslo a mať minimálne 8 znakov</strong></li><li>Nové heslo nebolo <strong>zhodné</strong> s potvrdzovacím heslom</li></ul>',
            ));
        }

        $data      = $request->all();
        $customerM = CustomerUser::where('customer_id', '=', $data['idcont']);
        $customer  = $customerM->first();

        if (Hash::check($data['old_password'], $customer['password'])) {
            if ($data['old_password'] != $data['password']) {

                $newPassword = Hash::make($data['password']);

                $customerM->update(array('password' => $newPassword, 'last_password_changed' => date('Y-m-d H:i:s')));

                return response(array(
                    'status' => false,
                    'message' => 'Heslo bolo <strong>úspešne</strong> zmenené !',
                ));

            } else {
                return response(array(
                    'status' => false,
                    'message' => 'Nové heslo <strong>nemôže byť<strong> rovnaké ako pôvodné',
                ));
            }
        } else {
            return response(array(
                'status' => false,
                'message' => 'Zadané pôvodné heslo sa <strong>nezhoduje<strong>',
            ));
        }
    }

    private function storeLog($request, $controller, $action)
    {
        $api_key = $request->header('x-authorization');
        $api     = ApiKey::where('key', '=', $api_key)->first();
        $api['id'];

        $log = new LogLibrary($controller, $action, null, '1', $api['id'], serialize($request->input()));
        $log->storeLog();
    }


}
