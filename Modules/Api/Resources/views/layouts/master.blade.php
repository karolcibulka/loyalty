<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>API docs, loyalty</title>


        <style>
            /*!
 * Start Bootstrap - Simple Sidebar (https://startbootstrap.com/template-overviews/simple-sidebar)
 * Copyright 2013-2019 Start Bootstrap
 * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap-simple-sidebar/blob/master/LICENSE)
 */
            body {
                overflow-x: hidden;
            }

            #sidebar-wrapper {
                min-height: 100vh;
                margin-left: -15rem;
                -webkit-transition: margin .25s ease-out;
                -moz-transition: margin .25s ease-out;
                -o-transition: margin .25s ease-out;
                transition: margin .25s ease-out;
            }

            #sidebar-wrapper .sidebar-heading {
                padding: 0.875rem 1.25rem;
                font-size: 1.2rem;
            }

            #sidebar-wrapper .list-group {
                width: 15rem;
            }

            #page-content-wrapper {
                min-width: 100vw;
            }

            #wrapper.toggled #sidebar-wrapper {
                margin-left: 0;
            }

            @media (min-width: 768px) {
                #sidebar-wrapper {
                    margin-left: 0;
                }

                #page-content-wrapper {
                    min-width: 0;
                    width: 100%;
                }

                #wrapper.toggled #sidebar-wrapper {
                    margin-left: -15rem;
                }
            }

        </style>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
        <style type="text/css">
            /* GitHub stylesheet for MarkdownPad (http://markdownpad.com) */
            /* Author: Nicolas Hery - http://nicolashery.com */
            /* Version: b13fe65ca28d2e568c6ed5d7f06581183df8f2ff */
            /* Source: https://github.com/nicolahery/markdownpad-github */

            /* RESET
            =============================================================================*/

            html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, embed, figure, figcaption, footer, header, hgroup, menu, nav, output, ruby, section, summary, time, mark, audio, video {
                margin: 0;
                padding: 0;
                border: 0;
            }

            /* BODY
            =============================================================================*/

            body {
                font-family: Helvetica, arial, freesans, clean, sans-serif;
                font-size: 14px;
                line-height: 1.6;
                color: #333;
                background-color: #fff;
                padding: 0px;
                width: 100%;
                margin: 0 auto;
            }

            body>*:first-child {
                margin-top: 0 !important;
            }

            body>*:last-child {
                margin-bottom: 0 !important;
            }

            /* BLOCKS
            =============================================================================*/

            p, blockquote, ul, ol, dl, table, pre {
                margin: 15px 0;
            }

            /* HEADERS
            =============================================================================*/

            h1, h2, h3, h4, h5, h6 {
                margin: 20px 0 10px;
                padding: 0;
                font-weight: bold;
                -webkit-font-smoothing: antialiased;
            }

            h1 tt, h1 code, h2 tt, h2 code, h3 tt, h3 code, h4 tt, h4 code, h5 tt, h5 code, h6 tt, h6 code {
                font-size: inherit;
            }

            h1 {
                font-size: 28px;
                color: #000;
            }

            h2 {
                font-size: 24px;
                border-bottom: 1px solid #ccc;
                color: #000;
            }

            h3 {
                font-size: 18px;
            }

            h4 {
                font-size: 16px;
            }

            h5 {
                font-size: 14px;
            }

            h6 {
                color: #777;
                font-size: 14px;
            }

            body>h2:first-child, body>h1:first-child, body>h1:first-child+h2, body>h3:first-child, body>h4:first-child, body>h5:first-child, body>h6:first-child {
                margin-top: 0;
                padding-top: 0;
            }

            a:first-child h1, a:first-child h2, a:first-child h3, a:first-child h4, a:first-child h5, a:first-child h6 {
                margin-top: 0;
                padding-top: 0;
            }

            h1+p, h2+p, h3+p, h4+p, h5+p, h6+p {
                margin-top: 10px;
            }

            /* LINKS
            =============================================================================*/

            a {
                color: #4183C4;
                text-decoration: none;
            }

            a:hover {
                text-decoration: underline;
            }

            /* LISTS
            =============================================================================*/

            ul, ol {
                padding-left: 30px;
            }

            ul li > :first-child,
            ol li > :first-child,
            ul li ul:first-of-type,
            ol li ol:first-of-type,
            ul li ol:first-of-type,
            ol li ul:first-of-type {
                margin-top: 0px;
            }

            ul ul, ul ol, ol ol, ol ul {
                margin-bottom: 0;
            }

            dl {
                padding: 0;
            }

            dl dt {
                font-size: 14px;
                font-weight: bold;
                font-style: italic;
                padding: 0;
                margin: 15px 0 5px;
            }

            dl dt:first-child {
                padding: 0;
            }

            dl dt>:first-child {
                margin-top: 0px;
            }

            dl dt>:last-child {
                margin-bottom: 0px;
            }

            dl dd {
                margin: 0 0 15px;
                padding: 0 15px;
            }

            dl dd>:first-child {
                margin-top: 0px;
            }

            dl dd>:last-child {
                margin-bottom: 0px;
            }

            /* CODE
            =============================================================================*/

            pre, code, tt {
                font-size: 12px;
                font-family: Consolas, "Liberation Mono", Courier, monospace;
            }

            code, tt {
                margin: 0 0px;
                padding: 0px 0px;
                white-space: nowrap;
                border: 1px solid #eaeaea;
                background-color: #f8f8f8;
                border-radius: 3px;
            }

            pre>code {
                margin: 0;
                padding: 0;
                white-space: pre;
                border: none;
                background: transparent;
            }

            pre {
                background-color: #f8f8f8;
                border: 1px solid #ccc;
                font-size: 13px;
                line-height: 19px;
                overflow: auto;
                padding: 6px 10px;
                border-radius: 3px;
            }

            pre code, pre tt {
                background-color: transparent;
                border: none;
            }

            kbd {
                -moz-border-bottom-colors: none;
                -moz-border-left-colors: none;
                -moz-border-right-colors: none;
                -moz-border-top-colors: none;
                background-color: #DDDDDD;
                background-image: linear-gradient(#F1F1F1, #DDDDDD);
                background-repeat: repeat-x;
                border-color: #DDDDDD #CCCCCC #CCCCCC #DDDDDD;
                border-image: none;
                border-radius: 2px 2px 2px 2px;
                border-style: solid;
                border-width: 1px;
                font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
                line-height: 10px;
                padding: 1px 4px;
            }

            /* QUOTES
            =============================================================================*/

            blockquote {
                border-left: 4px solid #DDD;
                padding: 0 15px;
                color: #777;
            }

            blockquote>:first-child {
                margin-top: 0px;
            }

            blockquote>:last-child {
                margin-bottom: 0px;
            }

            /* HORIZONTAL RULES
            =============================================================================*/

            hr {
                clear: both;
                margin: 15px 0;
                height: 0px;
                overflow: hidden;
                border: none;
                background: transparent;
                border-bottom: 4px solid #ddd;
                padding: 0;
            }

            /* TABLES
            =============================================================================*/

            table th {
                font-weight: bold;
            }

            table th, table td {
                border: 1px solid #ccc;
                padding: 6px 13px;
            }

            table tr {
                border-top: 1px solid #ccc;
                background-color: #fff;
            }

            table tr:nth-child(2n) {
                background-color: #f8f8f8;
            }

            /* IMAGES
            =============================================================================*/

            img {
                max-width: 100%
            }
        </style>
        <style>
            .itemActive{
                background-color:#d9dadb !important;
            }
            .itemToActivity{
                font-size: 10px !important;
            }
        </style>
    </head>

    <body>
    <div class="d-flex" id="wrapper">

       @include('api::layouts.navigation')

        <!-- Page Content -->
        @yield('content')

    </div>



        <!-- jQuery CDN - Slim version (=without AJAX) -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>
        $('.functionVisibility').hide();
        $('.thisVisible').show();
        $('.itemToActivity').on('click',function(e){
            e.preventDefault();
            e.stopPropagation();

            $('.functionVisibility').hide();
            $('.itemActive').removeClass('itemActive');
            $(this).addClass('itemActive');
            $($(this).attr('href')).show();
        });
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
    </script>

    </body>

</html>
