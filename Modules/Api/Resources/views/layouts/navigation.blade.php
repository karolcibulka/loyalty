<!-- Sidebar -->
<div class="bg-light border-right" id="sidebar-wrapper">
    <div class="sidebar-heading">Api docs loyalty</div>
    <div class="list-group list-group-flush">
        <a href="#start" class="list-group-item list-group-item-action bg-light itemToActivity itemActive">Na začiatok</a>
        @foreach($documentation as $itemKey => $item)
            <a href="#{{$itemKey}}" class="list-group-item itemToActivity list-group-item-action bg-light"><span class="badge {{$item['type'] == 'POST' ? 'badge-success' : 'badge-primary'}}">{{$item['type']}}</span> {{$item['method']}}</a>
        @endforeach
    </div>
</div>

<!-- /#sidebar-wrapper -->
