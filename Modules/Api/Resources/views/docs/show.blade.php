@extends('api::layouts.master')
@section('content')
    <div id="page-content-wrapper">

        <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
            <button class="btn btn-primary" id="menu-toggle">Metódy</button>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>


        </nav>
        <div class="container-fluid functionVisibility thisVisible" id="start">
            <h1 class="mt-4">Na začiatok</h1>
            <h4>Pre pripojenie použite endpoint</h4>
            <pre><code>{{url('/').'/api/'}}<i>názov_metódy</i></code></pre>
            <h4>X-Authorization</h4>
            <pre><code>api-key</code></pre>
            <h4>Success - data</h4>
            <pre><code>{{json_encode(array('status'=>true,'data'=>array('key'=>'val')),JSON_PRETTY_PRINT)}}</code></pre>
            <h4>Success - message</h4>
            <pre><code>{{json_encode(array('status'=>true,'message'=>'message'),JSON_PRETTY_PRINT)}}</code></pre>
            <h4>Unsuccess</h4>
            <pre><code>{{json_encode(array('status'=>false,'message'=>'message'),JSON_PRETTY_PRINT)}}</code></pre>
        </div>

        @foreach($documentation as $docItemKey => $item)
        <div class="container-fluid functionVisibility" id="{{$docItemKey}}">
            <h1 class="mt-4"><span class="badge {{$item['type'] == 'POST' ? 'badge-success' : 'badge-primary'}}">{{$item['type']}}</span>  {{$item['method']}}</h1>
            <pre><code>{{url('/').'/api/'.$item['method']}}</code></pre>
            <p>{{$item['description']}}</p>
            <h3>Parametre</h3>
            <ul>
                @foreach($item['request'] as $reqName => $reqItem)
                    <li><strong>{{$reqName}}</strong> - {!! $reqItem !!}</li>
                @endforeach
            </ul>
            <h4>Príklad požiadavky</h4>
            <pre><code>{!! isset($item['requestExample']) && !empty($item['requestExample']) ? $item['requestExample'] : '' !!}</code></pre>
            <h4>Odpoveď</h4>
            <ul>
                <li><strong>status : </strong> true/false</li>
                @if(isset($item['responseIsMessage']) && $item['responseIsMessage']==true)
                    <li><strong>message :</strong> správa</li>
                @else
                <li>
                    <strong>data : </strong>
                    <ul>
                        @foreach($item['response'] as $resName => $resItem)
                            <li><strong>{!! $resName !!}</strong> - {!! $resItem !!}</li>
                        @endforeach
                    </ul>
                </li>
                @endif
            </ul>
            <h4>Príklad odpovede</h4>
            <pre><code>{{isset($item['responseExample']) && !empty($item['responseExample']) ? $item['responseExample'] : ''}}</code></pre>
        </div>
            @endforeach
    </div>
    <!-- /#page-content-wrapper -->
@endsection
