<?php

namespace Modules\Loyalty\Libraries;


use App\Models\Crm\Customer;
use App\Models\Crm\LoyaltyAccountSetting;
use App\Models\Crm\Point;
use App\Models\Crm\PointHistory;

class PointLibrary
{
    public $customer_id;
    public $price;
    public $group_id;
    public $external_id;
    public $customer;

    public function __construct($customer_id,$price,$external_id)
    {
        $this->customer_id = $customer_id;
        $this->price = $price;
        $this->external_id = $external_id;
    }

    public function checkCustomerAndHisProfile(){
        $customer = Customer::find($this->customer_id);
        if(!is_null($customer)){
            $this->customer = $customer;
            return true;
        }
        else{
            return false;
        }
    }

    public function searchGroupAndAddPoints($type){
        if(!is_null($this->customer['group_id'])){
            $this->group_id = $this->customer['group_id'];
            switch($type){
                case 'ADD':
                    return $this->findAddProfilesForCustomer();
                    break;
            }
        }
        else{
            return false;
        }
    }

    private function findAddProfilesForCustomer(){
        $pointHistory = PointHistory::where('customer_id','=',$this->customer_id)->where('external_id','=',$this->external_id)->first();
        if(is_null($pointHistory)){
            $profiles = LoyaltyAccountSetting::where(function($query){
                $query->where('date_required','=','0');
                $query->where('active','=','1');
                $query->where('deleted','=','0');
                $query->where('group_id','=',$this->group_id);
                $query->where('loyalty_account_setting_type','=','ADD');
            })
                ->orWhere(function($query){
                    $query->where('date_required','=','1');
                    $query->where('date_from','<=',date('Y-m-d H:i:s'));
                    $query->where('date_to','>=',date('Y-m-d H:i:s'));
                    $query->where('active','=','1');
                    $query->where('deleted','=','0');
                    $query->where('group_id','=',$this->group_id);
                    $query->where('loyalty_account_setting_type','=','ADD');
                })
                ->join('loyalty_account_setting_groups as lasg','lasg.loyalty_account_setting_id','=','loyalty_account_settings.id')
                ->get();

            if(!is_null($profiles)){
                $points = 0;
                $price = $this->price;

                foreach($profiles as $profile){
                    if(!is_null($profile['extra_value']) && $profile['extra_key'] == 'RATIO'){
                        $points += $price * $profile['extra_value'];
                    }
                    elseif(!is_null($profile['extra_value']) && $profile['extra_key'] == 'POINTS'){
                        $points += $profile['extra_value'];
                    }
                }

                if($points>0){
                    $pointsRow = Point::where('customer_id','=',$this->customer_id)->first();

                    $insertHistoryRecord = array(
                        'customer_id' => $this->customer_id,
                        'points' => $points,
                        'old_points' => !is_null($pointsRow) ? $pointsRow['points'] : '0',
                        'new_points' =>  !is_null($pointsRow) ? $pointsRow['points'] + $points : $points,
                        'external_id' => $this->external_id,
                        'reason' => 'Extérna objednávka č.'.$this->external_id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'type' => '+'
                    );

                    PointHistory::insert($insertHistoryRecord);

                    if(!is_null($pointsRow)){

                        $updateData = array(
                            'points' => $pointsRow['points'] + $points,
                            'updated_at' => date('Y-m-d H:i:s')
                        );

                        Point::where('customer_id','=',$this->customer_id)->update($updateData);
                    }
                    else{
                        $insertArray = array(
                            'customer_id' => $this->customer_id,
                            'points' => $points,
                            'created_at' => date('Y-m-d H:i:s')
                        );

                        Point::insert($insertArray);
                    }
                    return true;
                }
                else{
                    return false;
                }

            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }
}
