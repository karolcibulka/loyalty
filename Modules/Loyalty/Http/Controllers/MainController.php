<?php


namespace Modules\Loyalty\Http\Controllers;

use App\Models\Crm\Customer;
use App\Models\Loyalty\LoyaltyNavigation;
use App\Models\Loyalty\LoyaltySetting;
use http\Env\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class MainController extends Controller
{

    public $template;
    public $lang;
    public $points;
    public $basketPrice;
    public $loyaltySettings;
    public $languages;

    public function __construct()
    {
        $this->template = 'default';
        $this->lang     = App::getLocale();

        if (Config::has('theme.template')) {
            $this->template = Config::get('theme.template');
        }

        //$loyaltySettings = LoyaltySetting::first();

        if (isset($_GET['deleteCache'])) {
            Cache::flush();
        }


        if (Cache::has('loyalty_navigation')) {
            View::share('loyalty_navigation', Cache::get('loyalty_navigation'));
        } else {
            $loyalty_navigation = $this->getLoyaltyNavigation();
            if (!empty($loyalty_navigation)) {
                Cache::put('loyalty_navigation', $loyalty_navigation, now()->addMinutes(30));
            }
            View::share('loyalty_navigation', $loyalty_navigation);
        }


        $this->middleware(function ($request, $next) {
            $this->points = 0;

            if(Cache::has('loyaltySettings')){
                $this->loyaltySettings = Cache::get('loyaltySettings');
            }
            else{
                $this->loyaltySettings = LoyaltySetting::first();
                if(is_null($this->loyaltySettings)){
                    $loyaltySettings = array(
                        'activation_token_durability' => '60',
                        'throttle_minutes' => '0',
                        'throttle_count' => '0',
                        'throttle_enable' => '1',
                    );
                    $this->loyaltySettings = $loyaltySettings;
                }
                Cache::put('loyaltySettings',$this->loyaltySettings);
            }

            if(isset($this->loyaltySettings['languages']) && !empty($this->loyaltySettings['languages'])){
                $this->languages = json_decode($this->loyaltySettings['languages'],true);
            }
            else{
                $this->languages = array(
                    'sk'
                );
            }

            View::share('loyalty_languages',$this->languages);

            //dd($this->languages);


            if (Auth::guard('customer')->check()) {

                $last_password_changed = Auth::guard('customer')->user()->last_password_changed;

                if (strtotime('+ '.$this->loyaltySettings['expiration_password'].' days', strtotime($last_password_changed)) <= strtotime(date('Y-m-d H:i:s')) || is_null($last_password_changed)) {
                    return redirect()->route('changeExpiredPassword');
                }
                View::share('loggedIn', 'true');
                if (Session::has('customer_session')) {
                    $customer     = Session::get('customer_session');
                    $this->points = $customer['points'];
                } else {
                    $customer = Customer::where('id', '=', Auth::guard('customer')->user()->customer_id)->leftJoin('points', 'points.customer_id', '=', 'customers.id')->get()->first();
                    Session::put('customer_session', $customer);
                    $this->points = $customer['points'];
                }


                $basketPrice = $this->basketPrice();
                View::share('basketPrice', $basketPrice);
                $this->basketPrice = $basketPrice;

                View::share('profile_picture', asset('profile_pictures/' . $customer['profile_image']));
                View::share('customerPoints', $this->points);
            } else {
                View::share('loggedIn', 'false');
            }

            $basketIDs   = array();
            $basketCount = 0;
            if (Session::has('basket')) {
                $basket = Session::get('basket');
                if (!empty($basket)) {
                    foreach ($basket as $key => $b) {
                        $id          = $key;
                        $basketIDs[] = $id;
                    }
                }
            }
            if (!empty($basket)) {
                $basketCount = count($basket);
            }

            View::share('currentLang', App::getLocale());
            View::share('basketIDs', $basketIDs);
            View::share('basketCount', $basketCount);

            return $next($request);
        });

        View::share('template', $this->template);

    }


    private function basketPrice()
    {
        $basketPrices           = array();
        $basketPrices['price']  = 0;
        $basketPrices['points'] = 0;
        if (Session::has('basket')) {
            $basket = Session::get('basket');
            if (!empty($basket)) {
                foreach ($basket as $key => $item) {
                    if (isset($item['price']) && !empty($item['price'])) {
                        $basketPrices['price'] += $item['price'];
                    }
                    if (isset($item['points']) && !empty($item['points'])) {
                        $basketPrices['points'] += $item['points'];
                    }
                }
            }
        }

        Session::put('basketPrice', $basketPrices);
        return $basketPrices;
    }

    private function getLoyaltyNavigation()
    {
        $records = LoyaltyNavigation::where('active', '=', '1')
            ->where('deleted', '=', '0')
            ->orderBy('order', 'asc')
            ->leftJoin('loyalty_navigation_langs as lnl', 'lnl.loyalty_navigation_id', '=', 'loyalty_navigations.id')
            ->get();

        $response = array();
        if (!empty($records)) {
            foreach ($records as $record) {
                $response[$record['id']]['titles'][$record['code']] = $record['title'];
                $response[$record['id']]['slugs'][$record['code']]  = $record['slug'];
                $response[$record['id']]['type']                    = $record['type'];
                $response[$record['id']]['href']                    = $record['href'];
                $response[$record['id']]['subpage']                 = $record['subpage'];
                $response[$record['id']]['download']                = $record['download'];
                $response[$record['id']]['file']                    = Config::get('theme.crm_path') . 'files/' . $record['file'];
                $response[$record['id']]['open']                    = $record['open'];
                $response[$record['id']]['order']                   = $record['order'];
                $response[$record['id']]['parent']                  = $record['parent'];
                $response[$record['id']]['id']                      = $record['id'];
            }
        }

        if (!empty($response)) {
            $response = $this->buildTree($response);
        }

        return $response;
    }

    private function buildTree($elements, $parentId = 0)
    {
        $branch = array();

        foreach ($elements as $element) {
            if ($element['parent'] == $parentId) {
                $children = $this->buildTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }
        return $branch;
    }
}
