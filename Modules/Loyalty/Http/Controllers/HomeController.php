<?php

namespace Modules\Loyalty\Http\Controllers;

use App\Libraries\MarketingLibrary;
use App\Mail\ActivationMail;
use App\Models\Crm\CrmSetting;
use App\Models\Crm\CustomerUser;
use App\Models\Crm\Property;
use App\Models\Crm\PropertyCategory;
use App\Models\Front\CustomerUsers;
use App\Models\Loyalty\Benefit;
use App\Models\Loyalty\LoyaltyFront;
use App\Models\Loyalty\Offer;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class HomeController extends MainController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public $template;

    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {

        $content    = LoyaltyFront::leftJoin('loyalty_front_langs', 'loyalty_fronts.id', '=', 'loyalty_front_langs.loyaltyfront_id')->get();
        $header     = $this->getHeader($content);
        $category   = $this->getCategory($content);
        $advantage  = $this->getAdvantage($content);
        $experience = $this->getExperience($content);

        return view('loyalty::' . $this->template . '.main')->with(array('header' => $header, 'category' => $category, 'advantage' => $advantage, 'experience' => $experience));
    }

    public function throttleLogin(Request $request)
    {
        return view('loyalty::' . $this->template . '.auth.throttleLogin');
    }

    public function newsletterActivation($token){
        $customer = CustomerUser::where('token', '=', $token)->first();
        if(!is_null($customer)){
            if($customer['newsletter_active'] == '1'){
                Session::flash('message', 'Už máte aktivovaný newsletter');
                Session::flash('alert-class', 'alert-warning');
                return redirect(route('login'));
            }

            Session::flash('message', 'Úspešne sme Vám aktivovali newsletter');
            Session::flash('alert-class', 'alert-success');
            CustomerUser::where('token', '=', $token)->update(array('newsletter_active'=>'1'));

            $marketingLibrary = new MarketingLibrary();
            $marketingLibrary->addCustomerToMailChimp($customer);

            return redirect()->route('login');
        }
        else{
            Session::flash('message', 'Nesprávny token !');
            Session::flash('alert-class', 'alert-danger');
            return redirect()->route('login');
        }
    }

    public function newsletterDeactivation($token){
        $customer = CustomerUser::where('token', '=', $token)->first();
        if(!is_null($customer)){

            $marketingLibrary = new MarketingLibrary();
            $marketingLibrary->addCustomerToMailChimp($customer);

            CustomerUser::where('token', '=', $token)->update(array('newsletter_active'=>'0','newsletter'=>'0'));

            Session::flash('message', 'Newsletter bol úspešne deaktivovaný');
            Session::flash('alert-class', 'alert-success');
            return redirect()->route('login');
        }
        else{
            Session::flash('message', 'Nesprávny token !');
            Session::flash('alert-class', 'alert-danger');
            return redirect()->route('login');
        }
    }

    public function activation($token)
    {
        $user = CustomerUser::where('token', '=', $token)->first();
        if ($user) {
            $user = $user->toArray();
            if ($user['activated'] == '1') {
                Session::flash('message', 'Tento účet už je aktivovaný. Môžete sa prihlásiť');
                Session::flash('alert-class', 'alert-success');
                return redirect(route('login'));
            } else {
                if (strtotime($user['token_active_to']) > strtotime(date('Y-m-d H:i:s'))) {

                    $updateData = array(
                        'activated' => '1',
                    );

                    CustomerUser::where('token', '=', $token)->update($updateData);
                    Session::flash('message', 'Účet bol úspešne aktivovaný. Môžete sa prihlásiť');
                    Session::flash('alert-class', 'alert-success');
                    return redirect(route('login'));
                } else {

                    $newToken = Str::random('40');

                    $updateData = array(
                        'token' => $newToken,
                        'token_active_to' => date('Y-m-d H:i:s', strtotime('+ ' . $this->loyaltySettings['activation_token_durability'] . ' minutes')),
                    );

                    $link = route('activation', ['token' => $newToken]);
                    Mail::to($user['email'])->send(new ActivationMail($link));

                    CustomerUser::where('token', '=', $token)->update($updateData);


                    Session::flash('message', 'Na email ' . $user['email'] . ' bol odoslaný nový aktivačný email!');
                    Session::flash('alert-class', 'alert-warning');
                    return redirect(route('login'));
                }
            }
        } else {
            Session::flash('message', 'Tento účet neexistuje! Vytvorte si nový!');
            Session::flash('alert-class', 'alert-danger');
            return redirect(route('login'));
        }
    }

    public function changeLang($lang){
        Session::put('basket',array());
        return redirect($lang);
        //dd($lang);
    }

    private function getHeader($content)
    {


        $response = array();

        if (isset($content) && !empty($content)) {
            foreach ($content as $c) {
                $response['id']                            = $c['loyaltyfront_id'];
                $response['animation_main_title']          = $c['animation_main_title'];
                $response['animation_main_description']    = $c['animation_main_description'];
                $response['images']                        = isset($c['images']) && !empty($c['images']) ? unserialize($c['images']) : '';
                $response['main_titles'][$c['code']]       = $c['main_title'];
                $response['main_descriptions'][$c['code']] = $c['main_description'];
                $response['show_wave']                     = $c['show_wave'];
            }
        }

        return $response;
    }

    private function getCategory($content)
    {

        $response = array();

        if (isset($content) && !empty($content)) {
            foreach ($content as $c) {
                $response['animation_category_title']          = $c['animation_category_title'];
                $response['animation_category_description']    = $c['animation_category_description'];
                $response['animation_category']                = $c['animation_category'];
                $response['category_titles'][$c['code']]       = $c['category_title'];
                $response['category_descriptions'][$c['code']] = $c['category_description'];
                $response['categories']                        = $c['categories'];
                $response['show_category']                     = $c['show_category'];
            }

            if (isset($response['categories']) && !empty($response['categories'])) {
                $categories = json_decode($response['categories'], true);
                if (isset($categories) && !empty($categories)) {
                    $categs        = array();
                    $allCategories = PropertyCategory::where('deleted', '=', '0')->join('property_category_langs', 'property_category_langs.property_category_id', '=', 'property_categories.id')->get();
                    if (isset($allCategories) && !empty($allCategories)) {
                        foreach ($allCategories as $cat) {
                            if (in_array($cat['id'], $categories)) {
                                $categs[$cat['id']]['title'][$cat['code']]             = $cat['title'];
                                $categs[$cat['id']]['short_description'][$cat['code']] = $cat['short_description'];
                                $categs[$cat['id']]['slug'][$cat['code']]              = $cat['slug'];
                                $categs[$cat['id']]['image']                           = $cat['image'];
                            }
                        }
                    }
                    $response['allCategories'] = $categs;
                }
            }
        }

        return $response;
    }

    private function getAdvantage($content)
    {
        $response = array();

        if (isset($content) && !empty($content)) {
            foreach ($content as $c) {
                $response['animation_advantage_title']          = $c['animation_advantage_title'];
                $response['animation_advantage_description']    = $c['animation_advantage_description'];
                $response['animation_advantage']                = $c['animation_advantage'];
                $response['advantage_titles'][$c['code']]       = $c['advantage_title'];
                $response['advantage_image']                    = $c['advantage_image'];
                $response['advantage_descriptions'][$c['code']] = $c['advantage_description'];
                $response['advantages']                         = $c['advantages'];
                $response['show_advantage']                     = $c['show_advantage'];
            }

            if (isset($response['advantages']) && !empty($response['advantages'])) {
                $advantages = json_decode($response['advantages'], true);
                if (isset($advantages) && !empty($advantages)) {
                    $advantgs      = array();
                    $allCategories = Benefit::where('deleted', '=', '0')->join('benefit_langs', 'benefit_langs.benefit_id', '=', 'benefits.id')->get();
                    if (isset($allCategories) && !empty($allCategories)) {
                        foreach ($allCategories as $ben) {
                            if (in_array($ben['id'], $advantages)) {
                                $advantgs[$ben['id']]['title'][$ben['code']]             = $ben['title'];
                                $advantgs[$ben['id']]['short_description'][$ben['code']] = $ben['short_description'];
                                $advantgs[$ben['id']]['image']                           = $ben['image'];
                                $advantgs[$ben['id']]['slug']                            = $ben['slug'];
                            }
                        }
                    }
                    $response['allAdvantages'] = $advantgs;
                }
            }
        }

        return $response;
    }

    private function getExperience($content)
    {
        $response = array();

        if (isset($content) && !empty($content)) {
            foreach ($content as $c) {
                $response['animation_experience_title']          = $c['animation_experience_title'];
                $response['animation_experience_description']    = $c['animation_experience_description'];
                $response['animation_experience']                = $c['animation_experience'];
                $response['experience_titles'][$c['code']]       = $c['experience_title'];
                $response['experience_image']                    = $c['experience_image'];
                $response['experience_descriptions'][$c['code']] = $c['experience_description'];
                $response['show_experience']                     = $c['show_experience'];
            }

            $response['experiences'] = array();
            $experiences             = Offer::leftJoin('offer_dates', 'offer_dates.offer_id', '=', 'offers.id')
                ->leftJoin('offer_langs', 'offer_langs.offer_id', '=', 'offers.id')
                ->where('offers.deleted', '0')
                ->where('offers.active', '1')
                ->where('offers.deleted', '0')
                ->where('offers.visible_from', '<=', date('Y-m-d H:i:s'))
                ->where('offers.visible_to', '>=', date('Y-m-d H:i:s'))
                ->orderBy('points')
                ->get();

            $allProperties = Property::select('properties.id as propertyID', 'property_category_langs.*')
                ->where('properties.deleted', '=', '0')
                ->join('property_categories', 'property_categories.id', '=', 'properties.category_id')
                ->join('property_category_langs', 'property_category_langs.property_category_id', '=', 'property_categories.id')
                ->where('code', '=', $this->lang)
                ->get();

            $properties = array();

            if (!empty($allProperties)) {
                foreach ($allProperties as $prop) {
                    $properties[$prop['propertyID']] = $prop;
                }
            }

            if (!empty($experiences)) {
                foreach ($experiences as $key => $experience) {
                    $response['experiences'][$experience['offer_id']]['lang'][$experience['lang']]['name']              = $experience['name'];
                    $response['experiences'][$experience['offer_id']]['lang'][$experience['lang']]['short_description'] = $experience['short_description'];
                    $response['experiences'][$experience['offer_id']]['lang'][$experience['lang']]['long_description']  = $experience['long_description'];
                    $response['experiences'][$experience['offer_id']]['lang'][$experience['lang']]['slug']              = $experience['slug'];
                    $response['experiences'][$experience['offer_id']]['visibleFrom']                                    = date('d.m.Y', strtotime($experience['visible_from']));
                    $response['experiences'][$experience['offer_id']]['visibleTo']                                      = date('d.m.Y', strtotime($experience['visible_to']));
                    $response['experiences'][$experience['offer_id']]['internalName']                                   = $experience['internal_name'];
                    $response['experiences'][$experience['offer_id']]['properties']                                     = !empty($experience['properties']) ? unserialize($experience['properties']) : array();
                    $response['experiences'][$experience['offer_id']]['categories']                                     = '';
                    if (!empty($response['experiences'][$experience['offer_id']]['properties'])) {
                        foreach ($response['experiences'][$experience['offer_id']]['properties'] as $prop) {
                            if (isset($properties[$prop]) && !empty($properties[$prop])) {
                                $response['experiences'][$experience['offer_id']]['categories'] .= $properties[$prop]['title'] . ',';
                            }
                        }
                    }
                    $response['experiences'][$experience['offer_id']]['offers_restrict_count'] = $experience['offers_restrict_count'];
                    $response['experiences'][$experience['offer_id']]['offers_restrict']       = $experience['offers_restrict'];
                    if (isset($experience['image']) && !empty($experience['image'])) {
                        $response['experiences'][$experience['offer_id']]['image']['thumb']  = asset('images/thumb/' . $experience['image']);
                        $response['experiences'][$experience['offer_id']]['image']['medium'] = asset('images/medium/' . $experience['image']);
                    }
                    $response['experiences'][$experience['offer_id']]['img']        = $experience['image'];
                    $response['experiences'][$experience['offer_id']]['price']      = $experience['price'];
                    $response['experiences'][$experience['offer_id']]['currency']   = $experience['currency'];
                    $response['experiences'][$experience['offer_id']]['active']     = $experience['active'];
                    $response['experiences'][$experience['offer_id']]['id']         = $experience['offer_id'];
                    $response['experiences'][$experience['offer_id']]['from'][$key] = date('d.m.Y', strtotime($experience['from']));
                    $response['experiences'][$experience['offer_id']]['to'][$key]   = date('d.m.Y', strtotime($experience['to']));
                }
            }
        }

        return $response;
    }
}
