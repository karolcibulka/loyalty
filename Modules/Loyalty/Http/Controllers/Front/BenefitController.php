<?php

namespace Modules\Loyalty\Http\Controllers\Front;


use App\Models\Loyalty\Benefit;
use Illuminate\Http\Request;
use Modules\Loyalty\Http\Controllers\MainController;

class BenefitController extends MainController
{

    public function index($slug){
        $benefit = Benefit::join('benefit_langs as bl','bl.benefit_id','=','benefits.id')->where('bl.slug','=',$slug)->first();

        return view('loyalty::'.$this->template.'.benefits.show')->with(array('benefit'=>$benefit));

    }
}
