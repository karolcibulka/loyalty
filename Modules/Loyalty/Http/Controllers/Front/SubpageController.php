<?php

namespace Modules\Loyalty\Http\Controllers\Front;


use App\Models\Crm\Offer;
use App\Models\Crm\Property;
use App\Models\Crm\PropertyCategory;
use App\Models\Loyalty\Gallery;
use App\Models\Loyalty\LoyaltyNavigationLang;
use App\Models\Loyalty\SubpageModule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\Loyalty\Http\Controllers\MainController;

class SubpageController extends MainController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index($slug)
    {

        $subpage = LoyaltyNavigationLang::select('s.*', 'sl.*')
            ->where('slug', '=', $slug)
            ->join('loyalty_navigations as ln', 'ln.id', '=', 'loyalty_navigation_langs.loyalty_navigation_id')
            ->join('subpages as s', 's.id', '=', 'ln.subpage')
            ->join('subpage_langs as sl', 'sl.subpage_id', '=', 's.id')
            ->where('sl.code', '=', $this->lang)
            ->first();

        $content = $this->generateContent($subpage['id']);

        return view('loyalty::' . $this->template . '.subpage.show')->with(array('subpage' => $subpage, 'content' => $content));
    }

    private function generateContent($id)
    {
        $modules = SubpageModule::where('subpage_id', '=', $id)
            ->orderBy('order', 'asc')
            ->get();

        $response = array();

        $galleries  = $this->getGalleries();
        $categories = $this->getCategories();
        $offers     = $this->getOffers($categories);


        if (isset($modules) && !empty($modules) && !is_null($modules)) {
            foreach ($modules as $module) {
                switch ($module['type']) {
                    case 'gallery':
                        $response[$module['order']] = $this->getGallery($module, $galleries);
                        break;
                    case 'category':
                        $response[$module['order']] = $this->getCategory($module, $categories);
                        break;
                    case 'offer':
                        $response[$module['order']] = $this->getOffer($module, $offers);
                        break;

                }
            }

            if (!empty($response)) {
                foreach ($response as $key => $res) {
                    if (!empty($res)) {
                        continue;
                    } else {
                        unset($response[$key]);
                    }
                }
            }
        }

        return $response;
    }


    private function getOffer($module, $offers)
    {
        $response = array();
        if (!empty($module) && !empty($offers)) {
            if ($module['show_all'] == '1') {
                $content = $offers;
            } else {
                $content = array();
                if (!is_null($module['show_items'])) {
                    $items = json_decode($module['show_items'], true);
                    foreach ($offers as $offer_id => $offer) {
                        if (in_array($offer_id, $items)) {
                            $content[$offer_id] = $offer;
                        }
                    }
                }
            }
        }

        if (!empty($content)) {
            $response['content'] = $content;
            $response['view']    = $module['type'];
            $response['type']    = null;
        }

        return $response;
    }


    private function getCategory($module, $categories)
    {
        $response = array();

        if (!empty($module) && !empty($categories)) {
            if ($module['show_all']) {
                $content = $categories;
            } else {
                $content = array();
                if (isset($module['show_items']) && !empty($module['show_items'])) {
                    $items = json_decode($module['show_items'], true);
                    foreach ($categories as $category_id => $category) {
                        if (in_array($category_id, $items)) {
                            $content[$category_id] = $category;
                        }
                    }
                }
            }
        }

        if (!empty($content)) {
            $response['content'] = $content;
            $response['view']    = $module['type'];
            $response['type']    = null;
        }

        return $response;
    }


    private function getGallery($module, $galleries)
    {
        $response = array();
        if (!empty($galleries) && !empty($module)) {
            $content = array();
            if ($module['informations'] == 'merge') {
                if ($module['show_all'] == '1') {
                    $content = array();
                    foreach ($galleries as $gallery) {
                        $content['title'] = '';
                        if (isset($gallery['images']) && !empty($gallery['images'])) {
                            foreach ($gallery['images'] as $image) {
                                $content['images'][] = $image;
                            }
                        }
                    }
                } else {

                    if (is_null($module['show_items'])) {
                        $items = json_decode($module['show_items'], true);
                        foreach ($galleries as $gallery_key => $gallery) {
                            if (in_array($gallery_key, $items)) {
                                $content['title'] = '';
                                if (isset($gallery['images']) && !empty($gallery['images'])) {
                                    foreach ($gallery['images'] as $image) {
                                        $content['images'][] = $image;
                                    }
                                }
                            }
                        }
                    }
                }
            } else {

                if ($module['show_all'] == '1') {
                    $content = array();
                    foreach ($galleries as $gallery) {
                        $content[$gallery['gallery_id']]['title'] = $gallery['title'];
                        if (isset($gallery['images']) && !empty($gallery['images'])) {
                            foreach ($gallery['images'] as $image) {
                                $content[$gallery['gallery_id']]['images'][] = $image;
                            }
                        }
                    }
                } else {
                    if (!is_null($module['show_items'])) {
                        $items = json_decode($module['show_items'], true);
                        foreach ($galleries as $gallery_key => $gallery) {
                            if (in_array($gallery_key, $items)) {
                                $content[$gallery_key]['title'] = $gallery['title'];
                                if (isset($gallery['images']) && !empty($gallery['images'])) {
                                    foreach ($gallery['images'] as $image) {
                                        $content[$gallery_key]['images'][] = $image;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if (!empty($content)) {
            $response['content'] = $content;
            $response['view']    = $module['type'];
            $response['type']    = $module['informations'];
        }
        return $response;
    }

    private function getGalleries()
    {
        $response  = array();
        $galleries = Gallery::select('gl.*', 'gi.*')
            ->where('galleries.active', '=', '1')
            ->where('galleries.deleted', '=', '0')
            ->where('gl.code', $this->lang)
            ->join('gallery_langs as gl', 'gl.gallery_id', '=', 'galleries.id')
            ->join('gallery_images as gi', 'gi.gallery_id', '=', 'galleries.id')
            ->where('gi.active', '=', '1')
            ->orderBy('order', 'asc')
            ->get();

        if (!empty($galleries) && !is_null($galleries)) {
            foreach ($galleries as $gallery) {
                $response[$gallery['gallery_id']]['images'][$gallery['image_id']]['thumb']  = asset('images/thumb/' . $gallery['image']);
                $response[$gallery['gallery_id']]['images'][$gallery['image_id']]['medium'] = asset('images/medium/' . $gallery['image']);
                $response[$gallery['gallery_id']]['images'][$gallery['image_id']]['large']  = asset('images/large/' . $gallery['image']);
                $response[$gallery['gallery_id']]['title']                                  = $gallery['title'];
                $response[$gallery['gallery_id']]['gallery_id']                             = $gallery['gallery_id'];
                $response[$gallery['gallery_id']]['short_description']                      = $gallery['short_description'];
                $response[$gallery['gallery_id']]['long_description']                       = $gallery['long_description'];
            }
        }

        return $response;
    }

    private function getCategories()
    {
        $response   = array();
        $categories = DB::table('property_categories')->select('property_categories.*', 'pcl.*', 'p.id as propertyID')
            ->where('property_categories.active', '=', '1')
            ->where('property_categories.deleted', '=', '0')
            ->join('property_category_langs as pcl', 'pcl.property_category_id', '=', 'property_categories.id')
            ->leftJoin('properties as p', 'p.category_id', '=', 'property_categories.id')
            ->where('pcl.code', $this->lang)
            ->get();


        if (!empty($categories) && !is_null($categories)) {
            $categories = $categories->toArray();
            foreach ($categories as $category) {
                $category                                                         = (array)$category;
                $response[$category['property_category_id']]['title']             = $category['title'];
                $response[$category['property_category_id']]['short_description'] = $category['short_description'];
                $response[$category['property_category_id']]['long_description']  = $category['long_description'];
                $response[$category['property_category_id']]['slug']              = $category['slug'];
                $response[$category['property_category_id']]['image']             = asset('images/categories/' . $category['image']);
                if (isset($category['propertyID']) && !empty($category['propertyID'])) {
                    $response[$category['property_category_id']]['properties'][$category['propertyID']] = $category['propertyID'];
                }
            }
        }
        return $response;
    }

    private function getOffers($categories)
    {
        $response = array();
        $offers   = Offer::where('active', '=', '1')
            ->where('visible_from', '<=', date('Y-m-d H:i:s'))
            ->where('visible_to', '>=', date('Y-m-d H:i:s'))
            ->where('deleted', '=', '0')
            ->join('offer_langs as ol', 'ol.offer_id', '=', 'offers.id')
            ->where('lang', '=', $this->lang)
            ->get();

        if (!empty($offers) && !is_null($offers)) {
            foreach ($offers as $offer) {
                $properties                                        = isset($offer['properties']) && !empty($offer['properties']) ? unserialize($offer['properties']) : array();
                $response[$offer['offer_id']]['categories']        = '';
                $response[$offer['offer_id']]['name']              = $offer['name'];
                $response[$offer['offer_id']]['slug']              = $offer['slug'];
                $response[$offer['offer_id']]['short_description'] = $offer['short_description'];
                $response[$offer['offer_id']]['date']              = date('d.m.Y', strtotime($offer['visible_from'])) . ' - ' . date('d.m.Y', strtotime($offer['visible_to']));
                $response[$offer['offer_id']]['currency']          = $offer['currency'];
                $response[$offer['offer_id']]['price']             = $offer['price'];
                $response[$offer['offer_id']]['long_description']  = $offer['long_description'];
                if (isset($properties) && !empty($properties)) {
                    if (isset($categories) && !empty($categories)) {
                        foreach ($categories as $category) {
                            if (isset($category['properties']) && !empty($category['properties'])) {
                                if (isset($properties) && !empty($properties)) {
                                    foreach ($properties as $property) {
                                        if (in_array($property, $category['properties'])) {
                                            $response[$offer['offer_id']]['categories'] .= $category['title'] . ',';
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                $response[$offer['offer_id']]['image']['thumb']  = asset('images/thumb/' . $offer['image']);
                $response[$offer['offer_id']]['image']['medium'] = asset('images/medium/' . $offer['image']);

            }
        }

        return $response;
    }
}
