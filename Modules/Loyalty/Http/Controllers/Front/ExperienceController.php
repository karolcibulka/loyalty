<?php

namespace Modules\Loyalty\Http\Controllers\Front;



use App\Models\Crm\Property;
use App\Models\Loyalty\Offer;
use Modules\Loyalty\Http\Controllers\MainController;

class ExperienceController extends MainController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index($slug){

        $experience = Offer::join('offer_langs','offers.id','=','offer_langs.offer_id')
            ->where('offer_langs.lang','=',$this->lang)
            ->leftJoin('offer_dates','offer_dates.offer_id','=','offers.id')
            ->where('slug','=',$slug)
            ->where('active','=','1')
            ->where('deleted','=','0')
            ->first();


        $allProperties = Property::select('properties.id as propertyID','property_category_langs.*')
            ->where('properties.deleted','=','0')
            ->join('property_categories','property_categories.id','=','properties.category_id')
            ->join('property_category_langs','property_category_langs.property_category_id','=','property_categories.id')
            ->where('code','=',$this->lang)
            ->get();

        $properties = array();

        if(!empty($allProperties)){
            foreach($allProperties as $prop){
                $properties[$prop['propertyID']] = $prop;
            }
        }


        $experiences = Offer::join('offer_langs','offers.id','=','offer_langs.offer_id')
            ->where('offer_langs.lang','=',$this->lang)
            ->leftJoin('offer_dates','offer_dates.offer_id','=','offers.id')
            ->whereDate('visible_from','<=',date('Y-m-d'))
            ->whereDate('visible_to','>=',date('Y-m-d'))
            ->where('slug','!=',$slug)->where('active','=','1')
            ->where('deleted','=','0')
            ->get();


        $experiences->map(function($experience,$index) use ($properties){
            if($experience)
            $experience['categories'] = '';
           if(!empty($experience['properties']) && !empty(unserialize($experience['properties']))){
               foreach(unserialize($experience['properties']) as $prop){
                    if(isset($properties[$prop]) && !empty($properties[$prop])){
                        $experience['categories'] .= $properties[$prop]['title'].'';
                    }
               }
           }
           return $experience;
        });


        return view('loyalty::'.$this->template.'.experiences.show')->with(array('experience'=>$experience,'experiences' => $experiences));
    }
}
