<?php

namespace Modules\Loyalty\Http\Controllers\Front;


use App\Models\Crm\Offer;
use App\Models\Crm\Property;
use Illuminate\Http\Request;
use Modules\Loyalty\Http\Controllers\MainController;

class PropertyController extends MainController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index($slug){

        $property = Property::join('property_langs','property_langs.property_id','=','properties.id')
            ->where('property_langs.slug','=',$slug)
            ->first();

        $offers = Offer::where('deleted','=','0')->where('active','1')
            ->where('visible_from','<=',date('Y-m-d H:i:s'))
            ->where('visible_to','>=',date('Y-m-d H:i:s'))
            ->join('offer_langs','offer_langs.offer_id','=','offers.id')
            ->where('offer_langs.lang','=',$this->lang)
            ->get();

        $allProperties = Property::select('properties.id as propertyID','property_category_langs.*')
            ->where('properties.deleted','=','0')
            ->join('property_categories','property_categories.id','=','properties.category_id')
            ->join('property_category_langs','property_category_langs.property_category_id','=','property_categories.id')
            ->where('code','=',$this->lang)
            ->get();

        $properties = array();

        if(!empty($allProperties)){
            foreach($allProperties as $prop){
                $properties[$prop['propertyID']] = $prop;
            }
        }

        if(!empty($offers) && count($offers)>0){
            $offers->map(function($offer,$key) use ($property,$offers,$properties){
               if(empty($offer['properties']) || !in_array($property['id'],unserialize($offer['properties']))){
                  unset($offers[$key]);
               }
               else{
                   $offer['categories'] = '';
                   foreach(unserialize($offer['properties']) as $prop){
                       if(isset($properties[$prop]) && !empty($properties[$prop])){
                           $offer['categories'] .= $properties[$prop]['title'].',';
                       }
                   }
                   return $offer;
               }
            });
        }

        return view('loyalty::'.$this->template.'.property.show')->with(array('property'=>$property,'offers'=>$offers));
    }
}
