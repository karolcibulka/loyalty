<?php

namespace Modules\Loyalty\Http\Controllers\Front;


use App\Models\Crm\Property;
use App\Models\Crm\PropertyCategory;
use Illuminate\Http\Request;
use Modules\Loyalty\Http\Controllers\MainController;

class CategoryController extends MainController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index($slug){

        $category = PropertyCategory::join('property_category_langs','property_category_langs.property_category_id','=','property_categories.id')->where('property_category_langs.slug','=',$slug)->first();
        $category['properties'] = Property::where('category_id','=',$category['id'])->where('deleted','=','0')->where('active','=','1')->join('property_langs','property_langs.property_id','=','properties.id')->where('code','=',$this->lang)->get();

        return view('loyalty::'.$this->template.'.categories.show')->with(array('category'=>$category));
    }
}
