<?php

namespace Modules\Loyalty\Http\Controllers\Auth;

use App\Mail\ActivationMail;
use App\Models\Crm\CustomerUser;
use App\Models\External\Country;
use App\Models\Front\CustomerUsers;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use Modules\Loyalty\Http\Controllers\MainController;

class LoginController extends MainController
{


    use AuthenticatesUsers;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    public $template;
    public $loyaltySettings;
    //protected $guard = 'customer';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->template = Config::get('theme.template');

        $this->loyaltySettings = Cache::get('loyaltySettings');

        if($this->loyaltySettings['throttle_enable'] == '1'){
            $this->middleware('throttle:'.$this->loyaltySettings['throttle_count'].','.$this->loyaltySettings['throttle_minutes']);
        }

        $this->middleware(function($request,$next){
            if(Auth::guard('customer')->check()){
                if(strpos($request->getRequestUri(),'logout')!==false){
                    return $next($request);
                }
                else{
                    return redirect(route('/'));
                }
            }
            else{
                return $next($request);
            }
        });
    }

    public function showLoginForm(Request $request)
    {
        $countries = Country::all();
        return view('loyalty::'.$this->template.'.auth.login')->with(array('countries'=>$countries));
    }



    protected function guard(){
        return Auth::guard('customer');
    }



    public function login(Request $request)
    {

        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);


        if (Auth::guard('customer')->attempt($this->credentials($request))) {
            //dd(Hash::make($request->password));
            $customer = CustomerUser::where('email','=',$request->email)->first();
            //dd($customer);
            $this->clearLoginAttempts($request);
            return redirect()->intended($this->redirectPath());
        }

        return back()->withInput($request->only('email', 'remember'));
    }


    protected function credentials(Request $request)
    {

        Session::flash('form-confirm','login');
        $user = CustomerUser::where('email','=',$request->email)->first();
        if(!is_null($user)){
            if(Hash::check($request->password,$user['password'])){
                if(!$user['activated'] ){

                    $newToken = Str::random('40');

                    $updateData = array(
                        'token' => $newToken,
                        'token_active_to' => date('Y-m-d H:i:s',strtotime('+ '.$this->loyaltySettings['activation_token_durability'].' minutes')),
                    );

                    $link = route('activation',['token'=>$newToken]);
                    $mail = Mail::to($user['email'])->send(new ActivationMail($link));

                    $cstmr = CustomerUser::where('email','=',$request->email)->update($updateData);

                    Session::flash('message','Tento užívateľ nie je aktivovaný, na email Vám bol odoslaný aktivačný link!');
                    Session::flash('alert-class','alert-warning');
                }
            }
        }
        $credentials = $request->only($this->username(), 'password');
        $credentials['activated'] = 1;

        return $credentials;
    }
}
