<?php


namespace Modules\Loyalty\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use App\Models\Crm\Customer;

use App\Models\Crm\CustomerUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class ExpiredPassword extends Controller
{

    public $template = 'default';

    public function __construct()
    {
        $this->middleware(function($request,$next){
            if( Auth::guard('customer')->check() ) {

                $last_password_changed = Auth::guard('customer')->user()->last_password_changed;
                if (strtotime('+ 2 days', strtotime($last_password_changed)) <= strtotime(date('Y-m-d H:i:s'))) {
                    View::share('loggedIn', 'true');
                    if (Session::has('customer_session')) {
                        $customer = Session::get('customer_session');
                        $this->points = $customer['points'];
                    } else {
                        $customer = Customer::where('id', '=', Auth::guard('customer')->user()->customer_id)->leftJoin('points', 'points.customer_id', '=', 'customers.id')->get()->first();
                        Session::put('customer_session', $customer);
                        $this->points = $customer['points'];
                    }

                    if(Cache::has('loyalty_navigation')){
                        View::share('loyalty_navigation',Cache::get('loyalty_navigation'));
                    }

                    $basketPrice = $this->basketPrice();
                    View::share('basketPrice', $basketPrice);
                    $this->basketPrice = $basketPrice;

                    View::share('profile_picture', asset('profile_pictures/' . $customer['profile_image']));
                    View::share('customerPoints', $this->points);

                    if(Config::has('theme.template')){
                        $this->template = Config::get('theme.template');
                    }
                    View::share('template', $this->template);

                    $basket = array();
                    $basketIDs = array();
                    $basketCount = 0;
                    if(Session::has('basket')){
                        $basket = Session::get('basket');
                        if(!empty($basket)){
                            foreach($basket as $key => $b){
                                $id = $key;
                                $basketIDs[] = $id;
                            }
                        }
                    }
                    if(!empty($basket)){
                        $basketCount = count($basket);
                    }

                    //dd(Session::all());

                    View::share('currentLang',App::getLocale());
                    View::share('basketIDs',$basketIDs);
                    View::share('basketCount',$basketCount);

                    return $next($request);
                }
               else{
                   return redirect()->route('login');
               }
            }
            else{
                return redirect()->route('login');
            }
        });


    }

    private function basketPrice(){
        $basketPrices = array();
        $basketPrices['price'] = 0;
        $basketPrices['points'] = 0;
        if(Session::has('basket')){
            $basket = Session::get('basket');
            if(!empty($basket)){
                foreach($basket as $key => $item){
                    if(isset($item['price']) && !empty($item['price'])){
                        $basketPrices['price'] += $item['price'];
                    }
                    if(isset($item['points']) && !empty($item['points'])){
                        $basketPrices['points'] += $item['points'];
                    }
                }
            }
        }

        Session::put('basketPrice',$basketPrices);
        return $basketPrices;
    }

    public function changeExpiredPassword(){
        return view('loyalty::'.$this->template.'.auth.expiredPassword');
    }

    public function changeExpiredPasswordPost(Request $request){
        $request->validate([
            'old_password' => ['required', 'string', 'min:8','regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$/'],
            'password' => ['required', 'string', 'min:8', 'confirmed','regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$/'],
        ]);

        $user = CustomerUser::where('email','=',$request->email)->first();
        if(Hash::check($request->old_password,$user['password']) && $request->old_password != $request->password){
            $updateData = array(
                'password' => Hash::make($request->password),
                'last_password_changed' => date('Y-m-d H:i:s')
            );
            CustomerUser::where('id','=',$user['id'])->update($updateData);
            return redirect()->route('/');
        }
        else{
            Session::flash('wrongData','1');
            return redirect()->route('changeExpiredPassword');
        }
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'password' => ['required', 'string', 'min:8', 'confirmed','regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$/'],
        ]);
    }
}
