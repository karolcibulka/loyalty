<?php

namespace Modules\Loyalty\Http\Controllers\Auth;


use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Modules\Loyalty\Http\Controllers\MainController;

class ForgotPasswordController extends MainController
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    public $template;

    public function __construct()
    {
        parent::__construct();
    }

    public function showLinkRequestForm()
    {
        return view('loyalty::'.$this->template.'.auth.forgot');
    }
}
