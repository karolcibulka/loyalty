<?php

namespace Modules\Loyalty\Http\Controllers\Auth;

use App\Models\Front\CustomerUsers;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Modules\Loyalty\Http\Controllers\MainController;

class ResetPasswordController extends MainController
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

    public $template;

    public function __construct()
    {
        parent::__construct();
    }

    public function showResetForm($token ,Request $request)
    {

        $array = array(
            'token' => $token,
            'email' => $request->email
        );
        return view('loyalty::'.$this->template.'.auth.reset')->with($array);
    }

    public function resetPassword($user, $password)
    {

        Session::flash('message','Teraz sa môžete prihlásiť!');
        Session::flash('alert-class','alert-success');

        $user->forceFill([
            'password' => bcrypt($password),
        ])->save();

        //dd($user);

        return redirect($this->redirectTo);
    }

    public function reset(Request $request)
    {
        $validatedData = $request->validate([
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|required_with:password_confirmation|same:password_confirmation|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$/'
        ]);


        if($validatedData){
            $user = CustomerUsers::where('email','=',$validatedData['email'])->get()->first();

            Session::flash('message','Teraz sa môžete prihlásiť!');
            Session::flash('alert-class','alert-success');

            $user->forceFill([
                'password' => bcrypt($validatedData['password']),
            ])->save();


            return redirect(route('login'));
        }
    }

    protected function rules()
    {
        return [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$/',
        ];
    }
}
