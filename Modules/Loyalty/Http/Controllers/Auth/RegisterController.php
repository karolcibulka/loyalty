<?php

namespace Modules\Loyalty\Http\Controllers\Auth;


use App\Libraries\CustomerLibrary;
use App\Libraries\MarketingLibrary;
use App\Mail\ActivationMail;
use App\Models\Crm\Customer;
use App\Models\Crm\CustomerGroup;
use App\Models\Crm\Point;
use App\Models\Crm\CustomerUser;
use App\Models\External\Country;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Modules\Loyalty\Http\Controllers\MainController;

class RegisterController extends MainController
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */


    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public $template;

    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest:customer');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */



    public function showRegistrationForm()
    {
        $countries = Country::all();
        return view('loyalty::'.$this->template.'.auth.login')->with(array('countries'=>$countries));
    }


    public function register(Request $request)
    {
        Session::flash('form-confirm','register');
        $token = Str::random(40);
        $data = $request->all();
        if(isset($data['email']) && !empty($data['email'])){
            $user = CustomerUser::where('email','=',$data['email'])->get()->first();
            if(!is_null($user)) {
                $user = $user->toArray();
                if($user['activated']){
                    Session::flash('message', 'Tento účet je už aktivovaný, môžete sa prihlásiť');
                    Session::flash('alert-class', 'alert-success');
                    return redirect(route('login'));
                }
                else{
                    $updateData = array(
                        'token' => $token,
                        'token_active_to' => date('Y-m-d H:i:s',strtotime('+ '.$this->loyaltySettings['activation_token_durability'].' minutes'))
                    );
                    CustomerUser::where('email','=',$data['email'])->update($updateData);

                    $link = route('activation',['token'=>$token]);


                    Mail::to($user['email'])->send(new ActivationMail($link));

                    Session::flash('message', 'Na email '.$user['email'].' bol odoslaný nový aktivačný email!');
                    Session::flash('alert-class', 'alert-warning');
                    return redirect(route('login'));
                    //neaktivovaný
                }
            }
        }
        $this->validator($request->all())->validate();

       event(new Registered($user = $this->create($request->all())));

        return $this->registered($request, $user)
            ?: redirect(route('login'));
    }


    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'city' => ['required', 'string', 'max:255'],
            'address' => ['required', 'string', 'max:255'],
            'zip' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],
            'country' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255','unique:customer_users'],
            'password' => ['required', 'string', 'min:8', 'confirmed','regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$/'],
            'language' => ['required']
        ]);
    }



    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $token = Str::random(40);

        $customerUserData = array(
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'encrypted_last_name' => Str::slug($data['last_name'], ''),
            'newsletter' => $data['newsletter'],
            'zip' => $data['zip'],
            'address' => $data['address'],
            'country' => $data['country'],
            'city' => $data['city'],
            'phone' => $data['phone'],
            'token' => $token,
            'activated' => '0',
            'language' => $data['language'],
            'token_active_to' => date('Y-m-d H:i:s',strtotime('+ '.$this->loyaltySettings['activation_token_durability'].' minutes')),
            'created_at'=> date('Y-m-d H:i:s'),
            'last_password_changed' => date('Y-m-d H:i:s')
        );

        $userID =  CustomerUser::insertGetId($customerUserData);

        $this->checkCustomer($data,$userID);

        $link = route('activation',['token'=>$token]);


        $mail = Mail::to($data['email'])->send(new ActivationMail($link));

        $marketingLibrary = new MarketingLibrary();
        if ($data['newsletter'] == '1') {
            $marketingLibrary->sendNewsletterActivationEmail($customerUserData);
            $updateDataCustomerUser['newsletter_active'] = '0';
        }


        Session::flash('message', 'Na email '.$data['email'].' bol odoslaný aktivačný email!');
        Session::flash('alert-class', 'alert-warning');

        return redirect()->route('login');
    }

    private function checkCustomer($data,$userID){
            $customer = Customer::where('encrypted_last_name','=',Str::slug($data['last_name'],''))->where('email','=',$data['email'])->first();
            if(!is_null($customer)){
                $customerUser = CustomerUser::find($userID);
                $customerUser->update(array('customer_id'=>$customer['id']));
                $cst = Customer::find($customer['id'])->update(array('is_registered'=>'1'));
                return true;
            }
            else{
                $customer_group = '';
                $customer_group_default = CustomerGroup::where('is_default','=','1')->first();
                if(!is_null($customer_group_default)){
                    $customer_group = $customer_group_default['id'];
                }

                $customerLibrary = new CustomerLibrary();
                $cardNumberFormat = $customerLibrary->getCardNumberFormat();
                $cardNumber = $customerLibrary->getCardNumber($cardNumberFormat);

                $token = Str::random(40);
                $insertData = array(
                    'first_name' => $data['first_name'],
                    'last_name' => $data['last_name'],
                    'email' => $data['email'],
                    'zip' => $data['zip'],
                    'adress' => $data['address'],
                    'state' => $data['country'],
                    'city' => $data['city'],
                    'phone' => $data['phone'],
                    'encrypted_last_name' => Str::slug($data['last_name'], ''),
                    'token' => $token,
                    'created_at'=> date('Y-m-d H:i:s'),
                    'group_id' => $customer_group,
                    'card_number' => $cardNumber,
                    'is_registered'=>'1'
                );

                $customerID = Customer::insertGetId($insertData);

                $point = Point::insert(array('customer_id'=>$customerID,'points'=>'0','created_at' => date('Y-m-d H:i:s')));
                $customerUser = CustomerUser::where('id','=',$userID)->update(array('customer_id'=>$customerID));

                return true;
            }
    }
}
