<?php

namespace Modules\Loyalty\Http\Controllers\Loyalty;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Modules\Loyalty\Http\Controllers\MainController;

class SummaryController extends MainController
{

    public $template;
    public $screen;
    public $points;
    public $basketPrice;


   public function __construct()
   {
       parent::__construct();
       $this->screen = 'summary';
   }

   public function index(){
       $priceText = '';
       if( $this->basketPrice['points'] > 0 ){
           $priceText .= $this->basketPrice['points'].' bodov ';
       }
       if( $this->basketPrice['price'] > 0 ){
           $priceText .= ( $this->basketPrice['points'] > 0 ) ? '+ '.$this->basketPrice['price'].' €' : $this->basketPrice['price'].' €';
       }

       if( Session::has('basket') ) {
           $data['basket'] = Session::get('basket');
           if(isset($data['basket']) && !empty($data['basket'])){
                //continue
           }
           else{

               return redirect(route('basket'));
           }

       }
       else{
           return redirect(route('basket'));
       }

       $data['basketPriceSummary'] = $priceText;

       return view('loyalty::'.$this->template.'.summary.show',$data);
   }
}
