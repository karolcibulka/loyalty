<?php

namespace Modules\Loyalty\Http\Controllers\Loyalty;


use App\Libraries\PointLibrary;
use App\Models\Crm\Customer;
use App\Models\Crm\Point;
use App\Models\Crm\PointHistory;
use App\Models\Front\Reservation;
use App\Models\Front\ReservationItem;
use App\Models\Loyalty\LoyaltySetting;
use App\Models\Loyalty\Offer;
use App\Models\Loyalty\Sale;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Modules\Loyalty\Http\Controllers\MainController;

class BasketController extends MainController
{
    public $template;
    public $screen;
    public $points;

    public function __construct()
    {
        parent::__construct();
        $this->screen = 'basket';
    }

    public function index(){
        $basket = array();
        if(Session::has('basket')){
            $basket = Session::get('basket');
        }

        //Session::flush();
        //dd($basket);
        return view('loyalty::'.$this->template.'.basket.show')->with(array('basket'=>$basket));
    }

    public function addItem(Request $request){

        $lang = App::getLocale();
        $data = $request->all();
        $type = 'item';

        if(isset($data['offer']) && !empty($data['offer'])){
            $type = 'offer';
            $item = Offer::where('offers.id','=',$data['id'])->where('offer_langs.lang','=',$lang)->join('offer_langs','offer_langs.offer_id','=','offers.id')->first();
        }
        else{
            $item = Sale::where('sales.id','=',$data['id'])->where('sale_langs.lang','=',$lang)->join('sale_langs','sale_langs.sale_id','=','sales.id')->first();
        }


        $basketPrices = $this->basketPrice();

        if(isset($item) && !empty($item)){
            if($type == 'item'){
                if(isset($item['points']) && !empty($item['points'])){
                    if($basketPrices['points'] + $item['points'] <= $this->points){
                       $request->session()->put('basket.item_'.$data['id'],$item->toArray());
                        $request->session()->save();
                        $response = array(
                            'status' => '1',
                            'basketCount' => $this->getBasketItemsCount(),
                            'basketPrices' => $this->basketPrice(),
                        );
                    }
                    else{
                        $response = array(
                            'status' => '2',
                        );
                    }
                }
                else{
                    $request->session()->put('basket.item_'.$data['id'],$item->toArray()); //->put('basket.item_'.$data['id'],$item->toArray())
                    $request->session()->save();
                    $response = array(
                        'status' => '1',
                        'basketCount' => $this->getBasketItemsCount(),
                        'basketPrices' => $this->basketPrice(),
                    );
                }
            }
            else{
                $request->session()->put('basket.offer_'.$data['id'],$item->toArray());
                $request->session()->save();
                $response = array(
                    'status' => '1',
                    'basketCount' => $this->getBasketItemsCount(),
                    'basketPrices' => $this->basketPrice(),
                );
            }
        }


        return response()->json($response);
    }

    public function removeItem(Request $request){
        $data = $request->all();

        $basket = array();
        if(Session::has('basket')){
            $basket = Session::get('basket');
            if(isset($data['offer']) && !empty($data['offer'])){
                if(isset($basket['offer_'.$data['id']]) && !empty($basket['offer_'.$data['id']])){
                    unset($basket['offer_'.$data['id']]);
                }
            }
            else{
                if(isset($basket['item_'.$data['id']]) && !empty($basket['item_'.$data['id']])){
                    unset($basket['item_'.$data['id']]);
                }
            }
            Session::put('basket',$basket);
        }

        $basketPrices = $this->basketPrice();

        $response = array(
            'status' => '1',
            'basketCount' => $this->getBasketItemsCount(),
            'basketPrices' => $basketPrices
        );

        return response()->json($response);
    }

    public function getBasketItemsCount(){
        $basket = $this->getBasket();
        $basketCount = 0;
        if(isset($basket) && !empty($basket)){
            $basketCount = count($basket);
        }

        return $basketCount;
    }

    public function getBasket(){
        $basket = array();
        if(Session::has('basket')){
            $basket = Session::get('basket');
        }
        return $basket;
    }

    public function basketPrice(){
        $basketPrices = array();
        $basketPrices['price'] = 0;
        $basketPrices['points'] = 0;
        if(Session::has('basket')){
            $basket = Session::get('basket');
            if(!empty($basket)){
                foreach($basket as $key => $item){
                    if(isset($item['price']) && !empty($item['price'])){
                        $basketPrices['price'] += $item['price'];
                    }
                    if(isset($item['points']) && !empty($item['points'])){
                        $basketPrices['points'] += $item['points'];
                    }
                }
            }
        }

        Session::put('basketPrice',$basketPrices);
        return $basketPrices;
    }

    private function cleanBasket(){
        $basket = array();
        Session::put('basket',$basket);
    }

    public function doReservation(){

       $customer = Session::get('customer_session');
       $basket = Session::get('basket');
       $basketPrice = $this->basketPrice();

        $reservation = array(
            'customer_id' => $customer['id'],
            'price' => $basketPrice['price'],
            'points' => $basketPrice['points'],
            'basket' => serialize($basket),
            'created_at' => date('Y-m-d H:i:s'),
        );

        $reservationID = Reservation::insertGetId($reservation);

        $reservationItems = array();
        $reservationOffers = array();
        foreach($basket as $item_key => $item) {
            if(strpos($item_key, 'item_') !== false) {
                if(isset($item['sales_restrict']) && !empty($item['sales_restrict']) && $item['sales_restrict'] == '1') {
                    $sale = Sale::find($item['id']);
                    $sale->available = $sale['available'] + 1;
                    $sale->update();
                }

                $reservationItem = array(
                    'reservation_id' => $reservationID,
                    'customer_id' => $customer['id'],
                    'sale_id' => $item['sale_id'],
                    'price' => $item['price'],
                    'points' => $item['points'],
                    'item' => serialize($item),
                    'created_at' => date('Y-m-d H:i:s'),
                );

                $reservationItems[] = $reservationItem;

            }
            else{
                if(isset($item['offers_restrict']) && !empty($item['offers_restrict']) && $item['offers_restrict'] == '1') {
                    $offer = Offer::find($item['id']);
                    $offer->available = $offer['available'] + 1;
                    $offer->update();
                }

                $reservationItem = array(
                    'reservation_id' => $reservationID,
                    'customer_id' => $customer['id'],
                    'offer_id' => $item['offer_id'],
                    'price' => $item['price'],
                    'points' => $item['points'],
                    'item' => serialize($item),
                    'created_at' => date('Y-m-d H:i:s'),
                );

                $reservationOffers[] = $reservationItem;


            }
        }

        if(!empty($reservationItems)){
            ReservationItem::insert($reservationItems);
        }
        if(!empty($reservationOffers)){
            ReservationItem::insert($reservationOffers);
        }

        if($basketPrice['points']>0){
            if(!is_null($customer['points'])){
                $newPoints = $customer['points'] - $basketPrice['points'];
            }
            else{
                $newPoints = $basketPrice['points'];
            }

            $points = array(
                'customer_id' => $customer['id'],
                'points' => $basketPrice['points'],
                'reason' => 'Rezervácia č.'.$reservationID,
                'created_at' => date('Y-m-d H:i:s'),
                'type' => '-',
                'old_points' => $customer['points'],
                'new_points' => $newPoints,
                'reservation_id' => $reservationID
            );

            PointHistory::insertGetId($points);

            $pointRecord = Point::where('customer_id','=',$customer['id'])->first();

            if(!is_null($pointRecord)){
                Point::where('customer_id','=',$customer['id'])->update(array('points'=>$newPoints,'updated_at'=>date('Y-m-d H:i:s')));
            }
            else{
                Point::insert(array('customer_id'=>$customer['id'],'points'=>$newPoints,'created_at'=>date('Y-m-d H:i:s')));
            }

            $customer = Customer::where('id','=',$customer['id'])->leftJoin('points','points.customer_id','=','customers.id')->first();
        }

        $pointlib = new PointLibrary($customer,$basket,$reservationID);
        if($pointlib->checkCustomerProfile()){
            $addedPoints = $pointlib->checkAddProfiles();
        }

        $email['email'] = $customer['email'];
        $email['template'] = $this->template;
        $email['basket'] = $basket;
        $email['basketPrice'] = $basketPrice;
        $email['customer'] = $customer;
        $email['loyalty'] = LoyaltySetting::find(1);
        Mail::to($email['email'])->send(new \App\Mail\Reservation($email));

        $this->cleanBasket();

        $view = View('loyalty::'.$this->template.'.status.success')->with(array('addedPoints'=>$addedPoints,'customer' => $customer))->render();

        $response = array(
            'status' => '1',
            'view' => $view,
            'basketCount' => $this->getBasketItemsCount()
        );

        return response()->json($response);

    }
}
