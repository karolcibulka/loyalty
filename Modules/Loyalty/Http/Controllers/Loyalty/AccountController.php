<?php

namespace Modules\Loyalty\Http\Controllers\Loyalty;


use App\Libraries\MarketingLibrary;
use App\Models\Crm\CrmSetting;
use App\Models\Crm\Customer;
use App\Models\Crm\CustomerUser;
use App\Models\External\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;
use Modules\Loyalty\Http\Controllers\MainController;

class AccountController extends MainController
{
    public $template;
    public $screen;

    public function __construct()
    {

        parent::__construct();
        $this->middleware('auth:customer');
        $this->screen = 'account';
    }

    public function index()
    {

        $user = Customer::select('customers.*', 'customer_users.newsletter','customer_users.language')
            ->where('customers.id', '=', Auth::guard('customer')->user()->customer_id)
            ->join('customer_users', 'customers.id', '=', 'customer_users.customer_id')
            ->get()
            ->first();

        $countries = Country::all();

        return view('loyalty::' . $this->template . '.loyalty.main')->with(array('screen' => $this->screen, 'user' => $user, 'countries' => $countries));
    }

    public function update(Request $request)
    {
        $data = $request->all();

        $updateData = array(
            'first_name' => $data['first_name'],
            'phone' => $data['phone'],
            'city' => $data['city'],
            'zip' => $data['zip'],
            'adress' => $data['adress'],
            'state' => $data['state'],
        );

        if ($data['removeProfilePicture'] == '1') {

            $path = public_path('/profile_pictures') . '/' . $data['old_profile_picture'];

            if (file_exists($path)) {

                unset($path);

            }

            $updateData['profile_image'] = '';

        }

        if ($request->hasFile('profile_picture')) {
            $image     = $request->file('profile_picture');
            $imagename = $this->template . '_' . time() . '.' . $image->extension();

            $updateData['profile_image'] = $imagename;

            $destinationPath = public_path('/profile_pictures');
            $img             = Image::make($image->path());
            $img->resize(250, 250, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath . '/' . $imagename);

            $path = public_path('/profile_pictures') . '/' . $data['old_profile_picture'];

            if (file_exists($path)) {

                unset($path);

            }
        }

        $updateDataCustomerUser = array(
            'first_name' => $data['first_name'],
            'phone' => $data['phone'],
            'city' => $data['city'],
            'zip' => $data['zip'],
            'address' => $data['adress'],
            'country' => $data['state'],
            'newsletter' => $data['newsletter'],
            'language' => $data['language']
        );


        $marketingLibrary = new MarketingLibrary();
        $customer         = CustomerUser::where('customer_id', '=', $data['id'])->first();
        if($customer['newsletter_active']){
            if($data['newsletter'] == '0'){
                $updateDataCustomerUser['newsletter_active'] = '0';
                $marketingLibrary->removeCustomerFromMailChimp($customer);
            }
        }
        else{
            if ($data['newsletter'] == '1') {
                $marketingLibrary->sendNewsletterActivationEmail($customer);
                $updateDataCustomerUser['newsletter_active'] = '0';
            } else {
                $updateDataCustomerUser['newsletter_active'] = '0';
                $marketingLibrary->removeCustomerFromMailChimp($customer);
            }
        }


        $ucu = CustomerUser::where('customer_id', $data['id'])->update($updateDataCustomerUser);

        $uc = Customer::where('id', '=', $data['id'])->update($updateData);

        $customer = Customer::where('id', '=', Auth::guard('customer')->user()->customer_id)
            ->leftJoin('points', 'points.customer_id', '=', 'customers.id')
            ->get()
            ->first();

        Session::put('customer_session', $customer);


        return redirect()->route('my_account');
    }
}
