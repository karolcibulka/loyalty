<?php

namespace Modules\Loyalty\Http\Controllers\Loyalty;


use App\Models\Loyalty\Sale;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Modules\Loyalty\Http\Controllers\MainController;

class DiscountController extends MainController
{

    public $template;
    public $screen;
    public $lang;

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth:customer');
        $this->screen = 'discounts';
    }

    public function index(){

        $sale = Sale::leftJoin('sale_dates','sale_dates.sale_id','=','sales.id')
            ->leftJoin('sale_langs','sale_langs.sale_id','=','sales.id')
            ->where('sales.active','1')
            ->where('sales.deleted','0')
            ->where('sales.visible_from','<=',date('Y-m-d H:i:s'))
            ->where('sales.visible_to','>=',date('Y-m-d H:i:s'))
            ->orderBy('points')
            ->get();

        $response = array();

        if(!empty($sale)){
            $sales = $sale->toArray();
            foreach($sales as $key => $sale){
                $response[$sale['id']]['lang'][$sale['lang']]['name']=$sale['name'];
                $response[$sale['id']]['lang'][$sale['lang']]['short_description']=$sale['short_description'];
                $response[$sale['id']]['lang'][$sale['lang']]['long_description']=$sale['long_description'];
                $response[$sale['id']]['visibleFrom'] = date('d.m.Y',strtotime($sale['visible_from']));
                $response[$sale['id']]['visibleTo'] = date('d.m.Y',strtotime($sale['visible_to']));
                $response[$sale['id']]['internal_name'] = $sale['internal_name'];
                $response[$sale['id']]['salesRestrictCount'] = $sale['sales_restrict_count'];
                $response[$sale['id']]['salesRestrict'] = $sale['sales_restrict'];
                if(isset($sale['image']) && !empty($sale['image'])){
                    $response[$sale['id']]['image']['thumb'] = asset('images/thumb/'.$sale['image']);
                    $response[$sale['id']]['image']['medium'] = asset('images/medium/'.$sale['image']);
                }
                $response[$sale['id']]['img'] = $sale['image'];
                $response[$sale['id']]['sale_id'] = $sale['sale_id'];
                $response[$sale['id']]['points'] = $sale['points'];
                $response[$sale['id']]['price'] = $sale['price'];
                $response[$sale['id']]['currency'] = $sale['currency'];
                $response[$sale['id']]['active'] = $sale['active'];
                $response[$sale['id']]['id'] = $sale['sale_id'];
                $response[$sale['id']]['from'][$key] = date('d.m.Y',strtotime($sale['from']));
                $response[$sale['id']]['to'][$key] = date('d.m.Y',strtotime($sale['to']));
                $response[$sale['id']][$sale['id']]['id'] = $sale['id'];
                $response[$sale['id']][$sale['id']]['name'] = $sale['id'];
            }
        }

        $sales = array();

        if(!empty($response)){
            foreach($response as $sale_id => $res){
                if(isset($res['lang'][$this->lang]['name']) && !empty($res['lang'][$this->lang]['name'])){
                    $sales[$sale_id]['id'] = $res['sale_id'];
                    $sales[$sale_id]['name'] = isset($res['lang'][$this->lang]['name']) && !empty($res['lang'][$this->lang]['name']) ? $res['lang'][$this->lang]['name'] : '';
                    $sales[$sale_id]['text'] = isset($res['lang'][$this->lang]['short_description']) && !empty($res['lang'][$this->lang]['short_description']) ? $res['lang'][$this->lang]['short_description'] : '';
                    $sales[$sale_id]['points'] = $res['points'];
                    $sales[$sale_id]['date'] = $res['visibleFrom']. ' - ' . $res['visibleTo'];
                    $sales[$sale_id]['price'] = $res['price'];
                    $sales[$sale_id]['currency'] = isset($res['currency']) && !empty($res['currency']) ? $res['currency'] : '€';
                    $sales[$sale_id]['oldPrice'] = isset($res['oldPrice']) && !empty($res['oldPrice']) ? $res['oldPrice'] : '';
                    $sales[$sale_id]['img']  = isset($res['image']) && !empty($res['image']) ? $res['image']['medium'] : '';


                }
            }
        }


        return view('loyalty::'.$this->template.'.loyalty.main')->with(array('screen' => $this->screen, 'discounts' => $sales));
    }
}
