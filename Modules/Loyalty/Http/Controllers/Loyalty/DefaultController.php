<?php

namespace Modules\Loyalty\Http\Controllers\Loyalty;


use Illuminate\Http\Request;
use Modules\Loyalty\Http\Controllers\MainController;

class DefaultController extends MainController
{

    public $template;
    public $screen;

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth:customer');
    }

    public function index(){

       return view('loyalty::'.$this->template.'.loyalty.main');
    }
}
