<?php

namespace Modules\Loyalty\Http\Controllers\Loyalty;


use App\Models\Crm\PointHistory;
use App\Models\Front\Reservation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Loyalty\Http\Controllers\MainController;

class HistoryController extends MainController
{
    public $template;
    public $screen;

   public function __construct()
   {
       parent::__construct();
       $this->middleware('auth:customer');
       $this->screen = 'history';
   }

   public function index(){

       $historiesArray = Reservation::where('customer_id','=',Auth::user()->customer_id)->orderBy('created_at','desc')->paginate(6);

       $histories = array();

       if(!empty($historiesArray)){
           foreach($historiesArray as $key => $history){
               $histories[date('Y',strtotime($history['created_at']))][date('F',strtotime($history['created_at']))][$key]['date'] = date('d.m.Y',strtotime($history['created_at']));
               $histories[date('Y',strtotime($history['created_at']))][date('F',strtotime($history['created_at']))][$key]['number'] = $history['id'];
               $histories[date('Y',strtotime($history['created_at']))][date('F',strtotime($history['created_at']))][$key]['points'] = $history['points'];
               $histories[date('Y',strtotime($history['created_at']))][date('F',strtotime($history['created_at']))][$key]['price'] = $history['price'];
               $histories[date('Y',strtotime($history['created_at']))][date('F',strtotime($history['created_at']))][$key]['id'] = $history['id'];
               $histories[date('Y',strtotime($history['created_at']))][date('F',strtotime($history['created_at']))][$key]['currency'] = '€';
           }
       }

       $type='history';

       return view('loyalty::'.$this->template.'.loyalty.main')->with(array('type'=>$type,'screen'=>$this->screen,'histories' => $histories,'historiesArray'=>$historiesArray));
   }

   public function transactions(){
       $histories = array();
       $historiesArray = PointHistory::where('customer_id','=',Auth::user()->customer_id)->orderBy('created_at','desc')->paginate(6);

       if(!empty($historiesArray)){
           foreach($historiesArray as $key => $history){
               $histories[date('Y',strtotime($history['created_at']))][date('F',strtotime($history['created_at']))][$key]['date'] = date('d.m.Y',strtotime($history['created_at']));
               $histories[date('Y',strtotime($history['created_at']))][date('F',strtotime($history['created_at']))][$key]['number'] = $history['reservation_id'];
               $histories[date('Y',strtotime($history['created_at']))][date('F',strtotime($history['created_at']))][$key]['points'] = $history['points'];
               $histories[date('Y',strtotime($history['created_at']))][date('F',strtotime($history['created_at']))][$key]['reason'] = $history['reason'];
               $histories[date('Y',strtotime($history['created_at']))][date('F',strtotime($history['created_at']))][$key]['operator'] = $history['type'];
               $histories[date('Y',strtotime($history['created_at']))][date('F',strtotime($history['created_at']))][$key]['id'] = $history['id'];
           }
       }

       $type= 'transaction';

       return view('loyalty::'.$this->template.'.loyalty.main')->with(array('type'=>$type,'screen'=>$this->screen,'histories' => $histories,'historiesArray'=>$historiesArray));
   }

   public function historyDetail(Request $request){
       $id = $request->id;
       $type = $request->type;

       if($type == 'transaction'){
           $title = '';
           $history = PointHistory::find($id);
           if(isset($history['reservation_id']) && !empty($history['reservation_id'])){
               $title = $history['reason'];
               $history['reservation'] = Reservation::find($history['reservation_id']);
               if(isset($history['reservation']['basket']) && !empty($history['reservation']['basket'])){
                   $history['reservation']['basket'] = unserialize($history['reservation']['basket']);
               }
           }
           else{
               $title = 'Systém - '.$history['reason'];
           }

           $view = View('loyalty::'.$this->template.'.partials.detail.transaction')->with(array('history'=>$history))->render();

           $response = array(
               'status' => '1',
               'title' => $title,
               'view' => $view
           );

           echo json_encode($response);
       }
       else{
           $title = '';
           $history = PointHistory::where('reservation_id','=',$id)->first();
           if(isset($history['reservation_id']) && !empty($history['reservation_id'])){
               $title = $history['reason'];
               $history['reservation'] = Reservation::find($history['reservation_id']);
               if(isset($history['reservation']['basket']) && !empty($history['reservation']['basket'])){
                   $history['reservation']['basket'] = unserialize($history['reservation']['basket']);
               }
           }
           else{
               $title = 'Systém - '.$history['reason'];
           }

           $view = View('loyalty::'.$this->template.'.partials.detail.transaction')->with(array('history'=>$history))->render();

           $response = array(
               'status' => '1',
               'title' => $title,
               'view' => $view
           );

           echo json_encode($response);
       }

   }
}
