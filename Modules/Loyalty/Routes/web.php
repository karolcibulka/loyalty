<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;

if (in_array(Request::segment(1), Config::get('app.alt_langs'))) {
    App::setLocale(Request::segment(1));
    Config::set('app.locale_prefix', Request::segment(1));
}


foreach(Lang::get('routes') as $k => $v) {
    Route::pattern($k, $v);
}

Route::get('/', function () {
    if (Auth::guard('customer')->check()) {
        $customerData = \App\Models\Crm\CustomerUser::where('customer_id','=',Auth::guard('customer')->user()->customer_id)->first();
        return redirect($customerData['language']);
    }
    return redirect(app()->getLocale());
});

Route::group(['prefix' => Config::get('app.locale_prefix')], function ($r) {

    Route::get('/', 'HomeController@index')->name('/');
    Auth::routes();
    Route::get('/activation/{token}', 'HomeController@activation')->name('activation');
    Route::get('/newsletterActivation/{token}', 'HomeController@newsletterActivation')->name('newsletterActivation');
    Route::get('/newsletterDeactivation/{token}', 'HomeController@newsletterDeactivation')->name('newsletterDeactivation');

    Route::get('/throttleLogin', 'HomeController@throttleLogin')->name('throttleLogin');
    Route::get('/changeExpiredPassword', 'Auth\ExpiredPassword@changeExpiredPassword')->name('changeExpiredPassword');
    Route::post('/changeExpiredPassword', 'Auth\ExpiredPassword@changeExpiredPasswordPost')->name('changeExpiredPassword');

    Route::get(Lang::get('routes.home', [], Config::get('app.locale_prefix')), 'HomeController@index')->name('home');
    Route::get(Lang::get('routes.loyalty', [], Config::get('app.locale_prefix')), 'Loyalty\DefaultController@index')->name('loyalty');
    Route::get(Lang::get('routes.discounts', [], Config::get('app.locale_prefix')), 'Loyalty\DiscountController@index')->name('discounts');
    Route::get(Lang::get('routes.history', [], Config::get('app.locale_prefix')), 'Loyalty\HistoryController@index')->name('history');
    Route::get(Lang::get('routes.history_transactions', [], Config::get('app.locale_prefix')), 'Loyalty\HistoryController@transactions')->name('history_transaction');
    Route::get(Lang::get('routes.my_account', [], Config::get('app.locale_prefix')), 'Loyalty\AccountController@index')->name('my_account');
    Route::get(Lang::get('routes.basket', [], Config::get('app.locale_prefix')), 'Loyalty\BasketController@index')->name('basket');
    Route::get(Lang::get('routes.summary', [], Config::get('app.locale_prefix')), 'Loyalty\SummaryController@index')->name('summary');
    Route::get(Lang::get('routes.experience', [], Config::get('app.locale_prefix')) . '/{slug}', 'Front\ExperienceController@index')->name('experience');
    Route::get(Lang::get('routes.category', [], Config::get('app.locale_prefix')) . '/{slug}', 'Front\CategoryController@index')->name('category');
    Route::get(Lang::get('routes.property', [], Config::get('app.locale_prefix')) . '/{slug}', 'Front\PropertyController@index')->name('property');
    Route::get(Lang::get('routes.benefit', [], Config::get('app.locale_prefix')) . '/{slug}', 'Front\BenefitController@index')->name('benefit');


    Route::get('/{slug}', 'Front\SubpageController@index')->name('subpage');
    Route::get('/changeLang/{lang}', 'HomeController@changeLang')->name('changeLang');


    //post routes
    Route::post('accountEdit', 'Loyalty\AccountController@update')->name('accountEdit');
    Route::post('addItemToBasket', 'Loyalty\BasketController@addItem')->name('addItemToBasket');
    Route::post('removeItemFromBasket', 'Loyalty\BasketController@removeItem')->name('removeItemFromBasket');
    Route::post('doReservation', 'Loyalty\BasketController@doReservation')->name('doReservation');
    Route::post('historyDetail', 'Loyalty\HistoryController@historyDetail')->name('historyDetail');


});

