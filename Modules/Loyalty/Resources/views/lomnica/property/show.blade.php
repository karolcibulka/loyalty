@extends('loyalty::'.$template.'.layouts.master')

@section('content')
    <div id="itemPage">
        <div class="container">
            <div class="row" style="padding-bottom:200px;">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6 w3-animate-left">
                            <h1 class="color fs72 title">{{$property['title']}}</h1>
                            <p class="ptext ">
                                {!! $property['long_description'] ? strip_tags($property['long_description']) : '' !!}
                            </p>
                        </div>
                        <div class="col-md-6 w3-animate-right">
                            <div class="experienceWrapperIn" style="{{$property['image'] ? 'background-image:url("'.asset('images/properties/'.$property['image']).'");' : ''}}"></div>
                            <div class="mt20"></div>
                        </div>
                    </div>
                    @if(isset($offers) && !empty($offers) && count($offers)>0)
                        <div class="row">
                            <div class="col-md-12 mt30 w3-animate-bottom">
                                <legend data-content="Ponuky v zariadení"></legend>
                            </div>
                            <div class="col-md-12 mt30">
                                <div class="row">
                                    @foreach($offers as $offer)
                                        <div class="col-md-6 col-xl-6 col-lg-6 w3-animate-bottom">
                                            <a href="{{route('experience',[$offer['slug']])}}">
                                                <div class="experienceWrapper" style="height: 300px;">
                                                    <div class="experienceItem" style="{{isset($offer['image']) && !empty($offer['image']) ? 'background-image:url("'.asset('images/medium/'.$offer['image']).'")' : ''}}">
                                                        <div class="experienceItemContent">
                                                            <span class="itemCategory">{{$offer['categories']!="" ? rtrim($offer['categories'],',') : ''}}</span>
                                                            <div class="itemInsideWrapper" style="padding-top: 80px;">
                                                                <h5 class="itemh5 text-white">{{$offer['name']}}</h5>
                                                                <span class="itemDate color999">{{date('d.m.Y',strtotime($offer['visible_from'])) .' - '. date('d.m.Y',strtotime($offer['visible_to']))}}</span>
                                                                <span class="itemPrice colo333">{{$offer['price']}} {{isset($offer['currency']) && !empty($offer['currency']) ? $offer['currency'] : '€'}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
