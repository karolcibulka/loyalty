<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('loyalty_assets/'.$template.'/css/animations.css')}}">
    <link rel="stylesheet" href="{{asset('loyalty_assets/'.$template.'/css/main.css')}}">
    <link href="https://fonts.googleapis.com/css?family=DM+Sans&display=swap" rel="stylesheet">
    <link href="{{asset('assets/css/vendor/font-awesome.css')}}" rel="stylesheet">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <title>
        @if(View::hasSection('title'))
            @yield('title')
        @else
            Bonne Vie
        @endif
    </title>
</head>
<body>

@include('loyalty::'.$template.'.partials.navigation')
    @yield('content')
@include('loyalty::'.$template.'.partials.footer')
@include('loyalty::'.$template.'.partials.modal')




<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<script>
    $(document).on('click','.login-button',function(){
       window.location.href="{{route('login')}}";
    });

    $(document).on('click','.loyalty-button',function(){
        //console.log('loyalty');
        window.location.href="{{route('discounts')}}";
    });

    $(document).on('click','.loyaltyLogOut',function(){
        $('#logOut').submit();
    });
</script>
</body>
</html>
