@extends('loyalty::'.$template.'.layouts.master')

@section('content')

    <style>
        .form-control{
            border-radius:0;
            height:50px;
            margin-bottom:10px;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="offset-lg-3 offset-md-3 col-lg-6 col-md-6">
                <div class="login-register-wrap">
                    <h2 class="main-h2 text-center">Zmena hesla</h2>
                    <div class="login-register-form">
                        <form action="{{ route('changeExpiredPassword') }}" method="POST">
                            @csrf
                            @if(Session::has('wrongData'))
                                <div class="alert alert-danger br0">
                                    <ul>
                                        <li>Nové heslo <strong>nemôže byť</strong> rovnaké ako staré heslo</li>
                                        <li>Uistite sa , že ste zadali správne <strong>staré heslo</strong></li>
                                    </ul>
                                </div>
                            @endif
                            <div class="sin-login-register">
                                <label>Emailová adresa <span>*</span></label>
                                <input class="form-control" type="email" value="{{Auth::guard('customer')->user()->email}}" disabled>
                            </div>
                            <div class="sin-login-register">
                                <label>Pôvodné heslo <span>*</span></label>
                                <input class="form-control" type="password" placeholder="Pôvodné heslo" name="old_password" autocomplete="off">
                            </div>
                            <input type="hidden" name="email" value="{{Auth::guard('customer')->user()->email}}">
                            <div class="sin-login-register">
                                <label>Nové heslo <span>*</span></label>
                                <input class="form-control" type="password" placeholder="Nové heslo" name="password">
                            </div>
                            <div class="sin-login-register">
                                <label>Nové heslo znovu <span>*</span></label>
                                <input class="form-control" type="password" placeholder="Nové heslo znovu" name="password_confirmation">
                            </div>
                            <div class="login-register-btn-remember" style="margin-top:10px;">
                                <div class="login-register-btn">
                                    <button type="submit" class="btn br0 btn-red w100 mt30">Zmeniť heslo
                                        <span class="border"><span></span><span></span><span></span><span></span></span>
                                    </button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
