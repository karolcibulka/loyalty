@extends('loyalty::'.$template.'.layouts.master')

@section('title')
    Zabudnuté heslo
@endsection

@section('content')
    <div id="loginRegister m-auto">
        <div class="self-container ">
            <div class="login-register-area section-padding-1 pt-100 pb-100">
                <div class="container">
                    @if (session('status'))
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="login-register-wrap">
                                <h3>Zabudnuté heslo</h3>
                                <div class="login-register-form">
                                    <form action="{{ route('password.email') }}" method="POST">
                                        @csrf
                                        <div class="sin-login-register">
                                            <label>Emailová adresa <span>*</span></label>
                                            <input class="form-control" type="email" name="email" value="{{old('email')}}" placeholder="Email" required>
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                        </div>
                                        <div class="login-register-btn-remember">
                                            <div class="login-register-btn">
                                                <button type="submit" class="btn btn-primary background text-white btn-own">Resetovať</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
