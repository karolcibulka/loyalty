@extends('loyalty::'.$template.'.layouts.master')

@section('title')
    Prihlásenie / Registrácia
@endsection

@section('content')

    <style>
        .form-control{
            border-radius:0;
            height:50px;
        }

        .sin-login-register label{
            margin-top:10px;
            font-size:14px;
            font-weight:bold;
        }
        .login-register-btn button{
            margin-left: 0 !important;
            margin-right: 0 !important;
            margin-top:30px !important;
        }
    </style>
    <div id="loginRegister">
        <div class="container" style="padding-bottom:200px;">
            @if(Session::has('message'))
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert {{(Session::has('alert-class')) ? Session::get('alert-class') : 'alert-warning'}}" style="text-align:center;">
                            {{Session::get('message')}}
                        </div>
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="login-register-wrap">
                        <h2 class="main-h2 text-center">Prihlásenie</h2>
                        <div class="login-register-form">
                            <form action="{{ route('login') }}" method="POST">
                                @csrf
                                @if(Session::has('throttleResponse'))
                                    <div class="alert alert-danger">
                                        Do ďalšieho prihlásenia musíte počkať <strong  class="timeCountDown">{{Session::get('throttleResponse')['Retry-After']}}</strong> sekúnd
                                    </div>
                                @endif
                                <div class="sin-login-register">
                                    <label>Emailová adresa <span>*</span></label>
                                    <input class="form-control" type="email" name="email" placeholder="email@email.com" value="{{(Session::has('form-confirm') && Session::get('form-confirm')=='login') ?  old('email')  : ''}}" required>
                                    @if(Session::has('form-confirm') && Session::get('form-confirm')=='login')
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    @endif
                                </div>
                                <div class="sin-login-register">
                                    <label>Heslo <span>*</span></label>
                                    <input class="form-control" type="password" placeholder="Heslo" name="password">
                                    @if(Session::has('form-confirm') && Session::get('form-confirm')=='login')
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    @endif
                                </div>
                                <div class="login-register-btn-remember" style="margin-top:10px;">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="login-register-remember">
                                                <label>
                                                    <input type="checkbox">
                                                    Zapamätať si ma
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="lost-password ">
                                                <a href="{{route('password.request')}}">Stratili ste heslo?</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="login-register-btn">
                                        <button type="submit" class="btn br0 btn-red w100 mt30 login-button-throttle">Prihlásiť sa
                                            <span class="border"><span></span><span></span><span></span><span></span></span>
                                        </button>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="login-register-wrap register-wrap">
                        <h2 class="main-h2 text-center">Registrácia</h2>
                        <div class="login-register-form">
                            <form action="{{ route('register') }}" method="POST">
                                @csrf
                                <div class="sin-login-register">
                                    <label>Meno <span>*</span></label>
                                    <input class="form-control" type="text" placeholder="Meno" name="first_name" value="{{ (Session::has('form-confirm') && Session::get('form-confirm')=='register') ? old('first_name') : '' }}">
                                </div>
                                @if(Session::has('form-confirm') && Session::get('form-confirm')=='register')
                                    @error('first_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>@lang('auth.'.$message)</strong>
                                        </span>
                                    @enderror
                                @endif
                                <div class="sin-login-register">
                                    <label>Priezvisko <span>*</span></label>
                                    <input class="form-control" type="text" placeholder="Priezvisko" name="last_name" value="{{(Session::has('form-confirm') && Session::get('form-confirm')=='register')  ? old('last_name') : ''}}">
                                </div>
                                @if(Session::has('form-confirm') && Session::get('form-confirm')=='register')
                                    @error('last_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>@lang('auth.'.$message)</strong>
                                        </span>
                                    @enderror
                                @endif
                                <br>
                                <div class="sin-login-register">
                                    <label>Mesto <span>*</span></label>
                                    <input class="form-control" type="text" placeholder="Mesto" name="city" value="{{(Session::has('form-confirm') && Session::get('form-confirm')=='register') ? old('city') : ''}}">
                                </div>
                                @if(Session::has('form-confirm') && Session::get('form-confirm')=='register')
                                    @error('city')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>@lang('auth.'.$message)</strong>
                                        </span>
                                    @enderror
                                @endif

                                <div class="sin-login-register">
                                    <label>Ulica <span>*</span></label>
                                    <input class="form-control" type="text" placeholder="Ulica" name="address" value="{{(Session::has('form-confirm') && Session::get('form-confirm')=='register') ? old('address') : ''}}">
                                </div>
                                @if(Session::has('form-confirm') && Session::get('form-confirm')=='register')
                                    @error('address')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>@lang('auth.'.$message)</strong>
                                        </span>
                                    @enderror
                                @endif

                                <div class="sin-login-register">
                                    <label>PSČ <span>*</span></label>
                                    <input class="form-control" type="text" placeholder="PSČ" name="zip" value="{{(Session::has('form-confirm') && Session::get('form-confirm')=='register') ? old('zip') : ''}}">
                                </div>
                                @if(Session::has('form-confirm') && Session::get('form-confirm')=='register')
                                    @error('zip')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>@lang('auth.'.$message)</strong>
                                        </span>
                                    @enderror
                                @endif

                                <div class="sin-login-register">
                                    <label>Štát <span>*</span></label>
                                    <select name="country" class="form-control">
                                        @if(isset($countries) && !empty($countries))
                                            @foreach($countries as $country)
                                                <option {{(Session::has('form-confirm') && Session::get('form-confirm')=='register') && old('country') == $country['country_code'] ? 'selected' : ''}} {{!old('country') && $country['country_code'] == 'SVK' ? 'selected' : ''}}  value="{{$country['country_code']}}">{{$country['country_name']}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                @if(Session::has('form-confirm') && Session::get('form-confirm')=='register')
                                    @error('country')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>@lang('auth.'.$message)</strong>
                                        </span>
                                    @enderror
                                @endif
                                <br>
                                <div class="sin-login-register">
                                    <label>Emailová adresa <span>*</span></label>
                                    <input class="form-control" type="email" placeholder="email@email.com" name="email" value="{{(Session::has('form-confirm') && Session::get('form-confirm')=='register') ? old('email') : ''}}">
                                </div>
                                @if(Session::has('form-confirm') && Session::get('form-confirm')=='register')
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>@lang('auth.'.$message)</strong>
                                        </span>
                                    @enderror
                                @endif

                                <div class="sin-login-register">
                                    <label>Telefónne číslo <span>*</span></label>
                                    <input class="form-control" type="text" placeholder="Telefónne číslo" name="phone" value="{{(Session::has('form-confirm') && Session::get('form-confirm')=='register') ? old('phone') : ''}}">
                                </div>
                                @if(Session::has('form-confirm') && Session::get('form-confirm')=='register')
                                    @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>@lang('auth.'.$message)</strong>
                                        </span>
                                    @enderror
                                @endif

                                <div class="sin-login-register">
                                    <label>Predvolený jazyk <span>*</span></label>
                                    <select name="language" class="form-control">
                                        @foreach($loyalty_languages as $loyalty_language)
                                            <option {{(Session::has('form-confirm') && Session::get('form-confirm')=='register' && old('language') == $loyalty_language) || ($currentLang == $loyalty_language) ? 'selected' : ''}} value="{{$loyalty_language}}">{{__('global.lang.'.$loyalty_language)}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="sin-login-register">
                                    <label>Heslo <span>*</span></label>
                                    <input class="form-control" type="password" placeholder="Heslo" name="password">
                                </div>
                                @if(Session::has('form-confirm') && Session::get('form-confirm')=='register')
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>@lang('auth.'.$message)</strong>
                                        </span>
                                    @enderror
                                @endif
                                <div class="sin-login-register">
                                    <label>Potvrdenie hesla <span>*</span></label>
                                    <input class="form-control" type="password" placeholder="Heslo znovu" name="password_confirmation">
                                </div>
                                <div class="sin-login-register">
                                    <input type="hidden" name="newsletter" value="0">
                                    <label><input type="checkbox" name="newsletter" value="1"> Príhlasiť odber newsletteru</label>
                                </div>
                                <div class="login-register-btn">
                                    <button type="submit" class="btn br0 w100 btn-red mt30 register-button-throttle">Registrovať
                                        <span
                                            class="border"><span></span><span></span><span></span><span></span></span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            countdown();
            @if(Session::has('throttleResponse'))
                $('.login-button-throttle').attr('disabled',true);
                $('.register-button-throttle').attr('disabled',true);
            @endif
        });

        var countdown = function(){
            var interval =  setInterval(function(){
                var _wrapper = $('.timeCountDown');
                if(typeof _wrapper !== 'undefined'){
                    var _seconds = _wrapper.html();
                    if((+_seconds)===0){
                        _wrapper.closest('.alert').remove();
                        $('.login-button-throttle').attr('disabled',false);
                        $('.register-button-throttle').attr('disabled',false);
                        clearInterval(interval);
                    }
                    _wrapper.html((+_seconds)-1);
                }
                else{
                    clearInterval(interval);
                }
            },1000)
        };
    </script>
@endsection
