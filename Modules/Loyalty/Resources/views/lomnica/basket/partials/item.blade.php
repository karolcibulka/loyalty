<div class="row discountWrapper w3-animate-right mr0">
    <div class="col-lg-2 col-md-12 pr0 pl0 col-xl-2">
        <div class="discountImageWrapper mh120" style="{{isset($image) && !empty($image) ? 'background-image:url("'.asset('images/medium/'.$image).'");background-size:cover;background-repeat: no-repeat;background-position:center;' : ''}}">

        </div>
    </div>
    <div class="col-lg-10 col-md-12 col-xl-10">
        <div class="row">
            <div class="col-xl-7 col-lg-12 col-md-12">
                <div class="discountTextWrapper">
                    <h3 class="loyalty-h3">{!! $name !!} </h3>
                    <span class="loyalty-discount-date">
                       {!! date('d.m.Y',strtotime($visible_from)) . ' - ' . date('d.m.Y',strtotime($visible_to)) !!}
                    </span>
                    <p class="loyalty-discount-text">
                        {!! $short_description !!}
                    </p>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-xl-5">
                <div class="discount-price-wrapper">
                    <div class="pt20"></div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="background color-white countersWrapper decrement" data-id="{{$id}}">-</div>
                            <div class="countersWrapper basketItemCount">1</div>
                            <div class="background color-white countersWrapper disabledWrapper increment" data-id="{{$id}}">+</div>
                        </div>
                        <div class="col-md-6">
                            @if(isset($points) && !empty($points))
                                <span class="discount-price">{!! '<span class="discount-price-points">'.$points.'</span> bodov' !!}</span>
                            @endif
                            @if(isset($price) && !empty($price))
                                <span class="discount-price">+ {!! '<span class="discount-price-price">'.$price.'</span>' !!} {{!empty($currency) ? $currency : '€'}}</span>
                            @endif
                            <div class="pt10"></div>
                            @if(isset($old_price) && !empty($old_price))
                                <span class="discount-old-price">Cena na pokladni: <span class="line-throught">{!! $old_price !!} {!! mb_strtoupper($currency) !!}</span></span>
                            @endif
                        </div>
                    </div>
                    <div class="pt30"></div>
                    <button class="btn btn-custom background br0 color-white fs12 w100 m-auto remove-from-basket btn-red {{explode('_',$key)[0]}}" data-id="{{$id}}">
                        <span class="button-text">{!! 'Odobrať z košíka' !!}</span>
                        <span class="border">
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
