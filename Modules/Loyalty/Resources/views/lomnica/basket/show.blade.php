@extends('loyalty::'.$template.'.layouts.master')

<style>
    body{
        background-color:#fff !important;
    }
</style>

@section('content')
    <style>

    </style>
    <div class="container">
        <div class="row" style="padding-bottom:200px;">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="main-h2 text-center">
                            Košík
                        </h2>
                    </div>
                </div>

                <div class="text-center emptyBasket" style="display:none;">
                    <h2>Váš košík je prázdny !</h2>
                </div>

                @if(isset($basket) && !empty($basket))
                    @foreach($basket as $key => $item)
                        @include('loyalty::'.$template.'.basket.partials.item',$item,['key'=>$key])
                    @endforeach
                @else
                    <script>
                        $('.emptyBasket').show();
                    </script>
                @endif

                <div class="priceCheckerBasket">
                    <div class="container">
                        <div class="row">
                            <div class="offset-md-3 offset-sm-0 offset-xs-0 col-md-9 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <span class="finalPriceBasket">  Spolu: <br><span class="finalPriceBasketNum">500 €</span></span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <button class="btn btn-custom background br0 color-white fs12 w100 btn-red btn-continue">
                                            <span class="button-text">Pokračovať ku objednávke</span>
                                            <span class="border">
                                            <span></span><span></span><span></span><span></span>
                                        </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('.decrement').on('click',function(event){
            event.preventDefault();
            event.stopPropagation();
            var wrapper = $(this).closest('.discountWrapper');
            var btn = wrapper.find('button.btn-red');
            var id = $(this).data().id;
            var name = wrapper.find('h3.loyalty-h3').html();
            name = '<br><strong>'+name+'</strong>';
            var count = $(this).next('.basketItemCount').html();
            if(count==='1'){
                Swal.fire({
                    title: 'Pozor!',
                    icon: 'warning',
                    html:'Naozaj chcete zmazať z košíka ponuku '+ name + '?',
                    confirmButtonText: 'Áno',
                    cancelButtonText: 'Nie',
                    confirmButtonColor:'#5e1212',
                    cancelButtonColor:'#464646',
                    showCancelButton:true,
                }).then(function(response){
                   if(response.value === true){
                       var data;
                       if(btn.hasClass('item')){
                            data = {id:id,_token:'{{csrf_token()}}'};
                       }
                       else{
                           data = {id:id,_token:'{{csrf_token()}}',offer:'1'};
                       }
                       $.ajax({
                          type:"post",
                          url:'{{route('removeItemFromBasket')}}',
                          dataType:"json",
                          data:data,
                          success:function(data){
                              if(data.status === '1'){
                                  wrapper.remove();
                                  $('.basket-icon').attr('data-content',data.basketCount);
                                  if(data.basketCount === 0){
                                      $('.emptyBasket').show();
                                  }
                                  checkPrices();
                              }
                          },
                       });
                       Swal.fire({
                           title: 'Odstránené!',
                           icon: 'success',
                           html:'Z košíku bola ponuka '+name + '<br> úspešné odstránená!',
                           confirmButtonText: 'Áno',
                           cancelButtonText: 'Zatvoriť',
                           confirmButtonColor:'#5e1212',
                           cancelButtonColor:'#464646',
                           showConfirmButton:false,
                           showCancelButton:true,
                       })
                   }
                });
            }
        });

        $('.remove-from-basket').on('click',function(event){
            event.preventDefault();
            event.stopPropagation();
            var wrapper = $(this).closest('.discountWrapper');
            var btn = wrapper.find('button.btn-red');
            var id = $(this).data().id;
            var name = wrapper.find('h3.loyalty-h3').html();
            name = '<br><strong>'+name+'</strong>';
            Swal.fire({
                title: 'Pozor!',
                icon: 'warning',
                html:'Naozaj chcete zmazať z košíka ponuku '+ name + '?',
                confirmButtonText: 'Áno',
                cancelButtonText: 'Nie',
                confirmButtonColor:'#5e1212',
                cancelButtonColor:'#464646',
                showCancelButton:true,
            }).then(function(response){
                if(response.value === true){
                    var data;
                    if(btn.hasClass('item')){
                        data = {id:id,_token:'{{csrf_token()}}'};
                    }
                    else if(btn.hasClass('offer')){
                        data = {id:id,_token:'{{csrf_token()}}',offer:'1'};
                    }
                    $.ajax({
                        type:"post",
                        url:'{{route('removeItemFromBasket')}}',
                        dataType:"json",
                        data:data,
                        success:function(data){
                            if(data.status === '1'){
                                wrapper.remove();
                                $('.basket-icon').attr('data-content',data.basketCount);
                                if(data.basketCount === 0){
                                    $('.emptyBasket').show();
                                }
                                checkPrices();
                            }
                        },
                    });
                    Swal.fire({
                        title: 'Odstránené!',
                        icon: 'success',
                        html:'Z košíku bola ponuka '+name + '<br> úspešné odstránená!',
                        confirmButtonText: 'Áno',
                        cancelButtonText: 'Zatvoriť',
                        confirmButtonColor:'#5e1212',
                        cancelButtonColor:'#464646',
                        showConfirmButton:false,
                        showCancelButton:true,
                    })
                }
            });

        });

        var checkPrices = function(){
            var objects = $(document).find('.discountWrapper');
            var wholeEuro = 0;
            var wholePoints = 0;
            objects.each(function(){
               var obj = $(this);
               var points = obj.find('.discount-price-points').html();
               var euros = obj.find('.discount-price-price').html();
               if(typeof points !== "undefined" && points){
                   wholePoints = (+wholePoints) + (+parseInt(points));
               }
               if(typeof euros !== "undefined" && euros){
                   wholeEuro = (+wholeEuro) + (+parseInt(euros));
               }
            });

            var price = '';
            if(wholePoints>0){
                price += wholePoints+' bodov';
            }
            if(wholeEuro>0){
                price += wholePoints>0 ? ' + '+wholeEuro+' €' : wholeEuro+' €';
            }

            if(wholeEuro === 0 && wholePoints === 0){
                price = '0 €';
                $('button.btn-continue').attr('disabled',true);
            }

            $('.finalPriceBasketNum').html(price);
        }

        checkPrices();

        $('.btn-continue').on('click',function(){
            var btn = $(this);
            btn.attr('disabled',true);
            btn.find('.button-text').html('<i class="rotating fa fa-spinner"></i>');
            window.location.href="{{route('summary')}}";
        });
    </script>
@endsection


