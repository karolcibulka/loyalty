@extends('loyalty::'.$template.'.layouts.master')

@section('content')
    <div id="itemPage">
        <div class="container">
            <div class="row" style="padding-bottom:200px;">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6 w3-animate-left">
                            <h1 class="color fs72 title">{{$experience['name']}}</h1>
                            <p class="ptext ">
                                {!! $experience['long_description'] ? strip_tags($experience['long_description']) : '' !!}
                            </p>
                        </div>
                        <div class="col-md-6 w3-animate-right">
                            <div class="experienceWrapperIn" style="{{$experience['image'] && file_exists(asset('images/medium/'.$experience['image'])) ? 'background-image:url("'.asset('images/medium/'.$experience['image']).'");' : ''}}"></div>
                            <div class="mt20"></div>
                            <div class="col-md-12 priceWrapper">
                                <span class="color333">Cena:</span> <span class="price color fs20">{{$experience['price']}} {{$experience['currency'] ? $experience['currency'] : '€'}}</span>
                            </div>
                            <div class="mt20"></div>
                            <button class="btn br0 btn-red {{in_array('offer_'.$experience['id'],$basketIDs) ? 'remove-from-basket' : 'add-to-basket'}}" data-id="{{$experience['id']}}">
                                <span class="button-text">{{in_array('offer_'.$experience['id'],$basketIDs) ? 'Odobrať z košíka' : 'Pridať do košíka'}}</span>
                                <span class="border"><span></span><span></span><span></span><span></span></span>
                            </button>
                        </div>
                    </div>
                    @if(isset($experiences) && !empty($experiences) && count($experiences)>0)
                        <div class="row">
                            <div class="col-md-12 mt30 w3-animate-bottom">
                                <legend data-content="Ostatné zážitky"></legend>
                            </div>
                            <div class="col-md-12 mt30">
                                <div class="row">
                                    @foreach($experiences as $exp)
                                        <div class="col-md-6 col-xl-6 col-lg-6 w3-animate-bottom">
                                            <a href="{{route('experience',[$exp['slug']])}}">
                                                <div class="experienceWrapper" style="height: 300px;">
                                                    <div class="experienceItem" style="{{isset($exp['image']) && !empty($exp['image']) ? 'background-image:url("'.asset('images/medium/'.$exp['image']).'")' : ''}}">
                                                        <div class="experienceItemContent">
                                                            <span class="itemCategory">{{$exp['categories']!="" ? rtrim($exp['categories'],',') : ''}}</span>
                                                            <div class="itemInsideWrapper" style="padding-top: 80px;">
                                                                <h5 class="itemh5 text-white">{{$exp['name']}}</h5>
                                                                <span class="itemDate color999">{{date('d.m.Y',strtotime($exp['visible_from'])) .' - '. date('d.m.Y',strtotime($exp['visible_to']))}}</span>
                                                                <span class="itemPrice colo333">{{$exp['price']}} {{isset($exp['currency']) && !empty($exp['currency']) ? $exp['currency'] : '€'}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <script>
        $('#itemPage button.btn-red').on('click',function(){
            var logged = '{{$loggedIn}}';
            var btn = $(this);
            var id = btn.data().id;
            var name = '<strong>'+$(this).closest('#itemPage').find('h1.title').html()+'</strong>';
            if(logged === 'true'){
                btn.attr('disabled',true);
                btn.find('.button-text').html('<i class="rotating fa fa-spinner"></i>');
                if(btn.hasClass('add-to-basket')){
                    $.ajax({
                        type:"post",
                        url:'{{route('addItemToBasket')}}',
                        dataType:'json',
                        data:{id:id,offer:'1',_token:'{{csrf_token()}}'},
                        success:function(data){
                            if(data.status === '1'){
                                btn.addClass('remove-from-basket').removeClass('add-to-basket');
                                $('.basket-icon').attr('data-content',data.basketCount);
                                btn.find('.button-text').html('odobrať z košíka');
                                Swal.fire({
                                    title: '',
                                    icon: 'success',
                                    html:'Do košíka ste si pridali:<br>' +name,
                                    confirmButtonText: 'Prejsť do košíka',
                                    cancelButtonText: 'Späť ku nákupu',
                                    confirmButtonColor:'#5e1212',
                                    cancelButtonColor:'#464646',
                                    showCancelButton:true,
                                }).then(function(response){
                                    if(response.value === true){
                                        $('.basket-icon').click();
                                    }
                                    else{
                                        btn.attr('disabled',false);
                                    }
                                });
                            }
                            else{
                                btn.attr('disabled',false);
                                btn.find('.button-text').html('pridať do košíka');
                            }
                        }
                    });
                }
                else{
                    Swal.fire({
                        title: '',
                        icon: 'warning',
                        html:'Noazaj chcete odobrať z košíka<br>' +name + ' ?',
                        confirmButtonText: 'Pokračovať v nákupe',
                        cancelButtonText: 'Odobrať',
                        confirmButtonColor:'#5e1212',
                        cancelButtonColor:'#464646',
                        showCancelButton:true,
                    }).then(function(response){
                        if(response.value === true){
                            btn.attr('disabled',false);
                            btn.find('.button-text').html('ODOBRAŤ Z KOŠÍKA');
                        }
                        else{
                            $.ajax({
                                type:"post",
                                url:'{{route('removeItemFromBasket')}}',
                                dataType:"json",
                                data:{id:id,offer:'1',_token:'{{csrf_token()}}'},
                                success:function(data){
                                    if(data.status === '1'){
                                        btn.removeClass('remove-from-basket').addClass('add-to-basket');
                                        btn.find('.button-text').html('PRIDAŤ DO KOŠÍKA');
                                        btn.attr('disabled',false);
                                        $('.basket-icon').attr('data-content',data.basketCount);
                                    }
                                }
                            });
                        }
                    });
                }
            }
            else{
                Swal.fire({
                   icon:'warning',
                   title:'Musíte byť prihlásený!',
                   showCancelButton:true,
                   confirmButtonText: 'Prihlásiť/registrovať',
                   cancelButtonText: 'Zrušiť',
                   confirmButtonColor:'#5e1212',
                   cancelButtonColor:'#464646',
                }).then(function(response){
                    if(response.value === true){
                        window.location.href='{{route('login')}}';
                    }
                });
            }
        });
    </script>
@endsection
