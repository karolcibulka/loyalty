@extends('loyalty::'.$template.'.layouts.master')

@section('content')
    <div id="itemPage">
        <div class="container">
            <div class="row" style="padding-bottom:200px;">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6 w3-animate-left">
                            <h1 class="color fs72 title">{{$category['title']}}</h1>
                            <p class="ptext ">
                                {!! $category['long_description'] ? strip_tags($category['long_description']) : '' !!}
                            </p>
                        </div>
                        <div class="col-md-6 w3-animate-right">
                            <div class="experienceWrapperIn" style="{{$category['image'] ? 'background-image:url("'.asset('images/categories/'.$category['image']).'");' : ''}}"></div>
                            <div class="mt20"></div>
                        </div>
                    </div>
                    @if(isset($category['properties']) && !empty($category['properties']) && count($category['properties'])>0)
                        <div class="row">
                            <div class="col-md-12 mt30 w3-animate-bottom">
                                <legend data-content="Zariadenia"></legend>
                            </div>
                            <div class="col-md-12 mt30">
                                <div class="row">
                                    @foreach($category['properties'] as $property)
                                        <div class="col-md-6 col-xl-6 col-lg-6 w3-animate-bottom">
                                            <a href="{{route('property',[$property['slug']])}}">
                                                <div class="experienceWrapper">
                                                    <div class="experienceItem" style="{{isset($property['image']) && !empty($property['image']) ? 'background-image:url("'.asset('images/properties/'.$property['image']).'")' : ''}}">
                                                        <div class="experienceItemContent">
                                                            <div class="itemInsideWrapper">
                                                                <h5 class="itemh5 text-white">{{$property['title']}}</h5>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
