<div class="container">
    <div class="row" style="padding-top:100px;padding-bottom:100px;min-height:100vh">
        <div class="col-md-12 text-center">
            <h2 class="main-h2 fs72 text-center">Vaša objednávka prebehla úspešne!</h2>
            <div class="swal2-icon swal2-success swal2-icon-show" style="display: flex;margin-top:20px;">
                <div class="swal2-success-circular-line-left" style="background-color: rgb(255, 255, 255);"></div>
                <span class="swal2-success-line-tip"></span> <span class="swal2-success-line-long"></span>
                <div class="swal2-success-ring"></div> <div class="swal2-success-fix" style="background-color: rgb(255, 255, 255);"></div>
                <div class="swal2-success-circular-line-right" style="background-color: rgb(255, 255, 255);"></div>
            </div>
            <h4 class="main-h4 color333 text-center mt10" style="padding-top: 20px;">
                {{$addedPoints > 0 ?
                'Milý/á '. $customer['first_name'].' '.$customer['last_name'].' , za Vašu objednávku sme na Váš účet sme pripísali '.$addedPoints .' bodov'
                :
                ''
                }}
            </h4>
        </div>
    </div>
</div>
