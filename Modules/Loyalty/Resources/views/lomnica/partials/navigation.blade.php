<div id="navigation">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="{{route('/')}}">
                <span class="navbarText">Bonne Vie</span>
            </a>
            <div style="display: inline-flex;">
                <button style="width:54px;height: 40px;margin-top: 5px;" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                @if(!Auth::guard('customer')->check())
                    <button type="button" class="btn show991 btn-custom login-button background text-white br0 btn-red">Prihlásiť sa
                        <span class="border"><span>

                            </span><span></span><span></span><span></span></span></button>
                @else
                    <button class="btn show991 btn-custom btn-custom loyalty-button background text-white br0 btn-red" style="margin-top:0;">{{Auth::guard('customer')->user()->first_name.' '. Auth::guard('customer')->user()->last_name}}
                        <span class="border"><span>

                            </span><span></span><span></span><span></span></span></button>
                @endif
                <a href="{{route('basket')}}"><i data-content="{{$basketCount}}" class="fa fa-shopping-cart color show991 basket"></i></a>
            </div>
            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav ml-auto">
                    {{-- active --}}
                    @if(isset($loyalty_navigation) && !empty($loyalty_navigation))
                        @foreach($loyalty_navigation as $navigation)
                            @if(isset($navigation['titles'][$currentLang]) && !empty($navigation['titles'][$currentLang]) && $navigation['titles'][$currentLang]!='')
                                @if(isset($navigation['children']) && !empty($navigation['children']) && count($navigation['children'])>0)
                                    <li class="nav-item dropdown">
                                        @php
                                            $href = '#';
                                            if($navigation['type'] == 'subpage'){
                                                $href=route('subpage',[$navigation['slugs'][$currentLang]]);
                                            }
                                            elseif($navigation['type'] == 'link'){
                                                $href = $navigation['href'];
                                            }
                                            elseif($navigation['type'] == 'file'){
                                                $href = $navigation['file'];
                                            }
                                        @endphp
                                        <a class="nav-link dropdown-toggle" href="{{$href}}" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{$navigation['titles'][$currentLang]}}
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            @foreach($navigation['children'] as $children)
                                                @if(isset($children['titles'][$currentLang]) && !empty($children['titles'][$currentLang]) && $children['titles'][$currentLang]!='')
                                                    @php
                                                        $href = '#';
                                                        if($children['type'] == 'subpage'){
                                                            $href=route('subpage',[$children['slugs'][$currentLang]]);
                                                        }
                                                        elseif($children['type'] == 'link'){
                                                            $href = $children['href'];
                                                        }
                                                        elseif($children['type'] == 'file'){
                                                            $href = $children['file'];
                                                        }
                                                    @endphp
                                                    <a class="dropdown-item" {{$children['open'] == 'current' ?: 'target="_blank" '}} href="{{$href}}" {{$children['type']=='file' && $children['download'] == '1' ? 'download' : ''}}>{{$children['titles'][$currentLang]}}</a>
                                                @endif
                                            @endforeach
                                        </div>
                                    </li>
                                @else
                                    <li class="nav-item active">
                                        @php
                                            $href = '#';
                                            if($navigation['type'] == 'subpage'){
                                                $href=route('subpage',[$navigation['slugs'][$currentLang]]);
                                            }
                                            elseif($navigation['type'] == 'link'){
                                                $href = $navigation['href'];
                                            }
                                            elseif($navigation['type'] == 'file'){
                                                $href = $navigation['file'];
                                            }
                                        @endphp
                                        <a class="nav-link" {{$navigation['open'] == 'current' ?: 'target="_blank" '}} href="{{$href}}" {{$navigation['type']=='file' && $navigation['download'] == '1' ? 'download' : ''}}>{{$navigation['titles'][$currentLang]}}</a>
                                    </li>
                                @endif

                            @endif
                        @endforeach
                    @endif
                </ul>

                @if(!Auth::guard('customer')->check())
                    <button type="button" class="btn hide991 btn-custom login-button background text-white br0 btn-red" style="margin-top:0;">Prihlásiť sa
                        <span class="border"><span>

                            </span><span></span><span></span><span></span></span></button>
                @else
                    <button class="btn hide991 btn-custom loyalty-button background text-white br0 btn-red" style="margin-top:0;">{{Auth::guard('customer')->user()->first_name.' '. Auth::guard('customer')->user()->last_name}}
                        <span class="border"><span>

                            </span><span></span><span></span><span></span></span></button>
                @endif
                <a href="{{route('basket')}}">
                    <i data-content="{{$basketCount}}" class="fa fa-shopping-cart color hide991 basket basket-icon"></i>
                </a>

                <div class="dropdown" style="margin-left:15px;">
                    <img id="dropdownMenuButton" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width:35px;cursor:pointer;" src="{{asset('loyalty_assets/'.$template.'/flags/'.$currentLang.'.svg')}}" alt="flag">
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        @foreach($loyalty_languages as $loyalty_language)
                           @if($loyalty_language != $currentLang)
                                <a class="dropdown-item" href="{{route('changeLang',[$loyalty_language])}}">
                                    <img src="{{asset('loyalty_assets/'.$template.'/flags/'.$loyalty_language.'.svg')}}" alt="" style="width:30px;">
                                    <small>{{__('global.lang.'.$loyalty_language)}}</small>
                                </a>
                            @endif
                        @endforeach
                    </div>
                </div>

                <a href="{{$loyalty_languages[1]}}" style="margin-left:10px"></a>
            </div>
        </nav>
    </div>
</div>
