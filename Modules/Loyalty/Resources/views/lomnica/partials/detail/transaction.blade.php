<style>
    .form-control{
        height: 50px;
        border-radius:0;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center" style="margin-bottom:30px;">
            <h3><small>{{date('d.m.Y',strtotime($history['created_at']))}}</small></h3>
        </div>
        <div class="mt30"></div>
        <div class="col-md-4">
            <div class="form-group">
                <label>Starý počet bodov</label>
                <input type="text" class="form-control" value="{{$history['old_points']}}" disabled>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label>Operácia s bodmi </label>
                <input type="text" class="form-control" value="{{$history['type']}} {{$history['points']}}" disabled>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label>Počet bodov po operácii</label>
                <input type="text" class="form-control" value="{{$history['new_points']}}" disabled>
            </div>
        </div>
        @if(isset($history['reservation']) && !empty($history['reservation']))
            <legend></legend>
            <div class="mt30"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        @if(isset($history['reservation']['basket']) && !empty($history['reservation']['basket']))
                            @foreach($history['reservation']['basket'] as $basket_key => $item)
                                @include('loyalty::'.$template.'.summary.partials.item',$item)
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        @endif

    </div>
</div>
