
<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="detailModalLabel" aria-hidden="true">
    <div class="modal-dialog mw70" role="document">
        <div class="modal-content br0">
            <div class="modal-header text-center" style="display:block;">
                <h3 class="modal-title text-center" id="detailModalLabel">
                    Detail objednávka č. 9
                </h3>
            </div>
            <div class="modal-body">
                content content
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary br0" data-dismiss="modal">ZATVORIŤ</button>
            </div>
        </div>
    </div>
</div>
