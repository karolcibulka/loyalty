<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <title>
    </title>
    <!--[if !mso]><!-- -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
        #outlook a {
            padding:0;
        }
        body {
            margin:0;
            padding:0;
            -webkit-text-size-adjust:100%;
            -ms-text-size-adjust:100%;
        }
        table, td {
            border-collapse:collapse;
            mso-table-lspace:0pt;
            mso-table-rspace:0pt;
        }
        img {
            border:0;
            height:auto;
            line-height:100%;
            outline:none;
            text-decoration:none;
            -ms-interpolation-mode:bicubic;
        }
        p {
            display:block;
            margin:13px 0;
        }
        a {
            color:white !important;
            text-decoration: none !important;
        }
    </style>
    <!--[if mso]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <!--[if lte mso 11]>
    <style type="text/css">
        .outlook-group-fix { width:100% !important; }
    </style>
    <![endif]-->
    <!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
    <style type="text/css">
        @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);
    </style>
    <!--<![endif]-->
    <style type="text/css">
        @media only screen and (min-width:480px) {
            .mj-column-per-100 {
                width:100% !important;
                max-width: 100%;
            }
            .mj-column-per-50 {
                width:50% !important;
                max-width: 50%;
            }
        }
    </style>
    <style type="text/css">
    </style>
</head>
<body style="background-color:#dedede;">
<div
    style="background-color:#dedede;"
>
    <!-- Company Header -->
    <!--[if mso | IE]>
    <table
        align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
    >
        <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                <v:rect  style="width:600px;" xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false">
                    <v:fill  origin="0.5, 0" position="0.5, 0" src="https://cdn.webnoviny.sk/sites/32/2019/10/c-marek-hajkovsky-foto-hotel-lomnica_1-676x362.jpg" color="#f0f0f0" type="tile" />
                    <v:textbox style="mso-fit-shape-to-text:true" inset="0,0,0,0">
    <![endif]-->
    <div  style="background:#f0f0f0 url(https://cdn.webnoviny.sk/sites/32/2019/10/c-marek-hajkovsky-foto-hotel-lomnica_1-676x362.jpg) top center / cover no-repeat;margin:0px auto;max-width:600px;">
        <div  style="line-height:0;font-size:0;">
            <table
                align="center" background="https://cdn.webnoviny.sk/sites/32/2019/10/c-marek-hajkovsky-foto-hotel-lomnica_1-676x362.jpg" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#f0f0f0 url(https://cdn.webnoviny.sk/sites/32/2019/10/c-marek-hajkovsky-foto-hotel-lomnica_1-676x362.jpg) top center / cover no-repeat;width:100%;"
            >
                <tbody>
                <tr>
                    <td
                        style="border:solid rgba(13,12,12,0);direction:ltr;font-size:0px;padding:20px 0;padding-bottom:100px;padding-left:0;padding-right:0;padding-top:100px;text-align:center;"
                    >
                        <!--[if mso | IE]>
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td
                                    class="" style="vertical-align:top;width:600px;"
                                >
                        <![endif]-->
                        <div
                            class="mj-column-per-100 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                        >
                            <table
                                border="0" cellpadding="0" cellspacing="0" role="presentation" style="background-color:rgba(0,0,0,0);vertical-align:top;" width="100%"
                            >
                                <tr>
                                    <td
                                        align="center" style="background:rgba(0,0,0,0);font-size:0px;padding:10px 25px;word-break:break-word;"
                                    >
                                        <div
                                            style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:center;color:#000000;"
                                        >
                                            <span style="color:#ffffff;"><span style="font-size:48px;">Bonne Vie</span></span>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!--[if mso | IE]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <!--[if mso | IE]>
    </v:textbox>
    </v:rect>
    </td>
    </tr>
    </table>
    <![endif]-->
    <!-- Image Header -->
    <!-- Intro text -->
    <!-- Side image -->
    <!--[if mso | IE]>
    <table
        align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
    >
        <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
    <![endif]-->
    <div  style="background:#5e1212;background-color:#5e1212;margin:0px auto;max-width:600px;">
        <table
            align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#5e1212;background-color:#5e1212;width:100%;"
        >
            <tbody>
            <tr>
                <td
                    style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;"
                >
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td
                                class="" style="vertical-align:top;width:600px;"
                            >
                    <![endif]-->
                    <div
                        class="mj-column-per-100 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                    >
                        <table
                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"
                        >
                            <tr>
                                <td
                                    align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                >
                                    <div
                                        style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:18px;line-height:1;text-align:center;color:#000000;"
                                    >
                                        <font color="#ffffff">OBJEDNÁVKA
                                        </font>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td>
                    </tr>
                    </table>
                    <![endif]-->
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <!--[if mso | IE]>
    </td>
    </tr>
    </table>
    <table
        align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
    >
        <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
    <![endif]-->
    <div  style="background:#5e1212;background-color:#5e1212;margin:0px auto;max-width:600px;">
        <table
            align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#5e1212;background-color:#5e1212;width:100%;"
        >
            <tbody>
                @if(isset($reservation['basket']) && !empty($reservation['basket']))
                    @foreach($reservation['basket'] as $key => $item)
                        <tr>
                            <td
                                align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                            >
                                <div
                                    style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;"
                                >
                                    <font color="#ffffff">{{$item['name']}}
                                    </font>
                                </div>
                                <div
                                    style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:right;color:#000000;"
                                >
                                    <font color="#ffffff">
                                        @php
                                            $price = '';
                                            if(isset($item['points']) && !empty($item['points'])){
                                                $price .= $item['points'].' bodov';
                                            }
                                            if(isset($item['price']) && !empty($item['price'])){
                                                if(isset($item['points']) && !empty($item['points'])){
                                                    $price .= ' + '.$item['price'].' €';
                                                }
                                                else{
                                                    $price .= $item['price'].' €';
                                                }
                                            }
                                        @endphp
                                        {{$price}}
                                    </font>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
    <!--[if mso | IE]>
    </td>
    </tr>
    </table>
    <table
        align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
    >
        <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
    <![endif]-->
    <div  style="background:#5e1212;background-color:#5e1212;margin:0px auto;max-width:600px;">
        <table
            align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#5e1212;background-color:#5e1212;width:100%;"
        >
            <tbody>
            <tr>
                <td
                    style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;"
                >
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td
                                class="" style="vertical-align:top;width:600px;"
                            >
                    <![endif]-->
                    <div
                        class="mj-column-per-100 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                    >
                        <table
                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"
                        >
                            <tr>
                                <td
                                    align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                >
                                    <div
                                        style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:15px;line-height:1;text-align:center;color:#000000;"
                                    >
                                        <span style="color:#ffffff;">Spolu:</span>
                                        <div>
                                            <span style="color:#ffffff;">
                                                @php
                                                    $wholePrice = '';
                                                @endphp
                                                @if(isset($reservation['basketPrice']) && !empty($reservation['basketPrice']))
                                                    @if($reservation['basketPrice']['points']>0)
                                                        <?php $wholePrice .= $reservation['basketPrice']['points'].' bodov '?>
                                                    @endif
                                                    @if($reservation['basketPrice']['price']>0)
                                                        @if($reservation['basketPrice']['points']>0)
                                                                <?php $wholePrice .= '+ '.$reservation['basketPrice']['price'].' €'?>
                                                        @else
                                                                <?php $wholePrice .= $reservation['basketPrice']['price'].' €'?>
                                                        @endif
                                                    @endif
                                                @endif
                                                {{$wholePrice}}
                                            </span>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td>
                    </tr>
                    </table>
                    <![endif]-->
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <!--[if mso | IE]>
    </td>
    </tr>
    </table>
    <table
        align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
    >
        <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
    <![endif]-->
    <div  style="background:#424242;background-color:#424242;margin:0px auto;max-width:600px;">
        <table
            align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#424242;background-color:#424242;width:100%;"
        >
            <tbody>
            <tr>
                <td
                    style="border:rgba(0,0,0,0) rgba(0,0,0,0);direction:ltr;font-size:0px;padding:20px 0;text-align:center;"
                >
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td
                                class="" style="vertical-align:top;width:300px;"
                            >
                    <![endif]-->
                    <div
                        class="mj-column-per-50 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                    >
                        <table
                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="border:0 solid rgba(0,0,0,0);vertical-align:top;" width="100%"
                        >
                            <tr>
                                <td
                                    align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                >
                                    <div
                                        style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:15px;line-height:1;text-align:left;color:#000000;"
                                    >
                                        <span style="color:#ffffff;">INFORMÁCIE O REZERVÁCII</span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td
                                    align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                >
                                    <div
                                        style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;"
                                    >
                                        <span style="color:#ffffff;">{{$reservation['customer']['first_name']}}&nbsp;{{$reservation['customer']['last_name']}}</span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td
                                    align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                >
                                    <div
                                        style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;"
                                    >
                                        <span style="color:#ffffff;"><span style="font-size:10px;">Vaše telefónne číslo: {{$reservation['customer']['phone']}}</span></span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td
                                    align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                >
                                    <div
                                        style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;"
                                    >
                                        <span style="color:#ffffff;"><span style="font-size:10px;">Váš email : {{$reservation['customer']['email']}}</span></span>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td>
                    <td
                        class="" style="vertical-align:top;width:300px;"
                    >
                    <![endif]-->
                    <div
                        class="mj-column-per-50 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                    >
                        <table
                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="border:0 solid rgba(0,0,0,0);vertical-align:top;" width="100%"
                        >
                            <tr>
                                <td
                                    align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                >
                                    <div
                                        style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:15px;line-height:1;text-align:left;text-decoration:none;color:#000000;"
                                    >
                                        <span style="color:#ffffff;">INFORMÁCIE O BONNE VIE</span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td
                                    align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                >
                                    <div
                                        style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;"
                                    >
                                        <span style="color:#ffffff;">{{$reservation['loyalty']['name']}}</span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td
                                    align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                >
                                    <div
                                        style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;"
                                    >
                                        <span style="color:#ffffff;"><span style="font-size:10px;">Adresa: {{$reservation['loyalty']['adress']}}</span></span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td
                                    align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                >
                                    <div
                                        style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;"
                                    >
                                        <span style="color:#ffffff;"><span style="font-size:10px;">Mesto : {{$reservation['loyalty']['city']}}</span></span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td
                                    align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                >
                                    <div
                                        style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;"
                                    >
                                        <span style="color:#ffffff;"><span style="font-size:10px;">Email : {{$reservation['loyalty']['email']}}</span></span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td
                                    align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                >
                                    <div
                                        style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;"
                                    >
                                        <span style="color:#ffffff;"><span style="font-size:10px;">Telefónne číslo: {{$reservation['loyalty']['phone']}}</span></span>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td>
                    </tr>
                    </table>
                    <![endif]-->
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <!--[if mso | IE]>
    </td>
    </tr>
    </table>
    <![endif]-->
    <!-- Icons -->
    <!-- Footer -->
    <!--[if mso | IE]>
    <table
        align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
    >
        <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
    <![endif]-->
    <div  style="background:#f8f8f8;background-color:#f8f8f8;margin:0px auto;max-width:600px;">
        <table
            align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#f8f8f8;background-color:#f8f8f8;width:100%;"
        >
            <tbody>
            <tr>
                <td
                    style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;"
                >
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td
                                class="" style="vertical-align:top;width:600px;"
                            >
                    <![endif]-->
                    <div
                        class="mj-column-per-100 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                    >
                        <table
                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"
                        >
                            <tr>
                                <td
                                    align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                >
                                    <!--[if mso | IE]>
                                    <table
                                        align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
                                    >
                                        <tr>
                                            <td>
                                    <![endif]-->
                                    <table
                                        align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;"
                                    >
                                        <tr
                                        >
                                            <td  style="padding:4px;">
                                                <table
                                                    border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#3b5998;border-radius:3px;width:30px;"
                                                >
                                                    <tr>
                                                        <td  style="font-size:0;height:30px;vertical-align:middle;width:30px;">
                                                            <a  href="https://www.facebook.com/sharer/sharer.php?u=http://softsolutions.sk/" target="_blank">
                                                                <img
                                                                    height="30" src="https://www.mailjet.com/images/theme/v1/icons/ico-social/facebook.png" style="border-radius:3px;display:block;" width="30"
                                                                />
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!--[if mso | IE]>
                                    </td>
                                    <td>
                                    <![endif]-->
                                    <table
                                        align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;"
                                    >
                                        <tr
                                        >
                                            <td  style="padding:4px;">
                                                <table
                                                    border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#3f729b;border-radius:3px;width:30px;"
                                                >
                                                    <tr>
                                                        <td  style="font-size:0;height:30px;vertical-align:middle;width:30px;">
                                                            <a  href="http://softsolutions.sk/" target="_blank">
                                                                <img
                                                                    height="30" src="https://www.mailjet.com/images/theme/v1/icons/ico-social/instagram.png" style="border-radius:3px;display:block;" width="30"
                                                                />
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!--[if mso | IE]>
                                    </td>
                                    <td>
                                    <![endif]-->
                                    <table
                                        align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;"
                                    >
                                        <tr
                                        >
                                            <td  style="padding:4px;">
                                                <table
                                                    border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#4BADE9;border-radius:3px;width:30px;"
                                                >
                                                    <tr>
                                                        <td  style="font-size:0;height:30px;vertical-align:middle;width:30px;">
                                                            <a  href="http://softsolutions.sk/" target="_blank">
                                                                <img
                                                                    height="30" src="https://www.mailjet.com/images/theme/v1/icons/ico-social/web.png" style="border-radius:3px;display:block;" width="30"
                                                                />
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!--[if mso | IE]>
                                    </td>
                                    <td>
                                    <![endif]-->
                                    <table
                                        align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="float:none;display:inline-table;"
                                    >
                                        <tr
                                        >
                                            <td  style="padding:4px;">
                                                <table
                                                    border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#EB3323;border-radius:3px;width:30px;"
                                                >
                                                    <tr>
                                                        <td  style="font-size:0;height:30px;vertical-align:middle;width:30px;">
                                                            <a  href="http://softsolutions.sk/" target="_blank">
                                                                <img
                                                                    height="30" src="https://www.mailjet.com/images/theme/v1/icons/ico-social/youtube.png" style="border-radius:3px;display:block;" width="30"
                                                                />
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!--[if mso | IE]>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td>
                    </tr>
                    </table>
                    <![endif]-->
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <!--[if mso | IE]>
    </td>
    </tr>
    </table>
    <table
        align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
    >
        <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
    <![endif]-->
    <div  style="background:#5e1212;background-color:#5e1212;margin:0px auto;max-width:600px;">
        <table
            align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#5e1212;background-color:#5e1212;width:100%;"
        >
            <tbody>
            <tr>
                <td
                    style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;"
                >
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td
                                class="" style="vertical-align:top;width:600px;"
                            >
                    <![endif]-->
                    <div
                        class="mj-column-per-100 outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                    >
                        <table
                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"
                        >
                            <tr>
                                <td
                                    align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;"
                                >
                                    <div
                                        style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:center;color:#000000;"
                                    >
                                        <span style="font-size:12px;"><span style="color:#ffffff;">Hotel Lomnica&nbsp; 2020</span></span>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td>
                    </tr>
                    </table>
                    <![endif]-->
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <!--[if mso | IE]>
    </td>
    </tr>
    </table>
    <![endif]-->
    <!-- Footer -->
</div>
</body>
</html>
