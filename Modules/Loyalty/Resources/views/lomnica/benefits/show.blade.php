@extends('loyalty::'.$template.'.layouts.master')

@section('content')
    <div id="itemPage">
        <div class="container">
            <div class="row" style="padding-bottom:200px;">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6 w3-animate-left">
                            <h1 class="color fs72 title">{{$benefit['title']}}</h1>
                            <p class="ptext ">
                                {!! $benefit['long_description'] ? strip_tags($benefit['long_description']) : '' !!}
                            </p>
                        </div>
                        <div class="col-md-6 w3-animate-right">
                            <div class="experienceWrapperIn" style="{{$benefit['image'] ? 'background-image:url("'.asset('images/benefits/'.$benefit['image']).'");' : ''}}"></div>
                            <div class="mt20"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
