@extends('loyalty::'.$template.'.layouts.master')

@section('title')
    Bonne Vie - domov
@endsection

@section('content')
    @if(isset($header) && !empty($header))
        @if($header['show_wave'] == '0')
            <style>
                #header:after{
                    display:none;
                }
            </style>
        @endif
        @if(isset($header['images']) && !empty($header['images']))
            @php
                $images = array();
            @endphp
            @foreach($header['images'] as $img)
                @php
                    $images[] = $img;
                @endphp
            @endforeach
            @if(count($header['images'])>1)
                <style>
                    .carousel-item{
                        height: 900px !important;
                        background-repeat: no-repeat;
                        background-position:center;
                        -webkit-background-size: cover;
                        -moz-background-size: cover;
                        -o-background-size: cover;
                        background-size: cover;
                    }

                    .crosselimgcustom{
                        width:0 !important;
                        height:0 !important;
                    }
                </style>
                <div id="header">
                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" style="max-height: 900px; !important">
                        <div class="carousel-inner">
                            @foreach($images as $key => $img)
                                <div class="carousel-item {{$key == '0' ? 'active' : ''}}" style="background-image: url('{{$crm_path.'images/header/'.$img}}')">
                                    <img class="d-block w-100 crosselimgcustom" src="{{asset('images/header/'.$img)}}" alt="First slide">
                                </div>
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                        <div class="self-container" style="position: absolute;top: 10%;left: 10%;">
                            <div class="row">
                                <div class="col-xl-4 col-lg-12 col-md-12 text-center-lg-12">
                                    <h1 class="color fs72 {{!is_null($header['animation_main_title']) ? 'w3-animate-'.$header['animation_main_title'] : ''}}">{{$header['main_titles'][$currentLang] ? $header['main_titles'][$currentLang] : ''}}</h1>
                                    <p class="ptext {{!is_null($header['animation_main_description']) ? 'w3-animate-'.$header['animation_main_description'] : ''}}">
                                        {!! isset($header['main_descriptions'][$currentLang]) && !empty($header['main_descriptions'][$currentLang]) ? strip_tags($header['main_descriptions'][$currentLang]) : '' !!}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <style>
                    #header{
                        background-image: url("{{asset('images/header/'.$images[0])}}");
                        background-size:cover;
                        background-position: center;
                        background-repeat:no-repeat;
                    }
                </style>

                <div id="header">
                    <div class="self-container">
                        <div class="row">
                            <div class="col-xl-4 col-lg-12 col-md-12 text-center-lg-12">
                                <h1 class="color fs72 {{!is_null($header['animation_main_title']) ? 'w3-animate-'.$header['animation_main_title'] : ''}}">{{ isset($header['main_titles'][$currentLang]) && !empty($header['main_titles'][$currentLang]) ? $header['main_titles'][$currentLang] : '' }}</h1>
                                <p class="ptext {{!is_null($header['animation_main_description']) ? 'w3-animate-'.$header['animation_main_description'] : ''}}">
                                    {!! isset($header['main_descriptions'][$currentLang]) && !empty($header['main_descriptions'][$currentLang]) ? strip_tags($header['main_descriptions'][$currentLang]) : '' !!}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @else
            <div id="header">
                <div class="self-container">
                    <div class="row">
                        <div class="col-xl-4 col-lg-12 col-md-12 text-center-lg-12">
                            <h1 class="color fs72 {{!is_null($header['animation_main_title']) ? 'w3-animate-'.$header['animation_main_title'] : ''}}">{{$header['main_titles'][$currentLang] ? $header['main_titles'][$currentLang] : ''}}</h1>
                            <p class="ptext {{!is_null($header['animation_main_description']) ? 'w3-animate-'.$header['animation_main_description'] : ''}}">
                                {!! isset($header['main_descriptions'][$currentLang]) && !empty($header['main_descriptions'][$currentLang]) ? strip_tags($header['main_descriptions'][$currentLang]) : '' !!}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        @endif

    @else
    @endif



    @if(isset($category) && !empty($category))
        @if($category['show_category'] == '1')
            <div id="categories">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-4 col-lg-12 col-md-12 mt100">
                            <h2 class="main-h2 color {{isset($category['animation_category_title']) && !empty($category['animation_category_title']) ? 'w3-animate-'.$category['animation_category_title'] : ''}}">
                                {{isset($category['category_titles'][$currentLang]) && !empty($category['category_titles'][$currentLang]) ? $category['category_titles'][$currentLang] : ''}}
                            </h2>
                            <p class="ptext color999 {{isset($category['animation_category_description']) && !empty($category['animation_category_description']) ? 'w3-animate-'.$category['animation_category_description'] : ''}}">
                                {!! isset($category['category_descriptions'][$currentLang]) && !empty($category['category_descriptions'][$currentLang]) ? strip_tags($category['category_descriptions'][$currentLang]) : '' !!}
                            </p>
                        </div>
                        <div class="col-xl-8 col-lg-12 col-md-12">
                            <div class="row">
                                @if(isset($category['allCategories']) && !empty($category['allCategories']))
                                    @foreach($category['allCategories'] as $categ)
                                        @if(isset($categ['title'][$currentLang]) && !empty($categ['title'][$currentLang]))
                                            <div class="col-lg-6 col-xl-6 col-md-6 col-sm-12">
                                                <a href="{{route('category',[$categ['slug'][$currentLang]])}}">
                                                    <div class="categoryWrapper {{isset($category['animation_category']) && !empty($category['animation_category']) ? 'w3-animate-'.$category['animation_category'] : ''}}">
                                                        <div class="categoryContent" style="{{isset($categ['image']) && !empty($categ['image']) ? 'background-image:url("'.asset('images/categories/'.$categ['image']).'");' : ''}}">
                                                            <div class="categoryContentAbsolute">
                                                                <h4 class="main-h4 text-white">
                                                                    {{isset($categ['title'][$currentLang]) && !empty($categ['title'][$currentLang]) ? $categ['title'][$currentLang] : ''}}
                                                                </h4>
                                                                <p class="ptext text-white mh60 mh60oh fs14">
                                                                    {{isset($categ['short_description'][$currentLang]) && !empty($categ['short_description'][$currentLang]) ? $categ['short_description'][$currentLang] : ''}}
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endif



    @if(isset($advantage) && !empty($advantage))
        @if(isset($advantage['show_advantage']) && !empty($advantage['show_advantage']) && $advantage['show_advantage'] == '1')
            @if(isset($advantage['advantage_titles'][$currentLang]) && !empty($advantage['advantage_titles'][$currentLang]))
                <div id="benefits">
                    <div class="container">
                        <div class="row" style="padding-top:100px;padding-bottom:100px;">
                            <div class="col-md-12 col-xl-6 col-lg-6">
                                <h2 class="main-h2 {{isset($advantage['animation_advantage_title']) && !empty($advantage['animation_advantage_title']) ? 'w3-animate-'.$advantage['animation_advantage_title'] : ''}} w100 text-center"> {{$advantage['advantage_titles'][$currentLang]}}</h2>
                                <div class="benefitsImage w3-animate-left">
                                    <div class="imageWrapper" style="{{isset($advantage['advantage_image']) && !empty($advantage['advantage_image']) ? 'background-image:url("'.asset('images/benefits/'.$advantage['advantage_image']).'");background-size:cover;background-position:center;background-repeat:no-repeat;margin:0 auto;' : ''}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-12">
                                <div class="row">
                                    @php
                                        $counter = 1;
                                    @endphp
                                    @if(isset($advantage['allAdvantages']) && !empty($advantage['allAdvantages']))
                                        @foreach($advantage['allAdvantages'] as $adv)
                                            @if(isset($adv['title'][$currentLang]) && !empty($adv['title'][$currentLang]))
                                                <a class="benefitAelement" href="{{route('benefit',[$adv['slug']])}}">
                                                    <div class="col-md-12 h130 mt30 {{isset($advantage['animation_advantage']) && !empty($advantage['animation_advantage']) ? 'w3-animate-'.$advantage['animation_advantage'] : ''}}">
                                                        <div class="row">
                                                            <div class="col-md-2 col-sm-2 col-xs-2">
                                                                <h2 class="main-h2 color333">{{$counter}}.</h2>
                                                            </div>
                                                            <div class="col-md-10 col-sm-10 col-xs-10">
                                                                <h4 class="main-h4 color333 pt0">{{$adv['title'][$currentLang]}}</h4>
                                                                <p class="ptext fs16 color999">{{$adv['short_description'][$currentLang]}}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                                @php
                                                    $counter += 1;
                                                @endphp
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endif
    @endif


    @if(isset($experience) && !empty($experience) && $experience['show_experience'] == '1' && isset($experience['experience_titles'][$currentLang]) && !empty($experience['experience_titles'][$currentLang]))
        <div id="experiences">
            <div class="container">
                <div class="row" style="padding-top:100px;padding-bottom:200px;">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <h2 class="main-h2 {{isset($experience['animation_experience_title']) && !empty($experience['animation_experience_title'] ? 'w3-animate-'.$experience['animation_experience_title'] : '')}}">{{$experience['experience_titles'][$currentLang]}}</h2>
                    </div>
                    <div class="col-md-12 col-lg-12 col-xl-12 mt50 w3-animate-bottom">
                        <div class="row">
                            @if(isset($experience['experiences']) && !empty($experience['experiences']))
                                @foreach($experience['experiences'] as $experience)
                                    @if(isset($experience['lang'][$currentLang]['name']) && !empty($experience['lang'][$currentLang]['name']))
                                        <div class="col-md-12 col-xl-6 col-lg-6">
                                            <a href="{{route('experience',[$experience['lang'][$currentLang]['slug']])}}">
                                                <div class="experienceWrapper">
                                                    <div class="experienceItem" style="{{isset($experience['image']) && !empty($experience['image']) ? 'background-image:url("'.$experience['image']['medium'].'")' : ''}}">
                                                        <div class="experienceItemContent">
                                                            <span class="itemCategory">{{$experience['categories']!='' ? rtrim($experience['categories'],',') : ''}}</span>
                                                            <div class="itemInsideWrapper">
                                                                <h5 class="itemh5 text-white">{{$experience['lang'][$currentLang]['name']}}</h5>
                                                                <span class="itemDate color999">{{$experience['visibleFrom'] .' - '. $experience['visibleTo']}}</span>
                                                                <span class="itemPrice colo333">{{$experience['price']}} {{isset($experience['currency']) && !empty($experience['currency']) ? $experience['currency'] : '€'}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    @endif
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>

            </div>
        </div>
    @endif
@endsection
