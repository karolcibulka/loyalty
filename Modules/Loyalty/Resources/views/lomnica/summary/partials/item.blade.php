<div class="row discountWrapper w3-animate-right mr0 mh80">
    <div class="col-lg-2 col-md-12 pr0 pl0 col-xl-2">
        <div class="discountImageWrapper mh80" style="{{isset($image) && !empty($image) ? 'background-image:url("'.asset('images/medium/'.$image).'");background-size:cover;background-repeat: no-repeat;background-position:center;' : ''}}">

        </div>
    </div>
    <div class="col-lg-10 col-md-12 col-xl-10 col-sm-12 col-xs-12">
        <div class="row">
            <div class="col-xl-7 col-lg-6 col-md-6  col-sm-6 col-xs-6 mh80">
                <div class="discountTextWrapper p4">
                    <h3 class="loyalty-h3">{!! $name !!} </h3>
                    <span class="loyalty-discount-date">
                       {!! date('d.m.Y',strtotime($visible_from)) . ' - ' . date('d.m.Y',strtotime($visible_to)) !!}
                    </span>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-xl-5 col-sm-6 col-xs-6 ">
                <div class="discount-price-wrapper p0">
                    <div class="pt20"></div>
                    <div class="row">
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-6">
                            @if(isset($points) && !empty($points))
                                <span class="discount-price">{!! '<span class="discount-price-points">'.$points.'</span> bodov' !!}</span>
                            @endif
                            @if(isset($price) && !empty($price))
                                <span class="discount-price">+ {!! '<span class="discount-price-price">'.$price.'</span>' !!} {{!empty($currency) ? $currency : '€'}}</span>
                            @endif
                            <div class="pt10"></div>
                            @if(isset($oldPrice) && !empty($oldPrice))
                                <span class="discount-old-price">Cena na pokladni: <span class="line-throught">{!! $old_price !!} {!! mb_strtoupper($currency) !!}</span></span>
                            @endif
                        </div>
                    </div>
                    <div class="pt30"></div>
                </div>
            </div>
        </div>
    </div>
</div>
