@extends('loyalty::'.$template.'.layouts.master')

<style>
    body{
        background-color:#fff !important;
    }
</style>

@section('content')
    <div class="container" id="contentWrapper">
        <div class="row"  style="margin-bottom:100px;">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="main-h2 text-center">
                            Sumár objednávky
                        </h2>
                    </div>
                </div>

                <div class="row discountWrapper w3-animate-right mr0 mh80">
                    <div class="col-md-12 p10 text-center">
                        <h4 class="fwb">{{Auth::guard('customer')->user()->first_name .' '.Auth::guard('customer')->user()->last_name}}</h4>
                    </div>
                </div>

                @if(isset($basket) && !empty($basket))
                    @foreach($basket as $item_id => $item)
                        @include('loyalty::'.$template.'.summary.partials.item',$item)
                    @endforeach
                @endif


                <div class="priceCheckerBasket">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 col-sm-4 col-xs-4">
                                <button class="btn btn-red btn-back btn-custom br0 color-white fs12 w100" style="width:30%;">
                                    <span class="button-text">Späť</span>
                                    <span class="border">
                                        <span></span><span></span><span></span><span></span>
                                    </span>
                                </button>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-8">
                                <div class="row">
                                    <div class="col-md-6 col-xs-6 col-sm-6">
                                        <span class="finalPriceBasket">  Spolu: <br><span class="finalPriceBasketNum">{{$basketPriceSummary}}</span></span>
                                    </div>
                                    <div class="col-md-6 col-xs-6 col-sm-6">
                                        <button class="btn btn-custom background br0 color-white fs12 w100 btn-red btn-continue">
                                            <span class="button-text">Rezervovať</span>
                                            <span class="border">
                                                <span></span>
                                                <span></span>
                                                <span></span>
                                                <span></span>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('.btn-back').on('click',function(){
            var btn = $(this);
            btn.attr('disabled',true);
            btn.find('.button-text').html('<i class="rotating fa fa-spinner"></i>');
           window.location.href = "{{route('basket')}}";
        });

        $('.btn-continue').on('click',function(){
            var btn = $(this);
            btn.find('.button-text').html('<i class="rotating fa fa-spinner"></i>');
            btn.attr('disabled',true);
            $.ajax({
                type:"post",
                url:'{{route('doReservation')}}',
                data:{_token:'{{csrf_token()}}'},
                dataType:'json',
                success:function(data){
                    if(data.status === '1'){
                       $('#contentWrapper').html(data.view);
                       $('.basket-icon').attr('data-content',data.basketCount)
                    }
                    else{
                        btn.attr('disabled',false);
                        btn.find('.button-text').html('Rezervovať');
                    }
                }
            })
        });
    </script>
@endsection
