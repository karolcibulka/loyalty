<div class="row discountWrapper p20 w3-animate-right mr0">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-2">
                <span class="textInBox">{{$date}}</span>
            </div>
            <div class="col-md-4">
                <span class="textInBox">Objednávka č. {{$number}}</span>
            </div>
            <div class="col-md-4">
                <span class="textInBox">{!! $points .' bodov' !!} {!!'+ ' . $price . ' ' .$currency!!}</span>
            </div>
            <div class="col-md-2">
               <button class="btn br0 btn-red btn-detail" data-id="{{$id}}" data-type="history">Detail
                   <span class="border"><span></span><span></span><span></span><span></span></span>
               </button>
            </div>
        </div>
    </div>
</div>
