<div class="row discountWrapper p20 w3-animate-right mr0">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-2">
                <span class="textInBox">{{$date}}</span>
            </div>
            <div class="col-md-4">
                <span class="textInBox">{{!empty($number) && !is_null($number) ? $reason : 'Systém'}}</span>
            </div>
            <div class="col-md-4">
                @if($operator == '+')
                    <span class="textInBox green ">{!! $operator. ' '. $points . 'bodov'!!}</span>
                @else
                    <span class="textInBox red ">{!! $operator. ' '. $points .' bodov' !!} </span>
                @endif
            </div>
            <div class="col-md-2">
                <button class="btn br0 btn-red btn-detail" data-id="{{$id}}" data-type="transaction">Detail
                    <span class="border"><span></span><span></span><span></span><span></span></span>
                </button>
            </div>
        </div>
    </div>
</div>
