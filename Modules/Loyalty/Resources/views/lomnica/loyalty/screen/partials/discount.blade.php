<div class="row discountWrapper w3-animate-right mr0 op1">
    <div class="col-lg-2 col-md-12 pr0 pl0 col-xl-2">
        <div class="discountImageWrapper mh250" style="{{isset($img) && !empty($img) ? 'background-image:url("'.$img.'");background-size:cover;background-repeat: no-repeat;background-position:center;' : ''}}">

        </div>
    </div>
    <div class="col-lg-10 col-md-12 col-xl-10">
        <div class="row">
            <div class="col-xl-7 col-lg-12 col-md-12">
                <div class="discountTextWrapper mh250">
                    <h3 class="loyalty-h3">
                        {!! '<span class="discountWrapperName">'.$name.'</span>' !!}
                    </h3>
                    <span class="loyalty-discount-date">
                       {!! $date !!}
                    </span>
                    <p class="loyalty-discount-text">
                        {!! $text !!}
                    </p>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-xl-5">
                <div class="discount-price-wrapper">
                    <div class="pt20"></div>
                    @if(isset($points) && !empty($points))
                        <span class="discount-price">{!! '<span class="discount-price-points">'.$points.'</span>'.' bodov' !!}</span>
                    @endif
                    @if(isset($price) && !empty($price))
                        <span class="discount-price">+ {!! '<span class="discount-price-price">'.$price.'</span>' . ' ' .$currency !!}</span>
                    @endif
                    <div class="pt10"></div>
                    @if(isset($oldPrice) && !empty($oldPrice))
                        <span class="discount-old-price">Cena na pokladni: <span class="line-throught">{!! $oldPrice !!} {!! mb_strtoupper($currency) !!}</span></span>
                    @endif
                    <div class="pt30"></div>
                    <div class="text-center notEnoughtPoints" style="display:none;">
                        <small class="color m-auto">Nemáš dostatok bodov !</small>
                    </div>
                    <button class="btn btn-custom background br0 color-white fs12 w100 m-auto {{in_array('item_'.$id,$basketIDs) ? 'remove-from-basket' : 'add-to-basket'}} btn-red" data-id="{{$id}}">
                        <span class="button-text">{!! in_array('item_'.$id,$basketIDs) ? 'Odobrať z košíka' : 'Pridať do košíka' !!}</span>
                        <span class="border">
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
