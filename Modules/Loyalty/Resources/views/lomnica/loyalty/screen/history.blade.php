<h2 class="main-h2 pb30">
    @if($type == 'history')
        História nákupov
    @elseif($type == 'transaction')
        História bodových transakcií
    @endif
</h2>

<style>
    ul.pagination{
        margin-top:30px;
    }
</style>

@if(isset($histories) && !empty($histories))
    @foreach($histories as $year => $months)
        @if(isset($months) && !empty($months))
            @foreach($months as $month_ => $month)
                <h6 class="main-h6">
                    {{__('global.month.'.strtolower($month_)) . ' ' . $year}}
                </h6>
                @if(isset($month) && !empty($month))
                    @foreach($month as $item)
                        @if($type == 'transaction')
                            @include('loyalty::'.$template.'.loyalty.screen.partials.transactionItem',$item)
                        @elseif($type == 'history')
                            @include('loyalty::'.$template.'.loyalty.screen.partials.historyItem',$item)
                        @endif
                    @endforeach
                @endif
            @endforeach
        @endif
    @endforeach
    {{$historiesArray->links()}}
@endif

<script>
    $('.btn-detail').on('click',function(){
       var btn = $(this);
       var data = btn.data();
       var modal = $('#detailModal');
       $.ajax({
           type:"post",
           url:'{{route('historyDetail')}}',
           dataType:'json',
           data:{type:data.type,id:data.id,_token:'{{csrf_token()}}'},
           success:function(data){
               if(data.status === '1'){
                   modal.find('#detailModalLabel').html(data.title);
                   modal.find('.modal-body').html(data.view);
                   modal.modal();
               }
           }
       });
    });
</script>
