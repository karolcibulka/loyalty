<h2 class="main-h2 pb30">
    Aktuálne zľavy
</h2>

@if(isset($discounts) && !empty($discounts))
    @foreach($discounts as $discount)
        @include('loyalty::'.$template.'.loyalty.screen.partials.discount',$discount)
    @endforeach
@endif

<script>

            $(document).on('click','.add-to-basket',function(event){
               event.preventDefault();
               event.stopPropagation();
               var id = $(this).data().id;
               var name = '<strong>'+$(this).closest('.discountWrapper').find('.discountWrapperName').html()+'</strong>';
               $(document).find('.discountWrapper').find('button').attr('disabled',true);
               var button = $(this);
               button.find('.button-text').html('<i class="rotating fa fa-spinner"></i>');
               button.attr('disabled',true);
               $.ajax({
                   type:"post",
                   url:'{{route('addItemToBasket')}}',
                   dataType:"json",
                   data:{id:id,_token:'{{csrf_token()}}'},
                   success:function(data){
                       if(data.status === '1'){
                           button.removeClass('add-to-basket');
                           button.addClass('remove-from-basket');
                           button.find('.button-text').html('ODOBRAŤ Z KOŠÍKA');
                           button.attr('disabled',false);
                           rerenderOffers(data.basketPrices.points);
                           $('.basket-icon').attr('data-content',data.basketCount);
                           Swal.fire({
                               title: '',
                               icon: 'success',
                               html:'Do košíka ste si pridali:<br>' +name,
                               confirmButtonText: 'Prejsť do košíka',
                               cancelButtonText: 'Späť ku nákupu',
                               confirmButtonColor:'#5e1212',
                               cancelButtonColor:'#464646',
                               showCancelButton:true,
                           }).then(function(response){
                               if(response.value === true){
                                   window.location.href = "{{route('basket')}}";
                               }
                           });
                       }
                       else{
                           button.attr('disabled',false);
                           button.find('.button-text').html('PRIDAŤ DO KOŠÍKA');
                           button.siblings('.notEnoughtPoints').show();
                       }
                   }
               });
               return false;
            });

            $(document).on('click','.remove-from-basket',function(event){
                event.preventDefault();
                event.stopPropagation();
                var id = $(this).data().id;
                //$(document).find('.discountWrapper').find('button').attr('disabled',true);
                var name = '<strong>'+$(this).closest('.discountWrapper').find('.discountWrapperName').html()+'</strong>';
                var button = $(this);
                button.find('.button-text').html('<i class="rotating fa fa-spinner"></i>');
                button.attr('disabled',true);
                Swal.fire({
                    title: '',
                    icon: 'warning',
                    html:'Noazaj chcete odobrať z košíka<br>' +name + ' ?',
                    confirmButtonText: 'Pokračovať v nákupe',
                    cancelButtonText: 'Odobrať',
                    confirmButtonColor:'#5e1212',
                    cancelButtonColor:'#464646',
                    showCancelButton:true,
                }).then(function(response){
                    if(response.value === true){
                        button.attr('disabled',false);
                        button.find('.button-text').html('ODOBRAŤ Z KOŠÍKA');
                    }
                    else{
                        $.ajax({
                            type:"post",
                            url:'{{route('removeItemFromBasket')}}',
                            dataType:"json",
                            data:{id:id,_token:'{{csrf_token()}}'},
                            success:function(data){
                                if(data.status === '1'){
                                    button.removeClass('remove-from-basket');
                                    button.addClass('add-to-basket');
                                    button.find('.button-text').html('PRIDAŤ DO KOŠÍKA');
                                    button.attr('disabled',false);
                                    rerenderOffers(data.basketPrices.points);
                                    $('.basket-icon').attr('data-content',data.basketCount);
                                }
                            }
                        });
                    }
                });

                return false;
            });

            var rerenderOffers = function(points){
                points = (+points);
                var objects = $(document).find('.discountWrapper');
                var customerPoints = '{{$customerPoints}}';
                objects.each(function(){
                   var item = $(this);
                   if(item.find('button').hasClass('add-to-basket')){
                       var pointss = item.find('.discount-price-points').html();
                       if(typeof pointss !== "undefined"){
                            var point = (+parseInt(pointss));
                            if(customerPoints >= point + points){
                                if(item.hasClass('op7')){
                                    item.removeClass('op7').addClass('op1');
                                }
                                item.find('button').attr('disabled',false);
                            }
                            else{
                                if(item.hasClass('op1')){
                                    item.removeClass('op1').addClass('op7');
                                }
                                item.find('button').attr('disabled',true);
                            }

                       }
                       else{
                           if(item.hasClass('op7')){
                               item.removeClass('op7').addClass('op1');
                           }
                           item.find('button').attr('disabled',false);
                       }
                   }
                   else{
                       item.find('button').attr('disabled',false);
                   }
                });
            }

            rerenderOffers('<?=$basketPrice['points']?>');

</script>
