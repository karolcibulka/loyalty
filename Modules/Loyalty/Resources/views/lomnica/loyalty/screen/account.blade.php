<h2 class="main-h2 pb30">
    Užívateľské nastavenia
</h2>

<style>
    .form-control{
        height:50px;
        border-radius:0;
    }

    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #5e1212;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
</style>

<form action="{{route('accountEdit')}}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <input type="hidden" name="id" value="{{$user['id']}}">
        <input type="hidden" name="old_profile_picture" value="{{$user['profile_image']}}">
         <input type="hidden" name="removeProfilePicture" class="removeProfilePicture" value="0">
        <div class="col-md-12 m-auto">
            <div class="profilePicture" style="{{isset($user['profile_image']) && !empty($user['profile_image']) ? 'background-image:url("'.asset('profile_pictures/'.$user['profile_image']).'");background-position:center;background-repeat:no-repeat;background-size:cover;' : ''}}"></div>


            <input type="file" name="profile_picture" id="profile_picture" value="{{$user['profile_image']}}" class="form-control" style="display:none;">
            <div class="text-center {{isset($user['profile_image']) && !empty($user['profile_image']) ? '' : 'hidden'}}">
                <button type="button" class="btn background btn-red br0 text-white removeProfilePicture">Zmazať profilový obrázok
                    <span class="border"><span></span><span></span><span></span><span></span></span>
                </button>
            </div>
        </div>
        <div class="col-md-6 mt10">
            <label>Meno</label>
            <input type="text" class="form-control" name="first_name" placeholder="Meno" value="{{$user['first_name']}}">
        </div>
        <div class="col-md-6 mt10">
            <label>Priezvisko</label>
            <input type="text" class="form-control" disabled placeholder="Priezvisko" value="{{$user['last_name']}}">
        </div>
        <div class="col-md-6 mt10">
            <label>Email</label>
            <input type="text" class="form-control" disabled placeholder="Email" value="{{$user['email']}}">
        </div>
        <div class="col-md-6 mt10">
            <label>Telefónne číslo</label>
            <input type="text" class="form-control"  name="phone" placeholder="Telefónne číslo" value="{{$user['phone']}}">
        </div>
        <legend></legend>
        <div class="col-md-6 mt10">
            <label>Mesto</label>
            <input type="text" class="form-control" name="city" placeholder="Mesto" value="{{$user['city']}}">
        </div>
        <div class="col-md-6 mt10">
            <label>PSČ</label>
            <input type="text" class="form-control" name="zip" placeholder="PSČ" value="{{$user['zip']}}">
        </div>
        <div class="col-md-6 mt10">
            <label>Adresa</label>
            <input type="text" class="form-control" name="adress" placeholder="Adresa" value="{{$user['adress']}}">
        </div>
        <div class="col-md-6 mt10">
            <label>Štát</label>
            <select name="state" class="form-control">
                @if(isset($countries) && !empty($countries))
                    @foreach($countries as $country)
                        <option {{$user['state'] == $country['country_code'] ? 'selected' : ''}} value="{{$country['country_code']}}">{{$country['country_name']}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <legend></legend>
        <div class="col-md-6 mt-10">
            <label>Predvolený jazyk</label>
            <select name="language" class="form-control">
                @foreach($loyalty_languages as $loyalty_language)
                    <option {{$user['language'] == $loyalty_language ? 'selected' : ''}} value="{{$loyalty_language}}">{{__('global.lang.'.$loyalty_language)}}</option>
                @endforeach
            </select>
        </div>
        <legend></legend>
        <div class="col-md-6 mt10">
            <input type="hidden" value="0" name="newsletter">
            <label>
                <label class="switch">
                    <input type="checkbox" value="1" name="newsletter" {{$user['newsletter'] == '1' ? 'checked' : ''}}>
                    <span class="slider round" style="transform: scale(0.7);"></span>
                </label>
                <span style="float: right;padding-top: 5px;">
                    Prihlásený odber
                </span>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 m-auto text-center">
            <button class="btn btn-custom background text-white w80 mt30 br0 btn-red">Uložiť
                <span class="border"><span></span><span></span><span></span><span></span></span>
            </button>
        </div>
    </div>
</form>

<script>

    function readURL(input) {
        if (input[0].files && input[0].files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.profilePicture').css({'backgroundImage':'url("'+e.target.result+'")','backgroundPosition':'center','backgroundRepeat':'no-repeat','backgroundSize':'cover'});
                $('.hidden').removeClass('hidden');
                $('.removeProfilePicture').val('0');
            }

            reader.readAsDataURL(input[0].files[0]);
        }
    }

    $('.profilePicture').on('click',function(){
        $('#profile_picture').click();
    });

    $('#profile_picture').on('input',function(){
        readURL($(this));
    });

    $('.removeProfilePicture').on('click',function(){
        $(this).closest('div').addClass('hidden');
        $('.profilePicture').removeAttr('style');
        $('#profile_picture').val('');
        $('.removeProfilePicture').val('1');
    })
</script>
