<style>
    @media(max-width: 767px){
        .navigationLoyalty{
            height:auto !important;
            padding-bottom:20px;
        }

        .navigationLoyalty ul{
            margin:0 auto;
        }

        .navigationLoyalty ul li{
            text-align:center;
        }
        .loyaltyList{
            padding-left:0;
        }
        .loyaltyLogOut{
            margin: 0 auto;
            text-align: center;
            padding-left: 0;
        }

        #footer{
            bottom: -120px !important;
        }

    }
</style>

<div class="col-md-3 navigationLoyalty" style="background-color:#eee;height:100vh">
    <div class="container pt10 pb-10">
        <div class="row">
            <div class="col-md-12">
                <div class="circle" style="{{isset($profile_picture) && !empty($profile_picture) ? 'background-image:url("'.$profile_picture.'");background-size:cover;background-repeat:no-repeat;background-position:center;' : ''}}"></div>
            </div>
            <div class="col-md-12 text-center mt10">
                {{Auth::guard('customer')->user()->first_name . ' '. Auth::guard('customer')->user()->last_name}}<br>
                ({{isset($customerPoints) && !empty($customerPoints) ? $customerPoints : 0}} b)
            </div>
            <div class="col-md-12">
                <div class="starWrapper m-auto mt20">
                    <div class="row m-auto">
                        <div class="empty_star m-auto"></div>
                    </div>
                    <div class="row m-auto">
                        <div class="row1">
                            <div class="row">
                                <div class="empty_star"></div>
                                <div class="empty_star"></div>
                                <div class="empty_star"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row m-auto">
                        <div class="row2">
                            <div class="row">
                                <div class="empty_star"></div>
                                <div class="empty_star"></div>
                                <div class="empty_star"></div>
                                <div class="empty_star"></div>
                                <div class="empty_star"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w60 m-auto text-center pt10">
                <span class="loyaltyRank color">Chevalier <span class="loyaltyRankSecondRow">de la Bonne Vie</span></span>
            </div>
            <legend></legend>
            <ul class="list-unstyled loyaltyList">
                <a href="{{route('my_account')}}"><li class="loaltyNavigationItem {{Request::segment(1) == 'my_account' ? 'navigationActive' : ''}}">Môj účet</li></a>
                <li class="loaltyNavigationItem">Kupóny</li>
                <a href="{{route('discounts')}}"><li class="loaltyNavigationItem {{Request::segment(1) == 'discounts' ? 'navigationActive' : ''}}">Aktuálne zľavy</li></a>
                <a href="{{route('history')}}"><li class="loaltyNavigationItem {{Request::segment(1) == 'history' ? 'navigationActive' : ''}}">História nákupov</li></a>
                <a href="{{route('history_transaction')}}"><li class="loaltyNavigationItem {{Request::segment(1) == 'history_transaction' ? 'navigationActive' : ''}}">História transakcií</li></a>
                <li class="loaltyNavigationItem">Nastavenia</li>
            </ul>
            <legend></legend>
            <form action="{{route('logout')}}" method="post" id="logOut">
                @csrf
            </form>
            <span class="loaltyNavigationItem loyaltyLogOut">Odhlásiť</span>
        </div>
    </div>
</div>
