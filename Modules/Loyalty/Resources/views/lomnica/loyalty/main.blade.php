@extends('loyalty::'.$template.'.layouts.master')

<style>
    body{
        background-color:#fff !important;
    }
</style>
@section('title')
    {{Auth::guard('customer')->user()->first_name . ' '. Auth::guard('customer')->user()->last_name}}
@endsection

@section('content')
    <div class="container">
        <div class="row" style="padding-bottom:200px;">
            @include('loyalty::'.$template.'.loyalty.partials.navigation')
            <div class="col-md-9">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 contentWrapper">
                            @if(isset($screen) && !empty($screen))
                                @include('loyalty::'.$template.'.loyalty.screen.'.$screen)
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
