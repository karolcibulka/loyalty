@extends('loyalty::'.$template.'.layouts.master')

@section('content')

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

    @if(isset($subpage) && !empty($subpage))
        @if(isset($subpage['structure']) && !empty($subpage['structure']))
            @include('loyalty::'.$template.'.subpage.structures.'.$subpage['structure'])
        @else
            @include('loyalty::'.$template.'.subpage.structures.'.$subpage['template'])
        @endif
    @endif
@endsection
