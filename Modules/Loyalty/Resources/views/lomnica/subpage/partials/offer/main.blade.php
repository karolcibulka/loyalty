<div class="row">
    <div class="col-md-12 mt30 w3-animate-bottom">
        <legend data-content="Ponuky"></legend>
    </div>
    <div class="col-md-12  mt30">
        <div class="row">
            @foreach($content as $c)
                <div class="col-md-12 col-xl-6 col-lg-6">
                    <a href="{{route('experience',[$c['slug']])}}">
                        <div class="experienceWrapper">
                            <div class="experienceItem" style="{{isset($c['image']) && !empty($c['image']) ? 'background-image:url("'.$c['image']['medium'].'")' : ''}}">
                                <div class="experienceItemContent">
                                    <span class="itemCategory">{{$c['categories']!='' ? rtrim($c['categories'],',') : ''}}</span>
                                    <div class="itemInsideWrapper">
                                        <h5 class="itemh5 text-white">{{$c['name']}}</h5>
                                        <span class="itemDate color999">{{$c['date']}}</span>
                                        <span class="itemPrice colo333">{{$c['price']}} {{isset($c['currency']) && !empty($c['currency']) ? $c['currency'] : '€'}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</div>
