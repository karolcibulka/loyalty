<div class="row">
    <div class="col-md-12 mt30 w3-animate-bottom">
        <legend data-content="Kategórie"></legend>
    </div>
    <div class="col-md-12  mt30">
        <div class="row">
            @foreach($content as $category)
                <div class="col-lg-6 col-xl-6 col-md-6 col-sm-12">
                    <a href="{{route('category',[$category['slug']])}}">
                        <div class="categoryWrapper w3-animate-right">
                            <div class="categoryContent" style="{{isset($category['image']) && !empty($category['image']) ? 'background-image:url("'.$category['image'].'");' : ''}}">
                                <div class="categoryContentAbsolute">
                                    <h4 class="main-h4 text-white">
                                        {{isset($category['title']) && !empty($category['title']) ? $category['title'] : ''}}
                                    </h4>
                                    <p class="ptext text-white mh60 mh60oh fs14">
                                        {{isset($category['short_description']) && !empty($category['short_description']) ? $category['short_description'] : ''}}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</div>
