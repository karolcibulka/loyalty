<style>
    .galleryController{
        font-size:25px;
        cursor:pointer;
    }
    .galleryController{
        color:#e3e3e3;
    }
    .disabledController{
        color:white !important;
    }
</style>
@if(isset($content) && !empty($content))
    @php
      $content = array_values($content);
    @endphp
    @foreach($content as $ref => $image)
        <div class="row galleryWrap" id="galleryWrapper_{{$ref}}" style="{{$ref!=0 ? 'display:none;' : ''}};position:relative;">
            <div style="position:absolute;top:50%;left:25px;width: 30px;height: 30px;z-index:10;" class="previousGallery">
                <i class="fa fa-arrow-left color333 galleryController disabledController"></i>
            </div>

            <div style="position:absolute;top:50%;right:25px;width: 30px;height: 30px;z-index:10;" class="nextGallery">
                <i class="fa fa-arrow-right color333 galleryController"></i>
            </div>

            <div class="col-md-12 mt30 w3-animate-bottom">
                <legend data-content="{{$image['title']}}"></legend>
            </div>
            @php
                $imagesContent = $image['images'];
            @endphp
            <div class="col-md-12 mt30 w3-animate-right">
                @if(isset($image['images']) && !empty($image['images']))
                    @if(count($image['images'])>1)
                        <div class="row mt30">
                            <div class="col-md-10 offset-md-1">
                                <div class="row">
                                    @foreach($image['images'] as $key => $img)
                                        @if($key == 0)
                                            <div class="col-md-12">
                                                <a href="{{$img['medium']}}" ref="{{'ref_'.$ref}}"
                                                   data-fancybox="gallery_{{$ref}}">
                                                    <div class="mainImageSubpage"
                                                         style="background-image:url('{{$img['medium']}}')"></div>
                                                </a>
                                            </div>
                                        @elseif($key<7)
                                            <div class="col-md-2 col-sm-2 col-xs-2" style="margin-top:10px;">
                                                <a href="{{$img['large']}}" ref="{{'ref_'.$ref}}"
                                                   data-fancybox="gallery_{{$ref}}">
                                                    <div class="smallImage"
                                                         style="background-image:url('{{$img['thumb']}}')"></div>
                                                </a>
                                            </div>
                                        @else
                                            <a href="{{$img['large']}}" ref="{{'ref_'.$ref}}" data-fancybox="gallery">
                                                <div class="smallImage"
                                                     style="display:none;background-image:url('{{$img['thumb']}}')"></div>
                                            </a>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="row">
                            <div class="col-md-12">
                                <a href="{{$img[0]['large']}}" ref="{{'ref_'.$ref}}" data-fancybox="gallery_{{$ref}}">
                                    <div class="mainImageSubpage"
                                         style="background-image:url('{{$images[0]['medium']}}')"></div>
                                </a>
                            </div>
                        </div>
                        {{--  1 obrázok--}}
                    @endif
                @endif
            </div>
        </div>
    @endforeach
@endif

<script>

    var countGalleries = $('.galleryWrap');
    var allGalleries = countGalleries.length-1;
    var index = 0;

    $('.nextGallery').on('click',function(){
        var mainIndex = index+1;
        if(mainIndex<=allGalleries) {
            index = mainIndex;
            $('.galleryWrap').hide();
            $('#galleryWrapper_'+mainIndex).show();

            if(mainIndex === allGalleries){
                $('.nextGallery').find('i').addClass('disabledController');
            }

            if(mainIndex>0){
                $('.previousGallery').find('i').removeClass('disabledController');
            }

        }


    });

    $('.previousGallery').on('click',function(){
        var mainIndex = index-1;
        if(mainIndex>=0) {
            index = mainIndex;
            $('.galleryWrap').hide();
            $('#galleryWrapper_'+ mainIndex).show();
        }
        if(mainIndex === 0){
            $('.previousGallery').find('i').addClass('disabledController');
        }
        if(index!==allGalleries){
            $('.nextGallery').find('i').removeClass('disabledController');
        }
    });
</script>
