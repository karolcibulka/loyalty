<div class="row">
    <div class="col-md-12 mt30 w3-animate-bottom">
        <legend data-content="Galéria"></legend>
    </div>
    <div class="col-md-12 mt30 w3-animate-bottom">
        @if(isset($content['images']) && !empty($content['images']))
            @if(count($content['images'])>1)
                <div class="row mt30">
                    <div class="col-md-10 offset-md-1">
                        <div class="row">
                            @foreach($content['images'] as $key => $image)
                                @if($key == 0)
                                    <div class="col-md-12">
                                        <a href="{{$image['medium']}}" data-fancybox="gallery">
                                            <div class="mainImageSubpage"
                                                 style="background-image:url('{{$image['medium']}}')"></div>
                                        </a>
                                    </div>
                                @elseif($key<7)
                                    <div class="col-md-2 col-sm-2 col-xs-2" style="margin-top:10px;">
                                        <a href="{{$image['large']}}" data-fancybox="gallery">
                                            <div class="smallImage"
                                                 style="background-image:url('{{$image['thumb']}}')"></div>
                                        </a>
                                    </div>
                                @else
                                    <a href="{{$image['large']}}" data-fancybox="gallery">
                                        <div class="smallImage"
                                             style="display:none;background-image:url('{{$image['thumb']}}')"></div>
                                    </a>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            @else
                <div class="row">
                    <div class="col-md-12">
                        <a href="{{$images[0]['large']}}" data-fancybox="gallery">
                            <div class="mainImageSubpage"
                                 style="background-image:url('{{$images[0]['medium']}}')"></div>
                        </a>
                    </div>
                </div>
                {{--  1 obrázok--}}
            @endif
        @endif
    </div>
</div>
