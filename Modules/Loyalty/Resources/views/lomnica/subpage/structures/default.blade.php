<div id="itemPage">
    <div class="container">
        <div class="row" style="padding-bottom:200px;">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6 w3-animate-left">
                        <h1 class="color fs72 title">{{$subpage['title']}}</h1>
                        <p class="ptext ">
                            {!! $subpage['long_description'] ? strip_tags($subpage['long_description']) : '' !!}
                        </p>
                    </div>
                    <div class="col-md-6 w3-animate-right">
                        <div class="experienceWrapperIn"
                             style="{{$subpage['image'] ? 'background-image:url("'.asset('images/medium/'.$subpage['image']).'");' : ''}}"></div>
                        <div class="mt20"></div>
                    </div>
                </div>
                <div class="mt20"></div>
                @if(isset($content) && !empty($content))
                    @foreach($content as $c)
                        @if(!is_null($c['type']))
                            @include('loyalty::'.$template.'.subpage.partials.'.$c['view'].'.'.$c['type'],['content'=>$c['content']])
                        @else
                            @include('loyalty::'.$template.'.subpage.partials.'.$c['view'].'.main',['content'=>$c['content']])
                        @endif
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
