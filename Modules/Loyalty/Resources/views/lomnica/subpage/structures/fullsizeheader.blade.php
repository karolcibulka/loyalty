<div id="itemPage">
    <div class="container">
        <div class="row" style="padding-bottom:200px;">
            <div class="col-md-12">
                <div class="experienceWrapperIn"
                     style="{{$subpage['image'] ? 'background-image:url("'.asset('images/medium/'.$subpage['image']).'");' : ''}};width:100%;position:relative;">
                    <div style="position: absolute;
                                top: 0;
                                left: 0;
                                width: 100%;
                                height: 100%;
                                transform: translate(0,0);">
                        <h1 class="text-white fs72 title text-center text-shadow" style="padding-top: 90px;">{{$subpage['title']}}</h1>
                    </div>
                </div>
                <div class="mt20"></div>
                <div class="row col-md-12">
                    <p class="ptext ">
                        {!! $subpage['long_description'] ? strip_tags($subpage['long_description']) : '' !!}
                    </p>
                </div>
                <div class="mt20"></div>


                @if(isset($content) && !empty($content))
                    @foreach($content as $c)
                        @if(!is_null($c['type']))
                            @include('loyalty::'.$template.'.subpage.partials.'.$c['view'].'.'.$c['type'],['content'=>$c['content']])
                        @else
                            @include('loyalty::'.$template.'.subpage.partials.'.$c['view'].'.main',['content'=>$c['content']])
                        @endif
                    @endforeach
                @endif

            </div>
        </div>
    </div>
</div>
