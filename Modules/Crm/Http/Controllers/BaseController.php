<?php
/**
 * Created by PhpStorm.
 * User: karol
 * Date: 2019-11-06
 * Time: 09:46
 */

namespace Modules\Crm\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Libraries\LogLibrary;
use App\Models\Developer\Developer;
use App\Models\External\Language;
use App\Models\Developer\Navigation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class BaseController extends Controller
{
    public $developers;
    public $crm;
    public $loyalty;
    public $tree;
    public $loyaltyTree;
    public $devel;
    public $languages;
    public $loggedUser;


    public function __construct()
    {
        $this->middleware(function($request,$next){
           if(Auth::check()){
              $this->loggedUser = Auth::user();
              return $next($request);
           }
           else{
             return redirect(route('login'));
           }
        });

        if(Session::has('lang')){
            App::setLocale(Session::get('lang'));
        }
        else{
            Session::put('lang','sk');
        }

        if(isset($_GET['removeSession'])){
            Session::flush();
        }


        $this->middleware('web');


        if(Session::has('languages')){
            $this->languages = Session::get('languages');
        }
        else{
            $this->languages = Language::all();
            Session::put(['languages' => $this->languages]);
        }

        $this->devel = ['kajo.cibulka@gmail.com','kral@traveldata.sk'];

        if(Session::has('loyaltyTree')){
            $this->loyaltyTree = Session::get('loyaltyTree');
        }
        else{
            $this->loyalty = Navigation::select('*','navigations.id as navigationID','navigations.show_name as navigation_name')
                ->where('navigations.navigation','=','loyalty')
                ->leftJoin('controllers','controllers.id','=','navigations.controller_id')
                ->orderBy('navigations.order','ASC')
                ->get();

            $this->loyaltyTree = $this->buildTree($this->loyalty);
            Session::put( [ 'loyaltyTree' => $this->loyaltyTree ]);
        }

        if(Session::has('navigationTree')){
            $this->tree = Session::get('navigationTree');
        }
        else{
            $this->crm = Navigation::select('*','navigations.id as navigationID','navigations.show_name as navigation_name')
                ->where('navigations.navigation','=','crm')
                ->leftJoin('controllers','controllers.id','=','navigations.controller_id')
                ->orderBy('navigations.order','ASC')
                ->get();
            $this->tree = $this->buildTree($this->crm);
            Session::put([ 'navigationTree' => $this->tree ]);
        }

        if(Session::has('navigationDeveloper')){
            $this->developers = Session::get('navigationDeveloper');
        }
        else{
            $this->developers = Developer::all();
            Session::put(['navigationDeveloper' => $this->developers]);
        }


        $share = array(
            'developers'=>$this->developers,
            'loyalties'=>$this->loyaltyTree,
            'navs'=>$this->tree,
            'dev'=>$this->devel,
            'languages'=>$this->languages,
        );

        View::share($share);
    }

    private function buildTree($elements, $parentId = 0) {
        $branch = array();

        foreach ($elements as $element) {
            if ($element['parent'] == $parentId) {
                $children = $this->buildTree($elements, $element['navigationID']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }
        return $branch;
    }

    public function changeLang(Request $request){

        $log = new LogLibrary('BaseController','changeLang',Auth::user()->id);
        $log->storeLog();

        $data = $request->all();
        Session::put('lang', $data['lang']);
        $response = array(
            'status'=> '1',
        );

        echo json_encode($response);
    }
}
