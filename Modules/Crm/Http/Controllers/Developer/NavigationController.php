<?php

namespace Modules\Crm\Http\Controllers\Developer;

use App\Models\External\Icon;
use App\Models\Developer\Navigation;
use App\Models\Developer\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Modules\Crm\Http\Controllers\BaseController;

class NavigationController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware(function($request,$next){
            if (!in_array($this->loggedUser->email, $this->devel)) {
                Session::put('permission','!! <strong>DEVELOPER ONLY</strong> !!');
                return redirect()->route('home');
            }
            else{
                return $next($request);
            }
        });
    }

    public function index()
    {
        $icons       = Icon::all()->toArray();
        $navigations = Navigation::where('navigation', '=', 'crm')->orderBy('order')->get()->toArray();
        $tree        = $this->buildTree($navigations);
        $loyalty     = Navigation::where('navigation', '=', 'loyalty')->orderBy('order')->get()->toArray();
        $treeLoyalty = $this->buildTree($loyalty);
        $controllers = Controller::all()->toArray();
        $parents     = Navigation::where('type', '=', 'placeholder')->get()->toArray();
        return view('crm::views.developer.navigation')->with(array('controllers' => $controllers, 'loyalties2' => $treeLoyalty, 'navigations' => $tree, 'parents' => $parents, 'icons' => $icons));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        if (isset($data['controller_name']) && !empty($data['controller_name']) && isset($data['icon']) && !empty($data['icon'])) {
            $insertData = array(
                'show_name' => $data['controller_name'],
                'type' => $data['type'],
                'order' => '0',
                'parent' => (isset($data['parent_id']) && !empty($data['parent_id'])) ? $data['parent_id'] : '0',
                'uri' => ($data['type'] == 'link') ? $data['uri'] : '0',
                'controller_id' => ($data['type'] == 'controller') ? $data['controller_id'] : '0',
                'icon' => $data['icon'],
                'navigation' => $data['navigation'],
            );
            $id         = Navigation::insertGetId($insertData);
            if (isset($id) && !empty($id)) {
                $navigations = Navigation::where('navigation', '=', 'crm')->orderBy('order')->get()->toArray();
                $tree        = $this->buildTree($navigations);
                $loyalty     = Navigation::where('navigation', '=', 'loyalty')->orderBy('order')->get()->toArray();
                $treeLoyalty = $this->buildTree($loyalty);
                $view        = view('crm::views.developer.partials.navigations')->with(array('navigations' => $tree, 'loyalties2' => $treeLoyalty))->render();
                $response    = array(
                    'status' => '1',
                    'view' => $view,
                );
            } else {
                $response = array(
                    'status' => '0'
                );
            }
        } else {
            $response = array(
                'status' => '2'
            );
        }
        echo json_encode($response);
    }

    public function editProccess(Request $request)
    {
        $data = $request->all();
        $id   = $data['id'];
        if (isset($data['controller_name']) && !empty($data['controller_name']) && isset($data['icon']) && !empty($data['icon'])) {
            $updateData = array(
                'show_name' => $data['controller_name'],
                'type' => $data['type'],
                'parent' => (isset($data['parent_id']) && !empty($data['parent_id'])) ? $data['parent_id'] : '0',
                'uri' => ($data['type'] == 'link') ? $data['uri'] : '0',
                'controller_id' => ($data['type'] == 'controller') ? $data['controller_id'] : '0',
                'icon' => $data['icon'],
                'navigation' => $data['navigation'],
            );
            Navigation::where('id', '=', $id)->update($updateData);
            if (isset($id) && !empty($id)) {
                $navigations = Navigation::where('navigation', '=', 'crm')->orderBy('order')->get()->toArray();
                $tree        = $this->buildTree($navigations);
                $loyalty     = Navigation::where('navigation', '=', 'loyalty')->orderBy('order')->get()->toArray();
                $treeLoyalty = $this->buildTree($loyalty);
                $view        = view('crm::views.developer.partials.navigations')->with(array('navigations' => $tree, 'loyalties2' => $treeLoyalty))->render();

                $icons       = Icon::all()->toArray();
                $controllers = Controller::all()->toArray();
                $parents     = Navigation::where('type', '=', 'placeholder')->get()->toArray();
                $view2       = view('crm::views.developer.partials.addItem')->with(array('controllers' => $controllers, 'parents' => $parents, 'icons' => $icons))->render();

                $response = array(
                    'status' => '1',
                    'view' => $view,
                    'view2' => $view2
                );
            } else {
                $response = array(
                    'status' => '0'
                );
            }
        } else {
            $response = array(
                'status' => '2'
            );
        }
        echo json_encode($response);
    }

    public function handleChangeOrder(Request $request)
    {
        $post = $request->all();
        if (isset($post['json']) && !empty($post['json'])) {
            $response = $post['json'];
        }
        $i = 1;
        foreach ($response as $key => $item) {
            $id   = $item['id'];
            $data = array(
                'order' => $i,
                'parent' => '0',
            );
            Navigation::where('id', '=', $id)->update($data);

            if (isset($item['children']) && !empty($item['children'])) {
                foreach ($item['children'] as $child) {
                    $child_id   = $child['id'];
                    $child_data = array(
                        'order' => $i,
                        'parent' => $item['id'],
                    );
                    Navigation::where('id', '=', $child_id)->update($child_data);
                    $i++;
                }
            }

            $i++;
        }
    }

    public function removeMenuItem(Request $request)
    {
        $data      = $request->all();
        $id        = $data['id'];
        $childrens = Navigation::where('parent', '=', $id)->get()->toArray();
        if (isset($childrens) && !empty($childrens)) {
            foreach ($childrens as $children) {
                $updateData = array(
                    'parent' => '0'
                );
                Navigation::where('id', '=', $children['id'])->update($updateData);
            }
        }
        Navigation::where('id', '=', $id)->delete();

        $navigations = Navigation::orderBy('order')->get()->toArray();
        $tree        = $this->buildTree($navigations);
        $view        = view('crm::views.developer.partials.navigations')->with(array('navigations' => $tree))->render();

        $response = array(
            'status' => '1',
            'view' => $view,
        );
        echo json_encode($response);
    }

    public function editItem(Request $request)
    {
        $data = $request->all();
        $item = Navigation::where('id', '=', $data['id'])->get()->toArray();
        if (isset($item['0']) && !empty($item['0'])) {
            $res = $item['0'];
        }

        $icons       = Icon::all()->toArray();
        $controllers = Controller::all()->toArray();
        $parents     = Navigation::where('type', '=', 'placeholder')->get()->toArray();

        $view = view('crm::views.developer.partials.addItem')->with(array('item' => $res, 'controllers' => $controllers, 'parents' => $parents, 'icons' => $icons))->render();

        $response = array(
            'status' => '1',
            'view' => $view,
        );
        echo json_encode($response);
    }

    public function buildTree($elements, $parentId = 0)
    {
        $branch = array();

        foreach ($elements as $element) {
            if ($element['parent'] == $parentId) {
                $children = $this->buildTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }
        return $branch;
    }
}
