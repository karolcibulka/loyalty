<?php

namespace Modules\Crm\Http\Controllers\Developer;

use Illuminate\Http\Request;
use App\Models\Developer\Controller;
use Illuminate\Support\Facades\Session;
use Modules\Crm\Http\Controllers\BaseController;
use Spatie\Permission\Models\Permission;

class ControllersController extends BaseController
{

    public $loggedUser;
    public function __construct()
    {
        parent::__construct();
        $this->middleware(function($request,$next){
            if (!in_array($this->loggedUser->email, $this->devel)) {
                Session::put('permission','!! <strong>DEVELOPER ONLY</strong> !!');
                return redirect()->route('home');
            }
            else{
                return $next($request);
            }
        });
    }

    public function index()
    {
        $controllers = Controller::all()->toArray();
        //dd($routes);
        return view('crm::views.developer.controllers')->with(array('controllers' => $controllers));
    }

    public function storeController(Request $request)
    {

        $request->validate([
            'controller' => 'required|unique:controllers',
            'name' => 'required',
            'show_name' => 'required'
        ]);

        $data       = $request->all();
        $insertData = array(
            'name' => $data['name'],
            'function' => 'index',
            'controller' => $data['controller'],
            'show_name' => $data['show_name'],
        );
        Controller::insert($insertData);


        $permissions = ['create', 'edit', 'delete', 'show'];
        $perms       = array();

        foreach ($permissions as $permission) {
            $perm = array(
                'name' => $data['name'] . '.' . $permission,
                'guard_name' => 'web',
                'created_at' => date('Y-m-d H:i:s'),
            );

            $perms[] = $perm;
        }

        Permission::insert($perms);

        return redirect()->route('controllers');
    }

    public function removeController(Request $request)
    {
        $data = $request->all();
        Controller::where('id', '=', $data['id'])->delete();
        $response = array(
            'status' => '1'
        );
        echo json_encode($response);
    }

    public function editController(Request $request)
    {
        $data = $request->all();
        $item = Controller::where('id', '=', $data['id'])->get()->toArray();

        $view = view('crm::views.developer.partials.controllerForm')->with(array('item' => $item[0]))->render();

        $response = array(
            'status' => '1',
            'view' => $view,
        );

        echo json_encode($response);
    }

    public function editItemController(Request $request)
    {
        $request->validate([
            'controller' => 'required',
            'name' => 'required',
            'show_name' => 'required',
            'id' => 'required'
        ]);
        $data       = $request->all();
        $updateData = array(
            'show_name' => $data['show_name'],
            'name' => $data['name'],
            'controller' => $data['controller']
        );

        Controller::where('id', '=', $data['id'])->update($updateData);
        return redirect()->route('controllers');
    }
}
