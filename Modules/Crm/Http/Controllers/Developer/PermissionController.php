<?php

namespace Modules\Crm\Http\Controllers\Developer;

use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Modules\Crm\Http\Controllers\BaseController;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function($request,$next){
            if (!in_array($this->loggedUser->email, $this->devel)) {
                Session::put('permission','!! <strong>DEVELOPER ONLY</strong> !!');
                return redirect()->route('home');
            }
            else{
                return $next($request);
            }
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions     = Permission::all()->toArray();
        $permissionArray = array();
        if (isset($permissions) && !empty($permissions)) {
            foreach ($permissions as $permission) {
                $expl = explode('.', $permission['name']);
                if (isset($permissionArray[$expl[0]]) && !empty($permissionArray[$expl[0]])) {
                    $permissionArray[$expl[0]]['ids'] .= $permission['id'] . ',';
                } else {
                    $permissionArray[$expl[0]]['ids'] = $permission['id'] . ',';
                }
                $permissionArray[$expl[0]]['id'] = $permission['id'];
            }
        }
        return view('crm::views.developer.permissions')->with(array('permissions' => $permissionArray));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('crm::views.developer.partials.permissions.create')->with(array());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);
        $data = $request->all();

        foreach ($this->methods() as $method) {
            $permission = array(
                'name' => $data['name'] . '.' . $method,
                'guard_name' => 'web',
                'created_at' => date('Y-m-d H:i:s'),
            );
            Permission::insert($permission);
        }

        return redirect(route('permission.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission      = Permission::find($id)->toArray();
        $expl            = explode('.', $permission['name']);
        $name            = $expl[0];
        $permissions     = Permission::all()->toArray();
        $permissionArray = array();
        if (isset($permissions) && !empty($permissions)) {
            foreach ($permissions as $permission) {
                $expl = explode('.', $permission['name']);
                if (isset($permissionArray[$expl[0]]) && !empty($permissionArray[$expl[0]])) {
                    $permissionArray[$expl[0]]['ids'] .= $permission['id'] . ',';
                } else {
                    $permissionArray[$expl[0]]['ids'] = $permission['id'] . ',';
                }
                $permissionArray[$expl[0]]['id'] = $permission['id'];
            }
        }
        $thisPermission = '';
        if (isset($permissionArray[$name]) && !empty($permissionArray[$name])) {
            $thisPermission = $permissionArray[$name];
        }

        return view('crm::views.developer.partials.permissions.edit')->with(array('id' => $id, 'name' => $name, 'permissions' => $thisPermission));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'ids' => 'required'
        ]);
        $data = $request->all();
        if ($data['name'] == $data['oldname']) {
            return (redirect(route('permission.index')));
        } else {
            $fids = array();
            $ids  = explode(',', $data['ids']);
            foreach ($ids as $id) {
                if ($id !== '') {
                    $fids[] = $id;
                }
            }
            foreach ($fids as $key => $fid) {
                $update = array(
                    'name' => $data['name'] . '.' . $this->methods()[$key],
                );
                Permission::where('id', '=', $fid)->update($update);
            }
        }
        return (redirect(route('permission.index')));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ids = array();
        for ($i = $id; $i > $id - 4; $i--) {
            $ids[] = $i;
        }
        foreach ($ids as $i) {
            Permission::find($i)->delete();
        }

        return (redirect(route('permission.index')));
    }

    public function methods()
    {
        $methods = ['create', 'edit', 'delete', 'show'];
        return $methods;
    }
}
