<?php

namespace Modules\Crm\Http\Controllers\Developer;

use Ejarnutowski\LaravelApiKey\Models\ApiKey;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Session;
use Modules\Crm\Http\Controllers\BaseController;

class ApiDeveloperController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        parent::__construct();
        $this->middleware(function($request,$next){
            if (!in_array($this->loggedUser->email, $this->devel)) {
                Session::put('permission','!! <strong>DEVELOPER ONLY</strong> !!');
                return redirect()->route('home');
            }
            else{
                return $next($request);
            }
        });
    }


    public function index()
    {
        $api = ApiKey::all();
        return view('crm::views.developer.api')->with(array('apis' => $api));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('crm::views.developer.partials.api.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['name' => 'required']);

        $data = $request->all();
        Artisan::call('apikey:generate', ['name' => $data['name']]);
        return (redirect(route('apideveloper.index')));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $api = ApiKey::find($id);
        return view('crm::views.developer.partials.api.edit')->with(array('api' => $api->toArray()));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(['name' => 'required']);
        $data = $request->all();

        ApiKey::where('id', '=', $id)->update(array('name' => $data['name']));
        return (redirect(route('apideveloper.index')));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function handleChangeApiStatus(Request $request)
    {
        $data = $request->all();
        $api  = ApiKey::where('name', '=', $data['name'])->get()->first();
        if ($api) {
            $api = $api->toArray();
            if ($api['active'] == 1) {
                Artisan::call('apikey:deactivate', ['name' => $data['name']]);
                $response = array(
                    'status' => '1',
                );
            } else {
                Artisan::call('apikey:activate', ['name' => $data['name']]);
                $response = array(
                    'status' => '2',
                );
            }
        } else {
            $response = array(
                'status' => '0',
            );
        }

        echo json_encode($response);
    }
}
