<?php

namespace Modules\Crm\Http\Controllers\Cron;

use App\Models\Crm\Newsletter;
use App\Models\External\EmailHistory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Mail;

class CronController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function emailRecieved($id, $email_setting_id)
    {
        EmailHistory::where('id', '=', $id)->where('email_setting_id', '=', $email_setting_id)->update(array('showed' => '1', 'updated_at' => date('Y-m-d H:i:s'), 'showed_at' => date('Y-m-d H:i:s')));
    }

    public function chooseType($type)
    {
        switch ($type) {
            case "email":
                $this->emailCron();
                break;
        }
    }

    private function emailCron()
    {
        $history = EmailHistory::select('email_histories.*')->where('sended', '=', '0')
            ->join('email_settings as es','es.id','=','email_histories.email_setting_id')
            ->whereDate('es.date_send_at','=',date('Y-m-d'))
            ->whereTime('es.hour_send_at','<=',date('H'))
            ->where('es.deleted','=','0')
            ->limit(5)
            ->get();


        if (isset($history) && !empty($history)) {
            foreach ($history as $h) {
                $data['template'] = Newsletter::find($h['template_id']);

                $uri = route('emailRecieved', [$h['id'], $h['email_setting_id']]);

                $data['template']['html'] = $data['template']['html'] . '<img src="' . $uri . '" width="1" height="1" style="width:1px;height:1px;background-color:transparent;">';

                $data['settings'] = $h;
                Mail::send('crm::views.admin.emails.templates.baseTemplate', $data, function ($message) use ($data) {
                    if(isset($data['template']['attachments']) && !empty($data['template']['attachments'])){
                        $attachments = json_decode($data['template']['attachments'],true);
                        if(!empty($attachments)){
                            foreach($attachments as $att){
                                $message->attach(public_path().'/uploads/'.$att);
                            }
                        }
                    }
                    $message->to($data['settings']['email'], $data['settings']['email'])
                        ->subject($data['template']['subject']);

                });

                if (Mail::failures()) {
                    EmailHistory::where('id', '=', $h['id'])
                        ->update(array('response' => 'fail', 'updated_at' => date('Y-m-d H:i:s')));
                } else {
                    EmailHistory::where('id', '=', $h['id'])
                        ->update(array('response' => 'success', 'sended' => '1', 'sended_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')));
                }
            }
        }
    }
}
