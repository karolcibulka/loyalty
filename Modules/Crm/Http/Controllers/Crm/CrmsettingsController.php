<?php

namespace Modules\Crm\Http\Controllers\Crm;

use App\Libraries\LogLibrary;
use App\Models\Crm\CrmSetting;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Modules\Crm\Http\Controllers\BaseController;

class CrmsettingsController extends BaseController
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if($this->loggedUser->can('setting.show')){
            $settings = CrmSetting::first();
            return view('crm::views.admin.crmsettings.show')->with(array('settings'=>$settings));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $settings = CrmSetting::find(1);
        if($this->loggedUser->can('setting.store') && is_null($settings)){

            $log = new LogLibrary('CrmsettingsController', 'edit', Auth::user()->id, null, null, serialize($request->input()), null);
            $log->storeLog();

            $insertData = array(
                'driver' => $data['driver'],
                'host' => $data['host'],
                'port' => $data['port'],
                'username' => $data['username'],
                'password' => $data['password'],
                'name' => $data['name'],
                'address' => $data['address'],
                'created_at' => date('Y-m-d H:i:s'),
            );

            CrmSetting::insert($insertData);

            return redirect()->route('settings.index');
        }
        elseif($this->loggedUser->can('setting.edit') && !is_null($settings)){

            $log = new LogLibrary('CrmsettingsController', 'update', Auth::user()->id, null, null, serialize($request->input()), null);
            $log->storeLog();

            $updateData = array(
                'driver' => $data['driver'],
                'host' => $data['host'],
                'port' => $data['port'],
                'username' => $data['username'],
                'password' => $data['password'],
                'name' => $data['name'],
                'address' => $data['address'],
                'updated_at' => date('Y-m-d H:i:s')
            );

            $settings->update($updateData);

            return redirect()->route('settings.index');
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('crm::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('crm::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
