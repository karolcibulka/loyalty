<?php

namespace Modules\Crm\Http\Controllers\Crm;

use App\Http\Controllers\Controller;
use App\Libraries\LogLibrary;
use App\Models\Crm\PropertyCategory as Propertycategory;
use App\Models\Crm\PropertyCategoryLang as Propertycategorylang;
use App\Models\External\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Modules\Crm\Http\Controllers\BaseController;

class PropertyCategoryController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
        if ($this->loggedUser->can('propertycategory.show')) {
            $propertycategories = Propertycategory::where('deleted', '=', '0')->paginate(10);
            return view('crm::views.admin.propertycategories')->with(array('propertycategories' => $propertycategories));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ($this->loggedUser->can('propertycategory.create')) {
            $languages = Language::all();
            return view('crm::views.admin.partials.propertycategories.create')->with(array('languages' => $languages));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->loggedUser->can('propertycategory.create')) {
            $log = new LogLibrary('PropertyCategoryController', 'store', Auth::user()->id, null, null, serialize($request->input()), null);
            $log->storeLog();


            $data = $request->all();

            $insertData['image']         = '';
            $insertData['internal_name'] = $data['internal_name'];
            $insertData['created_at']    = date('Y-m-d H:i:s');

            if($request->cropper_used == '1'){
                if(!empty($request->cropper_value)) {

                    $base = $request->cropper_value;

                    $extension = explode(';', $base);
                    $extension = explode('/', $extension[0]);
                    $extension = $extension[1];

                    $imageName = uniqid() . '_' . time() . '.' . $extension;

                    $base64Str = substr($base, strpos($base, ",") + 1);
                    $image_base     = base64_decode($base64Str);

                    $destinationPath = public_path('images/categories');
                    $img3            = Image::make($image_base);
                    $img3->encode('jpeg', '75')->save($destinationPath . '/' . $imageName);

                    $insertData['image'] = $imageName;
                }
            }
            elseif ($request->hasFile('image')) {
                $image               = $request->file('image');
                $insertData['image'] = uniqid() . '_' . time() . '.' . $image->extension();

                $destinationPath = public_path('images/categories');
                if (!is_dir($destinationPath)) {
                    mkdir($destinationPath, 0777, true);
                    chmod($destinationPath, 0777);
                }

                $img = Image::make($image->path());
                $img->encode('jpeg', '75')->save($destinationPath . '/' . $insertData['image']);
            }

            $categoryID = Propertycategory::insertGetId($insertData);

            //langs
            if (isset($data['title']) && !empty($data['title'])) {
                foreach ($data['title'] as $lang => $title) {
                    if (!is_null($data['title'][$lang])) {
                        $insertLang = array(
                            'property_category_id' => $categoryID,
                            'code' => $lang,
                            'title' => $data['title'][$lang],
                            'short_description' => $data['short_description'][$lang],
                            'long_description' => $data['long_description'][$lang],
                            'slug' => isset($data['slug'][$lang]) && !empty($data['slug'][$lang]) ? Str::slug($data['slug'][$lang]) : Str::slug($data['title'][$lang]),
                            'created_at' => date('Y-m-d H:i:s')
                        );

                        Propertycategorylang::insert($insertLang);
                    }
                }
            }

            return redirect(route('propertycategory'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($this->loggedUser->can('propertycategory.show')) {

        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($this->loggedUser->can('propertycategory.edit')) {
            $languages = Language::all();

            $propertycategories = Propertycategory::where('property_categories.id', '=', $id)->leftJoin('property_category_langs', 'property_categories.id', '=', 'property_category_langs.property_category_id')->get();

            $response = array();
            if (!empty($propertycategories)) {
                foreach ($propertycategories as $pc) {
                    $response['image']                          = $pc['image'];
                    $response['internal_name']                  = $pc['internal_name'];
                    $response['slug'][$pc['code']]              = $pc['slug'];
                    $response['title'][$pc['code']]             = $pc['title'];
                    $response['short_description'][$pc['code']] = $pc['short_description'];
                    $response['long_description'][$pc['code']]  = $pc['long_description'];

                }
            }

            return view('crm::views.admin.partials.propertycategories.edit')->with(array('languages' => $languages, 'id' => $id, 'propertycategory' => $response));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($this->loggedUser->can('propertycategory.edit')) {
            $propertycategories = Propertycategory::where('property_categories.id', '=', $id)->leftJoin('property_category_langs', 'property_categories.id', '=', 'property_category_langs.property_category_id')->get();

            $response = array();
            if (!empty($propertycategories)) {
                foreach ($propertycategories as $pc) {
                    $response['image']                          = $pc['image'];
                    $response['internal_name']                  = $pc['internal_name'];
                    $response['slug'][$pc['code']]              = $pc['slug'];
                    $response['title'][$pc['code']]             = $pc['title'];
                    $response['short_description'][$pc['code']] = $pc['short_description'];
                    $response['long_description'][$pc['code']]  = $pc['long_description'];

                }
            }

            $log = new LogLibrary('PropertyCategoryController', 'update', Auth::user()->id, null, null, serialize($request->input()), serialize($response));
            $log->storeLog();


            $data = $request->all();


            $oldCategory                  = Propertycategory::find($id);
            $oldCategory['image']         = $data['old_image'];
            $oldCategory['internal_name'] = $data['internal_name'];
            $oldCategory['updated_at']    = date('Y-m-d H:i:s');

            $destinationPath = public_path('images/categories');

            if (isset($data['removeOldImage']) && !empty($data['removeOldImage'])) {
                if (isset($data['oldImage']) && !empty($data['oldImage'])) {
                    File::delete($destinationPath . '/' . $data['oldImage']);
                }
            }

            if($request->cropper_used == '1'){
                if(!empty($request->cropper_value)) {

                    $base = $request->cropper_value;

                    $extension = explode(';', $base);
                    $extension = explode('/', $extension[0]);
                    $extension = $extension[1];

                    $imageName = uniqid() . '_' . time() . '.' . $extension;

                    $base64Str = substr($base, strpos($base, ",") + 1);
                    $image_base     = base64_decode($base64Str);

                    $destinationPath = public_path('images/categories');
                    $img            = Image::make($image_base);
                    $img->encode('jpeg', '75')->save($destinationPath . '/' . $imageName);

                    $oldCategory['image'] = $imageName;
                }
            }
            else if ($request->hasFile('image')) {
                $image                = $request->file('image');
                $oldCategory['image'] = uniqid() . '_' . time() . '.' . $image->extension();


                if (!is_dir($destinationPath)) {
                    mkdir($destinationPath, 0777, true);
                    chmod($destinationPath, 0777);
                }

                $img = Image::make($image->path());
                $img->encode('jpeg', '75')->save($destinationPath . '/' . $oldCategory['image']);
            }

            $oldCategory->update();

            //langs
            if (isset($data['title']) && !empty($data['title'])) {
                Propertycategorylang::where('property_category_id', '=', $id)->delete();
                foreach ($data['title'] as $lang => $title) {
                    if (!is_null($data['title'][$lang])) {
                        $insertLang = array(
                            'property_category_id' => $id,
                            'code' => $lang,
                            'title' => $data['title'][$lang],
                            'short_description' => $data['short_description'][$lang],
                            'long_description' => $data['long_description'][$lang],
                            'slug' => isset($data['slug'][$lang]) && !empty($data['slug'][$lang]) ? Str::slug($data['slug'][$lang]) : Str::slug($data['title'][$lang]),
                            'created_at' => date('Y-m-d H:i:s')
                        );

                        Propertycategorylang::insert($insertLang);
                    }
                }
            }

            return redirect(route('propertycategory'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->loggedUser->can('propertycategory.delete')) {
            $log = new LogLibrary('PropertyCategoryController', 'destroy', Auth::user()->id, null, null, $id, null);
            $log->storeLog();


            Propertycategory::where('id', '=', $id)->update(array('deleted' => '1'));
            return redirect('propertycategory');
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }
}
