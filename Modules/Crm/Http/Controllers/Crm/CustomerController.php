<?php

namespace Modules\Crm\Http\Controllers\Crm;

use App\Libraries\LogLibrary;
use App\Models\Crm\Customer;
use App\Models\Crm\CustomerGroup;
use App\Models\Crm\Point;
use App\Models\Crm\PointHistory as Pointhistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Modules\Crm\Http\Controllers\BaseController;

class CustomerController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        parent::__construct();
    }


    public function index(Request $request)
    {
        if ($this->loggedUser->can('customer.show')) {
            $requests = $request->all();
            $customer = Customer::leftJoin('points', 'points.customer_id', '=', 'customers.id')->leftJoin('customer_groups', 'customer_groups.id', '=', 'customers.group_id');
            $customer->select('customers.*', 'points.points', 'customers.created_at as created_customer', 'customer_groups.internal_name as group_name');
            foreach ($requests as $key => $req) {
                if ($key !== 'page') {
                    if (!is_null($req)) {
                        $customer->where($key, '=', $req);
                    }
                } else {
                    unset($requests[$key]);
                }
            }

            $customer->orderBy('created_customer', 'asc');
            $customers = $customer->paginate(10);

            $customer_groups = CustomerGroup::where('active', '=', '1')->where('deleted', '=', '0')->get();

            return view('crm::views.admin.customer')->with(array('customers' => $customers->appends($requests), 'requests' => $requests, 'customer_groups' => $customer_groups));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ($this->loggedUser->can('customer.create')) {

        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->loggedUser->can('customer.create')) {

        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($this->loggedUser->can('customer.show')) {
            $customer  = Customer::select('*')->where('id', '=', $id)->leftJoin('points', 'points.customer_id', '=', 'customers.id')->get()->first()->toArray();
            $points    = Pointhistory::where('customer_id', '=', $customer['id'])->orderByRaw('created_at DESC')->get();
            $allPoints = 0;
            if (!empty($points)) {
                foreach ($points as $point) {
                    if ($point['type'] == '+') {
                        $allPoints += $point['points'];
                    }
                }
            }
            $pointss   = $points = Pointhistory::where('customer_id', '=', $customer['id'])->orderByRaw('created_at DESC')->paginate(10);
            $countries = DB::table('countries')->select('*')->get();
            $countries = collect($countries)->map(function ($x) {
                return (array)$x;
            })->toArray();

            return view('crm::views.admin.partials.customers.customer')->with(array('customer' => $customer, 'points' => $pointss, 'allPoints' => $allPoints, 'countries' => $countries));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($this->loggedUser->can('customer.edit')) {

        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($this->loggedUser->can('customer.edit')) {
            $log = new LogLibrary('CustomerController', 'update', Auth::user()->id, null, null, serialize($request->input()), null);
            $log->storeLog();


            $data = $request->all();
            unset($data['_method']);
            unset($data['_token']);
            if (isset($data['birth']) && !empty($data['birth'])) {
                $data['birth'] = date('Y-m-d', strtotime($data['birth']));
            }
            Customer::where('id', '=', $id)->update($data);

            return redirect(route('customers.show', ['customer' => $id]));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    public function addPoints(Request $request)
    {
        if ($this->loggedUser->can('customer.edit')) {
            $log = new LogLibrary('CustomerController', 'addPoints', Auth::user()->id, null, null, serialize($request->input()), null);
            $log->storeLog();

            $request->validate([
                'customer_id' => 'required',
                'message' => 'required',
                'points' => 'required',
            ]);

            $data          = $request->all();
            $point         = Point::where('customer_id', '=', $data['customer_id'])->get()->first();
            $historyInsert = array(
                'created_at' => date('Y-m-d H:i:s'),
                'reason' => $data['message'],
                'customer_id' => $data['customer_id'],
                'type' => '+',
                'points' => $data['points'],
            );
            if ($point) {
                $point_array = $point->toArray();
                $updateData  = array(
                    'points' => $point_array['points'] + $data['points'],
                );
                Point::where('customer_id', '=', $data['customer_id'])->update($updateData);
                $historyInsert['old_points'] = $point_array['points'];
                $historyInsert['new_points'] = $point_array['points'] + $data['points'];
            } else {
                $createPointField = array(
                    'created_at' => date('Y-m-d H:i:s'),
                    'points' => $data['points'],
                    'customer_id' => $data['customer_id'],
                );
                Point::insert($createPointField);
                $historyInsert['old_points'] = '0';
                $historyInsert['new_points'] = $data['points'];
            }

            Pointhistory::insert($historyInsert);

            return redirect(route('customers.show', ['customer' => $data['customer_id']]));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    public function removePoints(Request $request)
    {
        if ($this->loggedUser->can('customer.edit')) {
            $log = new LogLibrary('CustomerController', 'removePoints', Auth::user()->id, null, null, serialize($request->input()));
            $log->storeLog();

            $request->validate([
                'customer_id' => 'required',
                'message' => 'required',
                'points' => 'required',
            ]);

            $data  = $request->all();
            $point = Point::where('customer_id', '=', $data['customer_id'])->get()->first();

            $historyInsert = array(
                'created_at' => date('Y-m-d H:i:s'),
                'reason' => $data['message'],
                'customer_id' => $data['customer_id'],
                'type' => '-',
                'points' => $data['points'],
            );

            if ($point) {
                $point_array = $point->toArray();
                $updateData  = array(
                    'points' => $point_array['points'] - $data['points'],
                );
                if ($updateData['points'] >= 0) {

                } else {
                    $updateData['points'] = 0;
                }

                Point::where('customer_id', '=', $data['customer_id'])->update($updateData);
                $historyInsert['old_points'] = $point_array['points'];
                if ($updateData['points'] >= 0) {
                    $historyInsert['new_points'] = $point_array['points'] - $data['points'];
                } else {
                    $historyInsert['new_points'] = 0;
                }
            } else {
                $createPointField = array(
                    'created_at' => date('Y-m-d H:i:s'),
                    'points' => '0',
                    'customer_id' => $data['customer_id'],
                );

                Point::insert($createPointField);
                $historyInsert['old_points'] = '0';
                $historyInsert['new_points'] = '0';
            }

            Pointhistory::insert($historyInsert);

            return redirect(route('customers.show', ['customer' => $data['customer_id']]));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    public function customer_change_group(Request $request)
    {
        if ($this->loggedUser->can('customer.edit')) {
            $log = new LogLibrary('CustomerController', 'changeGroup', Auth::user()->id, null, null, serialize($request->input()));
            $log->storeLog();

            $data = $request->all();

            $update = array(
                'group_id' => $data['val']
            );

            Customer::where('id', '=', $data['id'])->update($update);

            $response = array(
                'status' => '1'
            );

            echo json_encode($response);
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }
}
