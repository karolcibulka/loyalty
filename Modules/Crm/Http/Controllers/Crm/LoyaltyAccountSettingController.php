<?php

namespace Modules\Crm\Http\Controllers\Crm;

use App\Http\Controllers\Controller;
use App\Libraries\LogLibrary;
use App\Models\Crm\CustomerGroup;
use App\Models\Crm\LoyaltyAccountSetting;
use App\Models\Crm\LoyaltyAccountSettingGroup;
use App\Models\Loyalty\LoyaltySetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Modules\Crm\Http\Controllers\BaseController;

class LoyaltyAccountSettingController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
        if ($this->loggedUser->can('loyaltyaccountsetting.show')) {
            $loyalty_account_settings     = array();
            $all_loyalty_account_settings = LoyaltyAccountSetting::where('deleted', '=', '0')->get();

            if (!empty($all_loyalty_account_settings)) {
                foreach ($all_loyalty_account_settings as $loyalty_account_setting) {
                    if ($loyalty_account_setting['loyalty_account_setting_type'] == 'CREATE') {
                        $loyalty_account_settings['create'][$loyalty_account_setting['id']] = $loyalty_account_setting;
                    } elseif ($loyalty_account_setting['loyalty_account_setting_type'] == 'CHARGE') {
                        $loyalty_account_settings['charge'][$loyalty_account_setting['id']] = $loyalty_account_setting;
                    } elseif ($loyalty_account_setting['loyalty_account_setting_type'] == 'EXPIRE') {
                        $loyalty_account_settings['expire'][$loyalty_account_setting['id']] = $loyalty_account_setting;
                    } elseif ($loyalty_account_setting['loyalty_account_setting_type'] == 'ADD') {
                        $loyalty_account_settings['add'][$loyalty_account_setting['id']] = $loyalty_account_setting;
                    }
                }
            }

            $loyalty_settings = LoyaltySetting::first();

            return view('crm::views.admin.loyalty_account_setting.show')->with(array('loyalty_account_settings' => $loyalty_account_settings,'loyalty_settings'=>$loyalty_settings));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ($this->loggedUser->can('loyaltyaccountsetting.create')) {
            $customer_groups = CustomerGroup::where('deleted', '=', '0')->get();
            return view('crm::views.admin.loyalty_account_setting.create')->with(array('customer_groups' => $customer_groups));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->loggedUser->can('loyaltyaccountsetting.create')) {
            $log = new LogLibrary('LoyaltyAccountSettingController', 'store', Auth::user()->id, null, null, serialize($request->input()), null);
            $log->storeLog();

            $data = $request->all();

            $insert = array(
                'internal_name' => $data['internal_name'],
                'loyalty_account_setting_type' => $data['loyalty_account_setting_type'],
                'date_required' => $data['date_required'],
                'customer_groups' => $data['customer_groups'],
                'date_from' => $data['date_required'] == '1' ? date('Y-m-d H:i:s', strtotime($data['date_from'] . ' 00:00:00')) : null,
                'date_to' => $data['date_required'] == '1' ? date('Y-m-d H:i:s', strtotime($data['date_to'] . ' 00:00:00')) : null,
                'active' => '1',
                'deleted' => '0',
                'created_at' => date('Y-m-d H:i:s')
            );

            $insertID = LoyaltyAccountSetting::insertGetId($insert);

            $insert_groups = array();
            if (!is_null($data['customer_groups'])) {
                $customer_group = explode(',', $data['customer_groups']);
                if (!empty($customer_group)) {
                    foreach ($customer_group as $group) {
                        $pivot = array(
                            'loyalty_account_setting_id' => $insertID,
                            'group_id' => $group,
                            'extra_key' => $data['extra_key'][$group],
                            'extra_value' => number_format($data['extra_value'][$group], 2, '.', ''),
                            'created_at' => date('Y-m-d H:i:s')
                        );

                        $insert_groups[] = $pivot;
                    }
                }
            }

            if (!empty($insert_groups)) {
                LoyaltyAccountSettingGroup::insert($insert_groups);
            }

            return redirect(route('loyaltyaccountsetting'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($this->loggedUser->can('loyaltyaccountsetting.show')) {

        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($this->loggedUser->can('loyaltyaccountsetting.edit')) {
            $customer_groups = CustomerGroup::where('deleted', '=', '0')->get();
            $profiles        = LoyaltyAccountSetting::where('id', '=', $id)->leftJoin('loyalty_account_setting_groups as lasg', 'lasg.loyalty_account_setting_id', '=', 'loyalty_account_settings.id')->get();

            $profile = array();
            if (!empty($profiles)) {
                foreach ($profiles as $prof) {
                    $profile['id']                           = $prof['loyalty_account_setting_id'];
                    $profile['date_required']                = $prof['date_required'];
                    $profile['date_from']                    = $prof['date_from'];
                    $profile['internal_name']                = $prof['internal_name'];
                    $profile['date_to']                      = $prof['date_to'];
                    $profile['loyalty_account_setting_type'] = $prof['loyalty_account_setting_type'];
                    $profile['customer_groups']              = isset($prof['customer_groups']) && !empty($prof['customer_groups']) ? explode(',', $prof['customer_groups']) : '';
                    if (isset($prof['group_id']) && !empty($prof['group_id'])) {
                        $profile['profile'][$prof['group_id']]['extra_key']   = $prof['extra_key'];
                        $profile['profile'][$prof['group_id']]['extra_value'] = $prof['extra_value'];
                    }
                }
            }

            return view('crm::views.admin.loyalty_account_setting.edit')->with(array('customer_groups' => $customer_groups, 'profile' => $profile,'id'=>$id));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($this->loggedUser->can('loyaltyaccountsetting.edit')) {
            $profiles = LoyaltyAccountSetting::where('id', '=', $id)->leftJoin('loyalty_account_setting_groups as lasg', 'lasg.loyalty_account_setting_id', '=', 'loyalty_account_settings.id')->get();
            $profile  = array();
            if (!empty($profiles)) {
                foreach ($profiles as $prof) {
                    $profile['id']                           = $prof['loyalty_account_setting_id'];
                    $profile['date_required']                = $prof['date_required'];
                    $profile['date_from']                    = $prof['date_from'];
                    $profile['internal_name']                = $prof['internal_name'];
                    $profile['date_to']                      = $prof['date_to'];
                    $profile['loyalty_account_setting_type'] = $prof['loyalty_account_setting_type'];
                    $profile['customer_groups']              = isset($prof['customer_groups']) && !empty($prof['customer_groups']) ? explode(',', $prof['customer_groups']) : '';
                    if (isset($prof['group_id']) && !empty($prof['group_id'])) {
                        $profile['profile'][$prof['group_id']]['extra_key']   = $prof['extra_key'];
                        $profile['profile'][$prof['group_id']]['extra_value'] = $prof['extra_value'];
                    }
                }
            }

            $log = new LogLibrary('LoyaltyAccountSettingController', 'update', Auth::user()->id, null, null, serialize($request->input()), serialize($profile));
            $log->storeLog();

            $data   = $request->all();


            $update = array(
                'internal_name' => $data['internal_name'],
                'loyalty_account_setting_type' => $data['loyalty_account_setting_type'],
                'date_required' => $data['date_required'],
                'customer_groups' => $data['customer_groups'],
                'date_from' => $data['date_required'] == '1' ? date('Y-m-d H:i:s', strtotime($data['date_from'] . ' 00:00:00')) : null,
                'date_to' => $data['date_required'] == '1' ? date('Y-m-d H:i:s', strtotime($data['date_to'] . ' 00:00:00')) : null,
                'active' => '1',
                'deleted' => '0',
                'created_at' => date('Y-m-d H:i:s')
            );

            LoyaltyAccountSetting::where('id', '=', $id)->update($update);

            $insert_groups = array();
            LoyaltyAccountSettingGroup::where('loyalty_account_setting_id', '=', $id)->delete();
            //dd($data['customer_groups']);
            if (!is_null($data['customer_groups'])) {
                $customer_group = explode(',', $data['customer_groups']);
                if (!empty($customer_group)) {
                    foreach ($customer_group as $group) {
                        $pivot = array(
                            'loyalty_account_setting_id' => $id,
                            'group_id' => $group,
                            'extra_key' => $data['extra_key'][$group],
                            'extra_value' => number_format($data['extra_value'][$group], 2, '.', ''),
                            'created_at' => date('Y-m-d H:i:s')
                        );

                        $insert_groups[] = $pivot;
                    }
                }
            }

            if (!empty($insert_groups)) {
                LoyaltyAccountSettingGroup::insert($insert_groups);
            }

            return redirect(route('loyaltyaccountsetting'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->loggedUser->can('loyaltyaccountsetting.delete')) {
            $log = new LogLibrary('LoyaltyAccountSettingController', 'destroy', Auth::user()->id, null, null, $id, null);
            $log->storeLog();

            LoyaltyAccountSetting::where('id', '=', $id)->update(array('deleted' => '1'));
            return redirect(route('loyaltyaccountsetting'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    public function changeActivityProfile(Request $request)
    {
        if ($this->loggedUser->can('loyaltyaccountsetting.edit')) {
            $log = new LogLibrary('LoyaltyAccountSettingController', 'activity', Auth::user()->id, null, null, serialize($request->input()), null);
            $log->storeLog();


            $data = $request->all();

            $update = array(
                'active' => $data['value']
            );

            LoyaltyAccountSetting::where('id', '=', $data['id'])->update($update);

            echo json_encode(array('status' => '1'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    public function changeSettingsForTypes(Request $request){
        $data = $request->all();
        if($data['type'] == 'add'){
            $updateData = array(
                'adding_points' => $data['value'],
            );
            $loyaltySettings = LoyaltySetting::first();
            $loyaltySettings->update($updateData);
        }


        return response()->json(array('status'=>'1'));
    }
}
