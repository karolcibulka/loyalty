<?php

namespace Modules\Crm\Http\Controllers\Crm;

use App\Libraries\LogLibrary;
use App\Models\Crm\Filter;
use App\Models\Crm\Newsletter;
use App\Models\External\EmailHistory;
use App\Models\External\EmailSetting;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Modules\Crm\Http\Controllers\BaseController;

class EmailController extends BaseController
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if($this->loggedUser->can('email.show')){

            $emailSettings = EmailSetting::where('deleted','=','0')->paginate(10);

            return view('crm::views.admin.emails.show')->with(array('emailSettings'=>$emailSettings));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        if($this->loggedUser->can('email.create')){
            $filters = Filter::select('id','internal_name')->where('show_in','like','%newsletter%')->get();
            $templates = Newsletter::all();
            return view('crm::views.admin.emails.create')->with(array('filters'=>$filters,'templates'=>$templates));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        if($this->loggedUser->can('email.create')){

            $log = new LogLibrary('EmailController', 'store', Auth::user()->id, null, null, serialize($request->input()), null);
            $log->storeLog();

            $data = $request->all();

            $insertData = array(
                'internal_name' => $data['internal_name'],
                'template_id' => $data['template_id'],
                'filter_id' => $data['filter_id'],
                'date_send_at' => date('Y-m-d H:i:s',strtotime($data['date_send_at'].'00:00:00')),
                'hour_send_at' => $data['hour_send_at'],
                'created_at' => date('Y-m-d H:i:s'),
                'deleted' => '0'
            );

            $esID = EmailSetting::insertGetId($insertData);

            $filter = Filter::find($data['filter_id']);

            $filterData = DB::select($filter['sql']);

            if(isset($filterData) && !empty($filterData)){
                $history = array();
                foreach($filterData as $fd){
                    $insertHistory = array(
                        'email' => $fd->email,
                        'template_id' => $data['template_id'],
                        'email_setting_id' => $esID,
                        'sended' => '0',
                        'showed' => '0',
                        'response' => '',
                        'created_at' => date('Y-m-d H:i:s')
                    );

                    $history[] = $insertHistory;
                }

                if(!empty($history)){
                    $ehres = EmailHistory::insert($history);
                }
            }


            return redirect()->route('emails.index');
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $item = EmailSetting::find($id);
        $history = EmailHistory::select(DB::raw('COUNT(email) as count'),DB::raw('SUM(sended) as sended'),DB::raw('SUM(showed) as showed'))->where('email_setting_id','=',$id)->first();
        $paginate = EmailHistory::where('email_setting_id','=',$id)->paginate(10);

        return view('crm::views.admin.emails.item')->with(array('item'=>$item,'history'=>$history,'paginate'=>$paginate));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $filters = Filter::select('id','internal_name')->where('show_in','like','%newsletter%')->get();
        $templates = Newsletter::all();
        $setting = EmailSetting::find($id);
        return view('crm::views.admin.emails.edit')->with(array('setting'=>$setting,'id'=>$id,'filters'=>$filters,'templates'=>$templates));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        if($this->loggedUser->can('email.edit')){

            $log = new LogLibrary('EmailController', 'update', Auth::user()->id, null, null, serialize($request->input()), null);
            $log->storeLog();

            $data = $request->all();

            $insertData = array(
                'internal_name' => $data['internal_name'],
                'template_id' => $data['template_id'],
                'filter_id' => $data['filter_id'],
                'date_send_at' => date('Y-m-d H:i:s',strtotime($data['date_send_at'].'00:00:00')),
                'hour_send_at' => $data['hour_send_at'],
                'updated_at' => date('Y-m-d H:i:s'),
            );

            $eh = EmailHistory::where('email_setting_id','=',$id)->delete();
            $es = EmailSetting::where('id','=',$id)->update($insertData);

            $filter = Filter::find($data['filter_id']);

            $filterData = DB::select($filter['sql']);

            if(isset($filterData) && !empty($filterData)){
                $history = array();
                foreach($filterData as $fd){
                    $insertHistory = array(
                        'email' => $fd->email,
                        'template_id' => $data['template_id'],
                        'email_setting_id' => $id,
                        'sended' => '0',
                        'showed' => '0',
                        'response' => '',
                        'created_at' => date('Y-m-d H:i:s')
                    );

                    $history[] = $insertHistory;
                }

                if(!empty($history)){
                    $ehres = EmailHistory::insert($history);
                }
            }


            return redirect()->route('emails.index');
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        if($this->loggedUser->can('email.delete')){

            $log = new LogLibrary('EmailController', 'delete', Auth::user()->id, null, null, serialize(array('id'=>$id)), null);
            $log->storeLog();

            EmailSetting::where('id','=',$id)->update(array('updated_at'=>date('Y-m-d H:i:s'),'deleted'=>'1'));
            return redirect()->route('emails.index');
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }
}
