<?php

namespace Modules\Crm\Http\Controllers\Crm;

use App\Libraries\LogLibrary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Modules\Crm\Http\Controllers\BaseController;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ($this->loggedUser->can('role.show')) {
            $roles = Role::orderBy('role_order','asc')->get();
            return view('crm::views.admin.roles')->with(array('roles' => $roles));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ($this->loggedUser->can('role.create')) {
            $permissions      = Permission::all();
            $permissionsArray = array();
            if (isset($permissions) && !empty($permissions)) {
                foreach ($permissions as $permission) {
                    $epl                                = explode('.', $permission['name']);
                    $permissionsArray[$epl[0]][$epl[1]] = $permission['id'];
                }
            }
            return view('crm::views.admin.partials.roles.create')->with(array('permissions' => $permissionsArray));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->loggedUser->can('role.create')) {
            $log = new LogLibrary('RoleController', 'store', Auth::user()->id, null, null, serialize($request->input()), null);
            $log->storeLog();

            $request->validate([
                'name' => 'required'
            ]);

            $data    = $request->all();
            $role    = array(
                'name' => $data['name'],
                'guard_name' => 'web',
                'created_at' => date('Y-m-d H:i:s'),
            );
            $role_id = Role::insertGetId($role);

            if (isset($data['permissions']) && !empty($data['permissions'])) {
                foreach ($data['permissions'] as $permission) {
                    $insert = array(
                        'role_id' => $role_id,
                        'permission_id' => $permission,
                    );
                    DB::table('role_has_permissions')->insert($insert);
                }
            }
            return redirect(route('roles.index'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($type)
    {
        if ($this->loggedUser->can('role.show')) {
            switch($type){
                case 'order':
                    return $this->order();
                    break;
                default:
                    return redirect()->route('roles.index');
            }
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    public function order(){
        return view('crm::views.admin.partials.roles.order')->with(array('roles'=>Role::orderBy('role_order','asc')->get()));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($this->loggedUser->can('role.edit')) {
            $permissions      = Permission::all();
            $permissionsArray = array();
            $role             = Role::where('id', '=', $id)->get();
            if (isset($permissions) && !empty($permissions)) {
                foreach ($permissions as $permission) {
                    $epl                                = explode('.', $permission['name']);
                    $permissionsArray[$epl[0]][$epl[1]] = $permission['name'];
                }
            }
            $allPerms       = Role::find($id)->getAllPermissions();
            $allPermissions = array();
            if ($allPerms) {
                $allPerms = $allPerms->toArray();
                foreach ($allPerms as $onePerm) {
                    $allPermissions[] = $onePerm['name'];
                }
            }

            return view('crm::views.admin.partials.roles.edit')->with(array('permissions' => $permissionsArray, 'role' => $role['0'], 'allPermissions' => $allPermissions));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($this->loggedUser->can('role.edit')) {
            $log = new LogLibrary('RoleController', 'update', Auth::user()->id, null, null, serialize($request->input()), serialize(Role::find($id)));
            $log->storeLog();

            $request->validate([
                'id' => 'required',
                'name' => 'required'
            ]);

            $data = $request->all();
            $role = Role::find($data['id']);
            $role->update(array('name' => $data['name']));

            DB::table('role_has_permissions')->where('role_id', '=', $data['id'])->delete();
            if (isset($data['permissions']) && !empty($data['permissions'])) {
                $role->givePermissionTo($data['permissions']);
            }

            return (redirect(route('roles.index')));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->loggedUser->can('role.delete')) {
            $log = new LogLibrary('RoleController', 'destroy', Auth::user()->id, null, null, $id, null);
            $log->storeLog();

            Role::where('id', '=', $id)->delete();
            DB::table('role_has_permissions')->where('role_id', '=', $id)->delete();
            return (redirect(route('roles.index')));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    public function reorder(Request $request){
        if($request->has('json')){
            $data = $request->json;
            foreach($data as $order => $d){
                Role::where('id','=',$d['id'])->update(array('role_order'=>$order));
            }

            echo response()->json(array('status'=>'1'));
        }
        else{
            echo response()->json(array('status'=>'0'));
        }
    }
}
