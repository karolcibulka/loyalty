<?php

namespace Modules\Crm\Http\Controllers\Crm;

use App\Libraries\MarketingLibrary;
use App\Models\Crm\CrmSetting;
use App\Models\Crm\CustomerUser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Session;
use Modules\Crm\Http\Controllers\BaseController;
use Newsletter;

class MailChimpController extends BaseController
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

    protected $marketingLibrary;

    public function __construct()
    {
        parent::__construct();
        $this->marketingLibrary = new MarketingLibrary();
    }

    public function index()
    {
        if ($this->loggedUser->can('mailchimp.show')) {

            $apikey = env('MAILCHIMP_APIKEY');
            $listid = env('MAILCHIMP_LIST_ID');

            $message = '';
            if(is_null($apikey)){
                $message.='<li>Nie je dustupný api-key pre integráciu s MailChimp</li>';
            }
            if(is_null($listid)){
                $message.='<li>Nie je dustupný list-id pre integráciu s MailChimp</li>';
            }
            if(is_null($apikey) || is_null($listid)){
                $message .='<li><strong>Kontaktujte administrátora</strong></li>';
            }

            $crmSettings = CrmSetting::first();
            if($crmSettings['mailchimp'] != '1'){
                return view('crm::views.admin.mailchimp.mainWithoutSettings');
            }
            else {
                if ($message != '') {
                    $data = array(
                        'message' => $message
                    );
                } else {
                    $customers           = CustomerUser::where('customer_id', '!=', '')->where('newsletter', '=', '1')->paginate(15);
                    $subscribedCustomers = $this->marketingLibrary->getAllSubscribers();

                    $subscribedCustomers = $this->marketingLibrary->makePrettyArrayOfMembers($subscribedCustomers);


                    $data = array(
                        'message' => $message,
                        'customers' => $customers,
                        'subscribedCustomers' => $subscribedCustomers,
                    );
                }

                return view('crm::views.admin.mailchimp.main')->with($data);
            }
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }



    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('crm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('crm::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($val)
    {
        if ($this->loggedUser->can('mailchimp.edit')) {
            $crmSettings = CrmSetting::first();

            $crmSettings->update(array('mailchimp' => $val));

            if ($val == 0) {
                return redirect()->route('mailchimp.sync', 0);
            }

            return redirect()->route('mailchimp.index');
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function sync($operator){
        if ($this->loggedUser->can('mailchimp.show')) {

            $customers = CustomerUser::where('customer_id', '!=', '')->where('newsletter', '=', '1')->get();

            $this->marketingLibrary->updateAllCustomers($customers,$operator == 0 ? 'unsubscribed' : 'subscribed');


            return redirect()->route('mailchimp.index');
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    public function changeSubscribe($customerID,$operator){
        if ($this->loggedUser->can('mailchimp.show')) {
            $customer = CustomerUser::where('id', '=', $customerID)->first();

            if ($operator == 0) {

                $this->marketingLibrary->removeCustomerFromMailChimp($customer);
            } else {
                $this->marketingLibrary->addCustomerToMailChimp($customer);
            }

            return redirect()->route('mailchimp.index');
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }
}
