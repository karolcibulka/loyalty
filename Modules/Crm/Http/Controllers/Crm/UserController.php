<?php

namespace Modules\Crm\Http\Controllers\Crm;

use App\Libraries\LogLibrary;
use App\Models\External\ModelHasRole;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Modules\Crm\Http\Controllers\BaseController;
use Spatie\Permission\Models\Role;

class UserController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        parent::__construct();
    }


    public function index(Request $request)
    {
        if ($this->loggedUser->can('user.show')) {

            $userRoles = ModelHasRole::where('model_id','=',$this->loggedUser->id)
                ->join('roles as r','r.id','=','model_has_roles.role_id')
                ->orderBy('r.role_order','asc')
                ->get();

            $max_role = 0;
            if(!empty($userRoles)){
                $userRoles = $userRoles->toArray();
                $max_role = last($userRoles)['role_order'];
            }

            $users = User::select('users.*','r.name as roleName','r.id as roleID','r.role_order as roleOrder')
                ->orderBy('r.role_order', 'asc')
                ->leftJoin('model_has_roles as mhr','mhr.model_id','=','users.id')
                ->leftJoin('roles as r','r.id','=','mhr.role_id')
                ->get();

            $response = array();
            if(!empty($users)){
                foreach($users as $u){
                    $response[$u['id']]['id'] = $u['id'];
                    $response[$u['id']]['email'] = $u['email'];
                    $response[$u['id']]['name'] = $u['name'];
                    $response[$u['id']]['created_at'] = $u['created_at'];
                    $response[$u['id']]['roles_names'][$u['roleID']] = $u['roleName'];
                    $response[$u['id']]['roles_ids'][$u['roleID']] = $u['roleID'];
                    $response[$u['id']]['roles_order'][$u['roleID']] = $u['roleOrder'];
                }
            }

            return view('crm::views.admin.users', ['users'=>$response,'max_role'=>$max_role]);
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ($this->loggedUser->can('user.create')) {

            $userRoles = ModelHasRole::where('model_id','=',$this->loggedUser->id)
                ->join('roles as r','r.id','=','model_has_roles.role_id')
                ->orderBy('r.role_order','asc')
                ->get();

            $max_role = 0;
            if(!empty($userRoles)){
                $userRoles = $userRoles->toArray();
                $max_role = last($userRoles)['role_order'];
            }

            $roles = Role::orderBy('role_order','asc')->where('role_order','<=',$max_role)->get();

            return view('crm::views.admin.partials.users.create',['roles'=>$roles,'max_role'=>$max_role]);
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->loggedUser->can('user.create')) {
            $log = new LogLibrary('UserController', 'store', Auth::user()->id, null, null, serialize($request->input()));
            $log->storeLog();


            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|email|unique:users,email',
                'password' => 'required|same:confirm-password',
            ]);

            $input             = $request->all();
            $input['password'] = Hash::make($input['password']);

            $user = User::create($input);
            $user->assignRole($request->input('roles'));

            return redirect()->route('users.index')
                ->with('success', 'User created successfully');
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($this->loggedUser->can('user.show')) {
            $user = User::find($id);
            return view('crm::users.show', compact('user'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($this->loggedUser->can('user.edit')) {
            $user     = User::find($id);

            $userRolesExtend = ModelHasRole::where('model_id','=',$id)
                ->join('roles as r','r.id','=','model_has_roles.role_id')
                ->orderBy('r.role_order','asc')
                ->get();

            //dd($userRolesExtend);

            $userRoles = ModelHasRole::where('model_id','=',$this->loggedUser->id)
                ->join('roles as r','r.id','=','model_has_roles.role_id')
                ->orderBy('r.role_order','asc')
                ->get();

            $userRole = array();
            $max_role = 0;
            if(!empty($userRoles)){
                $userRoles = $userRoles->toArray();
                foreach($userRoles as $userR){
                    $userRole[] = $userR['role_id'];
                }
                $max_role = last($userRoles)['role_order'];

                $userRolesExtend = $userRolesExtend->toArray();

                if(last($userRolesExtend)['role_order']>$max_role){
                    return redirect()->route('home');
                }
            }



            $roles = Role::orderBy('role_order','asc')->where('role_order','<=',$max_role)->get();


            return view('crm::views.admin.partials.users.edit', compact('user', 'roles', 'userRole','max_role'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($this->loggedUser->can('user.edit')) {
            $user = User::find($id);

            $log = new LogLibrary('UserController', 'update', Auth::user()->id, null, null, serialize($request->input()), serialize($user));
            $log->storeLog();

            $this->validate($request, [
                'name' => 'required',
            ]);

            $input = $request->all();

            $user = User::find($id);
            $user->update($input);

            ModelHasRole::where('model_id', $id)->delete();

            $user->assignRole($request->input('roles'));

            return redirect()->route('users.index')
                ->with('success', 'User updated successfully');
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->loggedUser->can('user.delete')) {
            $log = new LogLibrary('UserController', 'destroy', Auth::user()->id, null, null, $id, null);
            $log->storeLog();

            User::find($id)->delete();
            return redirect()->route('users.index')
                ->with('success', 'User deleted successfully');
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }
}
