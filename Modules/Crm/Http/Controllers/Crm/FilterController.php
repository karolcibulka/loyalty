<?php

namespace Modules\Crm\Http\Controllers\Crm;

use App\Libraries\LogLibrary;
use App\Models\Crm\Filter;
use App\Models\External\Country;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Modules\Crm\Http\Controllers\BaseController;

class FilterController extends BaseController
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if ($this->loggedUser->can('filter.show')) {

            $filters = Filter::where('active', '=', '1')->where('deleted', '=', '0')->paginate(10);

            return view('crm::views.admin.filters.show')->with(array('filters' => $filters));

        } else {
            Session::flash('permission', 'permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        if ($this->loggedUser->can('filter.create')) {

            $countries = Country::all();
            return view('crm::views.admin.filters.create')->with(array('countries' => $countries));
        } else {
            Session::flash('permission', 'permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        if ($this->loggedUser->can('filter.create')) {

            $log = new LogLibrary('FilterController', 'store', Auth::user()->id, null, null, serialize($request->input()), null);
            $log->storeLog();

            $data = $request->all();

            $raw = array();
            if (isset($data['parenthesis-left']) && !empty($data['parenthesis-left'])) {
                foreach ($data['parenthesis-left'] as $important_key => $parenthesisleft) {
                    $raw[$important_key]['parenthesis-left']  = $data['parenthesis-left'][$important_key];
                    $raw[$important_key]['rule-model']        = $data['rule-model'][$important_key];
                    $raw[$important_key]['rule-operator']     = $data['rule-operator'][$important_key];
                    $raw[$important_key]['main-value']        = $data['main-value'][$important_key];
                    $raw[$important_key]['parenthesis-right'] = $data['parenthesis-right'][$important_key];
                    $raw[$important_key]['next-condition']    = $data['next-condition'][$important_key];
                    $raw[$important_key]['rule-model-value']  = $data['rule-model-value'][$important_key];
                }
            }
            $insertData = array(
                'internal_name' => $data['internal_name'],
                'description' => $data['description'],
                'sql' => $data['sql'],
                'raw' => serialize($raw),
                'show_in' => $data['show_in'],
                'active' => '1',
                'deleted' => '0',
                'created_at' => date('Y-m-d H:i:s')
            );

            Filter::insert($insertData);

            return redirect()->route('filters.index');
        } else {
            Session::flash('permission', 'permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        if ($this->loggedUser->can('filter.show')) {
            $filter   = Filter::find($id);
            $response = DB::select($filter['sql']);

            return view('crm::views.admin.filters.item')->with(array('response' => $response, 'filter' => $filter));
        } else {
            Session::flash('permission', 'permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        if ($this->loggedUser->can('filter.edit')) {
            $filter    = Filter::find($id);
            $countries = Country::all();

            $response        = $filter->toArray();
            $response['raw'] = unserialize($response['raw']);

            //dd($response);

            return view('crm::views.admin.filters.edit')->with(array('filter' => $response, 'id' => $id, 'countries' => $countries));
        } else {
            Session::flash('permission', 'permissionMessage');
            return redirect()->route('home');
        }

    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {

        if ($this->loggedUser->can('filter.edit')) {

            $log = new LogLibrary('FilterController', 'update', Auth::user()->id, null, null, serialize($request->input()), null);
            $log->storeLog();

            $data = $request->all();

            $raw = array();
            if (isset($data['parenthesis-left']) && !empty($data['parenthesis-left'])) {
                foreach ($data['parenthesis-left'] as $important_key => $parenthesisleft) {
                    $raw[$important_key]['parenthesis-left']  = $data['parenthesis-left'][$important_key];
                    $raw[$important_key]['rule-model']        = $data['rule-model'][$important_key];
                    $raw[$important_key]['rule-operator']     = $data['rule-operator'][$important_key];
                    $raw[$important_key]['main-value']        = $data['main-value'][$important_key];
                    $raw[$important_key]['parenthesis-right'] = $data['parenthesis-right'][$important_key];
                    $raw[$important_key]['next-condition']    = $data['next-condition'][$important_key];
                    $raw[$important_key]['rule-model-value']  = $data['rule-model-value'][$important_key];
                }
            }

            $updateData = array(
                'internal_name' => $data['internal_name'],
                'description' => $data['description'],
                'sql' => $data['sql'],
                'show_in' => $data['show_in'],
                'raw' => serialize($raw),
                'updated_at' => date('Y-m-d H:i:s')
            );

            Filter::find($id)->update($updateData);

            return redirect()->route('filters.index');

        } else {
            Session::flash('permission', 'permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        if ($this->loggedUser->can('filter.delete')) {

            $log = new LogLibrary('FilterController', 'destroy', Auth::user()->id, null, null, serialize(array('id'=>$id)), null);
            $log->storeLog();

            Filter::find($id)->update(array('deleted' => '1', 'updated_at' => date('Y-m-d H:i:s')));
            return redirect()->route('filters.index');
        }
        else{
            Session::flash('permission', 'permissionMessage');
            return redirect()->route('home');
        }
    }
}
