<?php

namespace Modules\Crm\Http\Controllers\Crm;

use App\Libraries\LogLibrary;
use App\Models\System\Log;
use App\Models\Developer\Controller;
use App\User;
use Ejarnutowski\LaravelApiKey\Models\ApiKey;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Modules\Crm\Http\Controllers\BaseController;

class LogController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        parent::__construct();
    }


    public function index(Request $request)
    {
        if ($this->loggedUser->can('log.show')) {
            $data = $request->all();
            if (isset($data['page'])) {
                unset($data['page']);
            }
            if (!empty($data)) {
                $logRecords = Log::select('logs.*', 'users.name as user_name', 'api_keys.name as api_name');
                $logRecords->where('logs.created_at', '>=', date('Y-m-d H:i:s', strtotime($data['created_from'] . ' 00:00:00')));
                $logRecords->where('logs.created_at', '<=', date('Y-m-d H:i:s', strtotime($data['created_to'] . ' 00:00:00')));
                if (isset($data['api']) && !is_null($data['api'])) {
                    $logRecords->where('logs.api', '=', $data['api']);
                }
                if (isset($data['api']) && !is_null($data['api']) && $data['api'] == '1' && isset($data['api_id']) && !is_null($data['api_id'])) {
                    $logRecords->where('api_keys.id', '=', $data['api_id']);
                }
                if (isset($data['api']) && !is_null($data['api']) && $data['api'] == '0' && isset($data['user_id']) && !is_null($data['user_id'])) {
                    $logRecords->where('users.id', '=', $data['user_id']);
                }
                if (isset($data['api']) && !is_null($data['api']) && $data['api'] == '0' && isset($data['controller']) && !empty($data['controller']) && !is_null($data['controller'])) {
                    $logRecords->where('logs.controller', '=', $data['controller']);
                }
                $logRecords->orderBy('logs.created_at', 'desc');
                $logRecords->leftJoin('users', 'users.id', '=', 'logs.user_id');
                $logRecords->leftJoin('api_keys', 'api_keys.id', '=', 'logs.api_id');
                $logs = $logRecords->paginate(15);
            } else {
                $logs = Log::select('logs.*', 'users.name as user_name', 'api_keys.name as api_name')
                    ->orderBy('logs.created_at', 'desc')
                    ->leftJoin('users', 'users.id', '=', 'logs.user_id')
                    ->leftJoin('api_keys', 'api_keys.id', '=', 'logs.api_id')
                    ->paginate(15);
            }

            $apis               = ApiKey::all();
            $users              = User::all();
            $controllersRecords = Controller::all();
            $controllers        = array();
            if (!is_null($controllersRecords)) {
                foreach ($controllersRecords as $key => $record) {
                    $controllers[$key]['id']   = $record['id'];
                    $controllers[$key]['name'] = explode('\\', $record['controller'])[1];
                }
            }

            return view('crm::views.admin.logs.show')->with(array('logs' => $logs->appends($data), 'apis' => $apis, 'users' => $users, 'filter' => $data, 'controllers' => $controllers));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($this->loggedUser->can('log.show')) {
            $log = Log::find($id);
            return view('crm::views.admin.logs.item')->with(array('log'=>$log));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
