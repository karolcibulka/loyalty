<?php

namespace Modules\Crm\Http\Controllers\Crm;

use App\Models\Crm\Newsletter;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Session;
use Modules\Crm\Http\Controllers\BaseController;

class NewsletterController extends BaseController
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if($this->loggedUser->can('newsletter.show')){

            $templates = Newsletter::paginate(10);
            return view('crm::views.admin.newsletters.show')->with(array('templates'=>$templates));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        if($this->loggedUser->can('newsletter.show')) {
            return view('crm::views.admin.newsletters.create');
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        if($this->loggedUser->can('newsletter.create')) {
            $data = $request->all();

            $attachments = array();

            $insertData = array(
                'internal_name' => $data['internal_name'],
                'subject' => $data['subject'],
                'created_at' => date('Y-m-d H:i:s'),
                'source' => $data['source']
            );

            if ($data['source'] == 'own') {
                $parsedData         = json_decode($data['ownSolution'], true);
                $insertData['mjml'] = $parsedData['mjml'];
                $insertData['html'] = $parsedData['html'];
            } elseif ($data['source'] == 'summernote') {
                $insertData['html'] = $data['summernote'];
            } else {
                //external
                $insertData['html'] = json_decode($data['codemirror'],true);
            }


            if($request->hasFile('attachment')){
                foreach($request->attachment as $file){
                    $destinationPath = public_path('uploads');
                    if (!is_dir($destinationPath)) {
                        mkdir($destinationPath, 0777, true);
                        chmod($destinationPath, 0777);
                    }

                    $name = uniqid() . '_' . time() . '.' . $file->extension();
                    $file->move($destinationPath, $name);

                    $attachments[] = $name;
                }
            }

            $insertData['attachments'] = json_encode($attachments);

            $res = Newsletter::insert($insertData);

            return redirect()->route('newsletters.index');
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        if($this->loggedUser->can('newsletter.show')) {
            //dd(env('APP_URL').asset('uploads/'.$id));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        if($this->loggedUser->can('newsletter.edit')) {
            $template = Newsletter::find($id);
            $response = $template;
            $response['attachments'] = json_decode($template['attachments'],true);
            return view('crm::views.admin.newsletters.edit')->with(array('template'=>$response,'id'=>$id));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        if($this->loggedUser->can('newsletter.edit')) {

            $data = $request->all();

            $attachments = array();

            $updateData = array(
                'internal_name' => $data['internal_name'],
                'updated_at' => date('Y-m-d H:i:s'),
                'subject' => $data['subject'],
                'source' => $data['source']
            );

            if(isset($data['allOldAttachments']) && !empty($data['allOldAttachments'])){

                $attachments = json_decode($data['allOldAttachments'],true);
            }

            if(isset($data['removeAttachments']) && !empty($data['removeAttachments'])){
                $exploded = explode(',',$data['removeAttachments']);
                foreach($exploded as $exploded_item){
                    $destinationPath = public_path('uploads');
                    $unset = $destinationPath.'/'.$exploded_item;
                    unset($unset);

                    if(isset($attachments) && !empty($attachments)){
                        foreach($attachments as $key => $attach){
                            if($attach == $exploded_item){
                                unset($attachments[$key]);
                            }
                        }
                    }

                }
            }

            if ($data['source'] == 'own') {
                $parsedData         = json_decode($data['ownSolution'], true);
                $updateData['mjml'] = $parsedData['mjml'];
                $updateData['html'] = $parsedData['html'];
            } elseif ($data['source'] == 'summernote') {
                $updateData['html'] = $data['summernote'];
            } else {
                $updateData['html'] = json_decode($data['codemirror'],true);
            }

            if($request->hasFile('attachment')){
                foreach($request->attachment as $file){
                    $destinationPath = public_path('uploads');
                    if (!is_dir($destinationPath)) {
                        mkdir($destinationPath, 0777, true);
                        chmod($destinationPath, 0777);
                    }

                    $name = uniqid() . '_' . time() . '.' . $file->extension();
                    $file->move($destinationPath, $name);


                    $attachments[] = $name;
                }
            }

            $updateData['attachments'] = json_encode($attachments);

            $res = Newsletter::where('id','=',$id)->update($updateData);

            return redirect()->route('newsletters.index');

        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
    if($this->loggedUser->can('newsletter.delete')) {
        Newsletter::find($id)->update(array('deleted' => '1', 'updated_at' => date('Y-m-d H:i:s')));
        return redirect()->route('home');
    }
    else{
        Session::flash('permission','permissionMessage');
        return redirect()->route('home');
    }
    }
}
