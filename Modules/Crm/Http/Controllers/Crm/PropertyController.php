<?php

namespace Modules\Crm\Http\Controllers\Crm;

use App\Libraries\LogLibrary;
use App\Models\Crm\Property;
use App\Models\Crm\PropertyCategory;
use App\Models\Crm\PropertyLang;
use App\Models\External\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Modules\Crm\Http\Controllers\BaseController;

class PropertyController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
        if ($this->loggedUser->can('property.show')) {
            $properties = Property::where('deleted', '=', '0')->paginate(10);
            return view('crm::views.admin.property')->with(array('properties' => $properties));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ($this->loggedUser->can('property.create')) {
            $languages  = Language::all();
            $categories = PropertyCategory::where('deleted', '=', '0')->where('active', '=', '1')->get();

            return view('crm::views.admin.partials.properties.create')->with(array('categories' => $categories, 'languages' => $languages));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->loggedUser->can('property.create')) {
            $log = new LogLibrary('PropertyController', 'store', Auth::user()->id, null, null, serialize($request->input()), null);
            $log->storeLog();

            $log = new LogLibrary('PropertyCategoryController', 'store', Auth::user()->id);
            $log->storeLog();


            $data = $request->all();

            $insertData['image']         = '';
            $insertData['internal_name'] = $data['internal_name'];
            $insertData['adress']        = $data['adress'];
            $insertData['zip']           = $data['zip'];
            $insertData['city']          = $data['city'];
            $insertData['state']         = $data['state'];
            $insertData['email']         = $data['email'];
            $insertData['phone']         = $data['phone'];
            $insertData['ico']           = $data['ico'];
            $insertData['dic']           = $data['dic'];
            $insertData['category_id']   = $data['category_id'];
            $insertData['active']        = '1';
            $insertData['deleted']       = '0';
            $insertData['created_at']    = date('Y-m-d H:i:s');

            if ($request->hasFile('image')) {
                $image               = $request->file('image');
                $insertData['image'] = uniqid() . '_' . time() . '.' . $image->extension();

                $destinationPath = public_path('images/properties');
                if (!is_dir($destinationPath)) {
                    mkdir($destinationPath, 0777, true);
                    chmod($destinationPath, 0777);
                }

                $img = Image::make($image->path());
                $img->encode('jpeg', '75')->save($destinationPath . '/' . $insertData['image']);
            }

            $categoryID = Property::insertGetId($insertData);

            $langs = array();
            if (isset($data['title']) && !empty($data['title'])) {
                foreach ($data['title'] as $lang => $title) {
                    if (!is_null($data['title'][$lang])) {
                        $insertLang = array(
                            'property_id' => $categoryID,
                            'code' => $lang,
                            'title' => $data['title'][$lang],
                            'short_description' => $data['short_description'][$lang],
                            'long_description' => $data['long_description'][$lang],
                            'slug' => isset($data['slug'][$lang]) && !empty($data['slug'][$lang]) ? Str::slug($data['slug'][$lang]) : Str::slug($data['title'][$lang]),
                            'created_at' => date('Y-m-d H:i:s')
                        );

                        $langs[] = $insertLang;
                    }
                }
            }

            if (!empty($langs)) {
                PropertyLang::insert($langs);
            }

            return redirect(route('properties.index'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($this->loggedUser->can('property.show')) {

        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($this->loggedUser->can('property.edit')) {
            $languages  = Language::all();
            $categories = PropertyCategory::where('deleted', '=', '0')->where('active', '=', '1')->get();
            $property   = Property::find($id);

            $properties = Property::where('properties.id', '=', $id)->leftJoin('property_langs', 'properties.id', '=', 'property_langs.property_id')->get();

            $response = array();
            if (!empty($properties)) {
                foreach ($properties as $pc) {
                    $response['id']                             = $pc['id'];
                    $response['image']                          = $pc['image'];
                    $response['adress']                         = $pc['adress'];
                    $response['city']                           = $pc['city'];
                    $response['state']                          = $pc['state'];
                    $response['category_id']                    = $pc['category_id'];
                    $response['zip']                            = $pc['zip'];
                    $response['ico']                            = $pc['ico'];
                    $response['dic']                            = $pc['dic'];
                    $response['email']                          = $pc['email'];
                    $response['phone']                          = $pc['phone'];
                    $response['internal_name']                  = $pc['internal_name'];
                    $response['slug'][$pc['code']]              = $pc['slug'];
                    $response['title'][$pc['code']]             = $pc['title'];
                    $response['short_description'][$pc['code']] = $pc['short_description'];
                    $response['long_description'][$pc['code']]  = $pc['long_description'];
                }
            }

            return view('crm::views.admin.partials.properties.edit')->with(array('property' => $response, 'categories' => $categories, 'languages' => $languages));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($this->loggedUser->can('property.edit')) {
            $properties = Property::where('properties.id', '=', $id)->leftJoin('property_langs', 'properties.id', '=', 'property_langs.property_id')->get();

            $response = array();
            if (!empty($properties)) {
                foreach ($properties as $pc) {
                    $response['id']                             = $pc['id'];
                    $response['image']                          = $pc['image'];
                    $response['adress']                         = $pc['adress'];
                    $response['city']                           = $pc['city'];
                    $response['state']                          = $pc['state'];
                    $response['category_id']                    = $pc['category_id'];
                    $response['zip']                            = $pc['zip'];
                    $response['ico']                            = $pc['ico'];
                    $response['dic']                            = $pc['dic'];
                    $response['email']                          = $pc['email'];
                    $response['phone']                          = $pc['phone'];
                    $response['internal_name']                  = $pc['internal_name'];
                    $response['slug'][$pc['code']]              = $pc['slug'];
                    $response['title'][$pc['code']]             = $pc['title'];
                    $response['short_description'][$pc['code']] = $pc['short_description'];
                    $response['long_description'][$pc['code']]  = $pc['long_description'];
                }
            }

            $log = new LogLibrary('PropertyController', 'update', Auth::user()->id, null, null, serialize($request->input()), serialize($response));
            $log->storeLog();

            $data = $request->all();

            $oldCategory                  = Property::find($id);
            $oldCategory['image']         = $data['old_image'];
            $oldCategory['internal_name'] = $data['internal_name'];
            $oldCategory['adress']        = $data['adress'];
            $oldCategory['zip']           = $data['zip'];
            $oldCategory['city']          = $data['city'];
            $oldCategory['state']         = $data['state'];
            $oldCategory['email']         = $data['email'];
            $oldCategory['phone']         = $data['phone'];
            $oldCategory['ico']           = $data['ico'];
            $oldCategory['dic']           = $data['dic'];
            $oldCategory['category_id']   = $data['category_id'];
            $oldCategory['updated_at']    = date('Y-m-d H:i:s');

            $destinationPath = public_path('images/properties');

            if (isset($data['removeOldImage']) && !empty($data['removeOldImage'])) {
                if (isset($data['oldImage']) && !empty($data['oldImage'])) {
                    File::delete($destinationPath . '/' . $data['oldImage']);
                }
            }

            if ($request->hasFile('image')) {
                $image                = $request->file('image');
                $oldCategory['image'] = uniqid() . '_' . time() . '.' . $image->extension();


                if (!is_dir($destinationPath)) {
                    mkdir($destinationPath, 0777, true);
                    chmod($destinationPath, 0777);
                }

                $img = Image::make($image->path());
                $img->encode('jpeg', '75')->save($destinationPath . '/' . $oldCategory['image']);
            }

            $oldCategory->update();

            //langs

            $langs = array();
            if (isset($data['title']) && !empty($data['title'])) {
                PropertyLang::where('property_id', '=', $id)->delete();
                foreach ($data['title'] as $lang => $title) {
                    if (!is_null($data['title'][$lang])) {
                        $insertLang = array(
                            'property_id' => $id,
                            'code' => $lang,
                            'title' => $data['title'][$lang],
                            'short_description' => $data['short_description'][$lang],
                            'long_description' => $data['long_description'][$lang],
                            'slug' => isset($data['slug'][$lang]) && !empty($data['slug'][$lang]) ? Str::slug($data['slug'][$lang]) : Str::slug($data['title'][$lang]),
                            'created_at' => date('Y-m-d H:i:s')
                        );

                        $langs[] = $insertLang;
                    }
                }
            }

            if (!empty($langs)) {
                PropertyLang::insert($langs);
            }

            return redirect(route('properties.index'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->loggedUser->can('property.delete')) {
            $log = new LogLibrary('PropertyController', 'destroy', Auth::user()->id, null, null, $id, null);
            $log->storeLog();


            Property::where('id', '=', $id)->update(array('deleted' => '1'));
            return redirect(route('properties.index'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    public function changeActivityProperty(Request $request)
    {

        if ($this->loggedUser->can('property.edit')) {
            $log = new LogLibrary('PropertyController', 'activity', Auth::user()->id, null, null, serialize($request->input()));
            $log->storeLog();

            $data = $request->all();

            $updateData = array(
                'active' => $data['value']
            );
            Property::where('id', '=', $data['id'])->update($updateData);

            $response = array(
                'status' => '1'
            );

            echo json_encode($response);
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }
}
