<?php

namespace Modules\Crm\Http\Controllers\Crm;

use App\Http\Controllers\Controller;
use App\Libraries\LogLibrary;
use App\Models\Crm\CustomerGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Modules\Crm\Http\Controllers\BaseController;

class CustomerGroupController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
        if ($this->loggedUser->can('customergroup.show')) {
            $customer_groups = CustomerGroup::where('deleted', '=', '0')->paginate(15);
            return view('crm::views.admin.customer_groups')->with(array('customer_groups' => $customer_groups));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ($this->loggedUser->can('customergroup.create')) {
            return view('crm::views.admin.partials.customergroups.create');
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->loggedUser->can('customergroup.create')) {
            $log = new LogLibrary('CustomerGroupController', 'store', Auth::user()->id, null, null, serialize($request->input()), null);
            $log->storeLog();

            $data = $request->all();

            $insertData = array(
                'internal_name' => $data['internal_name'],
                'created_at' => date('Y-m-d H:i:s'),
                'active' => '1',
                'deleted' => '0'
            );

            CustomerGroup::insert($insertData);

            return redirect(route('customergroup'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($this->loggedUser->can('customergroup.show')) {

        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($this->loggedUser->can('customergroup.edit')) {
            $group = CustomerGroup::find($id);
            return view('crm::views.admin.partials.customergroups.edit')->with(array('group' => $group));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($this->loggedUser->can('customergroup.edit')) {
            $log = new LogLibrary('CustomerGroupController', 'update', Auth::user()->id, null, null, serialize($request->input()), serialize($group = CustomerGroup::find($id)));
            $log->storeLog();

            $data   = $request->all();
            $update = array(
                'internal_name' => $data['internal_name']
            );

            CustomerGroup::where('id', '=', $id)->update($update);

            return redirect(route('customergroup'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->loggedUser->can('customergroup.delete')) {
            $log = new LogLibrary('CustomerGroupController', 'destroy', Auth::user()->id, null, null, $id, null);
            $log->storeLog();


            CustomerGroup::where('id', '=', $id)->update(array('deleted' => '1'));
            return redirect(route('customergroup'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    public function change_default_group(Request $request)
    {
        if ($this->loggedUser->can('customergroup.edit')) {
            $log = new LogLibrary('CustomerGroupController', 'activity', Auth::user()->id, null, null, serialize($request->input()), null);
            $log->storeLog();

            $data              = $request->all();
            $old_default_group = CustomerGroup::where('is_default', '=', '1')->first();
            if (!is_null($old_default_group)) {
                $old_default_group['is_default'] = '0';
                $old_default_group->update();
            }

            CustomerGroup::where('id', '=', $data['id'])->update(array('is_default' => '1'));

            $response = array(
                'status' => '1',
            );

            echo json_encode($response);
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }
}
