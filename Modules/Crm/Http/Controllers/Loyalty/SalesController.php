<?php

namespace Modules\Crm\Http\Controllers\Loyalty;

use App\Libraries\LogLibrary;
use App\Models\Crm\Property;
use App\Models\Loyalty\Sale;
use App\Models\Loyalty\SaleDate;
use App\Models\Loyalty\SaleLang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Modules\Crm\Http\Controllers\BaseController;

class SalesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if ($this->loggedUser->can('sale.show')) {
            $sales = Sale::where('deleted', '=', '0')->paginate(15);
            return view('crm::views.loyalty.sales.show', ['sales' => $sales]);
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ($this->loggedUser->can('sale.create')) {
            $properties = Property::where('deleted', '=', '0')->where('active', '=', '1')->get();
            return view('crm::views.loyalty.sales.create', ['properties' => $properties]);
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->loggedUser->can('sale.create')) {
            $log = new LogLibrary('SalesController', 'store', Auth::user()->id, null, null, serialize($request->input()), null);
            $log->storeLog();

            $data = $request->all();

            $insert_data = array(
                'internal_name' => $data['internal_name'],
                'points' => $data['points'],
                'price' => $data['price'],
                'visible_from' => date('Y-m-d H:i:s', strtotime($data['showed_from'])),
                'visible_to' => date('Y-m-d H:i:s', strtotime($data['showed_to'])),
                'sales_restrict' => $data['sales_restrict'],
                'sales_restrict_count' => isset($data['sales_restrict_count']) && !empty($data['sales_restrict_count']) ? $data['sales_restrict_count'] : 0,
                'available' => 0,
                'properties' => isset($data['properties']) && !empty($data['properties']) ? serialize($data['properties']) : '',
                'deleted' => '0',
                'created_at' => date('Y-m-d H:i:s'),
                'image' => null,
                'active' => $data['active'],
                'add_points' => $data['add_points'],
            );
            if($request->cropper_used == '1'){
                if(!empty($request->cropper_value)) {

                    $base = $request->cropper_value;

                    $extension = explode(';', $base);
                    $extension = explode('/', $extension[0]);
                    $extension = $extension[1];

                    $imageName = uniqid() . '_' . time() . '.' . $extension;

                    $base64Str = substr($base, strpos($base, ",") + 1);
                    $image_base     = base64_decode($base64Str);

                    $destinationPath = public_path('images/thumb');
                    $img             = Image::make($image_base);
                    $img->resize(100, 100, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($destinationPath . '/' . $imageName);

                    $destinationPath = public_path('images/medium');
                    $img2            = Image::make($image_base);
                    $img2->resize(500, 500, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($destinationPath . '/' . $imageName);

                    $destinationPath = public_path('images/large');
                    $img3            = Image::make($image_base);
                    $img3->save($destinationPath . '/' . $imageName);

                    $insert_data['image'] = $imageName;
                }
            }
            elseif ($request->hasFile('image')) {
                $image = $request->file('image');

                $imageName = uniqid() . '_' . time() . '.' . $image->extension();

                $destinationPath = public_path('images/thumb');
                $img             = Image::make($image->path());
                $img->resize(100, 100, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/' . $imageName);


                $destinationPath = public_path('images/medium');
                $img2            = Image::make($image->path());
                $img2->resize(500, 500, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/' . $imageName);

                $destinationPath = public_path('images/large');
                $img            = Image::make($image->path());
                $img->save($destinationPath . '/' . $imageName);
                $insert_data['image'] = $imageName;
            }

            $insertID = Sale::insertGetId($insert_data);

            //langs

            $langs = array();

            if (isset($data['name']) && !empty($data['name'])) {
                foreach ($data['name'] as $lang => $name) {
                    if (isset($data['name'][$lang]) && !empty($data['name'][$lang])) {
                        $insert_lang = array(
                            'sale_id' => $insertID,
                            'lang' => $lang,
                            'name' => $data['name'][$lang],
                            'short_description' => isset($data['short_description'][$lang]) && !empty($data['short_description'][$lang]) ? $data['short_description'][$lang] : '',
                            'long_description' => isset($data['long_description'][$lang]) && !empty($data['long_description'][$lang]) ? $data['long_description'][$lang] : '',
                            'slug' => isset($data['slug'][$lang]) && !empty($data['slug'][$lang]) ? Str::slug($data['slug'][$lang]) : Str::slug($data['name'][$lang]),
                            'created_at' => date('Y-m-d H:i:s')
                        );

                        $langs[] = $insert_lang;
                    }
                }
                if (!empty($langs)) {
                    SaleLang::insert($langs);
                }
            }

            $dates = array();

            if (isset($data['reservation_from']) && !empty($data['reservation_from'])) {
                foreach ($data['reservation_from'] as $key => $reservationFrom) {
                    if (isset($data['reservation_from'][$key]) && !empty($data['reservation_from'][$key]) && isset($data['reservation_to'][$key]) && !empty($data['reservation_to'][$key])) {
                        $insert_date = array(
                            'sale_id' => $insertID,
                            'from' => date('Y-m-d H:i:s', strtotime($data['reservation_from'][$key])),
                            'to' => date('Y-m-d H:i:s', strtotime($data['reservation_to'][$key])),
                            'created_at' => date('Y-m-d H:i:s')
                        );

                        $dates[] = $insert_date;
                    }
                }
                if (!empty($dates)) {
                    SaleDate::insert($dates);
                }
            }

            return redirect(route('sale'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($this->loggedUser->can('sale.show')) {

        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($this->loggedUser->can('sale.edit')) {
            $sales = Sale::where('sales.id', '=', $id)
                ->leftJoin('sale_dates', 'sale_dates.sale_id', '=', 'sales.id')
                ->leftJoin('sale_langs', 'sale_langs.sale_id', '=', 'sales.id')
                ->get();

            $response = array();
            foreach ($sales as $key => $sale) {
                $response['lang'][$sale['lang']]['name']              = $sale['name'];
                $response['lang'][$sale['lang']]['short_description'] = $sale['short_description'];
                $response['lang'][$sale['lang']]['long_description']  = $sale['long_description'];
                $response['lang'][$sale['lang']]['slug']              = $sale['slug'];
                $response['visible_from']                             = date('d.m.Y', strtotime($sale['visible_from']));
                $response['visible_to']                               = date('d.m.Y', strtotime($sale['visible_to']));
                $response['internal_name']                            = $sale['internal_name'];
                $response['sales_restrict_count']                     = $sale['sales_restrict_count'];
                $response['sales_restrict']                           = $sale['sales_restrict'];
                $response['image']                                    = isset($sale['image']) && !empty($sale['image']) ? asset('images/large/' . $sale['image']) : '';
                $response['img']                                      = $sale['image'];
                $response['points']                                   = $sale['points'];
                $response['price']                                    = $sale['price'];
                $response['slug']                                     = $sale['slug'];
                $response['currency']                                 = $sale['currency'];
                $response['active']                                   = $sale['active'];
                $response['add_points']                               = $sale['add_points'];
                $response['properties']                               = isset($sale['properties']) && !empty($sale['properties']) ? unserialize($sale['properties']) : '';
                $response['id']                                       = $sale['sale_id'];
                $response['from'][$sale['sale_date_id']]              = date('d.m.Y', strtotime($sale['from']));
                $response['to'][$sale['sale_date_id']]                = date('d.m.Y', strtotime($sale['to']));
            }

            $properties = Property::where('deleted', '=', '0')->where('active', '=', '1')->get();

            return view('crm::views.loyalty.sales.edit', ['sale' => $response, 'properties' => $properties]);
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($this->loggedUser->can('sale.edit')) {
            $sales = Sale::where('sales.id', '=', $id)
                ->leftJoin('sale_dates', 'sale_dates.sale_id', '=', 'sales.id')
                ->leftJoin('sale_langs', 'sale_langs.sale_id', '=', 'sales.id')
                ->get();

            $response = array();
            foreach ($sales as $key => $sale) {
                $response['lang'][$sale['lang']]['name']              = $sale['name'];
                $response['lang'][$sale['lang']]['short_description'] = $sale['short_description'];
                $response['lang'][$sale['lang']]['long_description']  = $sale['long_description'];
                $response['lang'][$sale['lang']]['slug']              = $sale['slug'];
                $response['visible_from']                             = date('d.m.Y', strtotime($sale['visible_from']));
                $response['visible_to']                               = date('d.m.Y', strtotime($sale['visible_to']));
                $response['internal_name']                            = $sale['internal_name'];
                $response['sales_restrict_count']                     = $sale['sales_restrict_count'];
                $response['sales_restrict']                           = $sale['sales_restrict'];
                $response['image']                                    = isset($sale['image']) && !empty($sale['image']) ? asset('images/large/' . $sale['image']) : '';
                $response['img']                                      = $sale['image'];
                $response['points']                                   = $sale['points'];
                $response['price']                                    = $sale['price'];
                $response['slug']                                     = $sale['slug'];
                $response['currency']                                 = $sale['currency'];
                $response['active']                                   = $sale['active'];
                $response['add_points']                               = $sale['add_points'];
                $response['properties']                               = isset($sale['properties']) && !empty($sale['properties']) ? unserialize($sale['properties']) : '';
                $response['id']                                       = $sale['sale_id'];
                $response['from'][$sale['sale_date_id']]              = date('d.m.Y', strtotime($sale['from']));
                $response['to'][$sale['sale_date_id']]                = date('d.m.Y', strtotime($sale['to']));
            }

            $log = new LogLibrary('SalesController', 'update', Auth::user()->id, null, null, serialize($request->input()), serialize($response));
            $log->storeLog();

            $data = $request->all();

            $update_data = array(
                'internal_name' => $data['internal_name'],
                'points' => $data['points'],
                'price' => $data['price'],
                'visible_from' => date('Y-m-d H:i:s', strtotime($data['showed_from'])),
                'visible_to' => date('Y-m-d H:i:s', strtotime($data['showed_to'])),
                'sales_restrict' => $data['sales_restrict'],
                'sales_restrict_count' => isset($data['sales_restrict_count']) && !empty($data['sales_restrict_count']) ? $data['sales_restrict_count'] : 0,
                'properties' => isset($data['properties']) && !empty($data['properties']) ? serialize($data['properties']) : '',
                'deleted' => '0',
                'created_at' => date('Y-m-d H:i:s'),
                'image' => $data['old_image'],
                'active' => $data['active'],
                'add_points' => $data['add_points'],
            );

            if ($data['remove_image'] == '1') {
                $thumb_path = public_path('images/thumb');
                if (file_exists($thumb_path . $data['old_image'])) {
                    File::delete($thumb_path . $data['old_image']);
                }
                $medium_path = public_path('images/medium');
                if (file_exists($medium_path . $data['old_image'])) {
                    File::delete($medium_path . $data['old_image']);
                }
                $update_data['image'] = '';
            }
            if($request->cropper_used == '1'){
                if(!empty($request->cropper_value)) {

                    $thumb_path = public_path('images/thumb/');
                    if (file_exists($thumb_path . $data['old_image'])) {
                        File::delete($thumb_path . $data['old_image']);
                    }
                    $medium_path = public_path('images/medium/');
                    if (file_exists($medium_path . $data['old_image'])) {
                        File::delete($medium_path . $data['old_image']);
                    }

                    $large_path = public_path('images/large/');
                    if (file_exists($large_path . $data['old_image'])) {
                        File::delete($large_path . $data['old_image']);
                    }

                    $base = $request->cropper_value;

                    $extension = explode(';', $base);
                    $extension = explode('/', $extension[0]);
                    $extension = $extension[1];

                    $imageName = uniqid() . '_' . time() . '.' . $extension;

                    $base64Str = substr($base, strpos($base, ",") + 1);
                    $image_base     = base64_decode($base64Str);

                    $destinationPath = public_path('images/thumb');
                    $img             = Image::make($image_base);
                    $img->resize(100, 100, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($destinationPath . '/' . $imageName);

                    $destinationPath = public_path('images/medium');
                    $img2            = Image::make($image_base);
                    $img2->resize(500, 500, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($destinationPath . '/' . $imageName);

                    $destinationPath = public_path('images/large');
                    $img3            = Image::make($image_base);
                    $img3->save($destinationPath . '/' . $imageName);

                    $update_data['image'] = $imageName;
                }
            }
            elseif ($request->hasFile('image')) {

                $thumb_path = public_path('images/thumb/');
                if (file_exists($thumb_path . $data['old_image'])) {
                    File::delete($thumb_path . $data['old_image']);
                }
                $medium_path = public_path('images/medium/');
                if (file_exists($medium_path . $data['old_image'])) {
                    File::delete($medium_path . $data['old_image']);
                }

                $image = $request->file('image');

                $imageName = uniqid() . '_' . time() . '.' . $image->extension();

                $destinationPath = public_path('images/thumb');
                $img             = Image::make($image->path());
                $img->resize(100, 100, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/' . $imageName);


                $destinationPath = public_path('images/medium');
                $img2            = Image::make($image->path());
                $img2->resize(500, 500, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/' . $imageName);
                $update_data['image'] = $imageName;
            }

            Sale::where('id', '=', $id)->update($update_data);

            $langs = array();

            if (isset($data['name']) && !empty($data['name'])) {
                SaleLang::where('sale_id', '=', $id)->delete();
                foreach ($data['name'] as $lang => $name) {
                    if (isset($data['name'][$lang]) && !empty($data['name'][$lang])) {
                        $insert_lang = array(
                            'sale_id' => $id,
                            'lang' => $lang,
                            'name' => $data['name'][$lang],
                            'short_description' => isset($data['short_description'][$lang]) && !empty($data['short_description'][$lang]) ? $data['short_description'][$lang] : '',
                            'long_description' => isset($data['long_description'][$lang]) && !empty($data['long_description'][$lang]) ? $data['long_description'][$lang] : '',
                            'slug' => isset($data['slug'][$lang]) && !empty($data['slug'][$lang]) ? Str::slug($data['slug'][$lang]) : Str::slug($data['name'][$lang]),
                            'created_at' => date('Y-m-d H:i:s')
                        );

                        $langs[] = $insert_lang;
                    }
                }

                if (!empty($langs)) {
                    SaleLang::insert($langs);
                }
            }

            $dates = array();
            if (isset($data['reservation_from']) && !empty($data['reservation_from'])) {
                SaleDate::where('sale_id', '=', $id)->delete();
                foreach ($data['reservation_from'] as $key => $reservationFrom) {
                    if (isset($data['reservation_from'][$key]) && !empty($data['reservation_from'][$key]) && isset($data['reservation_to'][$key]) && !empty($data['reservation_to'][$key])) {
                        $insert_date = array(
                            'sale_id' => $id,
                            'from' => date('Y-m-d H:i:s', strtotime($data['reservation_from'][$key])),
                            'to' => date('Y-m-d H:i:s', strtotime($data['reservation_to'][$key])),
                            'created_at' => date('Y-m-d H:i:s')
                        );

                        $dates[] = $insert_date;
                    }
                }

                if (!empty($dates)) {
                    SaleDate::insert($dates);
                }
            }

            return redirect(route('sale'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->loggedUser->can('sale.delete')) {
            $log = new LogLibrary('SalesController', 'destroy', Auth::user()->id, null, null, $id, null);
            $log->storeLog();

            $update = array(
                'deleted' => '1'
            );

            Sale::where('id', '=', $id)->update($update);
            return redirect(route('sale'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }


    public function changeActivitySale(Request $request)
    {
        if ($this->loggedUser->can('sale.edit')) {
            $log = new LogLibrary('SalesController', 'activity', Auth::user()->id, null, null, serialize($request->input()), null);
            $log->storeLog();

            $data = $request->all();

            if ($data['addpoints'] == '1') {
                Sale::where('id', '=', $data['id'])->update(array('add_points' => $data['value']));
            } else {
                Sale::where('id', '=', $data['id'])->update(array('active' => $data['value']));
            }

            echo json_encode(array('status' => '1'));
        } else {
            Session::flash('permission', 'permissionMessage');
            return redirect()->route('home');
        }
    }
}
