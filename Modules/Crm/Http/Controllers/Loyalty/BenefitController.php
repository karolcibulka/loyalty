<?php

namespace Modules\Crm\Http\Controllers\Loyalty;


use App\Libraries\LogLibrary;
use App\Models\External\Language;
use App\Models\Loyalty\Benefit;
use App\Models\Loyalty\BenefitLang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Modules\Crm\Http\Controllers\BaseController;

class BenefitController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
        if($this->loggedUser->can('benefit.show')) {
            $benefits = Benefit::where('deleted', '=', '0')->paginate(10);
            return view('crm::views.loyalty.benefits.show')->with(array('benefits' => $benefits));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if($this->loggedUser->can('benefit.create')) {
            $languages = Language::all();
            return view('crm::views.loyalty.benefits.partials.create')->with(array('languages' => $languages));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($this->loggedUser->can('benefit.create')) {
            $log = new LogLibrary('BenefitController', 'store', Auth::user()->id, null, null, serialize($request->input()), null);
            $log->storeLog();

            $data = $request->all();

            $insertData['image']         = '';
            $insertData['internal_name'] = $data['internal_name'];
            $insertData['created_at']    = date('Y-m-d H:i:s');

            if ($request->hasFile('image')) {
                $image               = $request->file('image');
                $insertData['image'] = uniqid() . '_' . time() . '.' . $image->extension();

                $destinationPath = public_path('images/benefits');
                if (!is_dir($destinationPath)) {
                    mkdir($destinationPath, 0777, true);
                    chmod($destinationPath, 0777);
                }

                $img = Image::make($image->path());
                $img->encode('jpeg', '75')->save($destinationPath . '/' . $insertData['image']);
            }

            $benefitID = Benefit::insertGetId($insertData);

            //langs
            if (isset($data['title']) && !empty($data['title'])) {
                foreach ($data['title'] as $lang => $title) {
                    if (!is_null($data['title'][$lang])) {
                        $insertLang = array(
                            'benefit_id' => $benefitID,
                            'code' => $lang,
                            'title' => $data['title'][$lang],
                            'short_description' => $data['short_description'][$lang],
                            'long_description' => $data['long_description'][$lang],
                            'slug' => isset($data['slug'][$lang]) && !empty($data['slug'][$lang]) ? Str::slug($data['slug'][$lang]) : Str::slug($data['title']['sk']),
                            'created_at' => date('Y-m-d H:i:s')
                        );

                        BenefitLang::insert($insertLang);
                    }
                }
            }

            return redirect(route('benefit'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($this->loggedUser->can('benefit.show')) {

        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if($this->loggedUser->can('benefit.edit')) {
            $languages = Language::all();

            $benefits = Benefit::leftJoin('benefit_langs', 'benefits.id', '=', 'benefit_langs.benefit_id')->where('benefits.id', '=', $id)->get();

            $response = array();
            if (!empty($benefits)) {
                foreach ($benefits as $b) {
                    $response['image']                         = $b['image'];
                    $response['internal_name']                 = $b['internal_name'];
                    $response['title'][$b['code']]             = $b['title'];
                    $response['slug'][$b['code']]              = $b['slug'];
                    $response['short_description'][$b['code']] = $b['short_description'];
                    $response['long_description'][$b['code']]  = $b['long_description'];

                }
            }

            return view('crm::views.loyalty.benefits.partials.edit')->with(array('languages' => $languages, 'id' => $id, 'benefit' => $response));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($this->loggedUser->can('benefit.edit')) {
            $benefits = Benefit::leftJoin('benefit_langs', 'benefits.id', '=', 'benefit_langs.benefit_id')->where('benefits.id', '=', $id)->get();

            $response = array();
            if (!empty($benefits)) {
                foreach ($benefits as $b) {
                    $response['image']                         = $b['image'];
                    $response['internal_name']                 = $b['internal_name'];
                    $response['title'][$b['code']]             = $b['title'];
                    $response['slug'][$b['code']]              = $b['slug'];
                    $response['short_description'][$b['code']] = $b['short_description'];
                    $response['long_description'][$b['code']]  = $b['long_description'];

                }
            }

            $log = new LogLibrary('BenefitController', 'update', Auth::user()->id, null, null, serialize($request->input()), serialize($response));
            $log->storeLog();

            $data = $request->all();


            $oldCategory                  = Benefit::find($id);
            $oldCategory['image']         = $data['old_image'];
            $oldCategory['internal_name'] = $data['internal_name'];
            $oldCategory['updated_at']    = date('Y-m-d H:i:s');

            $destinationPath = public_path('images/benefits');

            if (isset($data['remove_old_image']) && !empty($data['remove_old_image'])) {
                if (isset($data['old_image']) && !empty($data['old_image'])) {
                    File::delete($destinationPath . '/' . $data['old_image']);
                }
            }

            if ($request->hasFile('image')) {
                $image                = $request->file('image');
                $oldCategory['image'] = uniqid() . '_' . time() . '.' . $image->extension();


                if (!is_dir($destinationPath)) {
                    mkdir($destinationPath, 0777, true);
                    chmod($destinationPath, 0777);
                }

                $img = Image::make($image->path());
                $img->encode('jpeg', '75')->save($destinationPath . '/' . $oldCategory['image']);
            }

            $oldCategory->update();

            if (isset($data['title']) && !empty($data['title'])) {
                BenefitLang::where('benefit_id', '=', $id)->delete();
                foreach ($data['title'] as $lang => $title) {
                    if (!is_null($data['title'][$lang])) {
                        $insertLang = array(
                            'benefit_id' => $id,
                            'code' => $lang,
                            'title' => $data['title'][$lang],
                            'short_description' => $data['short_description'][$lang],
                            'slug' => isset($data['slug'][$lang]) && !empty($data['slug'][$lang]) ? Str::slug($data['slug'][$lang]) : Str::slug($data['title']['sk']),
                            'long_description' => $data['long_description'][$lang],
                            'created_at' => date('Y-m-d H:i:s')
                        );

                        BenefitLang::insert($insertLang);
                    }
                }
            }

            return redirect(route('benefit'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($this->loggedUser->can('benefit.delete')) {
            $log = new LogLibrary('BenefitController', 'destroy', Auth::user()->id, null, null, $id, null);
            $log->storeLog();

            Benefit::where('id', '=', $id)->update(array('deleted' => '1'));
            return redirect('benefit');
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }
}
