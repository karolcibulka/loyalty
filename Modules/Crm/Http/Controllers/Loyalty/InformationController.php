<?php

namespace Modules\Crm\Http\Controllers\Loyalty;


use App\Libraries\CustomerLibrary;
use App\Libraries\LogLibrary;
use App\Models\Loyalty\LoyaltySetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Modules\Crm\Http\Controllers\BaseController;

class InformationController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
        if ($this->loggedUser->can('information.show')) {
            $settings  = LoyaltySetting::first();
            $languages = array();
            if (isset($settings['languages']) && !empty($settings['languages'])) {
                $languages = json_decode($settings['languages']);
            }
            $currencies = array();
            if (isset($settings['currencies']) && !empty($settings['currencies'])) {
                $currencies = json_decode($settings['currencies']);
            }

            return view('crm::views.loyalty.informations.show')->with(array('settings' => $settings, 'languagess' => $languages, 'currencies' => $currencies));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ($this->loggedUser->can('information.create')) {

        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->loggedUser->can('information.create')) {
            $log = new LogLibrary('InformationController', 'store', Auth::user()->id, null, null, serialize($request->input()), null);
            $log->storeLog();

            $data                     = $request->all();
            $insertData               = array(
                'name' => isset($data['name']) && !empty($data['name']) ? $data['name'] : '',
                'email' => isset($data['email']) && !empty($data['email']) ? $data['email'] : '',
                'phone' => isset($data['phone']) && !empty($data['phone']) ? $data['phone'] : '',
                'state' => isset($data['state']) && !empty($data['state']) ? $data['state'] : '',
                'primary_color' => isset($data['primary_color']) && !empty($data['primary_color']) ? $data['primary_color'] : '',
                'adress' => isset($data['adress']) && !empty($data['adress']) ? $data['adress'] : '',
                'zip' => isset($data['zip']) && !empty($data['zip']) ? $data['zip'] : '',
                'throttle_enable' => isset($data['throttle_enable']) && !empty($data['throttle_enable']) ? $data['throttle_enable'] : '0',
                'throttle_count' => isset($data['throttle_count']) && !empty($data['throttle_count']) ? $data['throttle_count'] : '0',
                'throttle_minutes' => isset($data['throttle_minutes']) && !empty($data['throttle_minutes']) ? $data['throttle_minutes'] : '0',
                'activation_token_durability' => isset($data['activation_token_durability']) && !empty($data['activation_token_durability']) ? $data['activation_token_durability'] : '60',
                'expiration_password' => isset($data['expiration_password']) && !empty($data['expiration_password']) ? $data['expiration_password'] : '365',
            );
            $insertData['languages']  = $data['languages'];
            $insertData['currencies'] = $data['currencies'];
            if ($file = $request->file('logo')) {
                $time               = date('YmdHis');
                $logo               = md5('logo_' . $time) . "." . $file->getClientOriginalExtension();
                $insertData['logo'] = $logo;
                Storage::disk('public')->put($logo, File::get($file));
            }
            if ($fileF = $request->file('footer_logo')) {
                $time                      = date('YmdHis');
                $logo                      = md5('footer_logo_' . $time) . "." . $fileF->getClientOriginalExtension();
                $insertData['footer_logo'] = $logo;
                Storage::disk('public')->put($logo, File::get($fileF));
            }
            LoyaltySetting::insert($insertData);
            return redirect(route('information.index'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($this->loggedUser->can('information.show')) {

        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($this->loggedUser->can('information.edit')) {
            $loyalty = LoyaltySetting::where('id', '=', $id);


            $log = new LogLibrary('InformationController', 'update', Auth::user()->id, null, null, serialize($request->input()), serialize($loyalty->first()));
            $log->storeLog();

            $data                     = $request->all();
            $updateData               = array(
                'name' => isset($data['name']) && !empty($data['name']) ? $data['name'] : '',
                'email' => isset($data['email']) && !empty($data['email']) ? $data['email'] : '',
                'phone' => isset($data['phone']) && !empty($data['phone']) ? $data['phone'] : '',
                'state' => isset($data['state']) && !empty($data['state']) ? $data['state'] : '',
                'adress' => isset($data['adress']) && !empty($data['adress']) ? $data['adress'] : '',
                'primary_color' => isset($data['primary_color']) && !empty($data['primary_color']) ? $data['primary_color'] : '',
                'zip' => isset($data['zip']) && !empty($data['zip']) ? $data['zip'] : '',
                'throttle_enable' => isset($data['throttle_enable']) && !empty($data['throttle_enable']) ? $data['throttle_enable'] : '0',
                'throttle_count' => isset($data['throttle_count']) && !empty($data['throttle_count']) ? $data['throttle_count'] : '0',
                'throttle_minutes' => isset($data['throttle_minutes']) && !empty($data['throttle_minutes']) ? $data['throttle_minutes'] : '0',
                'activation_token_durability' => isset($data['activation_token_durability']) && !empty($data['activation_token_durability']) ? $data['activation_token_durability'] : '60',
                'expiration_password' => isset($data['expiration_password']) && !empty($data['expiration_password']) ? $data['expiration_password'] : '365',
            );
            $updateData['languages']  = $data['languages'];
            $updateData['currencies'] = $data['currencies'];
            $loyalty                  = LoyaltySetting::where('id', '=', $id);
            if ($file = $request->file('logo')) {
                $time               = date('YmdHis');
                $logo               = md5('logo_' . $time) . "." . $file->getClientOriginalExtension();
                $updateData['logo'] = $logo;
                $old                = $loyalty->first();
                if (!is_null($old)) {
                    if (isset($old['logo']) && !empty($old['logo'])) {
                        Storage::disk('public')->delete($old['logo']);
                    }
                }
                Storage::disk('public')->put($logo, File::get($file));
            }
            if ($file2 = $request->file('footer_logo')) {
                $time                      = date('YmdHis');
                $footer_logo               = md5('footer_logo' . $time) . "." . $file2->getClientOriginalExtension();
                $updateData['footer_logo'] = $footer_logo;
                $old2                      = $loyalty->first();
                if (!is_null($old2)) {
                    $old2 = $old2->toArray();
                    if (isset($old2['footer_logo']) && !empty($old2['footer_logo'])) {
                        Storage::disk('public')->delete($old2['footer_logo']);
                    }
                }
                Storage::disk('public')->put($footer_logo, File::get($file2));
            }
            $loyalty->update($updateData);

            return redirect(route('information.index'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->loggedUser->can('information.delete')) {

        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }
}
