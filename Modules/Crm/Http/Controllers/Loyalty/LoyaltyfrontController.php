<?php

namespace Modules\Crm\Http\Controllers\Loyalty;


use App\Http\Controllers\Controller;
use App\Libraries\LogLibrary;
use App\Models\Crm\PropertyCategory;
use App\Models\External\Language;
use App\Models\Loyalty\Benefit;
use App\Models\Loyalty\LoyaltyFront;
use App\Models\Loyalty\LoyaltyFrontLang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Modules\Crm\Http\Controllers\BaseController;

class LoyaltyfrontController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
        if ($this->loggedUser->can('loyaltyfront.show')) {
            $content  = LoyaltyFront::leftJoin('loyalty_front_langs', 'loyalty_fronts.id', '=', 'loyalty_front_langs.loyaltyfront_id')->get();
            $response = array();

            if (isset($content) && !empty($content)) {
                foreach ($content as $c) {
                    $response['id']                                 = $c['loyaltyfront_id'];
                    $response['animation_main_title']               = $c['animation_main_title'];
                    $response['animation_experience_title']         = $c['animation_experience_title'];
                    $response['animation_advantage_title']          = $c['animation_advantage_title'];
                    $response['animation_category_title']           = $c['animation_category_title'];
                    $response['animation_category_description']     = $c['animation_category_description'];
                    $response['animation_main_description']         = $c['animation_main_description'];
                    $response['animation_experience_description']   = $c['animation_experience_description'];
                    $response['animation_advantage_description']    = $c['animation_advantage_description'];
                    $response['show_category']                      = $c['show_category'];
                    $response['show_advantage']                     = $c['show_advantage'];
                    $response['show_experience']                    = $c['show_experience'];
                    $response['categories']                         = $c['categories'];
                    $response['advantages']                         = $c['advantages'];
                    $response['advantage_image']                    = $c['advantage_image'];
                    $response['images']                             = isset($c['images']) && !empty($c['images']) ? unserialize($c['images']) : '';
                    $response['main_titles'][$c['code']]            = $c['main_title'];
                    $response['category_titles'][$c['code']]        = $c['category_title'];
                    $response['experience_titles'][$c['code']]      = $c['experience_title'];
                    $response['advantage_titles'][$c['code']]       = $c['advantage_title'];
                    $response['main_description'][$c['code']]       = $c['main_description'];
                    $response['category_description'][$c['code']]   = $c['category_description'];
                    $response['experience_description'][$c['code']] = $c['experience_description'];
                    $response['advantage_description'][$c['code']]  = $c['advantage_description'];
                    $response['show_wave']                          = $c['show_wave'];
                    $response['animation_category']                 = $c['animation_category'];
                    $response['animation_advantage']                = $c['animation_advantage'];
                    $response['animation_experience']               = $c['animation_experience'];
                }
            }


            $propertycategories = PropertyCategory::where('active', '=', '1')->where('deleted', '=', '0')->get();
            $advantages         = Benefit::where('active', '=', '1')->where('deleted', '=', '0')->get();

            $languages = Language::all();

            return view('crm::views.loyalty.loyaltyfronts.show')->with(array('languages' => $languages, 'response' => $response, 'propertycategories' => $propertycategories, 'advantages' => $advantages));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $destinationPath = public_path('images/header');
        $data            = $request->all();
        if (is_null($data['loyaltyfront_id'])) {
            if ($this->loggedUser->can('loyaltyfront.create')) {
                $log = new LogLibrary('LoyaltyfrontController', 'store', Auth::user()->id, null, null, serialize($request->input()), null);
                $log->storeLog();

                $images = array();

                if ($request->hasFile('header_img')) {
                    $files = $request->file('header_img');
                    if (isset($files) && !empty($files)) {
                        foreach ($files as $file) {

                            $image     = $file;
                            $imageName = uniqid() . '_' . time() . '.' . $image->extension();

                            $destinationPath = public_path('images/header');
                            if (!is_dir($destinationPath)) {
                                mkdir($destinationPath, 0777, true);
                                chmod($destinationPath, 0777);
                            }

                            $img = Image::make($image->path());
                            $img->encode('jpeg', '75')->save($destinationPath . '/' . $imageName);

                            $images[] = $imageName;
                        }
                    }
                }

                $insertData = array(
                    'images' => serialize($images),
                    'animation_main_description' => $data['animation_main_description'],
                    'animation_category_description' => $data['animation_category_description'],
                    'animation_experience_description' => $data['animation_experience_description'],
                    'animation_advantage_description' => $data['animation_advantage_description'],
                    'animation_main_title' => $data['animation_main_title'],
                    'animation_category_title' => $data['animation_category_title'],
                    'animation_experience_title' => $data['animation_experience_title'],
                    'animation_advantage_title' => $data['animation_advantage_title'],
                    'show_wave' => $data['show_wave'],
                    'show_category' => $data['show_category'],
                    'show_advantage' => $data['show_advantage'],
                    'show_experience' => $data['show_experience'],
                    'animation_category' => $data['animation_category'],
                    'animation_advantage' => $data['animation_advantage'],
                    'animation_experience' => $data['animation_experience'],
                    'categories' => $data['categoriesAll'],
                    'created_at' => date('Y-m-d H:i:s')
                );

                $insertID = LoyaltyFront::insertGetId($insertData);

                $langs = array();
                foreach ($data['main_title'] as $lang => $maintitle) {
                    if (!is_null($data['main_title'][$lang])) {
                        $insertLangs = array(
                            'code' => $lang,
                            'loyaltyfront_id' => $insertID,
                            'main_title' => $data['main_title'][$lang],
                            'category_title' => $data['main_title'][$lang],
                            'main_description' => $data['main_description'][$lang],
                            'category_description' => $data['main_description'][$lang],
                            'created_at' => date('Y-m-d H:i:s')
                        );

                        $langs[] = $insertLangs;
                    }
                }
                if (!empty($langs)) {
                    LoyaltyFrontLang::insert($langs);
                }
            }
            else{
                Session::flash('permission','permissionMessage');
                return redirect()->route('home');
            }
        } else {
            if ($this->loggedUser->can('loyaltyfront.edit')) {
                $oldData = LoyaltyFront::find($data['loyaltyfront_id']);

                $log = new LogLibrary('LoyaltyfrontController', 'update', Auth::user()->id, null, null, serialize($request->input()), serialize($oldData));
                $log->storeLog();


                $images = array();
                if (isset($oldData['images']) && !empty($oldData['images'])) {
                    $images = unserialize($oldData['images']);
                }


                if (isset($data['delete_pictures']) && !empty($data['delete_pictures'])) {
                    $exploded = explode(',', $data['delete_pictures']);
                    foreach ($exploded as $key => $unsetimg) {
                        if (!empty($unsetimg)) {
                            if (isset($images) && !empty($images)) {
                                foreach ($images as $imageKey => $img) {
                                    if ($img == $unsetimg) {
                                        unset($images[$imageKey]);
                                    }
                                }
                            }
                            File::delete($destinationPath . '/' . $unsetimg);
                        }
                    }
                }

                if ($request->hasFile('header_img')) {
                    $files = $request->file('header_img');
                    if (isset($files) && !empty($files)) {
                        foreach ($files as $file) {

                            $image     = $file;
                            $imageName = uniqid() . '_' . time() . '.' . $image->extension();

                            $destinationPath = public_path('images/header');
                            if (!is_dir($destinationPath)) {
                                mkdir($destinationPath, 0777, true);
                                chmod($destinationPath, 0777);
                            }

                            $img = Image::make($image->path());
                            $img->encode('jpeg', '75')->save($destinationPath . '/' . $imageName);

                            $images[] = $imageName;
                        }
                    }
                }

                $destinationPathAdvatages = public_path('images/benefits');

                if (isset($data['remove_advantage_image']) && !empty($data['remove_advantage_image']) && $data['remove_advantage_image'] == '1') {
                    if (isset($data['old_advantage_image']) && !empty($data['old_advantage_image'])) {
                        File::delete($destinationPathAdvatages . '/' . $data['old_advantage_image']);
                    }
                }

                $advantage_image = $oldData['advantage_image'];

                if ($request->hasFile('advantage_image')) {
                    $image           = $request->file('advantage_image');
                    $advantage_image = uniqid() . '_' . time() . '.' . $image->extension();

                    if (!is_dir($destinationPathAdvatages)) {
                        mkdir($destinationPathAdvatages, 0777, true);
                        chmod($destinationPathAdvatages, 0777);
                    }

                    $img = Image::make($image->path());
                    $img->encode('jpeg', '75')->save($destinationPathAdvatages . '/' . $advantage_image);
                }

                $oldData->images                           = serialize($images);
                $oldData->animation_main_title             = $data['animation_main_title'];
                $oldData->animation_category               = $data['animation_category'];
                $oldData->animation_experience             = $data['animation_experience'];
                $oldData->animation_advantage              = $data['animation_advantage'];
                $oldData->animation_category_title         = $data['animation_category_title'];
                $oldData->animation_experience_title       = $data['animation_experience_title'];
                $oldData->animation_advantage_title        = $data['animation_advantage_title'];
                $oldData->animation_main_description       = $data['animation_main_description'];
                $oldData->animation_category_description   = $data['animation_category_description'];
                $oldData->animation_experience_description = $data['animation_experience_description'];
                $oldData->animation_advantage_description  = $data['animation_advantage_description'];
                $oldData->show_wave                        = $data['show_wave'];
                $oldData->categories                       = $data['categoriesAll'];
                $oldData->show_category                    = $data['show_category'];
                $oldData->show_advantage                   = $data['show_advantage'];
                $oldData->show_experience                  = $data['show_experience'];
                $oldData->advantages                       = $data['advantages_all'];
                $oldData->advantage_image                  = $advantage_image;
                $oldData->update();


                LoyaltyFrontLang::where('loyaltyfront_id', '=', $data['loyaltyfront_id'])->delete();

                $langs = array();
                foreach ($data['main_title'] as $lang => $maintitle) {
                    if (!is_null($data['main_title'][$lang])) {
                        $insertLangs = array(
                            'code' => $lang,
                            'loyaltyfront_id' => $data['loyaltyfront_id'],
                            'main_title' => $data['main_title'][$lang],
                            'category_title' => $data['category_title'][$lang],
                            'experience_title' => $data['experience_title'][$lang],
                            'advantage_title' => $data['advantage_title'][$lang],
                            'main_description' => $data['main_description'][$lang],
                            'category_description' => $data['category_description'][$lang],
                            'experience_description' => $data['experience_description'][$lang],
                            'advantage_description' => $data['advantage_description'][$lang],
                            'created_at' => date('Y-m-d H:i:s')
                        );

                        $langs[] = $insertLangs;
                    }
                }

                if (!empty($langs)) {
                    LoyaltyFrontLang::insert($langs);
                }
            }
            else{
                Session::flash('permission','permissionMessage');
                return redirect()->route('home');
            }
        }


        return redirect(route('loyaltyfront'));


    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($this->loggedUser->can('loyaltyfront.show')) {

        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($this->loggedUser->can('loyaltyfront.edit')) {

        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($this->loggedUser->can('loyaltyfront.edit')) {

        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->loggedUser->can('loyaltyfront.delete')) {

        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }
}
