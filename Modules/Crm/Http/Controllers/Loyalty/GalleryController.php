<?php

namespace Modules\Crm\Http\Controllers\Loyalty;


use App\Libraries\LogLibrary;
use App\Models\External\Language;
use App\Models\Loyalty\Gallery;
use App\Models\Loyalty\GalleryImage;
use App\Models\Loyalty\GalleryLang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;
use Modules\Crm\Http\Controllers\BaseController;

class GalleryController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
        if ($this->loggedUser->can('gallery.show')) {
            $galleries = Gallery::where('deleted', '=', '0')->paginate(15);

            return view('crm::views.loyalty.gallery.show')->with(array('galleries' => $galleries));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ($this->loggedUser->can('gallery.create')) {
            $languages = Language::all();
            return view('crm::views.loyalty.gallery.create')->with(array('languages' => $languages));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->loggedUser->can('gallery.create')) {
            $log = new LogLibrary('GalleryController', 'store', Auth::user()->id, null, null, serialize($request->input()), null);
            $log->storeLog();

            $data = $request->all();

            $insertData = array(
                'internal_name' => $data['internal_name'],
                'deleted' => '0',
                'active' => '1',
                'created_at' => date('Y-m-d H:i:s')
            );

            $insertID = Gallery::insertGetId($insertData);

            $langs = array();
            if (isset($data['title']) && !empty($data['title'])) {
                foreach ($data['title'] as $lang => $title) {
                    if (isset($data['title'][$lang]) && !empty($data['title'][$lang])) {
                        $insertLang = array(
                            'gallery_id' => $insertID,
                            'code' => $lang,
                            'title' => $data['title'][$lang],
                            'short_description' => $data['short_description'][$lang],
                            'long_description' => $data['long_description'][$lang],
                            'created_at' => date('Y-m-d H:i:s')
                        );
                        $langs[]    = $insertLang;
                    }
                }
            }
            if (!empty($langs)) {
                GalleryLang::insert($langs);
            }

            return redirect(route('galleries.show', [$insertID]));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($this->loggedUser->can('gallery.show')) {
            $images = GalleryImage::where('gallery_id', '=', $id)->orderBy('order', 'asc')->get();

            return view('crm::views.loyalty.gallery.showGallery')->with(array('id' => $id, 'images' => $images));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($this->loggedUser->can('gallery.edit')) {
            $languages = Language::all();

            $galleries = Gallery::where('id', '=', $id)->leftJoin('gallery_langs as gl', 'gl.gallery_id', '=', 'galleries.id')->get();
            $response  = array();

            if (!empty($galleries)) {
                foreach ($galleries as $gal) {
                    $response['title'][$gal['code']]             = $gal['title'];
                    $response['short_description'][$gal['code']] = $gal['short_description'];
                    $response['long_description'][$gal['code']]  = $gal['long_description'];
                    $response['id']                              = $gal['id'];
                    $response['internal_name']                   = $gal['internal_name'];
                }
            }


            return view('crm::views.loyalty.gallery.edit')->with(array('id' => $id, 'languages' => $languages, 'gallery' => $response));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if ($this->loggedUser->can('gallery.edit')) {
            $galleries = Gallery::where('id', '=', $id)->leftJoin('gallery_langs as gl', 'gl.gallery_id', '=', 'galleries.id')->get();
            $response  = array();

            if (!empty($galleries)) {
                foreach ($galleries as $gal) {
                    $response['title'][$gal['code']]             = $gal['title'];
                    $response['short_description'][$gal['code']] = $gal['short_description'];
                    $response['long_description'][$gal['code']]  = $gal['long_description'];
                    $response['id']                              = $gal['id'];
                    $response['internal_name']                   = $gal['internal_name'];
                }
            }

            $log = new LogLibrary('GalleryController', 'update', Auth::user()->id, null, null, serialize($request->input()), serialize($response));
            $log->storeLog();


            $data = $request->all();

            $updateData = array(
                'internal_name' => $data['internal_name'],
                'updated_at' => date('Y-m-d H:i:s')
            );

            Gallery::where('id', '=', $id)->update($updateData);

            $langs = array();
            GalleryLang::where('gallery_id', '=', $id)->delete();
            if (isset($data['title']) && !empty($data['title'])) {
                foreach ($data['title'] as $lang => $title) {
                    if (isset($data['title'][$lang]) && !empty($data['title'][$lang])) {
                        $insertLang = array(
                            'gallery_id' => $id,
                            'code' => $lang,
                            'title' => $data['title'][$lang],
                            'short_description' => $data['short_description'][$lang],
                            'long_description' => $data['long_description'][$lang],
                            'created_at' => date('Y-m-d H:i:s')
                        );
                        $langs[]    = $insertLang;
                    }
                }
            }
            if (!empty($langs)) {
                GalleryLang::insert($langs);
            }

            return redirect(route('galleries.index'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->loggedUser->can('gallery.delete')) {
            $log = new LogLibrary('GalleryController', 'destroy', Auth::user()->id, null, null, $id, null);
            $log->storeLog();

            Gallery::where('id', '=', $id)->update(array('deleted' => '1'));
            return redirect(route('galleries.index'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    public function imagestore(Request $request, $gallery_id)
    {

        if ($this->loggedUser->can('gallery.create')) {
            $log = new LogLibrary('GalleryController', 'imageStore', Auth::user()->id, null, null, serialize($request->input()));
            $log->storeLog();

            if ($request->hasFile('file')) {
                $image = $request->file('file');

                $imageName = uniqid() . '_' . time() . '.' . $image->extension();

                $destinationPath = public_path('images/thumb');
                if(!is_dir($destinationPath)){
                    mkdir($destinationPath,0775,true);
                }
                $img             = Image::make($image->path());
                $img->resize(100, 100, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/' . $imageName);


                $destinationPath = public_path('images/medium');
                if(!is_dir($destinationPath)){
                    mkdir($destinationPath,0775,true);
                }
                $img2            = Image::make($image->path());
                $img2->resize(500, 500, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/' . $imageName);

                $destinationPath = public_path('images/large');
                if(!is_dir($destinationPath)){
                    mkdir($destinationPath,0775,true);
                }
                $img3            = Image::make($image->path());
                $img3->save($destinationPath . '/' . $imageName);

                $insertData = array(
                    'image' => $imageName,
                    'gallery_id' => $gallery_id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'order' => '999999999',
                );

                GalleryImage::insert($insertData);


                $images = GalleryImage::where('gallery_id', '=', $gallery_id)->orderBy('order', 'asc')->get();
                $view   = view('crm::views.loyalty.gallery.partials.imageWrapper')->with(array('images' => $images, 'id' => $gallery_id))->render();

                return response()->json(array('status' => '1', 'view' => $view));
            }
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    public function galleryChangeOrder(Request $request)
    {
        if ($this->loggedUser->can('gallery.edit')) {
            $log = new LogLibrary('GalleryController', 'order', Auth::user()->id, null, null, serialize($request->input()), null);
            $log->storeLog();

            $data = $request->all();

            $order = 0;

            if (!empty($data['json'])) {
                foreach ($data['json'] as $item) {
                    GalleryImage::where('gallery_id', '=', $data['galleryID'])->where('image_id', '=', $item['id'])->update(array('order' => $order, 'updated_at' => date('Y-m-d H:i:s')));
                    $order += 1;
                }
            }

            echo json_encode(array('status' => '1'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    public function removeImage(Request $request)
    {
        if ($this->loggedUser->can('gallery.delete')) {
            $log = new LogLibrary('GalleryController', 'removeImage', Auth::user()->id, null, null, serialize($request->input()), null);
            $log->storeLog();

            $data = $request->all();

            $galleryImage = GalleryImage::where('image_id', '=', $data['id'])->first();

            $destinationPath = public_path('images/thumb');
            if(!is_dir($destinationPath)){
                mkdir($destinationPath,0775,true);
            }
            $path            = $destinationPath . '/' . $galleryImage['image'];
            File::delete($path);

            $destinationPath = public_path('images/medium');
            if(!is_dir($destinationPath)){
                mkdir($destinationPath,0775,true);
            }
            $path            = $destinationPath . '/' . $galleryImage['image'];
            File::delete($path);

            $destinationPath = public_path('images/large');
            if(!is_dir($destinationPath)){
                mkdir($destinationPath,0775,true);
            }
            $path            = $destinationPath . '/' . $galleryImage['image'];
            File::delete($path);

            GalleryImage::where('image_id', '=', $data['id'])->delete();

           return response()->json(array('status' => '1'));

        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    public function changeActivity(Request $request)
    {
        if ($this->loggedUser->can('gallery.edit')) {
            $log = new LogLibrary('GalleryController', 'activityImage', Auth::user()->id, null, null, serialize($request->input()), null);
            $log->storeLog();

            $data = $request->all();

            GalleryImage::where('image_id', '=', $data['id'])->update(array('active' => $data['value'], 'updated_at' => date('Y-m-d H:i:s')));

            echo json_encode(array('status' => '1'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    public function changeActivityGallery(Request $request)
    {
        if ($this->loggedUser->can('gallery.edit')) {
            $log = new LogLibrary('GalleryController', 'activityGallery', Auth::user()->id, null, null, serialize($request->input()), null);
            $log->storeLog();

            $data = $request->all();

            Gallery::where('id', '=', $data['id'])->update(array('active' => $data['value']));

            echo json_encode(array('status' => '1'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }
}
