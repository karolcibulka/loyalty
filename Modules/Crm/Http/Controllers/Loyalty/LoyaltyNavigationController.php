<?php

namespace Modules\Crm\Http\Controllers\Loyalty;


use App\Libraries\LogLibrary;
use App\Models\External\Language;
use App\Models\Loyalty\LoyaltyNavigation;
use App\Models\Loyalty\LoyaltyNavigationLang;
use App\Models\Loyalty\Subpage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Modules\Crm\Http\Controllers\BaseController;

class LoyaltyNavigationController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
        if ($this->loggedUser->can('loyaltynavigation.show')) {
            $navigationItems = LoyaltyNavigation::orderBy('order', 'asc')->where('deleted', '=', '0')->where('active', '=', '1')->get();
            if (!empty($navigationItems)) {
                $navigationItems = $this->buildTree($navigationItems);
            }

            return view('crm::views.loyalty.loyaltynavigation.show')->with(array('items' => $navigationItems));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ($this->loggedUser->can('loyaltynavigation.create')) {
            $languages = Language::all();
            $subpages  = Subpage::where('deleted', '=', '0')->get();
            return view('crm::views.loyalty.loyaltynavigation.create')->with(array('languages' => $languages, 'subpages' => $subpages));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->loggedUser->can('loyaltynavigation.create')) {
            $log = new LogLibrary('LoyaltyNavigationController', 'store', Auth::user()->id, null, null, serialize($request->input()), null);
            $log->storeLog();

            $data = $request->all();

            $insertData = array(
                'internal_name' => $data['internal_name'],
                'type' => $data['type'],
                'href' => $data['href'],
                'open' => $data['open'],
                'download' => $data['download'],
                'subpage' => $data['subpage'],
                'deleted' => '0',
                'active' => '1',
                'order' => '0',
                'file' => '',
                'created_at' => date('Y-m-d H:i:s')
            );


            if ($request->hasFile('file')) {

                $file = $request->file('file');

                $insertData['file'] = uniqid() . '_' . time() . '.' . $file->extension();

                $destinationPath = public_path('files/');

                if (!is_dir($destinationPath)) {
                    mkdir($destinationPath, 0777, true);
                    chmod($destinationPath, 0777);
                }

                $file->move($destinationPath, $insertData['file']);

            }

            $insertID = LoyaltyNavigation::insertGetId($insertData);

            //add translations
            $langs = array();
            if (!empty($data['title'])) {
                foreach ($data['title'] as $lang => $title) {
                    if (isset($data['title'][$lang]) && !empty($data['title'][$lang]) && $data['title'][$lang] != "") {
                        $translation = array(
                            'loyalty_navigation_id' => $insertID,
                            'code' => $lang,
                            'title' => $data['title'][$lang],
                            'slug' => isset($data['slug'][$lang]) && !empty($data['slug'][$lang]) ? Str::slug($data['slug'][$lang]) : Str::slug($data['title'][$lang]),
                            'created_at' => date('Y-m-d H:i:s'),
                        );

                        $langs[] = $translation;
                    }
                }

                if (!empty($langs)) {
                    LoyaltyNavigationLang::insert($langs);
                }
            }

            return redirect(route('loyaltynavigations.index'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($this->loggedUser->can('loyaltynavigation.show')) {

        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($this->loggedUser->can('loyaltynavigation.edit')) {
            $navigationRecord = LoyaltyNavigation::where('id', '=', $id)
                ->leftJoin('loyalty_navigation_langs as lnl', 'lnl.loyalty_navigation_id', '=', 'loyalty_navigations.id')
                ->get();
            $item             = array();
            if (!empty($navigationRecord)) {
                foreach ($navigationRecord as $nav) {
                    $item['titles'][$nav['code']] = $nav['title'];
                    $item['slugs'][$nav['code']]  = $nav['slug'];
                    $item['internal_name']        = $nav['internal_name'];
                    $item['type']                 = $nav['type'];
                    $item['href']                 = $nav['href'];
                    $item['subpage']              = $nav['subpage'];
                    $item['download']             = $nav['download'];
                    $item['file']                 = $nav['file'];
                    $item['open']                 = $nav['open'];
                    $item['id']                   = $nav['id'];
                }
            }

            $subpages = Subpage::where('deleted', '=', '0')->get();

            return view('crm::views.loyalty.loyaltynavigation.edit')->with(array('item' => $item, 'subpages' => $subpages));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($this->loggedUser->can('loyaltynavigation.edit')) {
            $navigationRecord = LoyaltyNavigation::where('id', '=', $id)->leftJoin('loyalty_navigation_langs as lnl', 'lnl.loyalty_navigation_id', '=', 'loyalty_navigations.id')->get();
            $item             = array();
            if (!empty($navigationRecord)) {
                foreach ($navigationRecord as $nav) {
                    $item['titles'][$nav['code']] = $nav['title'];
                    $item['slugs'][$nav['code']]  = $nav['slug'];
                    $item['internal_name']        = $nav['internal_name'];
                    $item['type']                 = $nav['type'];
                    $item['href']                 = $nav['href'];
                    $item['subpage']              = $nav['subpage'];
                    $item['download']             = $nav['download'];
                    $item['file']                 = $nav['file'];
                    $item['open']                 = $nav['open'];
                    $item['id']                   = $nav['id'];
                }
            }

            $log = new LogLibrary('LoyaltyNavigationController', 'update', Auth::user()->id, null, null, serialize($request->input()), serialize($item));
            $log->storeLog();

            $data = $request->all();

            $updateData = array(
                'internal_name' => $data['internal_name'],
                'type' => $data['type'],
                'href' => $data['href'],
                'open' => $data['open'],
                'download' => $data['download'],
                'subpage' => $data['subpage'],
                'order' => '0',
                'created_at' => date('Y-m-d H:i:s'),
                'file' => $data['oldFile']
            );


            if ($request->hasFile('file')) {

                $destinationPath = public_path('files/');

                if (isset($data['oldFile']) && !empty($data['oldFile']) && $data['oldFile'] != '') {
                    $unlink = $destinationPath . $data['oldFile'];
                    unset($unlink);
                }

                $file               = $request->file('file');
                $updateData['file'] = uniqid() . '_' . time() . '.' . $file->extension();


                if (!is_dir($destinationPath)) {
                    mkdir($destinationPath, 0777, true);
                    chmod($destinationPath, 0777);
                }

                $file->move($destinationPath, $updateData['file']);
                //$file->save($destinationPath,$insertData['file']);
            }

            LoyaltyNavigation::where('id', '=', $id)->update($updateData);

            //add translations
            $langs = array();
            LoyaltyNavigationLang::where('loyalty_navigation_id', '=', $id)->delete();
            if (!empty($data['title'])) {
                foreach ($data['title'] as $lang => $title) {
                    if (isset($data['title'][$lang]) && !empty($data['title'][$lang]) && $data['title'][$lang] != "") {
                        $translation = array(
                            'loyalty_navigation_id' => $id,
                            'code' => $lang,
                            'title' => $data['title'][$lang],
                            'slug' => isset($data['slug'][$lang]) && !empty($data['slug'][$lang]) ? Str::slug($data['slug'][$lang]) : Str::slug($data['title'][$lang]),
                            'created_at' => date('Y-m-d H:i:s'),
                        );

                        $langs[] = $translation;
                    }
                }

                if (!empty($langs)) {
                    LoyaltyNavigationLang::insert($langs);
                }
            }

            return redirect(route('loyaltynavigations.index'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->loggedUser->can('loyaltynavigation.destroy')) {
            $log = new LogLibrary('LoyaltyNavigationController', 'destory', Auth::user()->id, null, null, $id, null);
            $log->storeLog();

            LoyaltyNavigation::where('id', '=', $id)
                ->update(array('deleted' => '1'));
            return redirect(route('loyaltynavigations.index'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    public function buildTree($elements, $parentId = 0)
    {
        $branch = array();

        foreach ($elements as $element) {
            if ($element['parent'] == $parentId) {
                $children = $this->buildTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }
        return $branch;
    }

    public function navigationChangeOrder(Request $request)
    {
        if ($this->loggedUser->can('loyaltynavigation.edit')) {
            $log = new LogLibrary('LoyaltyNavigationController', 'order', Auth::user()->id, null, null, serialize($request->input()), null);
            $log->storeLog();

            $data = $request->all();

            $order = 0;
            if (!empty($data['json'])) {
                foreach ($data['json'] as $item) {
                    if (isset($item['children']) && !empty($item['children'])) {

                        LoyaltyNavigation::find($item['id'])->update(array('order' => $order, 'parent' => '0'));
                        $order += 1;
                        foreach ($item['children'] as $child) {
                            LoyaltyNavigation::find($child['id'])->update(array('order' => $order, 'parent' => $item['id']));
                            $order += 1;
                        }
                    } else {
                        LoyaltyNavigation::find($item['id'])->update(array('order' => $order, 'parent' => '0'));
                        $order += 1;
                    }
                }
            }

            echo json_encode(array('status' => '1'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    public function changeActivityNavigationLoyalty(Request $request)
    {
        if ($this->loggedUser->can('loyaltynavigation.edit')) {
            $log = new LogLibrary('LoyaltyNavigationController', 'activity', Auth::user()->id, null, null, serialize($request->input()), null);
            $log->storeLog();

            $data       = $request->all();
            $updateData = array(
                'active' => $data['value'],
            );

            LoyaltyNavigation::where('id', '=', $data['id'])->update($updateData);

            echo json_encode(array('status' => '1'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }
}
