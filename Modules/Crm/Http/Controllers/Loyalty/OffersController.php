<?php

namespace Modules\Crm\Http\Controllers\Loyalty;

use App\Libraries\LogLibrary;
use App\Models\Crm\Property;
use App\Models\Loyalty\Offer;
use App\Models\Loyalty\OfferDate;
use App\Models\Loyalty\OfferLang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Modules\Crm\Http\Controllers\BaseController;

class OffersController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if ($this->loggedUser->can('offer.show')) {
            $offers = Offer::where('deleted', '=', '0')->paginate(15);
            return view('crm::views.loyalty.offers.show')->with(array('offers' => $offers));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ($this->loggedUser->can('offer.create')) {
            $properties = Property::where('deleted', '=', '0')->where('active', '=', '1')->get();
            return view('crm::views.loyalty.offers.create')->with(array('properties' => $properties));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->loggedUser->can('offer.create')) {
            $log = new LogLibrary('OffersController', 'store', Auth::user()->id, null, null, serialize($request->input()), null);
            $log->storeLog();


            $data = $request->all();

            $insert_data = array(
                'internal_name' => $data['internal_name'],
                'price' => $data['price'],
                'visible_from' => date('Y-m-d H:i:s', strtotime($data['showed_from'])),
                'visible_to' => date('Y-m-d H:i:s', strtotime($data['showed_to'])),
                'offers_restrict' => $data['offers_restrict'],
                'offers_restrict_count' => isset($data['offers_restrict_count']) && !empty($data['offers_restrict_count']) ? $data['offers_restrict_count'] : 0,
                'available' => 0,
                'properties' => isset($data['properties']) && !empty($data['properties']) ? serialize($data['properties']) : '',
                'deleted' => '0',
                'created_at' => date('Y-m-d H:i:s'),
                'add_points' => $data['add_points'],
                'image' => null,
                'active' => $data['active'],
                'show_in_sales' => $data['show_in_sales'],
                'points' => '0'
            );
            if($request->cropper_used == '1'){
                if(!empty($request->cropper_value)) {

                    $base = $request->cropper_value;

                    $extension = explode(';', $base);
                    $extension = explode('/', $extension[0]);
                    $extension = $extension[1];

                    $imageName = uniqid() . '_' . time() . '.' . $extension;

                    $base64Str = substr($base, strpos($base, ",") + 1);
                    $image_base     = base64_decode($base64Str);

                    $destinationPath = public_path('images/thumb');
                    $img             = Image::make($image_base);
                    $img->resize(100, 100, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($destinationPath . '/' . $imageName);

                    $destinationPath = public_path('images/medium');
                    $img2            = Image::make($image_base);
                    $img2->resize(500, 500, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($destinationPath . '/' . $imageName);

                    $destinationPath = public_path('images/large');
                    $img3            = Image::make($image_base);
                    $img3->save($destinationPath . '/' . $imageName);

                    $insert_data['image'] = $imageName;
                }
            }
            elseif ($request->hasFile('image')) {
                $image = $request->file('image');

                $imageName = uniqid() . '_' . time() . '.' . $image->extension();

                $destinationPath = public_path('images/thumb');
                $img             = Image::make($image->path());
                $img->resize(100, 100, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/' . $imageName);


                $destinationPath = public_path('images/medium');
                $img2            = Image::make($image->path());
                $img2->resize(500, 500, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/' . $imageName);

                $destinationPath = public_path('images/large');
                $img            = Image::make($image->path());
                $img->save($destinationPath . '/' . $imageName);


                $insert_data['image'] = $imageName;
            }

            $insertID = Offer::insertGetId($insert_data);


            $langs = array();
            if (isset($data['name']) && !empty($data['name'])) {
                foreach ($data['name'] as $lang => $name) {
                    if (isset($data['name'][$lang]) && !empty($data['name'][$lang])) {
                        $insert_lang = array(
                            'offer_id' => $insertID,
                            'lang' => $lang,
                            'name' => $data['name'][$lang],
                            'short_description' => isset($data['short_description'][$lang]) && !empty($data['short_description'][$lang]) ? $data['short_description'][$lang] : '',
                            'long_description' => isset($data['long_description'][$lang]) && !empty($data['long_description'][$lang]) ? $data['long_description'][$lang] : '',
                            'slug' => isset($data['slug'][$lang]) && !empty($data['slug'][$lang]) ? Str::slug($data['slug'][$lang]) : Str::slug($data['name'][$lang]),
                            'created_at' => date('Y-m-d H:i:s')
                        );
                        $langs[]     = $insert_lang;

                    }
                }

                if (!empty($langs)) {
                    Offerlang::insert($langs);
                }

            }


            $dates = array();
            if (isset($data['reservation_from']) && !empty($data['reservation_from'])) {
                foreach ($data['reservation_from'] as $key => $reservationFrom) {
                    if (isset($data['reservation_from'][$key]) && !empty($data['reservation_from'][$key]) && isset($data['reservation_to'][$key]) && !empty($data['reservation_to'][$key])) {
                        $insert_date = array(
                            'offer_id' => $insertID,
                            'from' => date('Y-m-d H:i:s', strtotime($data['reservation_from'][$key])),
                            'to' => date('Y-m-d H:i:s', strtotime($data['reservation_to'][$key])),
                            'created_at' => date('Y-m-d H:i:s')
                        );

                        $dates[] = $insert_date;
                    }
                }
            }

            if (!empty($dates)) {
                OfferDate::insert($dates);
            }

            return redirect(route('offer'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($this->loggedUser->can('offer.show')) {

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($this->loggedUser->can('offer.edit')) {
            $offer = Offer::where('offers.id', '=', $id)
                ->leftJoin('offer_dates', 'offer_dates.offer_id', '=', 'offers.id')
                ->leftJoin('offer_langs', 'offer_langs.offer_id', '=', 'offers.id')
                ->get();

            $response = array();
            $offers   = $offer->toArray();
            foreach ($offers as $key => $offer) {
                $response['lang'][$offer['lang']]['name']              = $offer['name'];
                $response['lang'][$offer['lang']]['short_description'] = $offer['short_description'];
                $response['lang'][$offer['lang']]['long_description']  = $offer['long_description'];
                $response['lang'][$offer['lang']]['slug']              = $offer['slug'];
                $response['visible_from']                              = date('d.m.Y', strtotime($offer['visible_from']));
                $response['visible_to']                                = date('d.m.Y', strtotime($offer['visible_to']));
                $response['internal_name']                             = $offer['internal_name'];
                $response['add_points']                                = $offer['add_points'];
                $response['offers_restrict_count']                     = $offer['offers_restrict_count'];
                $response['offers_restrict']                           = $offer['offers_restrict'];
                $response['image']                                     = isset($offer['image']) && !empty($offer['image']) ? asset('images/medium/' . $offer['image']) : '';
                $response['img']                                       = $offer['image'];
                $response['show_in_sales']                             = $offer['show_in_sales'];
                $response['price']                                     = $offer['price'];
                $response['currency']                                  = $offer['currency'];
                $response['active']                                    = $offer['active'];
                $response['properties']                                = isset($offer['properties']) && !empty($offer['properties']) ? unserialize($offer['properties']) : '';
                $response['id']                                        = $offer['offer_id'];
                $response['from'][$offer['offer_date_id']]             = date('d.m.Y', strtotime($offer['from']));
                $response['to'][$offer['offer_date_id']]               = date('d.m.Y', strtotime($offer['to']));
            }

            $properties = Property::where('deleted', '=', '0')->where('active', '=', '1')->get();
            return view('crm::views.loyalty.offers.edit', ['offer' => $response, 'properties' => $properties]);
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($this->loggedUser->can('offer.edit')) {
            $offer = Offer::where('offers.id', '=', $id)
                ->leftJoin('offer_dates', 'offer_dates.offer_id', '=', 'offers.id')
                ->leftJoin('offer_langs', 'offer_langs.offer_id', '=', 'offers.id')
                ->get();

            $response = array();
            $offers   = $offer->toArray();
            foreach ($offers as $key => $offer) {
                $response['lang'][$offer['lang']]['name']              = $offer['name'];
                $response['lang'][$offer['lang']]['short_description'] = $offer['short_description'];
                $response['lang'][$offer['lang']]['long_description']  = $offer['long_description'];
                $response['lang'][$offer['lang']]['slug']              = $offer['slug'];
                $response['visible_from']                              = date('d.m.Y', strtotime($offer['visible_from']));
                $response['visible_to']                                = date('d.m.Y', strtotime($offer['visible_to']));
                $response['internal_name']                             = $offer['internal_name'];
                $response['add_points']                                = $offer['add_points'];
                $response['offers_restrict_count']                     = $offer['offers_restrict_count'];
                $response['offers_restrict']                           = $offer['offers_restrict'];
                $response['image']                                     = isset($offer['image']) && !empty($offer['image']) ? asset('images/large/' . $offer['image']) : '';
                $response['img']                                       = $offer['image'];
                $response['show_in_sales']                             = $offer['show_in_sales'];
                $response['price']                                     = $offer['price'];
                $response['currency']                                  = $offer['currency'];
                $response['active']                                    = $offer['active'];
                $response['properties']                                = isset($offer['properties']) && !empty($offer['properties']) ? unserialize($offer['properties']) : '';
                $response['id']                                        = $offer['offer_id'];
                $response['from'][$offer['offer_date_id']]             = date('d.m.Y', strtotime($offer['from']));
                $response['to'][$offer['offer_date_id']]               = date('d.m.Y', strtotime($offer['to']));
            }

            $log = new LogLibrary('OffersController', 'update', Auth::user()->id, null, null, serialize($request->input()), serialize($response));
            $log->storeLog();

            $data = $request->all();

            $update_data = array(
                'internal_name' => $data['internal_name'],
                'price' => $data['price'],
                'visible_from' => date('Y-m-d H:i:s', strtotime($data['showed_from'])),
                'visible_to' => date('Y-m-d H:i:s', strtotime($data['showed_to'])),
                'offers_restrict' => $data['offers_restrict'],
                'offers_restrict_count' => isset($data['offers_restrict_count']) && !empty($data['offers_restrict_count']) ? $data['offers_restrict_count'] : 0,
                'properties' => isset($data['properties']) && !empty($data['properties']) ? serialize($data['properties']) : '',
                'show_in_sales' => $data['show_in_sales'],
                'add_points' => $data['add_points'],
                'deleted' => '0',
                'created_at' => date('Y-m-d H:i:s'),
                'image' => $data['old_image'],
                'active' => $data['active'],
            );

            if ($data['remove_image'] == '1') {
                $thumb_path = public_path('images/thumb');
                if (file_exists($thumb_path . $data['old_image'])) {
                    File::delete($thumb_path . $data['old_image']);
                }
                $medium_path = public_path('images/medium');
                if (file_exists($medium_path . $data['old_image'])) {
                    File::delete($medium_path . $data['old_image']);
                }

                $large_path = public_path('images/large');
                if (file_exists($large_path . $data['old_image'])) {
                    File::delete($large_path . $data['old_image']);
                }

                $update_data['image'] = '';
            }
            if($request->cropper_used == '1'){
                if(!empty($request->cropper_value)) {

                    $thumb_path = public_path('images/thumb/');
                    if (file_exists($thumb_path . $data['old_image'])) {
                        File::delete($thumb_path . $data['old_image']);
                    }
                    $medium_path = public_path('images/medium/');
                    if (file_exists($medium_path . $data['old_image'])) {
                        File::delete($medium_path . $data['old_image']);
                    }

                    $large_path = public_path('images/large/');
                    if (file_exists($large_path . $data['old_image'])) {
                        File::delete($large_path . $data['old_image']);
                    }

                    $base = $request->cropper_value;

                    $extension = explode(';', $base);
                    $extension = explode('/', $extension[0]);
                    $extension = $extension[1];

                    $imageName = uniqid() . '_' . time() . '.' . $extension;

                    $base64Str = substr($base, strpos($base, ",") + 1);
                    $image_base     = base64_decode($base64Str);

                    $destinationPath = public_path('images/thumb');
                    $img             = Image::make($image_base);
                    $img->resize(100, 100, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($destinationPath . '/' . $imageName);

                    $destinationPath = public_path('images/medium');
                    $img2            = Image::make($image_base);
                    $img2->resize(500, 500, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($destinationPath . '/' . $imageName);

                    $destinationPath = public_path('images/large');
                    $img3            = Image::make($image_base);
                    $img3->save($destinationPath . '/' . $imageName);

                    $update_data['image'] = $imageName;
                }
            }
            else if ($request->hasFile('image')) {

                $thumb_path = public_path('images/thumb/');
                if (file_exists($thumb_path . $data['old_image'])) {
                    File::delete($thumb_path . $data['old_image']);
                }
                $medium_path = public_path('images/medium/');
                if (file_exists($medium_path . $data['old_image'])) {
                    File::delete($medium_path . $data['old_image']);
                }

                $large_path = public_path('images/large/');
                if (file_exists($large_path . $data['old_image'])) {
                    File::delete($large_path . $data['old_image']);
                }

                $image = $request->file('image');

                $imageName = uniqid() . '_' . time() . '.' . $image->extension();

                $destinationPath = public_path('images/thumb');
                $img             = Image::make($image->path());
                $img->resize(100, 100, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/' . $imageName);

                $destinationPath = public_path('images/medium');
                $img2            = Image::make($image->path());
                $img2->resize(500, 500, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/' . $imageName);

                $destinationPath = public_path('images/large');
                $img            = Image::make($image->path());
                $img->save($destinationPath . '/' . $imageName);

                $update_data['image'] = $imageName;
            }

            Offer::where('id', '=', $id)->update($update_data);


            $langs = array();
            if (isset($data['name']) && !empty($data['name'])) {
                OfferLang::where('offer_id', '=', $id)->delete();
                foreach ($data['name'] as $lang => $name) {
                    if (isset($data['name'][$lang]) && !empty($data['name'][$lang])) {
                        $insert_lang  = array(
                            'offer_id' => $id,
                            'lang' => $lang,
                            'name' => $data['name'][$lang],
                            'short_description' => isset($data['short_description'][$lang]) && !empty($data['short_description'][$lang]) ? $data['short_description'][$lang] : '',
                            'long_description' => isset($data['long_description'][$lang]) && !empty($data['long_description'][$lang]) ? $data['long_description'][$lang] : '',
                            'slug' => isset($data['slug'][$lang]) && !empty($data['slug'][$lang]) ? Str::slug($data['slug'][$lang]) : Str::slug($data['name'][$lang]),
                            'created_at' => date('Y-m-d H:i:s')
                        );
                        $langs[$lang] = $insert_lang;
                    }

                }

                if (!empty($langs)) {
                    OfferLang::insert($langs);
                }
            }

            $dates = array();

            if (isset($data['reservation_from']) && !empty($data['reservation_from'])) {
                OfferDate::where('offer_id', '=', $id)->delete();
                foreach ($data['reservation_from'] as $key => $reservationFrom) {
                    if (isset($data['reservation_from'][$key]) && !empty($data['reservation_from'][$key]) && isset($data['reservation_to'][$key]) && !empty($data['reservation_to'][$key])) {
                        $insert_date = array(
                            'offer_id' => $id,
                            'from' => date('Y-m-d H:i:s', strtotime($data['reservation_from'][$key])),
                            'to' => date('Y-m-d H:i:s', strtotime($data['reservation_to'][$key])),
                            'created_at' => date('Y-m-d H:i:s')
                        );

                        $dates[] = $insert_date;
                    }
                }

                if (!empty($dates)) {
                    OfferDate::insert($dates);
                }

            }

            return redirect(route('offer'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->loggedUser->can('offer.delete')) {
            $log = new LogLibrary('OffersController', 'destroy', Auth::user()->id, null, null, $id, null);
            $log->storeLog();


            $update = array(
                'deleted' => '1'
            );

            Offer::where('id', '=', $id)->update($update);
            return redirect(route('offer'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    public function changeActivityoffer(Request $request)
    {
        if ($this->loggedUser->can('offer.edit')) {
            $log = new LogLibrary('OffersController', 'activity', Auth::user()->id, null, null, serialize($request->input()));
            $log->storeLog();

            $data = $request->all();


            if (isset($data['type']) && !empty($data['type']) && $data['type'] == 'add_points') {
                $updateData = array(
                    'add_points' => $data['value'],
                );
            } else {
                $updateData = array(
                    'active' => $data['value']
                );
            }

            Offer::where('id', '=', $data['id'])->update($updateData);

            $response = array(
                'status' => '1'
            );

            echo json_encode($response);
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }
}
