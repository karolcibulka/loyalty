<?php

namespace Modules\Crm\Http\Controllers\Loyalty;


use App\Libraries\LogLibrary;
use App\Models\Loyalty\CategoryOffer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Modules\Crm\Http\Controllers\BaseController;

class CategoryofferController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
        if ($this->loggedUser->can('categoryoffer.show')) {
            $response = CategoryOffer::where('deleted', '=', '0')->paginate(10);
            return view('crm::views.loyalty.categoryoffers.show')->with(array('categoryoffers' => $response));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ($this->loggedUser->can('categoryoffer.create')) {
            return view('crm::views.loyalty.categoryoffers.create');
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->loggedUser->can('categoryoffer.create')) {
            $log = new LogLibrary('CategoryofferController', 'store', Auth::user()->id, null, null, serialize($request->input()), null);
            $log->storeLog();

            $data = $request->all();

            $storeData = array(
                'internal_name' => $data['internal_name'],
                'active' => '1',
                'deleted' => '0',
                'created_at' => date('Y-m-d H:i:s'),
            );

            CategoryOffer::insert($storeData);

            return redirect(route('categoryoffer'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($this->loggedUser->can('categoryoffer.show')) {

        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($this->loggedUser->can('categoryoffer.edit')) {
            $categoryoffer = CategoryOffer::find($id);
            return view('crm::views.loyalty.categoryoffers.edit')->with(array('categoryoffer' => $categoryoffer));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($this->loggedUser->can('categoryoffer.edit')) {
            $log = new LogLibrary('CategoryofferController', 'update', Auth::user()->id, null, null, serialize($request->input()), serialize(CategoryOffer::find($id)));
            $log->storeLog();


            $data       = $request->all();
            $updateData = array(
                'internal_name' => $data['internal_name'],
            );

            CategoryOffer::where('id', '=', $id)->update($updateData);

            return redirect(route('categoryoffer'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->loggedUser->can('categoryoffer.delete')) {
            $log = new LogLibrary('CategoryofferController', 'destroy', Auth::user()->id, null, null, $id, null);
            $log->storeLog();

            $updateData = array(
                'deleted' => '1',
            );

            CategoryOffer::where('id', '=', $id)->update($updateData);

            return redirect(route('categoryoffer'));
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }

    public function changeActivityCategoryoffer(Request $request)
    {
        if ($this->loggedUser->can('categoryoffer.edit')) {
            $log = new LogLibrary('CategoryofferController', 'activity', Auth::user()->id, null, null, serialize($request->input()), null);
            $log->storeLog();

            $data = $request->all();

            $updateData = array(
                'active' => $data['value']
            );

            CategoryOffer::where('id', '=', $data['id'])->update($updateData);

            $response = array(
                'status' => '1'
            );

            echo json_encode($response);
        }
        else{
            Session::flash('permission','permissionMessage');
            return redirect()->route('home');
        }
    }
}
