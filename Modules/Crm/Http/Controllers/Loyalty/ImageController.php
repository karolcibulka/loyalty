<?php

namespace Modules\Crm\Http\Controllers\Loyalty;


use App\Libraries\LogLibrary;
use App\Models\Loyalty\Gallery;
use App\Models\Loyalty\GalleryImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;
use Modules\Crm\Http\Controllers\BaseController;

class ImageController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $log = new LogLibrary('ImageController', 'store', Auth::user()->id, null, null, serialize($request->input()), null);
            $log->storeLog();

            $data = $request->all();

            $base = $data['base'];

            $extension = explode(';', $base);
            $extension = explode('/', $extension[0]);
            $extension = $extension[1];

            $base64Str = substr($base, strpos($base, ",") + 1);
            $image     = base64_decode($base64Str);
            $imageName = uniqid() . '_' . time() . '.' . $extension;

            $destinationPath = public_path('images/thumb');
            $img             = Image::make($image);
            $img->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath . '/' . $imageName);


            $destinationPath = public_path('images/medium');
            $img2            = Image::make($image);
            $img2->resize(500, 500, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath . '/' . $imageName);

            $destinationPath = public_path('images/large');
            $img3            = Image::make($image);
            $img3->save($destinationPath . '/' . $imageName);

            $image_id = $data['image_id'];
            $img      = GalleryImage::where('image_id', '=', $image_id)->first();

            if (isset($data['replace']) && !empty($data['replace'])) {
                $destinationPath = public_path('images/thumb');
                $path            = $destinationPath . '/' . $img['image'];
                File::delete($path);

                $destinationPath = public_path('images/medium');
                $path            = $destinationPath . '/' . $img['image'];
                File::delete($path);

                $destinationPath = public_path('images/large');
                $path            = $destinationPath . '/' . $img['image'];
                File::delete($path);

                GalleryImage::where('image_id', '=', $image_id)->update(array('image' => $imageName));
            } else {
                GalleryImage::insert(array(
                    'created_at' => date('Y-m-d H:i:s'),
                    'gallery_id' => $img['gallery_id'],
                    'image' => $imageName,
                    'active' => '1',
                    'order' => '99999',
                ));
            }

            echo json_encode(array('status' => '1'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = GalleryImage::where('image_id', '=', $id)->first();
        $img  = asset('images/large') . '/' . $data['image'];
        return view('crm::views.loyalty.gallery.partials.imageEdit')->with(array('id' => $id, 'img' => $img, 'gallery_id' => $data['gallery_id']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
