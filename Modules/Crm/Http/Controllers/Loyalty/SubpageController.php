<?php

namespace Modules\Crm\Http\Controllers\Loyalty;


use App\Libraries\LogLibrary;
use App\Models\Crm\Offer;
use App\Models\Crm\PropertyCategory;
use App\Models\External\Language;
use App\Models\Loyalty\Gallery;
use App\Models\Loyalty\Subpage;
use App\Models\Loyalty\SubpageLang;
use App\Models\Loyalty\SubpageModule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;
use Modules\Crm\Http\Controllers\BaseController;

class SubpageController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if ($this->loggedUser->can('subpage.show')) {
            $subpages = Subpage::where('deleted', '=', '0')->paginate(15);
            return view('crm::views.loyalty.subpages.show')->with(array('subpages' => $subpages));
        } else {
            Session::flash('permission', 'permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ($this->loggedUser->can('subpage.create')) {
            $languages  = Language::all();
            $galleries  = Gallery::where('active', '=', '1')->where('deleted', '=', '0')->get();
            $structures = $this->getStructures();

            $categories = PropertyCategory::where('active', '=', '1')->where('deleted', '=', '0')->get();
            $offers     = Offer::where('active', '=', '1')->where('deleted', '=', '0')->get();

            return view('crm::views.loyalty.subpages.create')->with(array('languages' => $languages, 'structures' => $structures, 'galleries' => $galleries, 'categories' => $categories, 'offers' => $offers));
        } else {
            Session::flash('permission', 'permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        if ($this->loggedUser->can('subpage.create')) {
            $log = new LogLibrary('SubpageController', 'store', Auth::user()->id, null, null, serialize($request->input()), null);
            $log->storeLog();

            $data = $request->all();

            $insertData = array(
                'internal_name' => $data['internal_name'],
                'image' => '',
                'deleted' => '0',
                'template' => $data['template'],
                'created_at' => date('Y-m-d H:i:s')
            );

            if ($request->hasFile('image')) {
                $image = $request->file('image');

                $imageName = uniqid() . '_' . time() . '.' . $image->extension();

                $destinationPath = public_path('images/thumb');
                $img             = Image::make($image->path());
                $img->resize(100, 100, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/' . $imageName);


                $destinationPath = public_path('images/medium');
                $img2            = Image::make($image->path());
                $img2->resize(500, 500, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/' . $imageName);
                $insertData['image'] = $imageName;
            }

            $insertID = Subpage::insertGetId($insertData);

            $langs = array();
            if (isset($data['title']) && !empty($data['title'])) {
                foreach ($data['title'] as $lang => $title) {
                    if (isset($data['title'][$lang]) && !empty($data['title'][$lang])) {
                        $insertLang = array(
                            'subpage_id' => $insertID,
                            'code' => $lang,
                            'title' => $data['title'][$lang],
                            'short_description' => $data['short_description'][$lang],
                            'long_description' => $data['long_description'][$lang],
                            'created_at' => date('Y-m-d H:i:s')
                        );
                        $langs[]    = $insertLang;
                    }
                }
            }
            if (!empty($langs)) {
                SubpageLang::insert($langs);
            }

            $modules = array();
            $order = 1;
            if (isset($data['type']) && !empty($data['type'])) {
                foreach ($data['type'] as $key => $type) {
                    $modules[$key]['subpage_id']   = $insertID;
                    $modules[$key]['type']         = $data['type'][$key];
                    $modules[$key]['show_items']   = $data['show_items'][$key];
                    $modules[$key]['show_all']     = $data['show_all'][$key];
                    $modules[$key]['order']        = $order;
                    $modules[$key]['informations'] = isset($data['informations'][$key]) && !empty($data['informations'][$key]) ? $data['informations'][$key] : '';

                    $order++;
                }
            }

            if(!empty($modules)){
                SubpageModule::insert($modules);
            }

            return redirect(route('subpages.index'));

        } else {
            Session::flash('permission', 'permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($this->loggedUser->can('subpage.show')) {

        } else {
            Session::flash('permission', 'permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($this->loggedUser->can('subpage.edit')) {
            $structures = $this->getStructures();
            $galleries  = Gallery::where('active', '=', '1')->where('deleted', '=', '0')->get();

            $categories = PropertyCategory::where('active', '=', '1')->where('deleted', '=', '0')->get();
            $offers     = Offer::where('active', '=', '1')->where('deleted', '=', '0')->get();

            $languages = Language::all();
            $subpages  = Subpage::where('id', '=', $id)
                ->leftJoin('subpage_langs as sl', 'sl.subpage_id', '=', 'subpages.id')
                ->get();

            $modules = SubpageModule::where('subpage_id','=',$id)
                ->orderBy('order','asc')
                ->get();


            $response = array();
            if (!empty($subpages)) {
                foreach ($subpages as $subpage) {
                    $response['title'][$subpage['code']]             = $subpage['title'];
                    $response['short_description'][$subpage['code']] = $subpage['short_description'];
                    $response['long_description'][$subpage['code']]  = $subpage['long_description'];
                    $response['image']                               = $subpage['image'];
                    $response['internal_name']                       = $subpage['internal_name'];
                    $response['id']                                  = $subpage['id'];
                    $response['template']                            = $subpage['template'];
                    $response['offers']                              = isset($subpage['offers']) && !empty($subpage['offers']) ? json_decode($subpage['offers'], true) : array();
                    if(isset($modules) && !empty($modules) && !is_null($modules)){
                        foreach($modules as $key => $module){
                            $response['modules'][$key]['type'] = $module['type'];
                            $response['modules'][$key]['show_all'] = $module['show_all'];
                            $response['modules'][$key]['show_items'] = $module['show_items'];
                            $response['modules'][$key]['order'] = $module['order'];
                            $response['modules'][$key]['informations'] = $module['informations'];
                        }
                    }
                }
            }

            return view('crm::views.loyalty.subpages.edit')->with(array('languages' => $languages, 'subpage' => $response, 'structures' => $structures, 'galleries' => $galleries, 'categories' => $categories, 'offers' => $offers));
        } else {
            Session::flash('permission', 'permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if ($this->loggedUser->can('subpage.edit')) {
            $subpages = Subpage::where('id', '=', $id)->leftJoin('subpage_langs as sl', 'sl.subpage_id', '=', 'subpages.id')->get();

            $response = array();
            if (!empty($subpages)) {
                foreach ($subpages as $subpage) {
                    $response['title'][$subpage['code']]             = $subpage['title'];
                    $response['short_description'][$subpage['code']] = $subpage['short_description'];
                    $response['long_description'][$subpage['code']]  = $subpage['long_description'];
                    $response['image']                               = $subpage['image'];
                    $response['internal_name']                       = $subpage['internal_name'];
                    $response['id']                                  = $subpage['id'];
                    $response['template']                            = $subpage['template'];
                    $response['offers']                              = isset($subpage['offers']) && !empty($subpage['offers']) ? json_decode($subpage['offers'], true) : array();
                }
            }

            $log = new LogLibrary('SubpageController', 'update', Auth::user()->id, null, null, serialize($request->input()), serialize($response));
            $log->storeLog();

            $data = $request->all();

            $updateData = array(
                'internal_name' => $data['internal_name'],
                'image' => $data['oldImage'],
                'deleted' => '0',
                'template' => $data['template'],
                'updated_at' => date('Y-m-d H:i:s')
            );

            if (!empty($data['deleteImage']) && !empty($data['oldImage']) && $data['oldImage'] != '') {
                $destinationPath = public_path('images/thumb');
                $file            = $destinationPath . $data['oldImage'];
                unset($file);

                $destinationPath = public_path('images/medium');
                $file            = $destinationPath . $data['oldImage'];
                unset($file);
            }

            if ($request->hasFile('image')) {
                $image = $request->file('image');

                $imageName = uniqid() . '_' . time() . '.' . $image->extension();

                $destinationPath = public_path('images/thumb');
                $img             = Image::make($image->path());
                $img->resize(100, 100, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/' . $imageName);


                $destinationPath = public_path('images/medium');
                $img2            = Image::make($image->path());
                $img2->resize(500, 500, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/' . $imageName);
                $updateData['image'] = $imageName;
            }

            Subpage::where('id', '=', $id)->update($updateData);

            $langs = array();
            SubpageLang::where('subpage_id', '=', $id)->delete();
            if (isset($data['title']) && !empty($data['title'])) {
                foreach ($data['title'] as $lang => $title) {
                    if (isset($data['title'][$lang]) && !empty($data['title'][$lang])) {
                        $insertLang = array(
                            'subpage_id' => $id,
                            'code' => $lang,
                            'title' => $data['title'][$lang],
                            'short_description' => $data['short_description'][$lang],
                            'long_description' => $data['long_description'][$lang],
                            'created_at' => date('Y-m-d H:i:s')
                        );
                        $langs[]    = $insertLang;
                    }
                }
            }
            if (!empty($langs)) {
                SubpageLang::insert($langs);
            }

            SubpageModule::where('subpage_id', '=', $id)->delete();
            $modules = array();
            $order = 1;
            if (isset($data['type']) && !empty($data['type'])) {
                foreach ($data['type'] as $key => $type) {
                    $modules[$key]['subpage_id']   = $id;
                    $modules[$key]['type']         = $data['type'][$key];
                    $modules[$key]['show_items']   = isset($data['show_items'][$key]) && !empty($data['show_items'][$key]) ? $data['show_items'][$key] : '';
                    $modules[$key]['show_all']     = $data['show_all'][$key];
                    $modules[$key]['order']        = $order;
                    $modules[$key]['informations'] = isset($data['informations'][$key]) && !empty($data['informations'][$key]) ? $data['informations'][$key] : '';

                    $order++;
                }
            }

            if (!empty($modules)) {
                SubpageModule::insert($modules);
            }

            return redirect(route('subpages.index'));
        } else {
            Session::flash('permission', 'permissionMessage');
            return redirect()->route('home');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->loggedUser->can('subpage.delete')) {
            $log = new LogLibrary('SubpageController', 'destroy', Auth::user()->id, '', '', $id, null);
            $log->storeLog();

            Subpage::where('id', '=', $id)->update(array('deleted' => '1'));


            return redirect(route('subpages.index'));
        } else {
            Session::flash('permission', 'permissionMessage');
            return redirect()->route('home');
        }
    }

    private function getStructures()
    {
        $module    = File::directories(module_path('Loyalty'));
        $templates = File::files($module[6] . '/views/' . Config::get('theme.template') . '/subpage/structures');

        $structures = array();

        if (!empty($templates)) {
            foreach ($templates as $template) {
                $structures[] = str_replace('.blade.php', '', $template->getFilename());
            }
        }

        return $structures;
    }
}
