<?php

namespace Modules\Crm\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Modules\Crm\Http\Controllers\BaseController;

class HomeController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        return view('crm::home');
    }
}
