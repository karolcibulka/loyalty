<style>
    .changeActivityProfile{
        cursor:pointer;
    }
</style>

<div class="table-responsive">
    <table id="customerTable" class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>#</th>
            <th>Interný názov</th>
            <th>Dátumovo závislé</th>
            <th>Dátum od</th>
            <th>Dátum do</th>
            <th>Typ</th>
            <th>Vytvorený</th>
            <th>Aktívna</th>
            <th>Akcia</th>
        </tr>
        </thead>
        <tbody>
        @if(isset($loyalty_account_settings['charge']) && !empty($loyalty_account_settings['charge']))
            @foreach($loyalty_account_settings['charge'] as $charge)
                <tr>
                    <td>{{$charge['id']}}</td>
                    <td>{{$charge['internal_name']}}</td>
                    <td>{{$charge['date_required'] == '0' ? 'Nie' : 'Áno' }}</td>
                    <td>{{!is_null($charge['date_from']) ? date('d.m.Y',strtotime($charge['date_from'])) : '-----'}}</td>
                    <td>{{!is_null($charge['date_to']) ? date('d.m.Y',strtotime($charge['date_to'])) : '-----'}}</td>
                    <td>{{$charge['loyalty_account_setting_type']}}</td>
                    <td>{{date('d.m.Y H:i:s',strtotime($charge['created_at']))}}</td>
                    <td>
                        @can('loyaltyaccountsetting.edit')
                            <?=$charge['active'] == '1' ? '<span class="changeActivityProfile" data-id="'.$charge['id'].'"> <i class="icon-check active" style="color:darkgreen"></i></span>' : '<span class="changeActivityProfile" data-id="'.$charge['id'].'"><i class="icon-cross2 nonactive" style="color:darkred"></i></span>'?>
                        @endcan
                    </td>
                    <td>
                        @can('loyaltyaccountsetting.edit')
                            <a href="{{route('loyaltyaccountsettings.edit',[$charge['id']])}}"><span class="badge badge-primary">Upraviť</span></a>
                        @endcan
                        @can('loyaltyaccountsetting.delete')
                            <form action="{{route('loyaltyaccountsettings.destroy',[$charge['id']])}}" method="post" style="display:inline" id="formDestroy_{{$charge['id']}}">
                                @csrf
                                {{method_field('delete')}}
                                <span class="badge badge-danger confirmDestroy" data-id="{{$charge['id']}}">Zmazať</span>
                            </form>
                        @endcan
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
