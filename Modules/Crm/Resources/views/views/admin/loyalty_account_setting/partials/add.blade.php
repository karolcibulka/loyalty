<style>
    .changeActivityProfile{
        cursor:pointer;
    }
</style>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label><strong>Pre pripočítavanie bodov z extérneho systému použiť:</strong></label>
            <select class="form-control change-points-settings" data-type="add">
                <option <?=(isset($loyalty_settings['adding_points']) && !empty($loyalty_settings['adding_points']) && $loyalty_settings['adding_points'] == 'DIRECT') ? 'selected' : ''?> value="DIRECT">Priame pripočítavanie</option>
                <option <?=(isset($loyalty_settings['adding_points']) && !empty($loyalty_settings['adding_points']) && $loyalty_settings['adding_points'] == 'STACK') ? 'selected' : ''?> value="STACK">Zásobnik</option>
            </select>
            <small>Zmena sa prejaví hneď po zmene hodnoty v selectboxe</small>
        </div>
    </div>
</div>
<legend></legend>
<div class="table-responsive">
    <table id="customerTable" class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>#</th>
            <th>Interný názov</th>
            <th>Dátumovo závislé</th>
            <th>Dátum od</th>
            <th>Dátum do</th>
            <th>Typ</th>
            <th>Vytvorený</th>
            <th>Aktívna</th>
            <th>Akcia</th>
        </tr>
        </thead>
        <tbody>
        @if(isset($loyalty_account_settings['add']) && !empty($loyalty_account_settings['add']))
            @foreach($loyalty_account_settings['add'] as $add)
                <tr>
                    <td>{{$add['id']}}</td>
                    <td>{{$add['internal_name']}}</td>
                    <td>{{$add['date_required'] == '0' ? 'Nie' : 'Áno' }}</td>
                    <td>{{!is_null($add['date_from']) ? date('d.m.Y',strtotime($add['date_from'])) : '-----'}}</td>
                    <td>{{!is_null($add['date_to']) ? date('d.m.Y',strtotime($add['date_to'])) : '-----'}}</td>
                    <td>{{$add['loyalty_account_setting_type']}}</td>
                    <td>{{date('d.m.Y H:i:s',strtotime($add['created_at']))}}</td>
                    <td>
                        @can('loyaltyaccountsetting.edit')
                            <?=$add['active'] == '1' ? '<span class="changeActivityProfile" data-id="'.$add['id'].'"> <i class="icon-check active" style="color:darkgreen"></i></span>' : '<span class="changeActivityProfile" data-id="'.$add['id'].'"><i class="icon-cross2 nonactive" style="color:darkred"></i></span>'?>
                        @endcan
                    </td>
                    <td>
                        @can('loyaltyaccountsetting.edit')
                            <a href="{{route('loyaltyaccountsettings.edit',[$add['id']])}}"><span class="badge badge-primary">Upraviť</span></a>
                        @endcan
                        @can('loyaltyaccountsetting.delete')
                            <form action="{{route('loyaltyaccountsettings.destroy',[$add['id']])}}" method="post" style="display:inline" id="formDestroy_{{$add['id']}}">
                                @csrf
                                {{method_field('delete')}}
                                <span class="badge badge-danger confirmDestroy" data-id="{{$add['id']}}">Zmazať</span>
                            </form>
                        @endcan
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
