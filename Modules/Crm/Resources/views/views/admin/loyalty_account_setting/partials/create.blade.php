<style>
    .changeActivityProfile{
        cursor:pointer;
    }
</style>

<div class="table-responsive">
    <table id="customerTable" class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>#</th>
            <th>Interný názov</th>
            <th>Dátumovo závislé</th>
            <th>Dátum od</th>
            <th>Dátum do</th>
            <th>Typ</th>
            <th>Vytvorený</th>
            <th>Aktívna</th>
            <th>Akcia</th>
        </tr>
        </thead>
        <tbody>
            @if(isset($loyalty_account_settings['create']) && !empty($loyalty_account_settings['create']))
                @foreach($loyalty_account_settings['create'] as $create)
                    <tr>
                        <td>{{$create['id']}}</td>
                        <td>{{$create['internal_name']}}</td>
                        <td>{{$create['date_required'] == '0' ? 'Nie' : 'Áno' }}</td>
                        <td>{{!is_null($create['date_from']) ? date('d.m.Y',strtotime($create['date_from'])) : '-----'}}</td>
                        <td>{{!is_null($create['date_to']) ? date('d.m.Y',strtotime($create['date_to'])) : '-----'}}</td>
                        <td>{{$create['loyalty_account_setting_type']}}</td>
                        <td>{{date('d.m.Y H:i:s',strtotime($create['created_at']))}}</td>
                        <td>
                            @can('loyaltyaccountsetting.edit')
                                <?=$create['active'] == '1' ? '<span class="changeActivityProfile" data-id="'.$create['id'].'"> <i class="icon-check active" style="color:darkgreen"></i></span>' : '<span class="changeActivityProfile" data-id="'.$create['id'].'"><i class="icon-cross2 nonactive" style="color:darkred"></i></span>'?>
                            @endcan
                        </td>
                        <td>
                            @can('loyaltyaccountsetting.edit')
                                <a href="{{route('loyaltyaccountsettings.edit',[$create['id']])}}"><span class="badge badge-primary">Upraviť</span></a>
                            @endcan
                            @can('loyaltyaccountsetting.edit')
                                <form action="{{route('loyaltyaccountsettings.destroy',[$create['id']])}}" method="post" style="display:inline" id="formDestroy_{{$create['id']}}">
                                    @csrf
                                    {{method_field('delete')}}
                                    <span class="badge badge-danger confirmDestroy" data-id="{{$create['id']}}">Zmazať</span>
                                </form>
                            @endcan
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
</div>
