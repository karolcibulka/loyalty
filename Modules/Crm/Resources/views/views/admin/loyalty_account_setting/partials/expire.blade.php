<style>
    .changeActivityProfile{
        cursor:pointer;
    }
</style>

<div class="table-responsive">
    <table id="customerTable" class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>#</th>
            <th>Interný názov</th>
            <th>Dátumovo závislé</th>
            <th>Dátum od</th>
            <th>Dátum do</th>
            <th>Typ</th>
            <th>Vytvorený</th>
            <th>Aktívna</th>
            <th>Akcia</th>
        </tr>
        </thead>
        <tbody>
        @if(isset($loyalty_account_settings['expire']) && !empty($loyalty_account_settings['expire']))
            @foreach($loyalty_account_settings['expire'] as $expire)
                <tr>
                    <td>{{$expire['id']}}</td>
                    <td>{{$expire['internal_name']}}</td>
                    <td>{{$expire['date_required'] == '0' ? 'Nie' : 'Áno' }}</td>
                    <td>{{!is_null($expire['date_from']) ? date('d.m.Y',strtotime($expire['date_from'])) : '-----'}}</td>
                    <td>{{!is_null($expire['date_to']) ? date('d.m.Y',strtotime($expire['date_to'])) : '-----'}}</td>
                    <td>{{$expire['loyalty_account_setting_type']}}</td>
                    <td>{{date('d.m.Y H:i:s',strtotime($expire['created_at']))}}</td>
                    <td>
                        @can('loyaltyaccountsetting.edit')
                            <?=$expire['active'] == '1' ? '<span class="changeActivityProfile" data-id="'.$expire['id'].'"> <i class="icon-check active" style="color:darkgreen"></i></span>' : '<span class="changeActivityProfile" data-id="'.$expire['id'].'"><i class="icon-cross2 nonactive" style="color:darkred"></i></span>'?>
                        @endcan
                    </td>
                    <td>
                        @can('loyaltyaccountsetting.edit')
                            <a href="{{route('loyaltyaccountsettings.edit',[$expire['id']])}}"><span class="badge badge-primary">Upraviť</span></a>
                        @endcan
                        @can('loyaltyaccountsetting.delete')
                            <form action="{{route('loyaltyaccountsettings.destroy',[$expire['id']])}}" method="post" style="display:inline" id="formDestroy_{{$expire['id']}}">
                                @csrf
                                {{method_field('delete')}}
                                <span class="badge badge-danger confirmDestroy" data-id="{{$expire['id']}}">Zmazať</span>
                            </form>
                        @endcan
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
