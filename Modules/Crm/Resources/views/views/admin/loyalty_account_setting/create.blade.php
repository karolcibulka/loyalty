@extends('crm::layouts.master')

@section('content')

    <style>
        .generated-wrapper-in{
            background-color:#ececec;
            border-radius:7px;
            padding:10px;
        }
        .generated-wrapper{
            display:none;
        }
    </style>
<script src="{{asset('assets/global_assets/js/plugins/forms/selects/select2.min.js')}}"></script>
<script src="{{asset('assets/global_assets/js/demo_pages/form_select2.js')}}"></script>

<div class="card">
    <div class="card-header">
        <h5>Vytvorenie nového profilu</h5>
    </div>
    <div class="card-body">
        <form action="{{route('loyaltyaccountsettings.store')}}" method="post">
            <div class="row">
                @csrf
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Interný názov</label>
                        <input type="text" name="internal_name" class="form-control" placeholder="Interný názov">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Typ</label>
                        <select name="loyalty_account_setting_type" id="" class="form-control">
                            <option value="CREATE">Vytvorenie</option>
                            <option value="ADD">Pripočítavanie</option>
                            <option value="CHARGE">Čerpanie</option>
                            <option value="EXPIRE">Expirácia</option>
                        </select>
                    </div>
                </div>
                <legend></legend>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Dátumové obmedzenie</label>
                        <select name="date_required" id="date_required" class="form-control">
                            <option value="0">Nie</option>
                            <option value="1">Áno</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Dátum od</label>
                        <div class="input-group-prepend date" id="datetimepicker1" data-target-input="nearest">
                            <input type="text" class="form-control datetimepicker-input date-control" name="date_from" value="{{date('d.m.Y')}}" data-target="#datetimepicker1" data-toggle="datetimepicker" disabled/>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Dátum do</label>
                        <div class="input-group-prepend date" id="datetimepicker2" data-target-input="nearest">
                            <input type="text" class="form-control datetimepicker-input date-control" name="date_to" value="{{date('d.m.Y',strtotime('+ 1 month'))}}" data-target="#datetimepicker2" data-toggle="datetimepicker" disabled/>
                        </div>
                    </div>
                </div>
                <legend></legend>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Pre užívateľské skupiny</label>
                        <select multiple="multiple" id="selectGroups" class="form-control select" data-fouc>
                            @if(isset($customer_groups) && !empty($customer_groups))
                                @foreach($customer_groups as $customer_group)
                                    <option value="{{$customer_group['id']}}">{{$customer_group['internal_name']}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <input type="hidden" name="customer_groups" value="" id="customer_groups">
                <div class="col-md-12">
                    <div class="row group_wrapper">
                        @if(isset($customer_groups) && !empty($customer_groups))
                            @foreach($customer_groups as $customer_group)
                                <div class="col-md-3 mt-1 generated-wrapper generated-wrapper-{{$customer_group['id']}}">
                                    <div class="generated-wrapper-in">
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <h5>{{$customer_group['internal_name']}}</h5>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Typ prepočtu</label>
                                                    <select name="extra_key[{{$customer_group['id']}}]" class="form-control">
                                                        <option value="points">Body</option>
                                                        <option value="ratio">Koeficient</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Počet bodov / koeficient</label>
                                                    <input type="text" class="form-control" placeholder="Počet bodov / koeficient" name="extra_value[{{$customer_group['id']}}]">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <legend></legend>
                <div class="col-md-12">
                    <button class="btn btn-primary btn-own btn-custom">Uložiť</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>

    $(function () {
        $('#datetimepicker1,#datetimepicker2').datetimepicker({
            format:"DD.MM.YYYY",
            timePicker:false,
        });
    });

    $('#date_required').on('change',function(){
        var _this = $(this);
        if(_this.val() === '1'){
            $('.date-control').attr('disabled',false);
        }
        else{
            $('.date-control').attr('disabled',true);
        }
    });

    $('#selectGroups').on('change',function(){
       var val = $(this).val();
       $('#customer_groups').val(val);
       $('.generated-wrapper').hide();
       $.each(val,function(key,val){
           $('.generated-wrapper-'+val).show();
       });
    });
</script>
@endsection
