@extends('crm::layouts.master')

@section('content')

    <style>
        .badge{
            cursor:pointer;
        }
    </style>
    <div class="card">
        <div class="card-header">
            <a href="{{route('loyaltyaccountsettings.create')}}" class="btn btn-primary btn-custom btn-own text-white">Vytvoriť nový profil</a>
            <legend></legend>
        </div>
        <div class="card-body">
            <ul class="nav nav-tabs nav-tabs-solid nav-justified rounded border-0">
                <li class="nav-item"><a href="#solid-rounded-justified-tab1" class="nav-link rounded-left active show" data-toggle="tab">Vytvorenie</a></li>
                <li class="nav-item"><a href="#solid-rounded-justified-tab4" class="nav-link" data-toggle="tab">Pripočítavanie</a></li>
                <li class="nav-item"><a href="#solid-rounded-justified-tab2" class="nav-link" data-toggle="tab">Čerpanie</a></li>
                <li class="nav-item"><a href="#solid-rounded-justified-tab3" class="nav-link" data-toggle="tab">Expirácia</a></li>
            </ul>
            <div class="tab-pane fade active show" id="solid-rounded-justified-tab1">
                @include('crm::views.admin.loyalty_account_setting.partials.create')
            </div>
            <div class="tab-pane fade" id="solid-rounded-justified-tab4">
                @include('crm::views.admin.loyalty_account_setting.partials.add')
            </div>
            <div class="tab-pane fade" id="solid-rounded-justified-tab2">
                @include('crm::views.admin.loyalty_account_setting.partials.charge')
            </div>
            <div class="tab-pane fade" id="solid-rounded-justified-tab3">
                @include('crm::views.admin.loyalty_account_setting.partials.expire')
            </div>
        </div>
    </div>

    <script>

        $('.confirmDestroy').on('click',function(){
            var id = $(this).data().id;
            Swal.fire({
                title:'Naozaj chcete zmazať tento profil?',
                showCancelButton:true,
                cancelButtonText:'ODVOLAŤ',
                confirmButtonText:'ZMAZAŤ',
                confirmButtonColor:'darkred',
            }).then(function(response){
                if(response.value === true){
                    $('#formDestroy_'+id).submit();
                }
            });
        });


        var getCheckedIcon = function(){
            var string = '<i class="icon-check active" style="color:darkgreen"></i></span>';
            return string
        }

        var getUncheckedIcon = function(){
            var string = '<i class="icon-cross2 nonactive" style="color:darkred"></i></span>';
            return string
        }

        var getSpinner = function(){
            var string = '<i class="spinner icon-spinner2" style="color:black"></i></span>';
            return string;
        }

        $('.changeActivityProfile').on('click',function(){
            var $this = $(this);
            var value = '0';
            if($this.find('i').hasClass('icon-spinner2')){
                return;
            }
            else if($this.find('i').hasClass('active')){
                $this.html(getSpinner());
                value= '0';
                $.ajax({
                    type: "POST",
                    url: '{{route('changeActivityProfile')}}',
                    data: {value:value,id:$this.data().id,_token:'{{csrf_token()}}'},
                    dataType:'json',
                    success:function(data){
                        if(data.status==='1'){
                            $this.html(getUncheckedIcon());
                        }
                    }
                });
            }
            else{
                $this.html(getSpinner());
                value = 1;
                $.ajax({
                    type: "POST",
                    url: '{{route('changeActivityProfile')}}',
                    data: {value:value,id:$this.data().id,_token:'{{csrf_token()}}'},
                    dataType:'json',
                    success:function(data){
                        if(data.status==='1'){
                            $this.html(getCheckedIcon());
                        }
                    }
                });
            }
        });

        $('.change-points-settings').on('change',function(e){
            e.preventDefault();
            e.stopPropagation();
            var _this = $(this);
            $.ajax({
                type:'post',
                url:'{{route('changeSettingsForTypes')}}',
                dataType:'json',
                data:{_token:'{{csrf_token()}}',type:_this.data().type,value:_this.val()},
                success:function(data){

                }
            });

        });
    </script>
@endsection
