@extends('crm::layouts.master')

@section('summernote')
    1
@endsection

@section('content')
    <script src="{{asset('assets/global_assets/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script src="{{asset('assets/global_assets/js/demo_pages/form_select2.js')}}"></script>

    <link href="https://cdn.jsdelivr.net/npm/grapesjs@0.15.5/dist/css/grapes.min.css" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/combine/npm/grapesjs@0.15.5,npm/grapesjs-mjml@0.0.31/dist/grapesjs-mjml.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.8.0/ckeditor.js"></script>
    <script src="{{asset('assets/mjml/dist/grapesjs-mjml.min.js')}}"></script>
    <script src="{{asset('assets/mjml/dist/grapesjs-plugin-ckeditor.min.js')}}"></script>
    <script src="{{asset('assets/mjml/src/grapeck.js')}}"></script>

    <link href="{{asset('assets/codemirror/lib/codemirror.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/codemirror/addon/display/fullscreen.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/codemirror/theme/night.css')}}" rel="stylesheet"/>
    <script src="{{asset('assets/codemirror/lib/codemirror.js')}}"></script>
    <script src="{{asset('assets/codemirror/mode/xml/xml.js')}}"></script>
    <script src="{{asset('assets/codemirror/addon/display/fullscreen.js')}}"></script>

    <style>
        #gjs{
            height:800px !important;
        }
    </style>

    <div class="card">
        <div class="card-header">
            <h5>Vytvorenie novej šablóny</h5>
        </div>
        <div class="card-body">
            <form action="{{route('newsletters.store')}}" enctype="multipart/form-data" method="post">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Interný názov</label>
                            <input type="text" class="form-control" placeholder="Interný názov" name="internal_name">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Predmet</label>
                            <input type="text" class="form-control" placeholder="Predmet" name="subject">
                        </div>
                    </div>
                    <legend></legend>
                    <div class="col-md-12"></div>
                        <strong>Premenné</strong><br>
                        <ul>
                            <li>{name}</li>
                            <li>{surname}</li>
                            <li>{address}</li>
                            <li>{state}</li>
                            <li>{zip}</li>
                        </ul>
                    <legend></legend>
                    <div class="col-md-12">
                        <strong>Prílohy</strong> <span class="badge badge-success badge-add-attachments" style="margin-left:10px;cursor:pointer;">Pridať prílohu</span><br>
                        <div id="attachments" style="width:100%;">

                        </div>
                        <div id="attachment" class="row" style="display:none;margin-top:10px;">
                            <div class="col-md-10">
                                <input type="file" class="form-control">
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn btn-danger btn-remove-attachment">Zmazať</button>
                            </div>
                        </div>
                    </div>
                    <legend></legend>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Zdroj emailovej šablóny</label>
                            <select name="source" id="source" class="form-control">
                                <option value="own">Vlastné</option>
                                <option value="summernote">Textový formát (SummerNote)</option>
                                <option value="external">Zdrojový kód z extérneho systému</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12 source" id="own">
                        <div id="gjs" style="height:800px; overflow:hidden">
                            <mjml>
                                <mj-body>

                                </mj-body>
                            </mjml>
                        </div>
                    </div>
                    <div class="col-md-12 source" id="summernote" style="display:none;">
                        <div class="row">
                            <div class="col-md-6">
                                <textarea name="summernote" class="form-control summernote" id="summernote-editor" cols="30" rows="10"></textarea>
                            </div>
                            <div class="col-md-6">
                                <iframe src="" id="summernote-iframe" frameborder="0" style="height: 330px;"></iframe>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-12 source" id="external" style="display:none;">
                        <div class="row">
                            <div class="col-md-6">
                                 <textarea id="codemirror" cols="30" rows="10" ></textarea>
                                <input type="hidden" name="codemirror" value="">
                            </div>
                            <div class="col-md-6">
                                <iframe src="" frameborder="0" width="100%" height="auto" id="codemirror-iframe" style="height: 330px;"></iframe>
                            </div>
                        </div>

                    </div>

                    <legend></legend>
                    <div class="col-md-12">
                        <button class="btn btn-primary btn-own btn-custom btn-save">
                            Uložiť
                        </button>
                    </div>
                </div>

                <input type="hidden" name="ownSolution" class="own-solution">
            </form>
        </div>
    </div>

    <script>

        var counter = 0;


        $(document).on('click','.btn-remove-attachment',function(e){
            e.preventDefault();
            e.stopPropagation();
            $(this).closest('.row').remove();
        });

        $('.badge-add-attachments').on('click',function(e){
            e.preventDefault();
            e.stopPropagation();

            var _wrapper = $('#attachments');
            var _elem = $('#attachment').clone();

            _elem.removeAttr('id')
            _elem.show();
            _elem.find('input').attr('name','attachment['+counter+']');

            _wrapper.append(_elem);
            counter++;

        });

        var codeEditor = CodeMirror.fromTextArea(document.getElementById("codemirror"), {
            lineNumbers: true,
            theme: "night",
            extraKeys: {
                "F11": function(cm) {
                    cm.setOption("fullScreen", !cm.getOption("fullScreen"));
                },
                "Esc": function(cm) {
                    if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
                }
            }
        });

        codeEditor.on('change',function(){
            var codeEditorValue = codeEditor.getValue();

            $('[name="codemirror"]').val(JSON.stringify(codeEditorValue));

            var iframe = document.getElementById('codemirror-iframe');
            iframe = iframe.contentWindow || ( iframe.contentDocument.document || iframe.contentDocument);

            iframe.document.open();
            iframe.document.write(codeEditor.getValue());
            iframe.document.close();
        });

        $('.summernote').summernote();

        $('.summernote').on('summernote.change',function(e){
            var iframe = document.getElementById('summernote-iframe');
            iframe = iframe.contentWindow || ( iframe.contentDocument.document || iframe.contentDocument);

            iframe.document.open();
            iframe.document.write($(this).val());
            iframe.document.close();
        })

        $('#source').on('change',function(){
            $('.source').hide();
            $('#'+$(this).val()).show();
        });

        var editor = grapesjs.init({
            height: '100%',
            noticeOnUnload: 0,
            storageManager: { autoload: 0 },
            container: '#gjs',
            fromElement: true,
            plugins: ['grapesjs-mjml', 'gjs-plugin-ckeditor'],
            pluginsOpts: {
                'gjs-plugin-ckeditor': {
                    position: 'center',
                    options: {
                        startupFocus: true,
                        extraAllowedContent: '*(*);*{*}', // Allows any class and any inline style
                        allowedContent: true, // Disable auto-formatting, class removing, etc.
                        enterMode: CKEDITOR.ENTER_BR,
                        extraPlugins: 'sharedspace,justify,colorbutton,panelbutton,font',
                        toolbar: [
                            { name: 'styles', items: ['Font', 'FontSize' ] },
                            ['Bold', 'Italic', 'Underline', 'Strike'],
                            {name: 'paragraph', items : [ 'NumberedList', 'BulletedList']},
                            {name: 'links', items: ['Link', 'Unlink']},
                            {name: 'colors', items: [ 'TextColor', 'BGColor' ]},
                        ],
                    }
                }
            },
            richTextEditor: {

            },
            assetManager: {
                upload: 0,
                uploadText: 'Uploading is not available',
            },
        });

        editor.on('change',function(){
            var html = editor.runCommand('mjml-get-code');
            var exportHtml = html.html;
            var mjml = editor.getHtml();

            var obj = {
                mjml:mjml,
                html:exportHtml,
            }

           $('.own-solution').val(JSON.stringify(obj));
        });




        var blockManager = editor.BlockManager;

        blockManager.add('my-name',{
            label: 'Meno',
            content: '<mj-text>{name}</mj-text>',
            attributes: { class: 'fa fa-user' },
        });

        blockManager.add('my-surname',{
            label: 'Priezvisko',
            content: '<mj-text>{surname}</mj-text>',
            attributes: { class: 'fa fa-user' },
        });

        blockManager.add('my-address',{
            label: 'Adresa',
            content: '<mj-text>{address}</mj-text>',
            attributes: { class: 'fa fa-user' },
        });

        blockManager.add('my-country',{
            label: 'Štát',
            content: '<mj-text>{country}</mj-text>',
            attributes: { class: 'fa fa-user' },
        });

        blockManager.add('my-zip',{
            label: 'PSČ',
            content: '<mj-text>{zip}</mj-text>',
            attributes: { class: 'fa fa-user' },
        });

        window.editor = editor;
        CKEDITOR.dtd.$editable.a = 1;

    </script>
@endsection
