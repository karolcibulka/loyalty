@if(isset($templates) && !empty($templates))
<div class="table-responsive">
    <table id="customerTable" class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>#</th>
            <th>Interný názov</th>
            <th>Vytvorené</th>
            <th>Akcia</th>
        </tr>
        </thead>
        <tbody>
        @foreach($templates as $template)
        <tr data-id="{{$template['id']}}">
            <td>{{$template['id']}}</td>
            <td>{{$template['internal_name']}}</td>
            <td>{{date('d.m.Y',strtotime($template['created_at']))}}</td>
            <td>
                @can('newsletter.edit')
                    <a href="{{route('newsletters.edit',$template['id'])}}"><span class="badge badge-primary">Upraviť</span></a>
                @endcan
                @can('newsletter.delete')
                        <form action="{{route('newsletters.destroy',$template['id'])}}" method="post" style="display:inline;">
                            @csrf
                            {{method_field('DELETE')}}
                        </form>
                    <span class="badge badge-danger badge-remove">Zmazať</span>
                @endcan
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

    <div id="links" style="margin-top:20px;float:right;">
        {{$templates->links()}}
    </div>

</div>

<script>
    $('.badge-remove').on('click',function(){
        var _this = $(this);
        Swal.fire({
            title:'Naozaj chcete zmazať tento filter?',
            showCancelButton:true,
            cancelButtonText:'ODVOLAŤ',
            confirmButtonText:'ZMAZAŤ',
            confirmButtonColor:'darkred',
        }).then(function(response) {
            if (response.value === true) {
                _this.closest('form').submit();
            }
        });
    });
</script>
@endif
