@extends('crm::layouts.master')

@section('content')
    <div class="card">
        <div class="card-header">
            <h5>Emailové šablóny</h5>
        </div>
        <div class="card-body">
            <a href="{{route('newsletters.create')}}" class="btn btn-primary btn-custom btn-own text-white">Vytvorenie novej šablóny</a>
            <legend></legend>
            <div class="row">
                @include('crm::views.admin.newsletters.partials.table')
            </div>
        </div>
    </div>
@endsection
