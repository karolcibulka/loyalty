@extends('crm::layouts.master')

@section('content')
    <div class="card">
        <div class="card-header">
            <h5>Zariadenia</h5>
        </div>
        <div class="card-body">
            <a href="{{route('properties.create')}}" style="color:white" class="btn btn primary btn-custom">Vytvoriť zariadenie</a>
            <legend></legend>
            @include('crm::views.admin.partials.properties.table')
        </div>
    </div>
@endsection
