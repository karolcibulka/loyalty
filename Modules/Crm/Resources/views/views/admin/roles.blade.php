@extends('crm::layouts.master')

@section('content')
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <div class="card">
        <div class="card-header">
            <h5>Roles</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-9">
                    <a href="{{route('roles.create')}}" class="btn btn-primary btn-custom" style="color:white;">Vytvoriť rolu</a>
                </div>
               <div class="col-md-3">
                   <a href="{{route('roles.show',['order'])}}" class="btn btn-success w-100" style="color:white;">Zmena poriadia</a>
               </div>
            </div>
            <legend></legend>
            <div id="tableWrapper">
                @include('crm::views.admin.partials.roles.rolesTable')
            </div>
        </div>
    </div>
@endsection
