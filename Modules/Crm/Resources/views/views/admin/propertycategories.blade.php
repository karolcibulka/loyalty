@extends('crm::layouts.master')

@section('content')
    <div class="card">
        <div class="card-header">
            <h5>Kategórie zariadení</h5>
        </div>
        <div class="card-body">
            <a href="{{route('propertycategories.create')}}" style="color:white" class="btn btn primary btn-custom">Vytvoriť kategóriu zariadení</a>
            <legend></legend>
            @include('crm::views.admin.partials.propertycategories.table')
        </div>
    </div>
@endsection
