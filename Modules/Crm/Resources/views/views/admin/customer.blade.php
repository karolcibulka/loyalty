@extends('crm::layouts.master')

@section('content')
    <div class="row">
        <div class="col-md-2">
            <div class="card">
                <div class="card-header">
                    <h5>Filter</h5>
                </div>
                <div class="card-body">
                    <form action="{{route('customer')}}" method="get">
                        <div class="form-group">
                            <label for="" class="form-label">Meno</label>
                            <input type="text" class="form-control" value="{{(isset($requests['first_name']) && !empty($requests['first_name'])) ? $requests['first_name'] : ''}}" name="first_name" placeholder="Meno">
                        </div>
                        <div class="form-group">
                            <label for="" class="form-label">Priezvisko</label>
                            <input type="text" class="form-control" value="{{(isset($requests['last_name']) && !empty($requests['last_name'])) ? $requests['last_name'] : ''}}" name="last_name" placeholder="Priezvisko">
                        </div>
                        <div class="form-group">
                            <label for="" class="form-label">Email</label>
                            <input type="text" class="form-control" value="{{(isset($requests['email']) && !empty($requests['email'])) ? $requests['email'] : ''}}" name="email" placeholder="Email">
                        </div>
                        <legend></legend>
                        <button class="btn btn-primary btn-custom">Vyhľadať</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <h5>Zoznam zákazníkov</h5>
                </div>
                <div class="card-body">
                    @include('crm::views.admin.partials.customers.table')
                </div>
            </div>
        </div>
    </div>
@endsection
