<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label>Driver</label>
            <select name="driver" id="driver" class="form-control">
                <option {{isset($settings['driver']) && !empty($settings['driver']) && $settings['driver'] == 'default' ? 'selected' : ''}} value="default">Defaultné nastavenia</option>
                <option {{isset($settings['driver']) && !empty($settings['driver']) && $settings['driver'] == 'smtp' ? 'selected' : ''}} value="smtp">SMTP</option>
                <option {{isset($settings['driver']) && !empty($settings['driver']) && $settings['driver'] == 'sendmail' ? 'selected' : ''}} value="sendmail">PHP Mail</option>
            </select>
        </div>
    </div>
</div>

<div class="row email_settings_wrapper email_settings_wrapper_smtp email_settings_wrapper_sendmail" style="{{isset($settings['driver']) && !empty($settings['driver']) && $settings['driver'] == 'smtp' ? '' : 'display:none;'}}">
    <legend></legend>
    <div class="col-md-6">
        <div class="form-group">
            <label>HOST</label>
            <input type="text" name="host" value="{{$settings['host']}}" placeholder="HOST" class="form-control">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>PORT</label>
            <input type="text" name="port" value="{{$settings['port']}}" placeholder="PORT" class="form-control">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>USERNAME</label>
            <input type="text" name="username" value="{{$settings['username']}}" placeholder="USERNAME" class="form-control">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>PASSWORD</label>
            <input type="text" name="password" value="{{$settings['password']}}" placeholder="PASSWORD" class="form-control">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>FROM</label>
            <input type="text" name="name" value="{{$settings['name']}}" placeholder="FROM" class="form-control">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>ADDRESS</label>
            <input type="text" name="address" value="{{$settings['address']}}" placeholder="ADDRESS" class="form-control">
        </div>
    </div>
</div>

<script>
    $('#driver').on('change',function(){
        var _this = $(this);
        var _wrapper = $('.email_settings_wrapper');

       if(_this.val()!=='default'){
           _wrapper.hide();
           $('.email_settings_wrapper_'+_this.val()).show();
       }
       else{
           _wrapper.hide();
       }
    });
</script>
