@extends('crm::layouts.master')

@section('content')
    <div class="card">
        <div class="card-header">
            <h5>Nastavenie CRM</h5>
        </div>
        <div class="card-body">
            <ul class="nav nav-tabs nav-tabs-solid nav-justified border-0">
                <li class="nav-item"><a href="#email_settings" class="nav-link active show" data-toggle="tab">Emailové nastavenia</a></li>
                <li class="nav-item"><a href="#solid-justified-tab2" class="nav-link" data-toggle="tab">Inactive</a></li>
            </ul>
            <legend></legend>
            <form action="{{route('settings.store')}}" method="post">
                @csrf
                <div class="tab-content">
                    <div class="tab-pane fade active show" id="email_settings">
                        @include('crm::views.admin.crmsettings.partials.email_settings')
                    </div>
                    <div class="tab-pane fade" id="solid-justified-tab2">
                        Coming soon...
                    </div>
                </div>
                <legend></legend>
                <button class="btn btn-primary btn-own btn-custom">
                    Uložiť
                </button>
            </form>
        </div>
    </div>
@endsection
