@if(isset($customers) && !empty($customers))
    <div class="table-responsive">
        <table id="customerTable" class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th>Meno</th>
                <th>Priezvisko</th>
                <th>Email</th>
                <th>Newsletter</th>
                <th>MailChimp Subscribe</th>
                <th>MailChimp - priemer otvorených</th>
                <th>MailChimp - priemer kliknutí</th>
                <th>MailChimp - zdroj</th>
                <th>MailChimp - id</th>
                <th>Akcia</th>
            </tr>
            </thead>
            <tbody>
                @foreach($customers as $customer)
                    <tr>
                        <td>{{$customer['id']}}</td>
                        <td>{{$customer['first_name']}}</td>
                        <td>{{$customer['last_name']}}</td>
                        <td>{{$customer['email']}}</td>
                        <td>{!! $customer['newsletter'] == '1' ? '<strong style="color:darkgreen">Áno</strong>' : '<strong style="color:darkred">Nie</strong>' !!}</td>
                        <td>{!! isset($subscribedCustomers['emails']) && !empty($subscribedCustomers['emails']) && in_array($customer['email'],$subscribedCustomers['emails']) ? '<strong style="color:darkgreen">Áno</strong>' : '<strong style="color:darkred">Nie</strong>' !!}</td>
                        @if(isset($subscribedCustomers['members'][$customer['email']]) && !empty($subscribedCustomers['members'][$customer['email']]))
                            <td>{{$subscribedCustomers['members'][$customer['email']]['stats']['opened']}}</td>
                            <td>{{$subscribedCustomers['members'][$customer['email']]['stats']['clicked']}}</td>
                            <td>{{$subscribedCustomers['members'][$customer['email']]['source']}}</td>
                            <td>{{$subscribedCustomers['members'][$customer['email']]['id']}}</td>
                        @else
                            <td>-----</td>
                            <td>-----</td>
                            <td>-----</td>
                            <td>-----</td>
                        @endif
                        <td>
                            @can('mailchimp.edit')
                                @if(isset($subscribedCustomers['emails']) && !empty($subscribedCustomers['emails']) && in_array($customer['email'],$subscribedCustomers['emails']))
                                    <a href="{{route('mailchimp.changeSubscribe',[$customer['id'],'0'])}}">
                                        <div class="badge badge-danger">
                                            Unsubscribe
                                        </div>
                                    </a>
                                @else
                                    <a href="{{route('mailchimp.changeSubscribe',[$customer['id'],'1'])}}">
                                        <div class="badge badge-success">
                                            Subscribe
                                        </div>
                                    </a>
                                @endif
                            @endcan
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="row">
        <div class="col-md-12 mt-2">
            {{$customers->links()}}
        </div>
    </div>
@endif
