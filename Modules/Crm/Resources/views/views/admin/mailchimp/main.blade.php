@extends('crm::layouts.master')

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="row">
                @if(isset($message) && !empty($message))
                    <div class="col-md-9">
                        <h5>MailChimp</h5>
                    </div>
                    <div class="col-md-3">
                        @can('mailchimp.edit')
                            <a href="{{route('mailchimp.edit',0)}}">
                                <div class="alert alert-info text-center">
                                    <strong>Deaktivovať MailChimp</strong>
                                </div>
                            </a>
                        @endcan
                    </div>
                @else
                    <div class="col-md-3">
                        <h5>MailChimp</h5>
                    </div>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-4">
                                @can('mailchimp.edit')
                                    <a href="{{route('mailchimp.sync',0)}}">
                                        <div class="alert alert-warning text-center">
                                            <strong>Odhlásiť všetkých</strong>
                                        </div>
                                    </a>
                                @endcan
                            </div>
                            <div class="col-md-4">
                                @can('mailchimp.edit')
                                    <a href="{{route('mailchimp.sync',1)}}">
                                        <div class="alert alert-success text-center">
                                            <strong>Prihlásiť všetkých</strong>
                                        </div>
                                    </a>
                                @endcan
                            </div>
                            <div class="col-md-4">
                                @can('mailchimp.edit')
                                    <a href="{{route('mailchimp.edit',0)}}">
                                        <div class="alert alert-info text-center">
                                            <strong>Deaktivovať MailChimp</strong>
                                        </div>
                                    </a>
                                @endcan
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <legend></legend>
        </div>
        <div class="card-body">
            <div class="row">
                @if(isset($message) && !empty($message))
                <div class="col-md-12">
                    <div class="alert alert-danger text-center">
                        <ul class="list-unstyled">
                            {!! $message !!}
                        </ul>
                    </div>
                </div>
                @else
                    @include('crm::views.admin.mailchimp.partials.table')
                @endif
            </div>
        </div>
    </div>
@endsection
