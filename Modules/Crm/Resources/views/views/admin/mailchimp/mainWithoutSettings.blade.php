@extends('crm::layouts.master')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h5>Nemáte povolený mailchamp</h5>
                </div>
                <div class="col-md-4 m-auto tex-center">
                    <div class="form-group text-center">
                        <label>Povoliť MailChimp</label>
                        <select class="form-control changeMailChimp">
                            <option value="0">Nie</option>
                            <option value="1">Áno</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('.changeMailChimp').on('change',function(e){
            e.preventDefault();
            e.stopPropagation();

            var val = $(this).val();
            if(val === '1'){
                window.location.href="{{route('mailchimp.edit',1)}}";
            }
        })
    </script>
@endsection
