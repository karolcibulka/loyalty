@extends('crm::layouts.master')

@section('content')
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <div class="card">
        <div class="card-header">
            <h5>Užívatelia</h5>
        </div>
        <div class="card-body">
            @can('user.create')
                <a href="{{route('users.create')}}" class="btn btn-primary btn-custom">Vytvoriť nového užívateľa</a>
            <legend></legend>
            @endcan
            <div id="tableWrapper">
                @include('crm::views.admin.partials.users.userTable')
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#userTable').DataTable();
        });
    </script>
@endsection

