@extends('crm::layouts.master')

@section('content')
    <div class="card">
        <div class="card-header">
            <h5>Vytvorenie novej skupiny zákazníkov</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <a href="{{route('customergroups.create')}}" class="btn btn-custom btn-primary btn-own text-white">Vytvoriť novú skupinu</a>
                </div>
                <legend></legend>
            </div>
            @include('crm::views.admin.partials.customergroups.table')
        </div>
    </div>
@endsection
