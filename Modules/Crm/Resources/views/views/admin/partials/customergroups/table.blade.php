

@if(isset($customer_groups) && !empty($customer_groups))
<div class="table-responsive">
    <table id="customerTable" class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>#</th>
            <th>Interný názov</th>
            <th>Vytvorená</th>
            <th>Defaultná</th>
            <th>Akcia</th>
        </tr>
        </thead>
        <tbody>
            @foreach($customer_groups as $group)
                <tr>
                    <td>{{$group['id']}}</td>
                    <td>{{$group['internal_name']}}</td>
                    <td>{{date('d.m.Y H:i:s',strtotime($group['created_at']))}}</td>
                    <td>
                        @can('customergroup.edit')
                            {!! $group['is_default'] == '1' ? '<span class="changeDefaultGroup changeDefaultGroup_'.$group['id'].'" data-id="'.$group['id'].'"> <i class="icon-check active" style="color:darkgreen"></i></span>' : '<span class="changeDefaultGroup changeDefaultGroup_'.$group['id'].'" data-id="'.$group['id'].'"><i class="icon-cross2 nonactive" style="color:darkred"></i></span>' !!}
                        @endcan
                    </td>
                    <td>
                        @can('customergroup.edit')
                            <a href="{{route('customergroups.edit',[$group['id']])}}"><span class="badge pointer badge-primary">Upraviť</span></a>
                        @endcan
                        @can('customergroup.delete')
                            <form action="{{route('customergroups.destroy',[$group['id']])}}" method="post" id="formDestroy_{{$group['id']}}" style="display:inline-block">
                                {{method_field('delete')}}
                                @csrf
                                <button class="badge pointer badge-danger" data-id="{{$group['id']}}">Zmazať</button>
                            </form>
                        @endcan
                    </td>
                </tr>
            @endforeach

        </tbody>
    </table>
    <div id="links" style="margin-top:20px;float:right;">
        {{$customer_groups->links()}}
    </div>
</div>
@endif

<script>
    $('button.badge-danger').on('click',function(event){
        event.preventDefault();
        event.stopPropagation();
        var id = $(this).data().id;
        Swal.fire({
            title:'Naozaj chcete zmazať túto skupinu?',
            showCancelButton:true,
            cancelButtonText:'ODVOLAŤ',
            confirmButtonText:'ZMAZAŤ',
            confirmButtonColor:'darkred',
        }).then(function(response){
            if(response.value === true){
                $('#formDestroy_'+id).submit();
            }
        });
    });

    var getCheckedIcon = function(){
        var string = '<i class="icon-check active" style="color:darkgreen"></i></span>';
        return string
    }

    var getUncheckedIcon = function(){
        var string = '<i class="icon-cross2 nonactive" style="color:darkred"></i></span>';
        return string
    }

    var getSpinner = function(){
        var string = '<i class="spinner icon-spinner2" style="color:black"></i></span>';
        return string;
    }


    $('.changeDefaultGroup').on('click',function(){
        var $this = $(this);

        if($this.find('i').hasClass('icon-spinner2')){
            return;
        }
        else if($this.find('i').hasClass('active')){

        }
        else{
            $('.changeDefaultGroup').html(getSpinner());
            //$this.html(getSpinner());
            $.ajax({
                type: "POST",
                url: '{{route('change_default_group')}}',
                data: {id:$this.data().id,_token:'{{csrf_token()}}'},
                dataType:'json',
                success:function(data){
                    if(data.status==='1'){
                        $('.changeDefaultGroup').html(getUncheckedIcon());
                        $this.html(getCheckedIcon());
                    }
                }
            });
        }
    });
</script>
