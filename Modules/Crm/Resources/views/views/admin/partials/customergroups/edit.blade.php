@extends('crm::layouts.master')

@section('content')
    <div class="card">
        <div class="card-header">
            <h5>Vytvorenie novej skupiny</h5>
        </div>
        <div class="card-body">
            <form action="{{route('customergroups.update',[$group['id']])}}" method="post">
                {{method_field('PUT')}}
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Interný názov</label>
                            <input type="text" name="internal_name" value="{{$group['internal_name']}}" placeholder="Názov skupiny" class="form-control">
                        </div>
                    </div>
                    <legend></legend>
                    <div class="col-md-12">
                        <button class="btn btn-primary btn-custom btn-own">Uložiť</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
