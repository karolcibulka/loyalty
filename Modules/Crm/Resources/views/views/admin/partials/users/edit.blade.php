@extends('crm::layouts.master')

@section('content')
    <style>
        ul li {
            list-style-type:none;
        }
    </style>
    <div class="card" style="width:60%;margin: 0 auto;">
        <div class="card-header">
            <h5>Úprava užívateľa</h5>
        </div>
        <div class="card-body">
            <form action="{{route('users.update',['user'=>$user->id])}}" method="post">
                {{ method_field('PUT') }}
                @csrf
                <input type="hidden" name="id" value="{{$user->id}}">
                <div class="form-group">
                    <label for="" class="form-label">Meno</label>
                    <input type="text" name="name" value="{{$user->name}}" class="form-control" placeholder="Meno a priezvisko">
                </div>
                <div class="form-group">
                    <label for="" class="form-label">Email</label>
                    <input type="text" name="email" value="{{$user->email}}" disabled class="form-control" placeholder="Email">
                </div>
                <input type="hidden" name="old-password" value="{{$user->password}}">
                <legend></legend>

                @if(isset($roles) && !empty($roles))
                    <h5>Rola</h5>
                    <ul>
                        @foreach($roles as $key => $role)
                            <li>
                                <label for="">
                                    <input type="checkbox" {{(in_array($role['id'],$userRole)) ? 'checked' : ''}} name="roles[]" value="{{$role['id']}}">
                                    {{$role['name']}}
                                </label>
                            </li>
                        @endforeach
                    </ul>
                @endif
                <legend></legend>
                <button class="btn btn-primary btn-custom">Upraviť</button>
            </form>
        </div>
    </div>
@endsection
