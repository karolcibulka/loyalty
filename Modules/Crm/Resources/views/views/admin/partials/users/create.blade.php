@extends('crm::layouts.master')

@section('content')
    <style>
        ul li {
            list-style-type:none;
        }
    </style>
    <div class="card" style="width:60%;margin: 0 auto;">
        <div class="card-header">
            <h5>Vytvorenie užívateľa</h5>
        </div>
        <div class="card-body">
            <form action="{{route('users.store')}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="" class="form-label">Meno</label>
                    <input type="text" name="name" class="form-control" placeholder="Meno a priezvisko">
                </div>
                <div class="form-group">
                    <label for="" class="form-label">Email</label>
                    <input type="text" name="email" class="form-control" placeholder="Email">
                </div>
                <div class="form-group">
                    <label for="" class="form-label">Heslo</label>
                    <input type="password" name="password" class="form-control" placeholder="Heslo">
                </div>
                <div class="form-group">
                    <label for="" class="form-label">Heslo</label>
                    <input type="password" name="confirm-password" class="form-control" placeholder="Potvrdenie hesla">
                </div>
                <legend></legend>

                @if(isset($roles) && !empty($roles))
                    <h5>Rola</h5>
                    <ul>
                    @foreach($roles as $key => $role)
                        <li>
                            <label for="">
                                <input type="checkbox" name="roles[]" value="{{$role['id']}}">
                                {{$role['name']}}
                            </label>
                        </li>
                    @endforeach
                    </ul>
                @endif
                <legend></legend>
                <button class="btn btn-primary btn-custom">Vytvoriť</button>
            </form>
        </div>
    </div>
@endsection
