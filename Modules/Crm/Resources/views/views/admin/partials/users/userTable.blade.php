<style>
    .dataTables_paginate {
        margin-top: 10px;
    }
</style>

<table id="userTable" class="table  table-striped table-bordered" style="width:100%">
    <thead>
    <tr>
        <th style="text-align: center;">#</th>
        <th style="text-align: center;">Meno</th>
        <th style="text-align: center;">E-mail</th>
        <th style="text-align: center;">Rola</th>
        <th style="text-align: center;">Zaregistrovaný</th>
        <th style="text-align: center;">Akcie</th>
    </tr>
    </thead>
    <tbody>
    @if(isset($users) && !empty($users))
        @foreach($users as $user)
        <tr>
            <td style="text-align: center;">{{$user['id']}}</td>
            <td class="editContent" data-type="show_name" data-id="{{$user['id']}}" style="text-align: center;">{{$user['name']}}</td>
            <td class="editContent" data-type="name" data-id="{{$user['id']}}" style="text-align: center;">{{$user['email']}}</td>
            <td class="editContent" data-type="name" data-id="{{$user['id']}}" style="text-align: center;">{{isset($user['roles_names']) && !empty($user['roles_names']) ? implode(', ',$user['roles_names']) : ''}}</td>
            <td class="editContent" data-type="controller" data-id="{{$user['id']}}" style="text-align:center;">{{date('d.m.Y H:i:s',strtotime($user['created_at']))}}</td>
            <td style="text-align: center;">
                @if(isset($user['roles_order']) && !empty($user['roles_order']) && last($user['roles_order'])<=$max_role)
                    @can('user.edit')
                    <a href="{{route('users.edit',['user'=>$user['id']])}}" class="editRow badge badge-primary" data-id="{{$user['id']}}" style="cursor:pointer;margin-right:10px;">
                        Upraviť
                    </a>
                    @endcan
                    @can('user.delete')
                        <form action="{{route('users.destroy',['user'=>$user['id']])}}" style="display:inline" id="user_form_delete_{{$user['id']}}" method="post">
                            @csrf
                            @method('DELETE')
                            <span class="deleteRow badge badge-danger" data-id="{{$user['id']}}" style="cursor:pointer;">
                                Vymazať
                            </span>
                        </form>
                    @endcan
                @else
                    <small>Nemáš právomoc na editáciu</small>
                @endif
            </td>
        </tr>
        @endforeach
    @endif
    </tbody>
</table>

<script>
    $('.deleteRow').on('click',function(){
       var id = $(this).data().id;
        Swal.fire({
            title:'Naozaj chcete zmazať tohto užívateľa?',
            showCancelButton:true,
            cancelButtonText:'ODVOLAŤ',
            confirmButtonText:'ZMAZAŤ',
            confirmButtonColor:'darkred',
        }).then(function(response){
            if(response.value === true){
                $('#user_form_delete_'+id).submit();
            }
        });
    });
</script>
