@extends('crm::layouts.master')

@section('content')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/nestable2/1.6.0/jquery.nestable.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/nestable2/1.6.0/jquery.nestable.min.css">

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-3">
                    <a href="{{route('roles.index')}}" class="btn btn-custom btn-primary text-white">Späť</a>
                </div>
            </div>
            <legend></legend>
            <div class="dd" style="min-width: 100%;">
                <ol class="dd-list">
                    @if(isset($roles) && !empty($roles))
                        @foreach($roles as $role)
                            <li class="dd-item" data-id="{{$role['id']}}">
                                <div class="dd-handle">{{$role['name']}}</div>
                            </li>
                        @endforeach
                    @endif
                </ol>
            </div>
        </div>
    </div>

    <script>
        $('.dd').nestable({
            maxDepth:1,
        });

        $('.dd').on('change',function(){
            var _json = $('.dd').nestable('serialize');

            $.ajax({
                type:'post',
                url:'{{route('roles.order')}}',
                dataType:'json',
                data:{json:_json,_token:'{{csrf_token()}}'},
            })
        })
    </script>
@endsection
