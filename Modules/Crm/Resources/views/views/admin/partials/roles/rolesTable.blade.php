<table id="rolesTable" class="table  table-striped table-bordered" style="width:100%">
    <thead>
    <tr>
        <th style="text-align: center;">#</th>
        <th style="text-align: center;">Poradie</th>
        <th style="text-align: center;">Rola</th>
        <th style="text-align: center;">Akcie</th>
    </tr>
    </thead>
    <tbody>
    @if(isset($roles) && !empty($roles))
        @foreach($roles as $role)
            <tr>
                <td class="editContent" data-type="showName" data-id="{{$role['id']}}" style="text-align: center;">{{$role['id']}}</td>
                <td class="editContent" data-type="showName" data-id="{{$role['id']}}" style="text-align: center;">{{$role['role_order']}}</td>
                <td class="editContent" data-type="showName" data-id="{{$role['id']}}" style="text-align: center;">{{$role['name']}}</td>
                <td style="text-align: center;">
                    @can('role.edit')
                    <a href="{{route('roles.edit',['role'=>$role['id']])}}" class="editRow badge badge-primary" data-id="{{$role['id']}}" style="cursor:pointer;margin-right:10px;">
                        Upraviť
                    </a>
                    @endcan
                    @can('role.delete')
                    <form action="{{route('roles.destroy',['role'=>$role['id']])}}" method="POST" id="form_delete_{{$role['id']}}" style="display: inline;">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <span class="deleteRow badge badge-danger" data-id="{{$role['id']}}" style="cursor:pointer;">
                            Vymazať
                        </span>
                    </form>
                    @endcan
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>

<script>
    $('.deleteRow').on('click',function(){
       var id = $(this).data().id;
        Swal.fire({
            title:'Naozaj chcete zmazať túto rolu?',
            showCancelButton:true,
            cancelButtonText:'ODVOLAŤ',
            confirmButtonText:'ZMAZAŤ',
            confirmButtonColor:'darkred',
        }).then(function(response){
            if(response.value === true){
                $('#form_delete_'+id).submit();
            }
        });
    });
</script>
