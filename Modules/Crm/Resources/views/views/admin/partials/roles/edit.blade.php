@extends('crm::layouts.master')

@section('content')
    <div class="card" style="width:60%;margin: 0 auto;">
        <form action="{{route('roles.update',['role',$role['id']])}}" method="POST">
            {{ method_field('PUT') }}
            <div class="card-header">
                <h5>Vytvorenie role</h5>
            </div>
            @csrf
            <input type="hidden" name="id" value="{{$role['id']}}">
            <div class="card-body">
                <div class="form-group">
                    <label for="" class="form-label">Názov role</label>
                    <input type="text" name="name" class="form-control" value="{{$role['name']}}" placeholder="Názov role">
                </div>
                @if(isset($permissions) && !empty($permissions))
                    <legend></legend>
                    <h5>Permissions</h5>
                    <table class="table  table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Názov</th>
                            <th>show</th>
                            <th>create</th>
                            <th>edit</th>
                            <th>delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($permissions as $name => $permission)
                            <tr>
                                <td>{{$name}}</td>
                                <td><input name="permissions[]" {{(in_array($permission['show'],$allPermissions)) ? 'checked' : ''}} value="{{$permission['show']}}" type="checkbox"></td>
                                <td><input name="permissions[]" {{(in_array($permission['create'],$allPermissions)) ? 'checked' : ''}} value="{{$permission['create']}}" type="checkbox"></td>
                                <td><input name="permissions[]" {{(in_array($permission['edit'],$allPermissions)) ? 'checked' : ''}} value="{{$permission['edit']}}" type="checkbox"></td>
                                <td><input name="permissions[]" {{(in_array($permission['delete'],$allPermissions)) ? 'checked' : ''}} value="{{$permission['delete']}}" type="checkbox"></td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                @endif
                <legend></legend>
                <button class="btn btn-primary btn-custom">Uložiť</button>
            </div>
        </form>
    </div>
@endsection
