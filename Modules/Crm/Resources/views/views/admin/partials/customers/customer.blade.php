@extends('crm::layouts.master')

@section('content')
    <style>
        .sidebar-component
        {
            display: block !important;
        }
</style>
        <!-- Content area -->
        <div class="content">

            <!-- Inner container -->
            <div class="d-flex align-items-start flex-column flex-md-row">

                <!-- Left content -->
                <div class="tab-content w-100 order-2 order-md-1">
                    <div class="tab-pane fade active show" id="activity">
                        <div class="card">
                            <div class="card-header header-elements-sm-inline">
                                <h5 class="card-title">{{$customer['first_name']}} {{$customer['last_name']}} - <span style="color:#24a69a">{{(isset($customer['points']) && !empty($customer['points'])) ? $customer['points'] : '0'}} bodov</span></h5>
                            </div>

                            <div class="card-body">
                                <div class="hideable" id="user">
                                    user
                                </div>
                                <div class="hideable" id="userSettings" style="display:none;">
                                    <form action="{{route('customers.update',['customer'=>$customer['id']])}}" method="post" id="editForm">
                                        <div class="row">
                                            {{method_field('put')}}
                                            @csrf
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-2">
                                                <div class="form-group">
                                                    <label for="" class="form-label">Oslovenie</label>
                                                    <select name="salutation" id="" class="form-control">
                                                        <option {{(isset($customer['salutation']) && !empty($customer['salutation']) && $customer['salutation']=='mr') ? 'selected' : ''}} value="mr">Pán</option>
                                                        <option {{(isset($customer['salutation']) && !empty($customer['salutation']) && $customer['salutation']=='mrs') ? 'selected' : ''}} value="mrs">Paní</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-5">
                                                <div class="form-group">
                                                    <label for="" class="form-label">Meno</label>
                                                    <input type="text" name="first_name" value="{{$customer['first_name']}}" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-5">
                                                <div class="form-group">
                                                    <label for="" class="form-label">Priezvisko</label>
                                                    <input type="text" name="last_name" value="{{$customer['last_name']}}" class="form-control" disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                                                <div class="form-group">
                                                    <label for="" class="form-label">Email</label>
                                                    <input type="text" name="email" value="{{$customer['email']}}" class="form-control" disabled>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                                                <div class="form-group">
                                                    <label for="" class="form-label">Telefónne číslo</label>
                                                    <input type="text" name="phone" value="{{$customer['phone']}}" class="form-control" placeholder="09XX XXX XXX">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                                                <div class="form-group">
                                                    <div class="form-group date" id="datetimepicker_1" data-target-input="nearest">
                                                        <label for="" class="form-label">Dátum narodenia</label>
                                                        <div class="input-group-append" data-target="#datetimepicker_1" data-toggle="datetimepicker">
                                                            <input type="text" autocomplete="off" id="birth" name="birth" value="{{(isset($customer['birth']) && !empty($customer['birth'])) ? date('d.m.Y',strtotime($customer['birth'])) : ''}}" class="form-control datetimepicker-input" data-target="#datetimepicker_1">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <legend></legend>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                                                <div class="form-group">
                                                    <label for="" class="form-label">Adresa</label>
                                                    <input type="text" name="adress" value="{{$customer['adress']}}" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                                                <div class="form-group">
                                                    <label for="" class="form-label">Mesto</label>
                                                    <input type="text" name="city" value="{{$customer['city']}}" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                                                <div class="form-group">
                                                    <label for="" class="form-label">PSČ</label>
                                                    <input type="text" name="zip" value="{{$customer['zip']}}" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-4">
                                                <div class="form-group">
                                                    <label for="" class="form-label">Štát</label>
                                                    <select name="state" id="" class="form-control">
                                                        @if(isset($countries) && !empty($countries))
                                                            @foreach($countries as $country)
                                                                <option {{isset($customer['state']) && !empty($customer['state']) && $country['country_code'] == $customer['state'] ? 'selected' : ''}} value="{{$country['country_code']}}">{{$country['country_name']}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <legend></legend>
                                        <button class="btn btn-primary btn-custom" id="editButton">Uložiť zmeny</button>
                                    </form>
                                </div>
                                @if(isset($customer['is_registered']) && !empty($customer['is_registered']))
                                <div class="hideable" id="pointService" style="display:none;">
                                    <form action="{{route('addPoints')}}" method="post">
                                        @csrf
                                        <div class="row">
                                            <div class="col-md-12"><h6>Pridať body</h6></div>
                                            <div class="col-sm-12 col-md-6">
                                                <input type="text" name="points" class="form-control" placeholder="napr. 100">
                                            </div>
                                            <input type="hidden" name="customer_id" value="{{$customer['id']}}">
                                            <input type="hidden" name="active_user_view" value="pointService">
                                            <div class="col-sm-12 col-md-6">
                                                <input type="text" name="message" class="form-control" placeholder="Správa">
                                            </div>
                                            <div class="col-md-12" style="margin-top:10px;"><button class="btn btn-success btn-custom" style="width:100%">Pridať body</button></div>
                                        </div>
                                    </form>
                                    <legend></legend>
                                    <form action="{{route('removePoints')}}" method="post">
                                        @csrf
                                        <div class="row">
                                            <div class="col-md-12"><h6>Odobrať body</h6></div>
                                            <input type="hidden" name="customer_id" value="{{$customer['id']}}">
                                            <input type="hidden" name="active_user_view" value="pointService">
                                            <div class="col-sm-12 col-md-6">
                                                <input type="text" name="points" class="form-control" placeholder="napr. 100">
                                            </div>
                                            <div class="col-sm-12 col-md-6">
                                                <input type="text" name="message" class="form-control" placeholder="Správa">
                                            </div>
                                            <div class="col-md-12" style="margin-top:10px;">
                                                <button class="btn btn-success" style="width:100%;background-color:#bf3b3b">Odobrať body</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                @endif
                                <div class="hideable" id="pointHistory" style="display:none;">
                                   <div class="table-responsive">
                                       <table class="table table-striped table-bordered">
                                           <thead>
                                            <tr>
                                                <th>Dátum</th>
                                                <th>Starý počet bodov</th>
                                                <th>Nový počet bodov</th>
                                                <th>Zmena bodov</th>
                                                <th>Dôvod</th>
                                            </tr>
                                           </thead>
                                           <tbody>
                                                @if(isset($points) && !empty($points))
                                                    @foreach($points as $point)
                                                        <tr>
                                                            <td>{{date('d.m.Y H:i',strtotime($point['created_at']))}}</td>
                                                            <td>{{$point['old_points']}}</td>
                                                            <td>{{$point['new_points']}}</td>
                                                            <td><?=($point['type']=='+') ? '<span style="color:darkgreen">+'.$point['points'].'</span>' : '<span style="color:darkred">-'.$point['points'].'</span>'?></td>
                                                            <td>{{$point['reason']}}</td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                           </tbody>
                                       </table>
                                       <div style="margin:20px;float:right">
                                       {{$points->links()}}
                                       </div>
                                   </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /left content -->


                <!-- Right sidebar component -->
                <div class="sidebar sidebar-light bg-transparent sidebar-component sidebar-component-right wmin-300 border-0 shadow-0 order-1 order-lg-2 sidebar-expand-md">

                    <!-- Sidebar content -->
                    <div class="sidebar-content">

                        <!-- Navigation -->
                        <div class="card">
                            <div class="card-header bg-transparent header-elements-inline">
                                <span class="card-title font-weight-semibold">Navigácia</span>
                                <div class="header-elements">
                                    <div class="list-icons">
                                        <a class="list-icons-item" data-action="collapse"></a>
                                    </div>
                                </div>
                            </div>

                            <div class="card-body p-0">
                                <ul class="nav nav-sidebar my-2">
                                    <li class="nav-item">
                                        <a href="#" class="nav-link active" data-type="user">
                                            <i class="icon-user"></i>
                                            {{$customer['first_name']}} {{$customer['last_name']}}
                                        </a>
                                    </li>

                                    <li class="nav-item-divider"></li>
                                    @if(isset($customer['is_registered']) && !empty($customer['is_registered']))
                                    <li class="nav-item">
                                        <a href="#" class="nav-link" data-type="pointService">
                                            <i class="icon-coin-dollar"></i>
                                            Správa bodov
                                            <span class="badge bg-teal-400 badge-pill ml-auto">{{(isset($customer['points']) && !empty($customer['points'])) ? $customer['points'] : '0'}}</span>
                                        </a>
                                    </li>
                                    @endif
                                    <li class="nav-item">
                                        <a href="#" class="nav-link" data-type="pointHistory">
                                            <i class="icon-calendar3"></i>
                                            História bodov
                                            <span class="badge bg-teal-400 badge-pill ml-auto">{{$allPoints}}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item-divider"></li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link" data-type="userSettings">
                                            <i class="icon-cog3"></i>
                                            Nastavenia účtu
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- /navigation -->

                    </div>
                    <!-- /sidebar content -->

                </div>
                <!-- /right sidebar component -->

            </div>
            <!-- /inner container -->

        </div>
        <!-- /content area -->
    </div>

<script>

    $('.nav-link').on('click',function(){
        $('.nav-link').removeClass('active');
        $(this).addClass('active');
        var type = $(this).data().type;
        $('.hideable').hide();
        $('#'+type).show();
    });

    $('#datetimepicker_1').datetimepicker(
        {
            format: 'DD.MM.YYYY',
            icons: {
                next: 'icon-arrow-right8',
                previous: 'icon-arrow-left8',
                up: 'icon-arrow-up8',
                down: 'icon-arrow-down8',
                time: "icon-alarm",
                date: "icon-calendar",
            },
            widgetPositioning: {
                vertical: 'bottom',
                horizontal:'auto'
            },
            pickSeconds: false,
            pick12HourFormat: false,
        }
    );


</script>

@endsection
