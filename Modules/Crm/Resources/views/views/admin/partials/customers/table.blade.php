@if(isset($customers) && !empty($customers))
    <div class="table-responsive">
        <table id="customerTable" class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th>Registrovaný</th>
                <th>Meno</th>
                <th>Priezvisko</th>
                <th>Email</th>
                <th>Skupina</th>
                <th>Počet bodov</th>
                <th>Vytvorený</th>
                <th>Akcia</th>
            </tr>
            </thead>
            <tbody>
            @foreach($customers as $customer)
                <tr data-id="{{$customer['id']}}">
                    <td>{{$customer['id']}}</td>
                    <td>{!! (isset($customer['is_registered']) && !empty($customer['is_registered'])) ? '<span style="color:green">Áno</span>' : '<span style="color:red">Nie</span>' !!}</td>
                    <td>{{$customer['first_name']}}</td>
                    <td>{{$customer['last_name']}}</td>
                    <td>{{$customer['email']}}</td>
                    <td>
                        @can('customer.edit')
                            <select name="" data-id="{{$customer['id']}}" class="form-control group_changer">
                                <option value="">Nepriradený</option>
                                @if(isset($customer_groups) && !empty($customer_groups))
                                    @foreach($customer_groups as $customer_group)
                                        <option {{!is_null($customer['group_id']) && $customer_group['id'] == $customer['group_id'] ? 'selected' : ''}} value="{{$customer_group['id']}}">{{$customer_group['internal_name']}}</option>
                                    @endforeach
                                @endif
                            </select>
                        @endcan
                    </td>
                    <td>{{(isset($customer['points']) && !empty($customer['points'])) ? $customer['points'] : '0'}}</td>
                    <td>{{date('d.m.Y',strtotime($customer['created_customer']))}}</td>
                    <td>
                        @can('customer.show')
                            <a href="{{route('customers.show',['customer'=>$customer['id']])}}"><i class="icon-eye"></i></a>
                        @endcan
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div id="links" style="margin-top:20px;float:right;">
            {{$customers->links()}}
        </div>

    </div>
@endif

<script>
    $('.group_changer').on('change',function(){
       var _this = $(this);
       var val = _this.val();
       $.ajax({
          type:"post",
          url:'{{route('customer_change_group')}}',
          data:{id:_this.data().id,val:val,_token:'{{csrf_token()}}'},
          dataType:"json",
          success:function(data){
             if(data.status === '1'){

             }
             else{

             }
          }
       });
    });
</script>
