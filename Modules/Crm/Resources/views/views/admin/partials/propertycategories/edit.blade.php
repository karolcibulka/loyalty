@extends('crm::layouts.master')

@section('content')
    <style>
        .languages{
            display:none;
        }
        .lang_sk{
            display:block;
        }

        .imageWrapper{
            width:100%;
            min-height:200px;
            background-repeat: no-repeat;
            background-size: cover;
            background-position:center;
        }
    </style>

    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote.min.js"></script>

    <div class="row" style="margin-bottom:10px;">
        <div class="col-md-9"></div>
        <div class="col-md-3">
            <select class="form-control" id="languageChanger">
                @if(isset($languages) && !empty($languages))
                    @foreach($languages as $language)
                        <option value="{{$language['lang_code']}}">{{__('global.'.$language['lang_name'])}}</option>
                    @endforeach
                @endif
            </select>
        </div>
    </div>
    <div class="card" style="margin:0 auto;">
        <div class="card-header">
            <h5>
                Úprava kategórie zariadení
            </h5>
        </div>
        <div class="card-body">
            <form action="{{route('propertycategories.update',['propertycategory'=>$id])}}" method="post" enctype="multipart/form-data">
                @csrf
                {{method_field('put')}}
                <div class="row">
                    @if(isset($languages) && !empty($languages))
                        @foreach($languages as $language)
                            <div class="col-md-4 languages lang_{{$language['lang_code']}}">
                                <div class="form-group">
                                    <label>Názov kategórie v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
                                    <input type="text" value="{{isset($propertycategory['title'][$language['lang_code']]) && !empty($propertycategory['title'][$language['lang_code']]) ? $propertycategory['title'][$language['lang_code']] : ''}}" name="title[{{$language['lang_code']}}]" placeholder="Názov" class="form-control">
                                </div>
                            </div>
                        @endforeach
                    @endif
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Interný názov</label>
                            <input type="text" name="internal_name" value="{{$propertycategory['internal_name']}}" placeholder="Interný názov" class="form-control">
                        </div>
                    </div>
                    @if(isset($languages) && !empty($languages))
                        @foreach($languages as $language)
                            <div class="col-md-4 languages lang_{{$language['lang_code']}}">
                                <label>Slug v jazyku {{__('global.'.$language['lang_name'])}}</label>
                                <input type="text" value="{{isset($propertycategory['slug'][$language['lang_code']]) && !empty($propertycategory['slug'][$language['lang_code']]) ? $propertycategory['slug'][$language['lang_code']] : ''}}" name="slug[{{$language['lang_code']}}]" placeholder="Slug" class="form-control">
                            </div>
                        @endforeach
                    @endif
                    <legend></legend>
                    @if(isset($languages) && !empty($languages))
                        @foreach($languages as $language)
                            <div class="col-md-6 languages lang_{{$language['lang_code']}}">
                                <div class="form-group">
                                    <label>Krátky popis v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
                                    <input type="text" name="short_description[{{$language['lang_code']}}]"  value="{{isset($propertycategory['short_description'][$language['lang_code']]) && !empty($propertycategory['short_description'][$language['lang_code']]) ? $propertycategory['short_description'][$language['lang_code']] : ''}}" placeholder="Krátky popis" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6 languages lang_{{$language['lang_code']}}">
                                <div class="form-group">
                                    <label>Dlhý popis v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
                                    <textarea type="text"  name="long_description[{{$language['lang_code']}}]" class="form-control summernote">{{isset($propertycategory['long_description'][$language['lang_code']]) && !empty($propertycategory['long_description'][$language['lang_code']]) ? $propertycategory['long_description'][$language['lang_code']] : ''}}</textarea>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
                <legend></legend>
                <div class="row">
                    <div class="col-md-6">
                        <div class="imageWrapper" style="{{isset($propertycategory['image']) && !empty($propertycategory['image']) ? 'background-image: url("'.asset('images/categories/'.$propertycategory['image']).'")' : ''}}"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <button type="button" class="btn btn-info w100 btn-changeimg">Pridať/zmeniť obrázok</button>
                            </div>
                            <legend></legend>
                            <div class="col-md-12">
                                <button type="button" class="btn btn-success cropImageButton" style="width: 100%; {{isset($propertycategory['image']) && !empty($propertycategory['image']) ? '' : 'display:none;'}}">Orezať obrázok</button>
                            </div>
                            <legend></legend>
                            <div class="col-md-12">
                                <button type="button" class="btn btn-danger w100 mt10 btn-removeImage">Zmazať obrázok</button>
                            </div>
                            <input type="file" name="image" id="img" style="display:none;">
                        </div>
                    </div>
                    <input type="hidden" name="cropper_used" value="0">
                    <input type="hidden" name="cropper_value" value="">

                    <input type="hidden" name="old_image" value="{{isset($propertycategory['image']) && !empty($propertycategory['image']) ? $propertycategory['image'] : ''}}">
                    <input type="hidden" name="remove_old_image" value="0">
                </div>
                <legend></legend>
                <button class="btn btn-primary btn-custom">Uložiť</button>
            </form>
        </div>
    </div>

    <div class="modal fade" id="cropper" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document" style="min-width: 80% !important;">
            <div class="modal-content">
                <div class="modal-body">
                    @include('crm::views.partials.cropper.cropper',['img'=>asset('images/categories/'.$propertycategory['image'])])
                </div>
            </div>
        </div>
    </div>

    <script>
        $('.summernote').summernote();

        $('.btn-changeimg').on('click',function(){
            $('#img').click();
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.removeOldImage').val('0');
                    $('[name="cropper_used"]').val('0');
                    $('[name="cropper_value"]').val('');
                    $('#image').attr('src',e.target.result);
                    $('.imageWrapper').css('background-image','url('+e.target.result+')');
                    $('.cropImageButton').show();
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('.cropImageButton').on('click',function(){
            $('#cropper').modal();
            setTimeout(function(){
                makeClickable()
            },500);
        });

        var makeClickable = function(){
            $(document).find('.aspectSixteenClickable').click();
        }

        $('.btn-removeImage').on('click',function(){
            $('#img').val('');
            $('.imageWrapper').removeAttr('style');
            $('.removeOldImage').val('1');
            $('.cropImageButton').hide();
            $('[name="cropper_used"]').val('0');
            $('[name="cropper_value"]').val('');
        });

        $('#img').on('change',function(e){
            e.preventDefault();
            e.stopPropagation();
            readURL(this);
            $('.removeOldImage').val('1');
        });

        $('#languageChanger').on('change',function(){
            $('.languages').hide();
            $('.lang_'+$(this).val()).show();
        });
    </script>
@endsection
