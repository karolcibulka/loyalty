<div class="table-responsive">
    @if(isset($propertycategories) && !empty($propertycategories))
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th style="text-align: center;">#</th>
            <th style="text-align: center;">Interný názov</th>
            <th style="text-align: center;">Akcia</th>
        </tr>
        </thead>
        <tbody>
            @foreach($propertycategories as $property)
                <tr>
                    <td style="text-align: center;">{{$property['id']}}</td>
                    <td style="text-align: center;">{{$property['internal_name']}}</td>
                    <td style="text-align: center;">
                        @can('propertycategory.edit')
                            <a href="{{route('propertycategories.edit',['propertycategory'=>$property['id']])}}"><span class="badge badge-primary" style="cursor:pointer;">Upraviť</span></a>
                        @endcan
                        @can('propertycategory.delete')
                            <form action="{{route('propertycategories.destroy',['propertycategory'=>$property['id']])}}" method="post" style="display:inline" id="formDestroy_{{$property['id']}}">
                                @csrf
                                {{method_field('delete')}}
                                <span class="badge badge-danger confirmDestroy" data-id="{{$property['id']}}" style="cursor:pointer;">Zmazať</span>
                            </form>
                        @endcan
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{$propertycategories->links()}}
    @endif
</div>

<script>
    $('.confirmDestroy').on('click',function(){
        var id = $(this).data().id;
        Swal.fire({
            title:'Naozaj chcete zmazať túto kategóriu?',
            showCancelButton:true,
            cancelButtonText:'ODVOLAŤ',
            confirmButtonText:'ZMAZAŤ',
            confirmButtonColor:'darkred',
        }).then(function(response){
            if(response.value === true){
                $('#formDestroy_'+id).submit();
            }
        });
    });
</script>
