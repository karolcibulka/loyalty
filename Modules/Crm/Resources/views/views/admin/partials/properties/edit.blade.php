@extends('crm::layouts.master')

@section('content')
    <style>
        .languages{
            display:none;
        }
        .lang_sk{
            display:block;
        }

        .imageWrapper{
            width:100%;
            min-height:200px;
            background-repeat: no-repeat;
            background-size: cover;
            background-position:center;
        }
    </style>

    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote.min.js"></script>

    <div class="row" style="margin-bottom:10px;">
        <div class="col-md-9"></div>
        <div class="col-md-3">
            <select class="form-control" id="languageChanger">
                @if(isset($languages) && !empty($languages))
                    @foreach($languages as $language)
                        <option value="{{$language['lang_code']}}">{{__('global.'.$language['lang_name'])}}</option>
                    @endforeach
                @endif
            </select>
        </div>
    </div>
    <div class="card" style="margin:0 auto;">
        <div class="card-header">
            <h5>
                Úprava kategórie zariadení
            </h5>
        </div>
        <div class="card-body">
            <form action="{{route('properties.update',$property['id'])}}" method="post" enctype="multipart/form-data">
                @csrf
                {{method_field('put')}}
                <div class="row">
                    @if(isset($languages) && !empty($languages))
                        @foreach($languages as $language)
                            <div class="col-md-4 languages lang_{{$language['lang_code']}}">
                                <div class="form-group">
                                    <label>Názov kategórie v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
                                    <input type="text" value="{{isset($property['title'][$language['lang_code']]) && !empty($property['title'][$language['lang_code']]) ? $property['title'][$language['lang_code']] : ''}}" name="title[{{$language['lang_code']}}]" placeholder="Názov" class="form-control">
                                </div>
                            </div>
                        @endforeach
                    @endif
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Interný názov</label>
                            <input type="text" name="internal_name" value="{{$property['internal_name']}}" placeholder="Interný názov" class="form-control">
                        </div>
                    </div>
                    @if(isset($languages) && !empty($languages))
                        @foreach($languages as $language)
                            <div class="col-md-4 languages lang_{{$language['lang_code']}}">
                                <label>Slug v jazyku {{__('global.'.$language['lang_name'])}}</label>
                                <input type="text" value="{{isset($property['slug'][$language['lang_code']]) && !empty($property['slug'][$language['lang_code']]) ? $property['slug'][$language['lang_code']] : ''}}" name="slug[{{$language['lang_code']}}]" placeholder="Slug" class="form-control">
                            </div>
                        @endforeach
                    @endif
                    <legend></legend>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Mesto</label>
                            <input type="text" placeholder="Mesto" value="{{$property['city']}}" name="city" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Štát</label>
                            <input type="text" placeholder="Štát" name="state" value="{{$property['state']}}" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Adresa</label>
                            <input type="text" placeholder="Adresa" name="adress" value="{{$property['adress']}}" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>PSČ</label>
                            <input type="text" placeholder="PSČ" name="zip" value="{{$property['zip']}}" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>IČO</label>
                            <input type="text" placeholder="IČO" name="ico" value="{{$property['ico']}}" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>DIČ</label>
                            <input type="text" placeholder="DIČ" name="dic" value="{{$property['dic']}}" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" placeholder="Email" name="email" value="{{$property['email']}}" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Telefónne číslo</label>
                            <input type="text" placeholder="Telefónne číslo" name="phone" value="{{$property['phone']}}" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Kategória</label>
                            <select name="category_id" class="form-control">
                                @if(isset($categories) && !empty($categories))
                                    @foreach($categories as $category)
                                        <option {{$property['category_id'] == $category['id'] ? 'selected' : ''}} value="{{$category['id']}}">{{$category['internal_name']}}</option>
                                    @endforeach
                                @endif

                            </select>
                        </div>
                    </div>
                    <legend></legend>
                    @if(isset($languages) && !empty($languages))
                        @foreach($languages as $language)
                            <div class="col-md-6 languages lang_{{$language['lang_code']}}">
                                <div class="form-group">
                                    <label>Krátky popis v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
                                    <input type="text" name="short_description[{{$language['lang_code']}}]"  value="{{isset($property['short_description'][$language['lang_code']]) && !empty($property['short_description'][$language['lang_code']]) ? $property['short_description'][$language['lang_code']] : ''}}" placeholder="Krátky popis" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6 languages lang_{{$language['lang_code']}}">
                                <div class="form-group">
                                    <label>Dlhý popis v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
                                    <textarea type="text"  name="long_description[{{$language['lang_code']}}]" class="form-control summernote">{{isset($property['long_description'][$language['lang_code']]) && !empty($property['long_description'][$language['lang_code']]) ? $property['long_description'][$language['lang_code']] : ''}}</textarea>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
                <legend></legend>
                <div class="row">
                    <div class="col-md-6">
                        <div class="imageWrapper" style="{{isset($property['image']) && !empty($property['image']) ? 'background-image: url("'.asset('images/properties/'.$property['image']).'")' : ''}}"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <button type="button" class="btn btn-success w100 btn-changeimg">Pridať/zmeniť obrázok</button>
                            </div>
                            <div class="col-md-12">
                                <button type="button" class="btn btn-danger w100 mt10 btn-removeImage">Zmazať obrázok</button>
                            </div>
                            <input type="file" name="image" id="image" style="display:none;">
                        </div>
                    </div>
                    <input type="hidden" name="old_image" value="{{isset($property['image']) && !empty($property['image']) ? $property['image'] : ''}}">
                    <input type="hidden" name="remove_old_image" value="0">
                </div>
                <legend></legend>
                <button class="btn btn-primary btn-custom">Uložiť</button>
            </form>
        </div>
    </div>

    <script>
        $('.summernote').summernote();

        $('.btn-changeimg').on('click',function(){
            $('#image').click();
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.imageWrapper').css('background-image','url('+e.target.result+')');
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('.btn-removeImage').on('click',function(){
            $('#image').val('');
            $('.imageWrapper').removeAttr('style');
            $('.removeOldImage').val('1');
        });

        $('#image').on('change',function(e){
            e.preventDefault();
            e.stopPropagation();
            readURL(this);
            $('.removeOldImage').val('1');
        });

        $('#languageChanger').on('change',function(){
            $('.languages').hide();
            $('.lang_'+$(this).val()).show();
        });
    </script>
@endsection
