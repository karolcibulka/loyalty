@extends('crm::layouts.master')

@section('content')

    <div class="card">
        <div class="card-header">
            <h5>Filtre</h5>
        </div>
        <div class="card-body">
            @can('filter.create')
                <div class="row">
                    <a href="{{route('filters.create')}}" class="btn btn-primary btn-own btn-custom text-white">Vytvoriť nový filter</a>
                </div>
            @endcan
            <legend></legend>
            @include('crm::views.admin.filters.partials.table')
        </div>
    </div>
@endsection
