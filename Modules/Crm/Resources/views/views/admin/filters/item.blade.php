@extends('crm::layouts.master')

@section('content')
    <div class="card">
        <div class="card-header">
            <h5>{{$filter['internal_name']}} - výsledky hľadania</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-3">
                    <a href="{{route('filters.index')}}" class="btn btn-primary btn-custom btn-own">Späť</a>
                </div>
            </div>
            <legend></legend>
            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                   <thead>
                        <tr>
                            <td>#</td>
                            <td>Meno</td>
                            <td>Priezvisko</td>
                            <td>Dátum narodenia</td>
                            <td>Email</td>
                            <td>Telefónne číslo</td>
                            <td>Mesto</td>
                            <td>PSČ</td>
                            <td>Adresa</td>
                            <td>Štát</td>
                            <td>Registrovaný</td>
                        </tr>
                   </thead>
                    <tbody>
                    @if(isset($response) && !empty($response))
                        @foreach($response as $r)
                            <tr>
                                <td>{{$r->id}}</td>
                                <td>{{$r->first_name}}</td>
                                <td>{{$r->last_name}}</td>
                                <td>{{date('d.m.Y',strtotime($r->birth))}}</td>
                                <td>{{$r->email}}</td>
                                <td>{{$r->phone}}</td>
                                <td>{{$r->city}}</td>
                                <td>{{$r->zip}}</td>
                                <td>{{$r->adress}}</td>
                                <td>{{$r->state}}</td>
                                <td>{{$r->is_registered == '1' ? 'Áno' : 'Nie'}}</td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
