@extends('crm::layouts.master')


@section('content')
    <style>
        .add-parenthesis{
            cursor:pointer;
            color:#4baf50;
        }

        .remove-parenthesis{
            cursor:pointer;
            color:#f44335;
        }

        .active-item{
            display:block;
        }

        .disable-item{
            display:none;
        }
    </style>
    <div class="card">
        <div class="card-header">
            <h5>Vytvorenie nového filtru</h5>
        </div>
        <div class="card-body">
            <form action="{{route('filters.update',[$id])}}" method="post">
                {{method_field('PUT')}}
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Interný názov</label>
                            <input type="text" name="internal_name" id="internal_name" value="{{$filter['internal_name']}}" placeholder="Interný názov" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Popis</label>
                            <textarea name="description" id="" cols="30" rows="5" placeholder="Popis" class="form-control">{{$filter['description']}}</textarea>
                        </div>
                    </div>

                    <legend></legend>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Zobraziť filter v</label>
                            <select multiple class="form-control select show_in_wrapper" data-fouc>
                                <option {{isset($filter['show_in']) && !empty($filter['show_in']) && in_array('newsletter',json_decode($filter['show_in'],true)) ? 'selected' : ''}} value="newsletter">Newsletter</option>
                                <option {{isset($filter['show_in']) && !empty($filter['show_in']) && in_array('loyalty',json_decode($filter['show_in'],true)) ? 'selected' : ''}} value="loyalty">Loyalty</option>
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="show_in" class="show_in" value="{{$filter['show_in']}}">
                    <legend></legend>

                    <div class="col-md-12">

                        @if(in_array(Auth()->user()->email,$dev))
                            <div class="form-group">
                                <label>Ukážka SQL Query</label>
                                <textarea class="form-control generatedSQL" disabled></textarea>
                            </div>
                            <legend></legend>
                        @endif
                        <div class="row">
                            <div class="col-md-3">
                                <select name="" class="form-control models_wrapper">
                                    <option class="check_activity_models_wrapper" value="birth">Dátum narodenia</option>
                                    <option class="check_activity_models_wrapper" value="acommodation_count">Počet pobytov</option>
                                    <option class="check_activity_models_wrapper" value="total_spend">Celková tržba</option>
                                    <option class="check_activity_models_wrapper" value="total_spend_by_date">Celková tržba za obdobie</option>
                                    <option class="check_activity_models_wrapper" value="leave_at">Dátum odchodu</option>
                                    <option class="check_activity_models_wrapper" value="accomodation_date_range">Pobyt za obdobie</option>
                                    <option class="check_activity_models_wrapper" value="state">Štát</option>
                                    <option class="check_activity_models_wrapper" value="city">Mesto</option>
                                    <option class="check_activity_models_wrapper" value="hotel">Hotel</option>
                                    <option class="check_activity_models_wrapper" value="customer_id">Číslo hosťa</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <button type="button" class="btn btn-success btn-add-filter">Pridať nový riadok podmienky</button>
                            </div>
                        </div>

                        <legend></legend>
                        <div class="row" id="filters">

                        </div>
                    </div>
                    <div class="col-md-12 ruleRow" id="myRow" style="display:none;margin-bottom:5px;">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-1">
                                        <span class="btn-parenthesis add-parenthesis" data-type="("> + </span>
                                        <span class="all-parenthesis-left"></span>
                                        <input type="hidden" class="parenthesis-left" value="0">
                                        <span class="btn-parenthesis remove-parenthesis" data-type="("> - </span>
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control rule-model" value="" readonly>
                                        <input type="hidden" class="rule-model-value">
                                    </div>
                                    <div class="col-md-2">
                                        <select class="form-control rule-operator">
                                            <option class="operator-simple operator-all" value="=">rovné</option>
                                            <option class="operator-simple operator-all" value="!=">rôzne</option>
                                            <option class="operator-all" value="<">menšie</option>
                                            <option class="operator-all" value=">">väčšie</option>
                                            <option class="operator-all" value=">=">väčšie/rovné</option>
                                            <option class="operator-all" value="<=">menšie/rovné</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 row-values">

                                    </div>
                                    <div class="col-md-1">
                                        <span class="btn-parenthesis add-parenthesis" data-type=")"> + </span>
                                        <span class="all-parenthesis-right"></span>
                                        <input type="hidden" class="parenthesis-right" value="0">
                                        <span class="btn-parenthesis remove-parenthesis" data-type=")"> - </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-md-6">
                                        <select class="form-control next-condition">
                                            <option value="and">a zároveň</option>
                                            <option value="or">alebo</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <span class="btn btn-danger btn-remove-filter">Zmazať</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" name="sql" id="sql" value="">

                    @include('crm::views.admin.filters.rule_partials.birth')
                    @include('crm::views.admin.filters.rule_partials.acommodation_count')
                    @include('crm::views.admin.filters.rule_partials.total_spend')
                    @include('crm::views.admin.filters.rule_partials.total_spend_by_date')
                    @include('crm::views.admin.filters.rule_partials.leave_at')
                    @include('crm::views.admin.filters.rule_partials.accomodation_date_range')
                    @include('crm::views.admin.filters.rule_partials.state')
                    @include('crm::views.admin.filters.rule_partials.city')
                    @include('crm::views.admin.filters.rule_partials.hotel')
                    @include('crm::views.admin.filters.rule_partials.customer_id')

                    <button class="btn btn-primary btn-custom btn-own text-white mt-5" disabled>Uložiť</button>
                </div>
            </form>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="{{asset('assets/global_assets/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script src="{{asset('assets/global_assets/js/demo_pages/form_select2.js')}}"></script>
    <script>

        $('.show_in_wrapper').on('change',function(){
            $('.show_in').val(JSON.stringify($(this).val()));
        });

        var rules = '<?=(isset($filter['raw']) && !empty($filter['raw'])) ? json_encode($filter['raw']) : ''?>';

        var counter = 0;

        var _queryObj = {
            birth:{
                operators:'operator-all',
                name:'Dátum narodenia',
                active:true,
                hasDatepicker:true,
                datepickerCount:1,
                column:'customers.birth',
            },
            acommodation_count:{
                operators:'operator-all',
                name:'Počet pobytov',
                active:false,
                hasDatepicker:false,
                datepickerCount:0,
                column:'',
            },
            total_spend:{
                operators:'operator-all',
                name:'Celková tržba',
                active:false,
                hasDatepicker:false,
                datepickerCount:0,
                column:'',
            },
            total_spend_by_date:{
                operators:'operator-all',
                name:'',
                active:false,
                hasDatepicker:true,
                datepickerCount:2,
                column:'',
            },
            leave_at:{
                operators:'operator-all',
                name:'',
                active:false,
                hasDatepicker:true,
                datepickerCount:1,
                column:'',
            },
            accomodation_date_range:{
                operators:'operator-all',
                name:'',
                active:false,
                hasDatepicker:true,
                datepickerCount:2,
                column:'',
            },
            state:{
                operators:'operator-simple',
                name:'Štát',
                active:true,
                hasDatepicker:false,
                datepickerCount:0,
                column:'customers.state',
            },
            city:{
                operators:'operator-simple',
                name:'Mesto',
                active:true,
                hasDatepicker:false,
                datepickerCount:0,
                column:'customers.city',
            },
            hotel:{
                operators:'operator-simple',
                name:'Hotel',
                active:false,
                hasDatepicker:false,
                datepickerCount:0,
                column:'customers.hotel',
            },
            customer_id:{
                operators:'operator-simple',
                name:'Číslo hosťa',
                active:true,
                hasDatepicker:false,
                datepickerCount:0,
                column:'customers.id',
            }
        };

        $(document).ready(function(){
            var _options = $('.models_wrapper').find('.check_activity_models_wrapper');
            _options.each(function(){
                if(_queryObj[$(this).val()].active === false){
                    $(this).remove();
                }
            });
        });

        $(document).ready(function(){
            $.each(JSON.parse(rules),function(key,val){
                generateExistingRule(val);
            });
        });




        var generateExistingRule = function(value){
            var _current_type = value['rule-model-value'];
            var _wrapper = $('#filters');
            var _item = $('#myRow').clone();
            var _count = 0;


            _item.removeAttr('id');
            _item.show();
            _item.find('.next-condition').addClass('disable-item').removeClass('active-item');


            var _itemContent = $('#'+_current_type).clone();
            _itemContent.removeAttr('id');
            _itemContent.show();

            if(_queryObj[_current_type].hasDatepicker){
                if(_queryObj[_current_type].datepickerCount === 1){
                    var _datePicker = _itemContent.find('#datetimepicker1');
                    _datePicker.removeAttr('id');
                    _datePicker.attr('id','datepicker_'+counter+'_'+counter);
                    _datePicker.find('input').attr('data-target','#datepicker_'+counter+'_'+counter);

                    _itemContent.find('#datepicker_'+counter+'_'+counter).datetimepicker({
                        format:"DD.MM.YYYY",
                        timePicker:false,
                    });
                }
                else{
                    var _datePicker = _itemContent.find('#datetimepicker1');
                    _datePicker.removeAttr('id');
                    _datePicker.attr('id','datepicker_'+counter+'_'+counter);
                    _datePicker.find('input').attr('data-target','#datepicker_'+counter+'_'+counter);

                    _itemContent.find('#datepicker_'+counter+'_'+counter,_itemContent).datetimepicker({
                        format:"DD.MM.YYYY",
                        timePicker:false,
                    });

                    var _datePicker_second = _itemContent.find('#datetimepicker2');
                    _datePicker_second.removeAttr('id');
                    _datePicker_second.attr('id','datepicker_'+counter+'_'+counter+1);
                    _datePicker_second.find('input').attr('data-target','#datepicker_'+counter+'_'+counter+1);

                    _itemContent.find('#datepicker_'+counter+'_'+counter+1,_itemContent).datetimepicker({
                        format:"DD.MM.YYYY",
                        timePicker:false,
                    });
                }
            }

            _item.find('.row-values').html(_itemContent);

            _item.find('.parenthesis-left').attr('name','parenthesis-left['+counter+']');
            _item.find('.parenthesis-right').attr('name','parenthesis-right['+counter+']');
            _item.find('.rule-model').attr('name','rule-model['+counter+']');
            _item.find('.rule-operator').attr('name','rule-operator['+counter+']');
            _item.find('.next-condition').attr('name','next-condition['+counter+']');
            _itemContent.find('.main-value').attr('name','main-value['+counter+']');
            _item.find('.rule-model-value').attr('name','rule-model-value['+counter+']');


            _item.find('.parenthesis-left').val(value['parenthesis-left']); // dorobit vykreslenie zatvoriek
            if(parseInt(value['parenthesis-left'])>0){
                var _cp = '';
                for(var i=0;i<parseInt(value['parenthesis-left']);i++){
                    _cp += '(';
                }
                _item.find('.all-parenthesis-left').html(_cp);
            }
            _item.find('.parenthesis-right').val(value['parenthesis-right']); //dorobit vykreslenie zatvoriek

            if(parseInt(value['parenthesis-right'])>0){
                var _cp = '';
                for(var i=0;i<parseInt(value['parenthesis-right']);i++){
                    _cp += ')';
                }
                _item.find('.all-parenthesis-right').html(_cp);
            }
            _item.find('.rule-model').val(value['rule-model']);
            _item.find('.rule-operator').val(value['rule-operator']);
            _item.find('.next-condition').val(value['next-condition']);
            _itemContent.find('.main-value').val(value['main-value']);
            _item.find('.rule-model-value').val(value['rule-model-value']);

            _item.find('.operator-all').hide();
            _item.find('.'+_queryObj[_current_type].operators).show();

            _item.find('.rule-model').val(_queryObj[_current_type].name).attr({'data-type':_current_type,'data-column':_queryObj[_current_type].column});

            _item.find('.rule-model-value').val(_current_type);
            _count = _wrapper.find('.ruleRow').length;
            if(_count>0){
                _wrapper.find('.ruleRow').last().find('.next-condition').addClass('active-item').removeClass('disable-item');
            }

            counter++;

            _wrapper.append(_item);

            generateSQL();
        }


        $('.btn-add-filter').on('click',function(){
            var _current_type = $('.models_wrapper').val();

            var _wrapper = $('#filters');
            var _item = $('#myRow').clone();
            var _count = 0;


            _item.removeAttr('id');
            _item.show();
            _item.find('.next-condition').addClass('disable-item').removeClass('active-item');


            var _itemContent = $('#'+_current_type).clone();
            _itemContent.removeAttr('id');
            _itemContent.show();

            if(_queryObj[_current_type].hasDatepicker){
                if(_queryObj[_current_type].datepickerCount === 1){
                    var _datePicker = _itemContent.find('#datetimepicker1');
                    _datePicker.removeAttr('id');
                    _datePicker.attr('id','datepicker_'+counter+'_'+counter);
                    _datePicker.find('input').attr('data-target','#datepicker_'+counter+'_'+counter);

                    _itemContent.find('#datepicker_'+counter+'_'+counter).datetimepicker({
                        format:"DD.MM.YYYY",
                        timePicker:false,
                    });
                }
                else{
                    var _datePicker = _itemContent.find('#datetimepicker1');
                    _datePicker.removeAttr('id');
                    _datePicker.attr('id','datepicker_'+counter+'_'+counter);
                    _datePicker.find('input').attr('data-target','#datepicker_'+counter+'_'+counter);

                    _itemContent.find('#datepicker_'+counter+'_'+counter,_itemContent).datetimepicker({
                        format:"DD.MM.YYYY",
                        timePicker:false,
                    });

                    var _datePicker_second = _itemContent.find('#datetimepicker2');
                    _datePicker_second.removeAttr('id');
                    _datePicker_second.attr('id','datepicker_'+counter+'_'+counter+1);
                    _datePicker_second.find('input').attr('data-target','#datepicker_'+counter+'_'+counter+1);

                    _itemContent.find('#datepicker_'+counter+'_'+counter+1,_itemContent).datetimepicker({
                        format:"DD.MM.YYYY",
                        timePicker:false,
                    });
                }
            }

            _item.find('.row-values').html(_itemContent);

            _item.find('.parenthesis-left').attr('name','parenthesis-left['+counter+']');
            _item.find('.parenthesis-right').attr('name','parenthesis-right['+counter+']');
            _item.find('.rule-model').attr('name','rule-model['+counter+']');
            _item.find('.rule-operator').attr('name','rule-operator['+counter+']');
            _item.find('.next-condition').attr('name','next-condition['+counter+']');
            _itemContent.find('.main-value').attr('name','main-value['+counter+']');
            _item.find('.rule-model-value').attr('name','rule-model-value['+counter+']');

            _item.find('.operator-all').hide();
            _item.find('.'+_queryObj[_current_type].operators).show();

            _item.find('.rule-model').val(_queryObj[_current_type].name).attr({'data-type':_current_type,'data-column':_queryObj[_current_type].column});

            _item.find('.rule-model-value').val(_current_type);
            _count = _wrapper.find('.ruleRow').length;
            if(_count>0){
                _wrapper.find('.ruleRow').last().find('.next-condition').addClass('active-item').removeClass('disable-item');
            }

            counter++;

            _wrapper.append(_item);

            generateSQL();
        });


        $(document).on('click','.btn-remove-filter',function(e){
            e.preventDefault();
            e.stopPropagation();
            $(this).closest('.ruleRow').remove();

            var _wrapper = $('#filters');
            var _count = 0;

            _count = _wrapper.find('.ruleRow').length;
            if(_count>0){
                _wrapper.find('.ruleRow').last().find('.next-condition').addClass('disable-item').removeClass('active-item');
            }

            generateSQL();
        });

        $(document).on('click','.add-parenthesis',function(e){
            e.preventDefault();
            e.stopPropagation();

            var _this = $(this);
            var _ruleRow = _this.closest('.ruleRow');
            var _type = _this.data().type;
            var _wrapper_left = _ruleRow.find('.all-parenthesis-left');
            var _wrapper_right = _ruleRow.find('.all-parenthesis-right');
            var _content = '';
            var _content_length = 0;



            if(_type === ')'){
                _content_length = _wrapper_right.html().length;
                _ruleRow.find('.parenthesis-right').val(_ruleRow.find('.parenthesis-right').val()+1);
                for(var i = 0;i<_content_length+1;i++){
                    _content+=')';
                }
                _ruleRow.find('.parenthesis-right').val(_content.length);
                _wrapper_right.html(_content);
            }
            else{
                _content_length = _wrapper_left.html().length;
                for(var i = 0;i<_content_length+1;i++){
                    _content+='(';
                }
                _ruleRow.find('.parenthesis-left').val(_content.length);
                _wrapper_left.html(_content);
            }

            generateSQL();
        });


        $(document).on('click','.remove-parenthesis',function(e){
            e.preventDefault();
            e.stopPropagation();
            var _this = $(this);
            var _type = _this.data().type;
            var _ruleRow = _this.closest('.ruleRow');
            var _wrapper_left = _ruleRow.find('.all-parenthesis-left');
            var _wrapper_right = _ruleRow.find('.all-parenthesis-right');
            var _content = '';
            var _content_length = 0;

            if(_type === ')'){
                _content_length = _wrapper_right.html().length;
                if(_content_length !== 0){
                    for(var i = 0;i<_content_length-1;i++){
                        _content+=')';
                    }
                    _ruleRow.find('.parenthesis-right').val(_content.length);
                    _wrapper_right.html(_content);
                }
            }
            else{
                _content_length = _wrapper_left.html().length;
                if(_content_length !== 0){
                    for(var i = 0;i<_content_length-1;i++){
                        _content+='(';
                    }
                    _ruleRow.find('.parenthesis-left').val(_content.length);
                    _wrapper_left.html(_content);
                }
            }

            generateSQL();
        });

        $(document).on('change','.rule-model, .rule-operator, .rule-value, .next-condition, .main-value',function(){
            generateSQL();
        });
        $(document).on('input','.main-value',function(){
            generateSQL();
        });

        var generateSQL = function(){
            var _content = '';
            var _objects = $('#filters').find('.ruleRow');
            if(_objects.length>0){
                _content += 'select * from customers inner join customer_users on customer_users.customer_id = customers.id where customer_users.newsletter = 1 and ';
                _objects.each(function(){
                    var _this = $(this);
                    var _value_count = _this.find('.main-value').length;

                    var _type = _this.find('.main-value').data().type;

                    _content +=  _this.find('.all-parenthesis-left').html();
                    _content += _this.find('.rule-model').data().column;
                    _content += _this.find('.rule-operator').val();

                    if(_value_count === 1){
                        _content += generateType(_this.find('.main-value').data().type,_this.find('.main-value').val());
                    }
                    else{

                    }

                    _content += _this.find('.all-parenthesis-right').html();
                    var _next_condition = _this.find('.next-condition');
                    if(_next_condition.hasClass('active-item')){
                        _content += ' '+_next_condition.val()+' ';
                    }
                });
            }
            $('#sql').val(_content);
            $('.generatedSQL').html(_content);
            checkButton();
        }

        var generateType = function(type,value){
            if(type === 'string'){
                return "\"" + value + "\"";
            }
            else if(type === 'date'){
                return "\""+moment(value,'DD.MM.YYYY',true).format('YYYY-MM-DD')+"\"";
            }
            else{
                return value;
            }
        }

        $('#internal_name ,#sql').on('input',function(){
            checkButton();
        });

        var checkButton = function(){
            if($('#internal_name').val()!=="" && $('#sql').val()!==""){
                $('button.btn-own').attr('disabled',false);
            }
            else{
                $('button.btn-own').attr('disabled',true);
            }
        }

    </script>

@endsection
