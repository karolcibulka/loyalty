<div class="row" id="state" style="display:none;">
    <div class="col-md-12">
        <select class="form-control main-value" data-type="string">
            @if(isset($countries) && !empty($countries))
                @foreach($countries as $country)
                    <option value="{{$country['country_code']}}">{{$country['country_name']}}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>
