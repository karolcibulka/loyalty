@if(isset($filters) && !empty($filters))
    <div class="table-responsive">
        <table id="customerTable" class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th>Názov</th>
                <th>Akcia</th>
            </tr>
            </thead>
            <tbody>
            @foreach($filters as $filter)
                <tr data-id="{{$filter['id']}}">
                    <td>{{$filter['id']}}</td>
                    <td>{{$filter['internal_name']}}</td>
                    <td>
                        @can('filter.show')
                            <a href="{{route('filters.show',$filter['id'])}}"><span class="badge badge-primary">Zobraziť výsledky</span></a>
                        @endcan
                        @can('filter.edit')
                            <a href="{{route('filters.edit',$filter['id'])}}"><span class="badge badge-success">Upraviť</span></a>
                        @endcan
                        @can('filter.delete')
                                <form action="{{route('filters.destroy',$filter['id'])}}" method="post" style="display:inline">
                                    {{method_field('DELETE')}}
                                    @csrf
                                    <span class="badge badge-danger badge-remove" style="cursor:pointer;">Zmazať</span>
                                </form>
                        @endcan
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div id="links" style="margin-top:20px;float:right;">
            {{$filters->links()}}
        </div>

    </div>

    <script>
        $('.badge-remove').on('click',function(){
            var _this = $(this);
            Swal.fire({
                title:'Naozaj chcete zmazať tento filter?',
                showCancelButton:true,
                cancelButtonText:'ODVOLAŤ',
                confirmButtonText:'ZMAZAŤ',
                confirmButtonColor:'darkred',
            }).then(function(response) {
                if (response.value === true) {
                    _this.closest('form').submit();
                }
            });
        });
    </script>
@endif
