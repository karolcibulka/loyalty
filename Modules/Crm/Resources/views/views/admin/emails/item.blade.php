@extends('crm::layouts.master')

@section('content')
    <div class="row mb-2">
        <div class="col-md-3">
            <a href="{{route('emails.index')}}" class="btn btn-primary btn-custom btn-own text-white">Späť</a>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h5>Detail ku zasielaniu emailom - {{$item['internal_name']}}</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-4 text-center">
                    Počet úspešne odoslaných<br>
                    <h4>{{$history['sended']}}/ <strong>{{$history['count']}}</strong></h4>
                </div>
                <div class="col-md-4 text-center">
                    Počet prijatých<br>
                    <h4>{{$history['sended']}}/ <strong>{{$history['count']}}</strong></h4>
                </div>
                <div class="col-md-4 text-center">
                    Počet zhliadnutých emailov<br>
                    <h4>{{$history['showed']}}/ <strong>{{$history['count']}}</strong></h4>
                </div>
            </div>
            <legend></legend>
            <div class="row">
                @if(isset($paginate) && !empty($paginate))
                    <div class="table-responsive">
                        <table id="customerTable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>Email</th>
                                <th>Odoslané</th>
                                <th>Prijaté</th>
                                <th>Pozreté</th>
                                <th>Pozreté dňa/o</th>
                                <th>Odoslané dňa/o</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($paginate as $hi)
                                <tr data-id="{{$hi['id']}}">
                                    <td>{{$hi['email']}}</td>
                                    <td>{{$hi['sended']== '1' ? 'Áno' : 'Nie'}}</td>
                                    <td>{{$hi['sended']== '1' ? 'Áno' : 'Nie'}}</td>
                                    <td>{{$hi['showed']== '1' ? 'Áno' : 'Nie'}}</td>
                                    <td>{{$hi['showed_at'] ? date('d.m.Y H:i',strtotime($hi['showed_at'])) : 'Nepozreté'}}</td>
                                    <td>{{$hi['sended_at'] ? date('d.m.Y H:i',strtotime($hi['sended_at'])) : 'Neodoslané'}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <div id="links" style="margin-top:20px;float:right;">
                            {{$paginate->links()}}
                        </div>

                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
