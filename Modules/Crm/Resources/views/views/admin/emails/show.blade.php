@extends('crm::layouts.master')

@section('content')
    <div class="card">
        <div class="card-header">
            <h5>Odosielanie emailov</h5>
        </div>
        <div class="card-body">
            <a href="{{route('emails.create')}}" class="btn btn-primary btn-own btn-custom text-white">Vytvoriť nové nastavenie odosielania emailov</a>
            <legend></legend>
            @include('crm::views.admin.emails.partials.table')
        </div>
    </div>
@endsection
