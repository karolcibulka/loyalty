@if(isset($emailSettings) && !empty($emailSettings))
    <div class="table-responsive">
        <table id="customerTable" class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th>Názov</th>
                <th>Akcia</th>
            </tr>
            </thead>
            <tbody>
            @foreach($emailSettings as $emailSetting)
                <tr data-id="{{$emailSetting['id']}}">
                    <td>{{$emailSetting['id']}}</td>
                    <td>{{$emailSetting['internal_name']}}</td>
                    <td>
                        @can('email.show')
                            <a href="{{route('emails.show',$emailSetting['id'])}}"><span class="badge badge-success">Zobraziť</span></a>
                        @endcan
                        @if(strtotime($emailSetting['date_send_at']) < strtotime(date('Y-m-d H:i:s')))
                            @can('email.edit')
                                <a href="{{route('emails.edit',$emailSetting['id'])}}"><span class="badge badge-primary">Upraviť</span></a>
                            @endcan
                            @can('email.delete')
                                <form action="{{route('emails.destroy',$emailSetting['id'])}}" method="post" style="display:inline">
                                    {{method_field('DELETE')}}
                                    @csrf
                                    <span class="badge badge-danger badge-remove" style="cursor:pointer;">Zmazať</span>
                                </form>
                            @endcan
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div id="links" style="margin-top:20px;float:right;">
            {{$emailSettings->links()}}
        </div>

    </div>

    <script>
        $('.badge-remove').on('click',function(){
            var _this = $(this);
            Swal.fire({
                title:'Naozaj chcete zmazať tieto emailové nastavenia?',
                showCancelButton:true,
                cancelButtonText:'ODVOLAŤ',
                confirmButtonText:'ZMAZAŤ',
                confirmButtonColor:'darkred',
            }).then(function(response) {
                if (response.value === true) {
                    _this.closest('form').submit();
                }
            });
        });
    </script>
@endif
