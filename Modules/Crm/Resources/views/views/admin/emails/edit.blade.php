@extends('crm::layouts.master')

@section('content')
    <div class="card">
        <div class="card-header">
            <h5>Editácia nastavenia</h5>
        </div>
        <div class="card-body">
            <form action="{{route('emails.update',$id)}}" method="post">
                {{method_field('PUT')}}
                @csrf
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Interný názov</label>
                            <input type="text" class="form-control" value="{{$setting['internal_name']}}" name="internal_name" placeholder="Interný názov">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Pre užívateľov (filter)</label>
                            <select name="filter_id" class="form-control">
                                <option value=""> --- </option>
                                @if(isset($filters) && !empty($filters))
                                    @foreach($filters as $filter)
                                        <option {{$setting['filter_id']==$filter['id'] ? 'selected' : ''}} value="{{$filter['id']}}">{{$filter['internal_name']}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Šablóna</label>
                            <select name="template_id" class="form-control">
                                <option value="">---</option>
                                @if(isset($templates) && !empty($templates))
                                    @foreach($templates as $template)
                                        <option {{$setting['template_id']==$template['id'] ? 'selected' : ''}} value="{{$template['id']}}">{{$template['internal_name']}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <legend></legend>
                    <div class="col-md-12">
                        <h5>
                            Nastavenie odosielania
                        </h5>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Odoslať dňa</label>
                            <div class="input-group-prepend date" id="datetimepicker1" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input" name="date_send_at" value="{{date('d.m.Y',strtotime($setting['date_send_at']))}}" data-target="#datetimepicker1" data-toggle="datetimepicker"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>V hodine</label>
                            <select name="hour_send_at" class="form-control">
                                @for($i=1;$i<24;$i++)
                                    <option {{$setting['hour_send_at'] == $i ? 'selected' : ''}} value="{{date('H:i',strtotime($i.':00'))}}">{{date('H:i',strtotime($i.':00'))}}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                    <legend></legend>
                    <button class="btn btn-primary btn-own btn-custom btn-save">
                        Uložiť
                    </button>
                </div>
            </form>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('#datetimepicker1').datetimepicker({
                format:"DD.MM.YYYY",
                timePicker:false,
            });
        });

        $('[name="internal_name"],[name="template_id"],[name="filter_id"],[name="date_send_at"],[name="hour_send_at"]').on('change',function(){
            checkValidity();
        });

        $('[name="internal_name"]').on('input',function(){
            checkValidity();
        });

        var checkValidity = function(){
            var internal_name = $('[name="internal_name"]').val();
            var template_id = $('[name="template_id"]').val();
            var filter_id = $('[name="filter_id"]').val();
            var date_send_at = $('[name="date_send_at"]').val();
            var hour_send_at = $('[name="hour_send_at"]').val();

            if(internal_name!=="" && template_id!=="" && filter_id!=="" && date_send_at!=="" && hour_send_at!==""){
                $('.btn-save').attr('disabled',false);
            }
            else{
                $('.btn-save').attr('disabled',true);
            }
        }
    </script>
@endsection
