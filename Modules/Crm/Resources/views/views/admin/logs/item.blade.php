@extends('crm::layouts.master')

@section('content')
    <div class="row col-md-3 mb-2">
        <a href="{{route('log')}}" class="btn btn-primary btn-custom btn-own text-white">Späť</a>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <h5>Staré dáta</h5>
                    <legend></legend>
                    <pre>
                        @php
                            highlight_string("<?php\n\$data =\n" . var_export(unserialize($log['old_data']),true).";\n?>")
                        @endphp
                    </pre>
                </div>
                <div class="col-md-6">
                    <h5>Nové dáta</h5>
                    <legend></legend>
                    <pre>
                        @php
                            highlight_string("<?php\n\$data =\n" . var_export(unserialize($log['request']),true).";\n?>")
                        @endphp
                    </pre>
                </div>
            </div>
        </div>
    </div>
@endsection
