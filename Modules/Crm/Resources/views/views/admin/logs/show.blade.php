@extends('crm::layouts.master')
@section('content')
    <div class="card">
        <div class="card-header">
            <h5>Logy</h5>
        </div>
        <div class="card-body">
            <form action="{{route('logs.index')}}" method="get">
                <div class="row">
                    <div class="col-xl-2 col-lg-3 col-md-3">
                        <div class="form-group">
                            <label>Systém</label>
                            <select name="api" id="system" class="form-control">
                                <option {{isset($filter['api']) && $filter['api'] == '' ? 'selected' : ''}} value="">Všetky</option>
                                <option {{isset($filter['api']) && $filter['api'] == '0' ? 'selected' : ''}} value="0">Interný</option>
                                <option {{isset($filter['api']) && $filter['api'] == '1' ? 'selected' : ''}} value="1">Extérny</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-3  system-selector system-select-1" style="{{isset($filter['api'])  && $filter['api'] == '1' ?: 'display:none'}}">
                        <label>Api</label>
                        <select name="api_id" id="" class="form-control">
                            <option value="">Všetky</option>
                            @if(isset($apis) && !empty($apis))
                                @foreach($apis as $api)
                                    <option {{isset($filter['api_id']) && !empty($filter['api_id']) && $filter['api_id'] == $api['id'] ? 'selected' : ''}} value="{{$api['id']}}">{{$api['name']}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-3 system-selector system-select-0" style="{{isset($filter['api'])  && $filter['api'] == '0' ?: 'display:none'}}">
                        <label>Užívatelia</label>
                        <select name="user_id" id="" class="form-control ">
                            <option value="">Všetci</option>
                            @if(isset($users) && !empty($users))
                                @foreach($users as $user)
                                    <option {{isset($filter['user_id']) && !empty($filter['user_id']) && $filter['user_id'] == $user['id'] ? 'selected' : ''}} value="{{$user['id']}}">{{$user['name']}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-3 system-selector system-select-0" style="{{isset($filter['api'])  && $filter['api'] == '0' ?: 'display:none'}}">
                        <div class="form-group">
                            <label>Kontroller</label>
                            <select name="controller" id="" class="form-control">
                                <option value="">Všetky</option>
                                    @if(isset($controllers) && !empty($controllers))
                                        @foreach($controllers as $controller)
                                            <option {{isset($filter['controller']) && !empty($filter['controller']) && $filter['controller'] == $controller['name'] ? 'selected' : ''}} value="{{$controller['name']}}">{{$controller['name']}}</option>
                                        @endforeach
                                    @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-3 ">
                        <label>Dátum od</label>
                        <div class="input-group-prepend date" id="datetimepicker1" data-target-input="nearest">
                            <input type="text" class="form-control datetimepicker-input" name="created_from" value="{{isset($filter['created_from']) && !empty($filter['created_from']) ? $filter['created_from'] : date('d.m.Y')}}" data-target="#datetimepicker1" data-toggle="datetimepicker"/>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-3 ">
                        <label>Dátum do</label>
                        <div class="input-group-prepend date" id="datetimepicker2" data-target-input="nearest">
                            <input type="text" class="form-control datetimepicker-input" name="created_to" value="{{isset($filter['created_to']) && !empty($filter['created_to']) ? $filter['created_to'] : date('d.m.Y',strtotime('+ 1 month'))}}" data-target="#datetimepicker2" data-toggle="datetimepicker"/>
                        </div>
                    </div>
                    <div class="col-md-12 mt-3">
                        <button class="btn btn-primary btn-custom btn-own">Vyhľadať</button>
                    </div>
                </div>
            </form>
            <legend></legend>
            @if(isset($logs) && !empty($logs))
                <div class="table-responsive">
                    <table id="customerTable" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Systém</th>
                            <th>API</th>
                            <th>Užívateľ</th>
                            <th>Kontroler</th>
                            <th>Akcia</th>
                            <th>Zmeny</th>
                            <th>Vykonané</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($logs) && !empty($logs))
                            @foreach($logs as $log)
                                <tr>
                                    <td>{{$log['id'] }}</td>
                                    <td>{{$log['api'] ? 'Extérny' : 'Intérny'}}</td>
                                    <td>{{$log['api_id'] ? $log['api_name'] : '---'}}</td>
                                    <td>{{$log['user_id'] ? $log['user_name'] : ''}}</td>
                                    <td>{{$log['controller']}}</td>
                                    <td>{{$log['action']}}</td>
                                    <td>
                                        @can('log.show')
                                            <a href="{{route('logs.show',$log['id'])}}"><i class="icon-eye"></i></a>
                                        @endcan
                                    </td>
                                    <td>{{date('d.m.Y H:i:s',strtotime($log['created_at']))}}</td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
                <div class="row" style="margin-top:10px;">
                    {{$logs->appends(request()->query())->links()}}
                </div>
            @endif
        </div>
    </div>

    <script>
        $(function () {
            $('#datetimepicker1,#datetimepicker2').datetimepicker({
                format:"DD.MM.YYYY",
                timePicker:false,
            });
        });

        $('#system').on('change',function(){
            if($(this).val() === ''){
                $('.system-selector').hide();
            }
            else{
                $('.system-selector').hide();
                $('.system-select-'+$(this).val()).show();
            }
        });
    </script>
@endsection
