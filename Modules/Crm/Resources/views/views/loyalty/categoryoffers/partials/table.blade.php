<style>
    .changeActivityCategoryOffer{
        cursor: pointer;
    }
</style>

@if(isset($categoryoffers) && !empty($categoryoffers))
    <table id="salesTable" class="table  table-striped table-bordered" style="width:100%">
        <thead>
        <tr>
            <th style="text-align: center;">#</th>
            <th style="text-align: center;">Interný názov kategórie</th>
            <th style="text-align: center;">Aktívna</th>
            <th style="text-align: center;">Akcie</th>
        </tr>
        </thead>
        <tbody>
        @foreach($categoryoffers as $categoryoffer)
            <tr>
                <td class="editContent" style="text-align: center;">{{$categoryoffer['id']}}</td>
                <td class="editContent" style="text-align: center;">{{$categoryoffer['internal_name']}}</td>
                <td class="editContent" data-type="showName" data-id="{{$categoryoffer['id']}}" style="text-align: center;">
                    @can('categoryoffer.edit')
                        <?=$categoryoffer['active'] == '1' ? '<span class="changeActivityCategoryOffer" data-id="'.$categoryoffer['id'].'"> <i class="icon-check active" style="color:darkgreen"></i></span>' : '<span class="changeActivityCategoryOffer" data-id="'.$categoryoffer['id'].'"><i class="icon-cross2 nonactive" style="color:darkred"></i></span>'?>
                    @endcan
                </td>
                <td style="text-align: center;">
                    @can('categoryoffer.edit')
                        <a href="{{route('categoryoffers.edit',['categoryoffer'=>$categoryoffer['id']])}}" class="editRow badge badge-primary" data-id="{{$categoryoffer['id']}}" style="cursor:pointer;margin-right:10px;">
                            Upraviť
                        </a>
                    @endcan
                    @can('categoryoffer.delete')
                        <form action="{{route('categoryoffers.destroy',['categoryoffer'=>$categoryoffer['id']])}}" method="POST" id="form_delete_{{$categoryoffer['id']}}" style="display: inline;">
                            @csrf
                            <input type="hidden" name="_method" value="DELETE">
                            <span class="deleteRow badge badge-danger" data-id="{{$categoryoffer['id']}}" style="cursor:pointer;">
                                    Vymazať
                                </span>
                        </form>
                    @endcan
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="row" style="margin-top:20px;">
        <div class="col-md-12">
            {{$categoryoffers->links()}}
        </div>
    </div>
@endif

<script>
    var getCheckedIcon = function(){
        var string = '<i class="icon-check active" style="color:darkgreen"></i></span>';
        return string
    }

    var getUncheckedIcon = function(){
        var string = '<i class="icon-cross2 nonactive" style="color:darkred"></i></span>';
        return string
    }

    var getSpinner = function(){
        var string = '<i class="spinner icon-spinner2" style="color:black"></i></span>';
        return string;
    }

    $('.deleteRow').on('click',function(){
        var id = $(this).data().id;
        Swal.fire({
            title:'Naozaj chcete zmazať túto kategóriu?',
            showCancelButton:true,
            cancelButtonText:'ODVOLAŤ',
            confirmButtonText:'ZMAZAŤ',
            confirmButtonColor:'darkred',
        }).then(function(response){
            if(response.value === true){
                $('#form_delete_'+id).submit();
            }
        });
    });

    $('.changeActivityCategoryOffer').on('click',function(){
        var $this = $(this);
        var value = '0';
        if($this.find('i').hasClass('icon-spinner2')){
            return;
        }
        else if($this.find('i').hasClass('active')){
            $this.html(getSpinner());
            value= '0';
            $.ajax({
                type: "POST",
                url: '{{route('changeActivityCategoryoffer')}}',
                data: {value:value,id:$this.data().id,_token:'{{csrf_token()}}'},
                dataType:'json',
                success:function(data){
                    if(data.status==='1'){
                        $this.html(getUncheckedIcon());
                    }
                }
            });
        }
        else{
            $this.html(getSpinner());
            value = 1;
            $.ajax({
                type: "POST",
                url: '{{route('changeActivityCategoryoffer')}}',
                data: {value:value,id:$this.data().id,_token:'{{csrf_token()}}'},
                dataType:'json',
                success:function(data){
                    if(data.status==='1'){
                        $this.html(getCheckedIcon());
                    }
                }
            });
        }
    });
</script>
