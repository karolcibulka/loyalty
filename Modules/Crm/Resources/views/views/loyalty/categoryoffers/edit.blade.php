@extends('crm::layouts.master')

@section('content')
    <div class="card">
        <div class="card-header">
            <h5>Editácia kategórie ponúk</h5>
        </div>
        <div class="card-body">
            <form action="{{route('categoryoffers.update',[$categoryoffer['id']])}}" method="post">
                {{ method_field('PUT') }}
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Interný názov</label>
                            <input type="text" name="internal_name" value="{{$categoryoffer['internal_name']}}" class="form-control" placeholder="Interný názov">
                        </div>
                    </div>
                    <div class="col-md-6"></div>
                    <legend></legend>
                    <button class="btn-custom btn text-white btn-own">Uložiť</button>
                </div>
            </form>
        </div>
    </div>
@endsection
