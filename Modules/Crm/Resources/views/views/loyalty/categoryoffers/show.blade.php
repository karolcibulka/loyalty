@extends('crm::layouts.master')

@section('content')
    <div class="card">
        <div class="card-header">
            <h5>Kategórie ponúk</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <a href="{{route('categoryoffers.create')}}" class="btn btn-primary btn-custom" style="color:white;">Vytvoriť novú kategóriu</a>
            </div>
            <legend></legend>
            <div id="tableWrapper">
                @include('crm::views.loyalty.categoryoffers.partials.table')
            </div>
        </div>
    </div>
@endsection
