@if(isset($subpages) && !empty($subpages))
    <table class="table  table-striped table-bordered" style="width:100%">
        <thead>
        <tr>
            <th style="text-align: center;">#</th>
            <th style="text-align: center;">Interný názov</th>
            <th style="text-align: center;">Akcie</th>
        </tr>
        </thead>
        <tbody>
        @foreach($subpages as $subpage)
            <tr>
                <td class="editContent" data-type="showName" data-id="{{$subpage['id']}}" style="text-align: center;">{{$subpage['id']}}</td>
                <td class="editContent" data-type="showName" data-id="{{$subpage['id']}}" style="text-align: center;">{{$subpage['internal_name']}}</td>
                <td style="text-align: center;">
                    @can('subpage.edit')
                    <a href="{{route('subpages.edit',$subpage['id'])}}" class="editRow badge badge-primary" data-id="{{$subpage['id']}}" style="cursor:pointer;margin-right:10px;">
                        Upraviť
                    </a>
                    @endcan
                    @can('subpage.delete')
                        <form action="{{route('subpages.destroy',$subpage['id'])}}" method="POST" id="form_delete_{{$subpage['id']}}" style="display: inline;">
                            @csrf
                            <input type="hidden" name="_method" value="DELETE">
                            <span class="deleteRow badge badge-danger" data-id="{{$subpage['id']}}" style="cursor:pointer;">
                                    Vymazať
                                </span>
                        </form>
                    @endcan
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="row" style="margin-top:20px;">
        <div class="col-md-12">
            {{$subpages->links()}}
        </div>
    </div>
    <script>
        $('.deleteRow').on('click',function(){
            var id = $(this).data().id;
            Swal.fire({
                title:'Naozaj chcete zmazať túto podstránku?',
                showCancelButton:true,
                cancelButtonText:'ODVOLAŤ',
                confirmButtonText:'ZMAZAŤ',
                confirmButtonColor:'darkred',
            }).then(function(response){
                if(response.value === true){
                    $('#form_delete_'+id).submit();
                }
            });
        });
    </script>
@endif
