<li class="dd-item dd3-item" id="{{isset($order) && !empty($order) ? '' : 'category'}}" style="{{isset($order) && !empty($order) ? '' : 'display:none;'}}">
    <div class="dd-handle dd-handle3" style="height: 92px;">
        <i class="icon-minus2" style="padding-top:37px;"></i>
    </div>
    <div class="col-md-12 wrapperDD">
        <div class="row">
            <div class="col-md-3 showCategoriesWrapper showAllCategoriesWrapper" style="">
                <div class="form-group">
                    <label>Zobraziť všetky kategórie</label>
                    <select name="{{isset($order) && !empty($order) ? 'show_all['.$order.']' : ''}}" class="form-control show_class">
                        <option {{isset($show_all) && !empty($show_all) && $show_all == '1' ? 'selected' : ''}} value="1">Áno</option>
                        <option {{isset($show_all) && !is_null($show_all) && $show_all == '0' ? 'selected' : ''}} value="0">Nie</option>
                    </select>
                </div>
            </div>
            <input type="hidden" value="category"  name="{{isset($order) && !empty($order) ? 'type['.$order.']' : ''}}" class="type">
            <div class="col-md-3 showCategoriesWrapper showIndividualCategories" style="">
                <div class="form-group">
                    <label>Zobraziť tieto kategórie</label>
                    <select multiple="multiple" class="form-control select {{isset($order) && !empty($order) ? 'isAddedInView' : ''}} selectChanger" style="display:none;">
                        @if(isset($categories) && !empty($categories))
                            @foreach($categories as $category)
                                <option {{isset($show_items) && !empty($show_items) && in_array($category['id'],json_decode($show_items,true)) ? 'selected' : ''}} value="{{$category['id']}}">{{$category['internal_name']}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <input type="hidden" name="{{isset($order) && !empty($order) ? 'show_items['.$order.']' : ''}}" value="{{isset($show_items) && !empty($show_items) ? $show_items : ''}}" class="show_items">
            <div class="col-md-3"></div>
            <div class="col-md-3">
                <div class="form-group">
                    <label style="color:white">asd</label><br>
                    <button type="button" class="btn btn-danger deleteRow">Zmazať</button>
                </div>
            </div>
        </div>
    </div>
</li>
