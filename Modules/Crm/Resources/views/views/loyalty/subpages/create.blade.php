@extends('crm::layouts.master')

@section('summernote')
    1
@endsection

@section('content')
    <style>
        .languages{
            display:none;
        }

        .language-sk{
            display:block;
        }

        .dd-empty{
            display: none !important;
        }

        .wrapperDD{
            position:absolute;
            top:10px;left:40px;
            width:80%;height:100%;
        }


        .imageoffer{
         height:400px;
         background-repeat : no-repeat;
         background-size: cover;
         background-position: center;
     }
    </style>
    <script src="{{asset('assets/global_assets/js/plugins/forms/selects/select2.min.js')}}"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/nestable2/1.6.0/jquery.nestable.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/nestable2/1.6.0/jquery.nestable.min.js"></script>
    <div class="row">
        <div class="col-md-9"></div>
        <div class="col-md-3">
            <select id="language-changer" class="form-control">
                @if(isset($languages) && !empty($languages))
                    @foreach($languages as $language)
                        <option value="{{$language['lang_code']}}">{{__('global.'.$language['lang_name'])}}</option>
                    @endforeach
                @endif
            </select>
        </div>
    </div>
    <div class="card" style="margin-top:10px;">
        <div class="card-header">
            <h5>Vytvorenie novej podstránky</h5>
        </div>
        <div class="card-body">
            <form action="{{route('subpages.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Interný názov</label>
                            <input type="text" name="internal_name" placeholder="Interný názov" class="form-control">
                        </div>
                    </div>
                    @if(isset($languages) && !empty($languages))
                        @foreach($languages as $language)
                            <div class="col-md-6 languages language-{{$language['lang_code']}}">
                                <div class="form-group">
                                    <label>Nadpis v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
                                    <input type="text" name="title[{{$language['lang_code']}}]" placeholder="Nadpis v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}" class="form-control">
                                </div>
                            </div>
                        @endforeach
                    @endif
                    @if(isset($languages) && !empty($languages))
                        @foreach($languages as $language)
                            <div class="col-md-6 languages language-{{$language['lang_code']}}">
                                <div class="form-group">
                                    <label>Krátky popis v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
                                    <input type="text" name="short_description[{{$language['lang_code']}}]" placeholder="Krátky popis v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}" class="form-control">
                                </div>
                            </div>
                        @endforeach
                    @endif
                    @if(isset($languages) && !empty($languages))
                        @foreach($languages as $language)
                            <div class="col-md-6 languages language-{{$language['lang_code']}}">
                                <div class="form-group">
                                    <label>Dlhý popis v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
                                    <textarea type="text" name="long_description[{{$language['lang_code']}}]" class="form-control summernote"></textarea>
                                </div>
                            </div>
                        @endforeach
                    @endif
                    <legend></legend>
                    <div class="col-md-6">
                        <div class="imageoffer"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <button type="button" class="btn btn-info imagePickerButton" style="width:100%;">Vybrať obrázok</button>
                                <input type="file" name="image" class="imagePicker" style="display:none;">
                            </div>
                            <legend></legend>
                            <div class="col-md-12">
                                <button type="button" class="btn btn-danger removeImage" style="width:100%;">Zmazať obrázok</button>
                            </div>
                        </div>
                    </div>
                    <legend></legend>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Štruktúra témy</label>
                            <select name="template" class="form-control">
                                @if(isset($structures) &&!empty($structures))
                                    @foreach($structures as $structure)
                                        <option value="{{$structure}}">{{$structure}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <legend></legend>
                    <div class="col-md-12">
                        <h5>Moduly</h5>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Typ modulu</label>
                                    <select class="form-control typeChooser">
                                        <option value="gallery">Galéria</option>
                                        <option value="category">Kategórie</option>
                                        <option value="offer">Ponuky</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label style="color:white">asd</label><br>
                                    <button class="btn btn-success addNewModule" type="button">Pridať nový modul</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <legend></legend>
                    <div class="dd" id="nestable" style="min-width: 100%;">
                        <ol class="dd-list" >
                            @if(isset($subpage['modules']) && !empty($subpage['modules']))
                                @foreach($subpage['modules'] as $module)
                                    @include('crm::views.loyalty.subpages.partials.'.$module['type'],$module)
                                @endforeach
                            @endif
                        </ol>
                    </div>

                    @include('crm::views.loyalty.subpages.partials.gallery')
                    @include('crm::views.loyalty.subpages.partials.category')
                    @include('crm::views.loyalty.subpages.partials.offer')
                </div>
                <legend></legend>
                <button class="btn btn-primary btn-own btn-custom w100">Uložiť</button>
            </form>
        </div>
    </div>

    <script>

        $(document).ready(function(){
            $('.isAddedInView').select2();
        })

        $(document).on('change','.selectChanger',function(){
            var _this =  $(this);
            _this.closest('.wrapperDD').find('.show_items').val(JSON.stringify(_this.val()));
        });


        var counter = '{{isset($subpage['modules']) && !empty($subpage['modules']) ? count($subpage['modules'])+1 : 1}}';
        counter = parseInt(counter);

        $('#nestable').nestable({
            maxDepth: 1
        });

        $('.addNewModule').on('click',function(){
            var module = $('.typeChooser').val();
            var moduleWrapper = $('#'+module).clone();

            moduleWrapper.removeAttr('id');
            moduleWrapper.attr('data-id',counter);
            moduleWrapper.find('.show_items').attr('name',"show_items["+counter+"]");
            moduleWrapper.find('.informations').attr('name',"informations["+counter+"]");
            moduleWrapper.find('.show_class').attr('name',"show_all["+counter+"]");
            moduleWrapper.find('.type').attr('name',"type["+counter+"]");
            moduleWrapper.find('.select').select2();
            moduleWrapper.show();

            $('#nestable').find('ol.dd-list').append(moduleWrapper);
            counter +=1;

            var ser =  $('#nestable').nestable('serialize');
            $('#order').val(JSON.stringify(ser));
        });

        $('#nestable').on('change', function() {
            var ser = $('#nestable').nestable('serialize');
            $('#order').val(JSON.stringify(ser));
        });


        function readURL(input) {
            if (input[0].files && input[0].files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.imageoffer').css({'backgroundImage':'url("'+e.target.result+'")'});
                }

                reader.readAsDataURL(input[0].files[0]);
            }
        }

        $('.galleryChanger').on('change',function(){
            console.log($(this).val());
        });


        $('.imagePicker').on('change',function(){
            readURL($(this));
        });

        $('.imagePickerButton').on('click',function(){
            var html = $(this).html();
            $('.imagePicker').click();
            $(this).html(html);
        });

        $('.removeImage').on('click',function(){
            $('.imageoffer').removeAttr('style');
            $('.imagePicker').val('');
        });

        $('.summernote').summernote();

        $('#language-changer').on('change',function(){
           $('.languages').hide();
           $('.language-'+$(this).val()).show();
        });
    </script>
@endsection
