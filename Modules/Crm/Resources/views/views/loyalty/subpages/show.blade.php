@extends('crm::layouts.master')

@section('content')
    <div class="card">
        <div class="card-body">
            <a href="{{route('subpages.create')}}" class="btn btn-primary btn-own btn-primary w100 btn-custom text-white">Vytvoriť novú podstránku</a>
            <legend></legend>
            @include('crm::views.loyalty.subpages.partials.table')
        </div>
    </div>
@endsection
