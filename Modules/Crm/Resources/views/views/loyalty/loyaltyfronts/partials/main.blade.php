<div class="row">
    @foreach($languages as $language)
        <div class="col-md-6 languages lang_{{$language['lang_code']}}">
            <div class="form-group">
                <label>Hlavný nadpis v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
                <input type="text" placeholder="Hlavný nadpis" value="{{isset($response['main_titles'][$language['lang_code']]) && !empty($response['main_titles'][$language['lang_code']]) ? $response['main_titles'][$language['lang_code']] : ''}}" name="main_title[{{$language['lang_code']}}]" class="form-control">
            </div>
        </div>
    @endforeach
    <div class="col-md-6">
        <div class="form-group">
            <label>Animácia hlavného nadpisu</label>
            <select name="animation_main_title" class="form-control">
                <option value="">žiadna</option>
                <option {{isset($response['animation_main_title']) && !empty($response['animation_main_title']) && $response['animation_main_title']=='top' ? 'selected' : ''}} value="top">zhora</option>
                <option {{isset($response['animation_main_title']) && !empty($response['animation_main_title']) && $response['animation_main_title']=='bottom' ? 'selected' : ''}} value="bottom">zdola</option>
                <option {{isset($response['animation_main_title']) && !empty($response['animation_main_title']) && $response['animation_main_title']=='right' ? 'selected' : ''}} value="right">z pravej strany</option>
                <option {{isset($response['animation_main_title']) && !empty($response['animation_main_title']) && $response['animation_main_title']=='left' ? 'selected' : ''}} value="left">z ľavej strany</option>
            </select>
        </div>
    </div>
    @foreach($languages as $language)
        <div class="col-md-6 languages lang_{{$language['lang_code']}}">
            <label>Hlavný popis v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
            <textarea class="form-control summernote" name="main_description[{{$language['lang_code']}}]" id="" cols="30" rows="10">{{isset($response['main_description'][$language['lang_code']]) && !empty($response['main_description'][$language['lang_code']]) ? $response['main_description'][$language['lang_code']] : ''}}</textarea>
        </div>
    @endforeach
    <div class="col-md-6">
        <label>Animácia hlavného popisu</label>
        <select name="animation_main_description" class="form-control">
            <option value="">žiadna</option>
            <option {{isset($response['animation_main_description']) && !empty($response['animation_main_description']) && $response['animation_main_description'] == 'top' ? 'selected' : ''}} value="top">zhora</option>
            <option {{isset($response['animation_main_description']) && !empty($response['animation_main_description']) && $response['animation_main_description'] == 'bottom' ? 'selected' : ''}} value="bottom">zdola</option>
            <option {{isset($response['animation_main_description']) && !empty($response['animation_main_description']) && $response['animation_main_description'] == 'right' ? 'selected' : ''}} value="right">z pravej strany</option>
            <option {{isset($response['animation_main_description']) && !empty($response['animation_main_description']) && $response['animation_main_description'] == 'left' ? 'selected' : ''}} value="left">z ľavej strany</option>
        </select>
    </div>
    <legend></legend>
    <div class="col-md-6">
        <label>Obrázky</label>
        <div class="row duplicate">
            <div class="col-md-5">
                <div class="bannerWrapper img-wrapper-1">
                </div>
            </div>
            <div class="col-md-7">
                <div class="row">
                    <div class="col-md-6"><input type="file" name="header_img[]" class="form-control img-chooser" data-in="1"></div>
                    <div class="col-md-6"><button type="button" class="btn btn-success clone">+</button> <button type="button" style="display:none;" class="btn btn-danger removeRow">-</button></div>
                </div>
            </div>
        </div>
        <div id="duplicates" style="width:100%">
        </div>
        <div id="uploadedImages">
            <legend></legend>
            @if(isset($response['images']) && !empty($response['images']))
                @foreach($response['images'] as $key => $image)
                    <div class="row" style="margin-top:10px;">
                        <div class="col-md-5">
                            <div class="bannerWrapper" style="background-image:url('{{asset('images/header/'.$image)}}')">
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="row">
                                <div class="col-md-12"><button data-name="{{$image}}" type="button" class="btn btn-danger removeRowUploaded">-</button></div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Zobraziť vlnu</label>
            <select name="show_wave" class="form-control">
                <option {{isset($response['show_wave']) && $response['show_wave']=='1' ? 'selected' : ''}} value="1">Áno</option>
                <option {{isset($response['show_wave']) && $response['show_wave']=='0' ? 'selected' : ''}} value="0">Nie</option>
            </select>
        </div>
    </div>
</div>
