<style>
    .imageWrapperAdvantage{
        min-height:250px;
        width:100%;
        background-size:cover;
        background-position: center;
        background-repeat:no-repeat;
    }
</style>
<div class="row">
    @foreach($languages as $language)
    <div class="col-md-6 languages lang_{{$language['lang_code']}}">
        <div class="form-group">
            <label>Nadpis zážitkov v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
            <input type="text" placeholder="Nadpis zážitkov" value="{{isset($response['experience_titles'][$language['lang_code']]) && !empty($response['experience_titles'][$language['lang_code']]) ? $response['experience_titles'][$language['lang_code']] : ''}}" name="experience_title[{{$language['lang_code']}}]" class="form-control">
        </div>
    </div>
    @endforeach
    <div class="col-md-6">
        <div class="form-group">
            <label>Animácia nadpisu zážitkov</label>
            <select name="animation_experience_title" class="form-control">
                <option value="">žiadna</option>
                <option {{isset($response['animation_experience_title']) && !empty($response['animation_experience_title']) && $response['animation_experience_title']=='top' ? 'selected' : ''}} value="top">zhora</option>
                <option {{isset($response['animation_experience_title']) && !empty($response['animation_experience_title']) && $response['animation_experience_title']=='bottom' ? 'selected' : ''}} value="bottom">zdola</option>
                <option {{isset($response['animation_experience_title']) && !empty($response['animation_experience_title']) && $response['animation_experience_title']=='right' ? 'selected' : ''}} value="right">z pravej strany</option>
                <option {{isset($response['animation_experience_title']) && !empty($response['animation_experience_title']) && $response['animation_experience_title']=='left' ? 'selected' : ''}} value="left">z ľavej strany</option>
            </select>
        </div>
    </div>
    @foreach($languages as $language)
    <div class="col-md-6 languages lang_{{$language['lang_code']}}">
        <label>Popis zážitkov v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
        <textarea class="form-control summernote" name="experience_description[{{$language['lang_code']}}]" id="" cols="30" rows="10">{{isset($response['experience_description'][$language['lang_code']]) && !empty($response['experience_description'][$language['lang_code']]) ? $response['experience_description'][$language['lang_code']] : ''}}</textarea>
    </div>
    @endforeach
    <div class="col-md-6">
        <div class="form-group">
            <label>Animácia popisu zážitkov</label>
            <select name="animation_experience_description" class="form-control">
                <option value="">žiadna</option>
                <option {{isset($response['animation_experience_description']) && !empty($response['animation_experience_description']) && $response['animation_experience_description'] == 'top' ? 'selected' : ''}} value="top">zhora</option>
                <option {{isset($response['animation_experience_description']) && !empty($response['animation_experience_description']) && $response['animation_experience_description'] == 'bottom' ? 'selected' : ''}} value="bottom">zdola</option>
                <option {{isset($response['animation_experience_description']) && !empty($response['animation_experience_description']) && $response['animation_experience_description'] == 'right' ? 'selected' : ''}} value="right">z pravej strany</option>
                <option {{isset($response['animation_experience_description']) && !empty($response['animation_experience_description']) && $response['animation_experience_description'] == 'left' ? 'selected' : ''}} value="left">z ľavej strany</option>
            </select>
        </div>
    </div>
    <legend></legend>
    <div class="col-md-6">
        <div class="form-group">
            <input type="hidden" name="show_experience" value="0">
            <label>Zobraziť zážitky na hlavnej stránke<br>
                <label class="switch">
                    <input type="checkbox" name="show_experience" {{isset($response['show_experience']) && !empty($response['show_experience']) && $response['show_experience'] == '1' ? 'checked' : ''}} value="1">
                    <span class="slider round"></span>
                </label>
            </label>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Animácia zážitkov</label>
            <select name="animation_experience" class="form-control">
                <option value="">žiadna</option>
                <option {{isset($response['animation_experience']) && !empty($response['animation_experience']) && $response['animation_experience'] == 'top' ? 'selected' : ''}} value="top">zhora</option>
                <option {{isset($response['animation_experience']) && !empty($response['animation_experience']) && $response['animation_experience'] == 'bottom' ? 'selected' : ''}} value="bottom">zdola</option>
                <option {{isset($response['animation_experience']) && !empty($response['animation_experience']) && $response['animation_experience'] == 'right' ? 'selected' : ''}} value="right">z pravej strany</option>
                <option {{isset($response['animation_experience']) && !empty($response['animation_experience']) && $response['animation_experience'] == 'left' ? 'selected' : ''}} value="left">z ľavej strany</option>
            </select>
        </div>
    </div>
</div>

<script>
    $('#selectAdvantages').on('change',function(){
        var val = JSON.stringify($(this).val());
        $('#experiencesAll').val(val);
    });
</script>
