<div class="row">
    @foreach($languages as $language)
        <div class="col-md-6 languages lang_{{$language['lang_code']}}">
            <div class="form-group">
                <label>Nadpis kategórii v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
                <input type="text" placeholder="Nadpis kategórií" value="{{isset($response['category_titles'][$language['lang_code']]) && !empty($response['category_titles'][$language['lang_code']]) ? $response['category_titles'][$language['lang_code']] : ''}}" name="category_title[{{$language['lang_code']}}]" class="form-control">
            </div>
        </div>
    @endforeach
    <div class="col-md-6">
        <div class="form-group">
            <label>Animácia nadpisu kategórie</label>
            <select name="animation_category_title" class="form-control">
                <option value="">žiadna</option>
                <option {{isset($response['animation_category_title']) && !empty($response['animation_category_title']) && $response['animation_category_title']=='top' ? 'selected' : ''}} value="top">zhora</option>
                <option {{isset($response['animation_category_title']) && !empty($response['animation_category_title']) && $response['animation_category_title']=='bottom' ? 'selected' : ''}} value="bottom">zdola</option>
                <option {{isset($response['animation_category_title']) && !empty($response['animation_category_title']) && $response['animation_category_title']=='right' ? 'selected' : ''}} value="right">z pravej strany</option>
                <option {{isset($response['animation_category_title']) && !empty($response['animation_category_title']) && $response['animation_category_title']=='left' ? 'selected' : ''}} value="left">z ľavej strany</option>
            </select>
        </div>
    </div>
    @foreach($languages as $language)
        <div class="col-md-6 languages lang_{{$language['lang_code']}}">
            <label>Popis kategórii v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
            <textarea class="form-control summernote" name="category_description[{{$language['lang_code']}}]" id="" cols="30" rows="10">{{isset($response['category_description'][$language['lang_code']]) && !empty($response['category_description'][$language['lang_code']]) ? $response['category_description'][$language['lang_code']] : ''}}</textarea>
        </div>
    @endforeach
    <div class="col-md-6">
        <div class="form-group">
            <label>Animácia popisu kategórii</label>
            <select name="animation_category_description" class="form-control">
                <option value="">žiadna</option>
                <option {{isset($response['animation_category_description']) && !empty($response['animation_category_description']) && $response['animation_category_description'] == 'top' ? 'selected' : ''}} value="top">zhora</option>
                <option {{isset($response['animation_category_description']) && !empty($response['animation_category_description']) && $response['animation_category_description'] == 'bottom' ? 'selected' : ''}} value="bottom">zdola</option>
                <option {{isset($response['animation_category_description']) && !empty($response['animation_category_description']) && $response['animation_category_description'] == 'right' ? 'selected' : ''}} value="right">z pravej strany</option>
                <option {{isset($response['animation_category_description']) && !empty($response['animation_category_description']) && $response['animation_category_description'] == 'left' ? 'selected' : ''}} value="left">z ľavej strany</option>
            </select>
        </div>
    </div>
    <legend></legend>
    <div class="col-md-4">
        <div class="form-group">
            <input type="hidden" name="show_category" value="0">
            <label>Zobraziť kategórie na hlavnej stránke<br>
                <label class="switch">
                    <input type="checkbox" name="show_category" {{isset($response['show_category']) && !empty($response['show_category']) && $response['show_category'] == '1' ? 'checked' : ''}} value="1">
                    <span class="slider round"></span>
                </label>
                </label>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Zobrazené kategórie</label>
            <select multiple="multiple" id="selectCategories" name="categories" class="form-control select" data-fouc>
                @if(isset($propertycategories) && !empty($propertycategories))
                    @foreach($propertycategories as $ps)
                        <option {{isset($response['categories']) && !empty($response['categories']) && in_array($ps['id'],json_decode($response['categories'],TRUE)) ? 'selected' : ''}} value="{{$ps['id']}}">{{$ps['internal_name']}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <input type="hidden" name="categoriesAll" id="categoriesAll" value="{{isset($response['categories']) && !empty($response['categories']) ? $response['categories'] : ''}}">
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Animácia kategórie</label>
            <select name="animation_category" class="form-control">
                <option value="">žiadna</option>
                <option {{isset($response['animation_category']) && !empty($response['animation_category']) && $response['animation_category'] == 'top' ? 'selected' : ''}} value="top">zhora</option>
                <option {{isset($response['animation_category']) && !empty($response['animation_category']) && $response['animation_category'] == 'bottom' ? 'selected' : ''}} value="bottom">zdola</option>
                <option {{isset($response['animation_category']) && !empty($response['animation_category']) && $response['animation_category'] == 'right' ? 'selected' : ''}} value="right">z pravej strany</option>
                <option {{isset($response['animation_category']) && !empty($response['animation_category']) && $response['animation_category'] == 'left' ? 'selected' : ''}} value="left">z ľavej strany</option>
            </select>
        </div>
    </div>
</div>

<script>
    $('#selectCategories').on('change',function(){
        var val = JSON.stringify($(this).val());
        $('#categoriesAll').val(val);
    });
</script>
