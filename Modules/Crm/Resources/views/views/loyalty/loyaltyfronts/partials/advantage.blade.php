<style>
    .imageWrapperAdvantage{
        min-height:250px;
        width:100%;
        background-size:cover;
        background-position: center;
        background-repeat:no-repeat;
    }
</style>
<div class="row">
    @foreach($languages as $language)
        <div class="col-md-6 languages lang_{{$language['lang_code']}}">
            <div class="form-group">
                <label>Nadpis výhod v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
                <input type="text" placeholder="Nadpis výhod" value="{{isset($response['advantage_titles'][$language['lang_code']]) && !empty($response['advantage_titles'][$language['lang_code']]) ? $response['advantage_titles'][$language['lang_code']] : ''}}" name="advantage_title[{{$language['lang_code']}}]" class="form-control">
            </div>
        </div>
    @endforeach
    <div class="col-md-6">
        <div class="form-group">
            <label>Animácia nadpisu výhod</label>
            <select name="animation_advantage_title" class="form-control">
                <option value="">žiadna</option>
                <option {{isset($response['animation_advantage_title']) && !empty($response['animation_advantage_title']) && $response['animation_advantage_title']=='top' ? 'selected' : ''}} value="top">zhora</option>
                <option {{isset($response['animation_advantage_title']) && !empty($response['animation_advantage_title']) && $response['animation_advantage_title']=='bottom' ? 'selected' : ''}} value="bottom">zdola</option>
                <option {{isset($response['animation_advantage_title']) && !empty($response['animation_advantage_title']) && $response['animation_advantage_title']=='right' ? 'selected' : ''}} value="right">z pravej strany</option>
                <option {{isset($response['animation_advantage_title']) && !empty($response['animation_advantage_title']) && $response['animation_advantage_title']=='left' ? 'selected' : ''}} value="left">z ľavej strany</option>
            </select>
        </div>
    </div>
    @foreach($languages as $language)
        <div class="col-md-6 languages lang_{{$language['lang_code']}}">
            <label>Popis výhod v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
            <textarea class="form-control summernote" name="advantage_description[{{$language['lang_code']}}]" id="" cols="30" rows="10">{{isset($response['advantage_description'][$language['lang_code']]) && !empty($response['advantage_description'][$language['lang_code']]) ? $response['advantage_description'][$language['lang_code']] : ''}}</textarea>
        </div>
    @endforeach
    <div class="col-md-6">
        <div class="form-group">
            <label>Animácia popisu výhod</label>
            <select name="animation_advantage_description" class="form-control">
                <option value="">žiadna</option>
                <option {{isset($response['animation_advantage_description']) && !empty($response['animation_advantage_description']) && $response['animation_advantage_description'] == 'top' ? 'selected' : ''}} value="top">zhora</option>
                <option {{isset($response['animation_advantage_description']) && !empty($response['animation_advantage_description']) && $response['animation_advantage_description'] == 'bottom' ? 'selected' : ''}} value="bottom">zdola</option>
                <option {{isset($response['animation_advantage_description']) && !empty($response['animation_advantage_description']) && $response['animation_advantage_description'] == 'right' ? 'selected' : ''}} value="right">z pravej strany</option>
                <option {{isset($response['animation_advantage_description']) && !empty($response['animation_advantage_description']) && $response['animation_advantage_description'] == 'left' ? 'selected' : ''}} value="left">z ľavej strany</option>
            </select>
        </div>
    </div>
    <legend></legend>
    <div class="col-md-4">
        <div class="form-group">
            <input type="hidden" name="show_advantage" value="0">
            <label>Zobraziť výhody na hlavnej stránke<br>
                <label class="switch">
                    <input type="checkbox" name="show_advantage" {{isset($response['show_advantage']) && !empty($response['show_advantage']) && $response['show_advantage'] == '1' ? 'checked' : ''}} value="1">
                    <span class="slider round"></span>
                </label>
            </label>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Zobrazené výhody</label>
            <select multiple="multiple" id="selectAdvantages" name="advantages" class="form-control select" data-fouc>
                @if(isset($advantages) && !empty($advantages))
                    @foreach($advantages as $advantage)
                        <option {{isset($response['advantages']) && !empty($response['advantages']) && in_array($advantage['id'],json_decode($response['advantages'],TRUE)) ? 'selected' : ''}} value="{{$advantage['id']}}">{{$advantage['internal_name']}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <input type="hidden" name="advantages_all" id="advantagesAll" value="{{isset($response['advantages']) && !empty($response['advantages']) ? $response['advantages'] : ''}}">
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Animácia výhod</label>
            <select name="animation_advantage" class="form-control">
                <option value="">žiadna</option>
                <option {{isset($response['animation_advantage']) && !empty($response['animation_advantage']) && $response['animation_advantage'] == 'top' ? 'selected' : ''}} value="top">zhora</option>
                <option {{isset($response['animation_advantage']) && !empty($response['animation_advantage']) && $response['animation_advantage'] == 'bottom' ? 'selected' : ''}} value="bottom">zdola</option>
                <option {{isset($response['animation_advantage']) && !empty($response['animation_advantage']) && $response['animation_advantage'] == 'right' ? 'selected' : ''}} value="right">z pravej strany</option>
                <option {{isset($response['animation_advantage']) && !empty($response['animation_advantage']) && $response['animation_advantage'] == 'left' ? 'selected' : ''}} value="left">z ľavej strany</option>
            </select>
        </div>
    </div>

    <legend></legend>
    <div class="col-md-6">
        <div class="imageWrapperAdvantage" style="{{isset($response['advantage_image']) && !empty($response['advantage_image']) ? 'background-image:url("'.asset('images/benefits/'.$response['advantage_image']).'")' : ''}}"></div>
    </div>
    <div class="col-md-6">
        <div class="col-md-12">
            <button type="button" class="btn btn-success w100 btn-changeImageAdvantage">Pridať/zmeniť obrázok</button>
        </div>
        <div class="col-md-12">
            <button type="button" class="btn btn-danger w100 mt10 btn-removeImageAdvantage">Zmazať obrázok</button>
        </div>
        <input type="file" name="advantage_image" id="advantageImage" style="display:none;">
        <input type="hidden" name="removeAdvantageImage" id="removeAdvantageImage" value="0">
        <input type="hidden" name="oldAdvantageImage" value="{{isset($response['advantage_image']) && !empty($response['advantage_image']) ? $response['advantage_image'] : ''}}">
    </div>
</div>

<script>

    $('.btn-changeImageAdvantage').on('click',function(){
        $('#advantageImage').click();
    });

    $('.btn-removeImageAdvantage').on('click',function(){
        $('.imageWrapperAdvantage').removeAttr('style');
        $('#removeAdvantageImage').val('1');
    });

    function readURLAdvantage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#removeAdvantageImage').val('1');
                $('.imageWrapperAdvantage').css('background-image','url('+e.target.result+')');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $('#advantageImage').on('change',function(){
        readURLAdvantage(this);
    });

    $('#selectAdvantages').on('change',function(){
        var val = JSON.stringify($(this).val());
        $('#advantagesAll').val(val);
    });
</script>
