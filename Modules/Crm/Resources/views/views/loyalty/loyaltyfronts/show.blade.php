@extends('crm::layouts.master')

@section('content')
    <style>
        .bannerWrapper{
            width:100%;
            min-height:150px;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
        }

        .languages{
            display:none;
        }
        .lang_sk{
            display:block;
        }
    </style>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote.min.js"></script>
    <script src="{{asset('assets/global_assets/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script src="{{asset('assets/global_assets/js/demo_pages/form_select2.js')}}"></script>

    <div class="row" style="margin-bottom:20px;">
        <div class="col-md-9"></div>
        <div class="col-md-3">
            <select name="" id="language_switcher" class="form-control">
                @foreach($languages as $lang)
                    <option value="{{$lang['lang_code']}}">{{__('global.'.$lang['lang_name'])}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <ul class="nav nav-tabs nav-tabs-solid nav-justified rounded border-0">
                <li class="nav-item"><a href="#solid-rounded-justified-tab1" class="nav-link rounded-left active show" data-toggle="tab">Nastavenie headeru</a></li>
                <li class="nav-item"><a href="#solid-rounded-justified-tab2" class="nav-link" data-toggle="tab">Kategórie</a></li>
                <li class="nav-item"><a href="#solid-rounded-justified-tab3" class="nav-link" data-toggle="tab">Výhody</a></li>
                <li class="nav-item"><a href="#solid-rounded-justified-tab4" class="nav-link" data-toggle="tab">Najnovšie zážitky</a></li>
            </ul>
            <form action="{{route('loyaltyfronts.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="tab-pane fade active show" id="solid-rounded-justified-tab1">
                    @include('crm::views.loyalty.loyaltyfronts.partials.main')
                </div>
                <div class="tab-pane fade" id="solid-rounded-justified-tab2">
                    @include('crm::views.loyalty.loyaltyfronts.partials.category')
                </div>
                <div class="tab-pane fade" id="solid-rounded-justified-tab3">
                    @include('crm::views.loyalty.loyaltyfronts.partials.advantage')
                </div>
                <div class="tab-pane fade" id="solid-rounded-justified-tab4">
                    @include('crm::views.loyalty.loyaltyfronts.partials.experience')
                </div>

                <input type="hidden" name="loyaltyfront_id" value="{{isset($response['id']) && !empty($response['id']) ? $response['id'] : ''}}">
                <input type="hidden" name="deletePictures" id="deletePictures" value="">

                <legend></legend>
                <div class="row text-center m-auto">
                    @can('loyaltyfront.edit')
                        <button class="btn btn-primary btn-custom">Uložiť</button>
                    @endcan
                </div>
            </form>
        </div>


    </div>

    <script>
        $('.summernote').summernote();

        var iterator = 2;

        function readURL(input,inputIndex) {
            //console.log(input.files);
            if (input.files && input.files[0]) {

                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.img-wrapper-'+inputIndex).css('background-image','url('+e.target.result+')');
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).on('change','.img-chooser',function(e){
            e.preventDefault();
            e.stopPropagation();
            var inputIndex = $(this).data().in;
            readURL(this,inputIndex);
        });

        $('#language_switcher').on('change',function(){
           var val = $(this).val();
           $('.languages').hide();
           $('.lang_'+val).show();
        });



        $(document).on('click','.clone',function(e){
            e.preventDefault();
            e.stopPropagation();
            iterator = iterator + 1;
            var clone = $(document).find('.duplicate').clone();
            clone.find('.removeRow').show();
            clone.find('.img-wrapper-1').removeClass('img-wrapper-1').addClass('img-wrapper-'+iterator).removeAttr('style');
            clone.find('.img-chooser').attr('data-in',iterator);
            clone.css('margin-top','10px').removeClass('duplicate');
            clone.find('.img-chooser').val('');

            $('#duplicates').append(clone);
        });

        $(document).on('click','.removeRow',function(){
           $(this).closest('.row').closest('.col-md-7').closest('.row').remove();
        });

        $('.removeRowUploaded').on('click',function(){
            var name = $(this).data().name;
            var val = $('#deletePictures').val();
            var newVal = val+name+',';
            $('#deletePictures').val(newVal);
            $(this).closest('.row').closest('.col-md-7').closest('.row').remove();
        });
    </script>
@endsection
