@extends('crm::layouts.master')

@section('content')

    <script src="{{asset('assets/global_assets/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script src="{{asset('assets/global_assets/js/demo_pages/form_select2.js')}}"></script>

    <style>
        img{
            max-width:180px;
        }
    </style>
    <div class="card">
        <div class="card-header">
            <h5>Základné nastavenia loyalty systému</h5>
        </div>
        <div class="card-body">
            <form enctype="multipart/form-data" action="{{(isset($settings) && !empty($settings)) ? route('information.update',['information'=>$settings['id']]) : route('information.store')}}" method="POST">
                @if(isset($settings) && !empty($settings))
                    {{method_field('PUT')}}
                @endif
                @csrf
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="" class="form-label">Názov</label>
                            <input name="name" type="text" class="form-control" value="{{(isset($settings['name']) && !empty($settings['name'])) ? $settings['name'] : ''}}" placeholder="Názov">
                        </div>
                        <div class="form-group">
                            <label for="" class="form-label">Emailová adresa</label>
                            <input name="email" type="text" value="{{(isset($settings['email']) && !empty($settings['email'])) ? $settings['email'] : ''}}" class="form-control" placeholder="Emailová adresa">
                        </div>
                        <div class="form-group">
                            <label for="" class="form-label">Telefónne číšlo</label>
                            <input type="text" name="phone" value="{{(isset($settings['phone']) && !empty($settings['phone'])) ? $settings['phone'] : ''}}" class="form-control" placeholder="Telefónne číslo">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="" class="form-label">Štát</label>
                            <input type="text" name="state" value="{{(isset($settings['state']) && !empty($settings['state'])) ? $settings['state'] : ''}}" class="form-control" placeholder="Štát">
                        </div>
                        <div class="form-group">
                            <label for="" class="form-label">Adresa</label>
                            <input type="text" name="adress" value="{{(isset($settings['adress']) && !empty($settings['adress'])) ? $settings['adress'] : ''}}" class="form-control" placeholder="Adresa">
                        </div>
                        <div class="form-group">
                            <label for="" class="form-label">PSČ</label>
                            <input type="text" name="zip" value="{{(isset($settings['zip']) && !empty($settings['zip'])) ? $settings['zip'] : ''}}" class="form-control" placeholder="PSČ">
                        </div>
                    </div>
                </div>
                <legend></legend>
                    <div class="form-group">
                        <label>Jazykové mutácie</label>
                        <select multiple="multiple" id="selectLanguages" name="languages" class="form-control select" data-fouc>
                            <option {{(isset($languagess) && !empty($languagess) && in_array("sk",$languagess)) ? 'selected' : ''}} value="sk" selected>Slovenčina</option>
                            <option {{(isset($languagess) && !empty($languagess) && in_array("en",$languagess)) ? 'selected' : ''}} value="en">Angličtina</option>
                            <option {{(isset($languagess) && !empty($languagess) && in_array("de",$languagess)) ? 'selected' : ''}} value="de">Nemčina</option>
                            <option {{(isset($languagess) && !empty($languagess) && in_array("pl",$languagess)) ? 'selected' : ''}} value="pl">Polština</option>
                            <option {{(isset($languagess) && !empty($languagess) && in_array("hu",$languagess)) ? 'selected' : ''}} value="hu">Maďarčina</option>
                            <option {{(isset($languagess) && !empty($languagess) && in_array("cz",$languagess)) ? 'selected' : ''}} value="cz">Čeština</option>
                            <option {{(isset($languagess) && !empty($languagess) && in_array("ru",$languagess)) ? 'selected' : ''}} value="ru">Ruština</option>
                        </select>
                    </div>
                    <input type="hidden" name="languages" id="languages" value="{{(isset($settings['languages']) && !empty($settings['languages'])) ? $settings['languages'] : '["sk"]'}}">
                    <legend></legend>
                    <div class="form-group">
                        <label>Mena</label>
                        <select multiple="multiple" id="selectCurrency" name="currencies" class="form-control select" data-fouc>
                            <option {{(isset($currencies) && !empty($currencies) && in_array("eur",$currencies)) ? 'selected' : ''}} value="eur" selected>Euro</option>
                            <option {{(isset($currencies) && !empty($currencies) && in_array("huf",$currencies)) ? 'selected' : ''}} value="huf">Maďarský forint</option>
                            <option {{(isset($currencies) && !empty($currencies) && in_array("gpb",$currencies)) ? 'selected' : ''}} value="gpb">Libra</option>
                            <option {{(isset($currencies) && !empty($currencies) && in_array("czk",$currencies)) ? 'selected' : ''}} value="czk">Česká koruna</option>
                            <option {{(isset($currencies) && !empty($currencies) && in_array("usd",$currencies)) ? 'selected' : ''}} value="usd">Americký dolár</option>
                        </select>
                    </div>
                    <input type="hidden" name="currencies" id="currencies" value="{{(isset($settings['currency']) && !empty($settings['currency'])) ? $settings['currency'] : '["eur"]'}}">

                    <div class="" style="display:none;">
                    <legend></legend>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="form-label">Primárna farba</label>
                                <input  class="form-control colorInput" type="text" name="primary_color" value="{{(isset($settings['primary_color']) && !empty($settings['primary_color'])) ? $settings['primary_color'] : ''}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="form-label" style="color:white;">primárna farba</label>
                                <input type="color" class="form-control colorSelect" value="{{(isset($settings['primary_color']) && !empty($settings['primary_color'])) ? $settings['primary_color'] : ''}}">
                            </div>
                        </div>
                    </div>
                    <legend></legend>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="form-label">Logo</label>
                                <input type="file" name="logo" class="form-control" onchange="readURL(this);">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <img id="blah" src="{{asset('/uploads/'.$settings['logo'])}}" alt="your image" style="{{(isset($settings['logo']) && !empty($settings['logo'])) ? 'display:block;margin: 0 auto;' : 'display:none;'}}"/>
                        </div>
                    </div>
                    <legend></legend>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="form-label">Footer Logo</label>
                                <input type="file" name="footer_logo" class="form-control" onchange="readURL2(this);">
                            </div>
                        </div>
                        <div class="col-md-6" style="background-color:#1C1C1C">
                            <img id="blah2" src="{{asset('/uploads/'.$settings['footer_logo'])}}" alt="your image" style="{{(isset($settings['footer_logo']) && !empty($settings['footer_logo'])) ? 'display:block;margin: 0 auto;' : 'display:none;'}}"/>
                        </div>
                    </div>
                </div>
                <legend></legend>
                <div class="row">
                    <div class="col-md-12">
                        <h5>Nastavenie prihlásenia</h5>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Obmedziť počet pokusov o prihlásenie</label>
                            <select name="throttle_enable" id="" class="form-control">
                                <option {{$settings['throttle_enable'] == '1' ? 'selected' : ''}} value="1">Áno</option>
                                <option {{$settings['throttle_enable'] == '0' || $settings['throttle_enable'] == null ? 'selected' : ''}} value="0">Nie</option>
                            </select>
                            <small>Defaultná hodnota, vypnuté</small>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Počet pokusov o prihlásenie</label>
                            <input type="number" class="form-control" value="{{$settings['throttle_count']}}" placeholder="Počet pokusov o prihlásenie" name="throttle_count">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Počet minút obmedzenia o prihlásenie</label>
                            <input type="number" class="form-control" value="{{$settings['throttle_minutes']}}" placeholder="Počet minút obmedzenia o prihlásenie" name="throttle_minutes">
                        </div>
                    </div>
                </div>
                <legend></legend>
                <div class="row">
                    <div class="col-md-12">
                        <h5>Nastavenie aktivačného emailu</h5>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Dĺžka platnosti aktivačného kódu v minútach</label>
                            <input type="number" class="form-control" value="{{$settings['activation_token_durability']}}" placeholder="Dĺžka platnosti aktivačného kódu v minútach" name="activation_token_durability">
                            <small>Defaultná hodnota je 60 minút (1 hodina)</small>
                        </div>
                    </div>
                </div>
                <legend></legend>
                <div class="row">
                    <div class="col-md-12">
                        <h5>Nastavenie aktivačného emailu</h5>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Expirácia hesla v dňoch</label>
                            <input type="number" class="form-control" value="{{$settings['expiration_password']}}" placeholder="Expirácia hesla v dňoch" name="expiration_password">
                            <small>Hovorí o tom, ako často je zákazník nútený meniť svoje heslo, defaultná hodnota 365 dní (1 rok)</small>
                        </div>
                    </div>
                </div>
                <legend></legend>
                    <div class="row">
                        <div class="col-md-12">
                            <h5>Nastavenie číselníka pre vytvorenie čísla karty</h5>
                        </div>
                        <div class="col-md-6">
                            <label>Formát</label>
                            <input type="text" value="{{$settings['card_number_format']}}" class="form-control" placeholder="{{date('ymd')}}0001">
                            <small>Defaultná hodnota je <strong>RRMMMDDCCCC</strong></small>
                        </div>
                        <div class="col-md-6">
                            <strong>C</strong> - číslo<br>
                            <strong>RR</strong> - 2 čísla z roku (napr. 21)<br>
                            <strong>RRRR</strong> - 4 čísla z roku (napr. 2021)<br>
                            <strong>MM</strong> - 2 čísla - mesiac<br>
                            <strong>DD</strong> - 2 čísla - deň<br>
                            <legend></legend>
                            Príklady:<br>
                            <strong>2020030401</strong> - <strong>RRRRMMDDCC</strong><br>
                            <strong>BV-20200403001</strong> - <strong>BV-RRRRMMDDCCC</strong>
                        </div>
                    </div>
                <legend></legend>
                <div class="row">
                    <div class="col-lg-12">
                        <button class="btn btn-primary btn-custom">
                            @can('information.edit')
                                Uložiť
                            @endcan
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script>
        $('.colorSelect').on('change',function(){
            var val = $(this).val();
            $('.colorInput').val(val);
        });
        $('.colorInput').on('input',function(){
            var val = $(this).val();
            $('.colorSelect').val(val);
        });



        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result).show();
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
        function readURL2(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah2')
                        .attr('src', e.target.result).show();
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
        $('#selectLanguages').on('change',function(){
            var val = JSON.stringify($(this).val());
            $('#languages').val(val);
        });
        $('#selectCurrency').on('change',function(){
            var val = JSON.stringify($(this).val());
            $('#currencies').val(val);
        });
    </script>
@endsection
