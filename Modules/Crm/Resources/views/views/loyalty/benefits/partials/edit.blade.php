@extends('crm::layouts.master')

@section('content')
    <style>
        .languages{
            display:none;
        }
        .lang_sk{
            display:block;
        }

        .imageWrapper{
            width:100%;
            min-height:200px;
            background-repeat: no-repeat;
            background-size: cover;
            background-position:center;
        }
    </style>

    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote.min.js"></script>

    <div class="row" style="margin-bottom:10px;">
        <div class="col-md-9"></div>
        <div class="col-md-3">
            <select class="form-control" id="languageChanger">
                @if(isset($languages) && !empty($languages))
                    @foreach($languages as $language)
                        <option value="{{$language['lang_code']}}">{{__('global.'.$language['lang_name'])}}</option>
                    @endforeach
                @endif
            </select>
        </div>
    </div>
    <div class="card" style="margin:0 auto;">
        <div class="card-header">
            <h5>
                Úprava benefitu
            </h5>
        </div>
        <div class="card-body">
            <form action="{{route('benefits.update',['benefit'=>$id])}}" method="post" enctype="multipart/form-data">
                @csrf
                {{method_field('put')}}
                <div class="row">
                    @if(isset($languages) && !empty($languages))
                        @foreach($languages as $language)
                            <div class="col-md-4 languages lang_{{$language['lang_code']}}">
                                <div class="form-group">
                                    <label>Názov kategórie v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
                                    <input type="text" value="{{isset($benefit['title'][$language['lang_code']]) && !empty($benefit['title'][$language['lang_code']]) ? $benefit['title'][$language['lang_code']] : ''}}" name="title[{{$language['lang_code']}}]" placeholder="Názov" class="form-control">
                                </div>
                            </div>
                        @endforeach
                    @endif
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Interný názov</label>
                            <input type="text" name="internal_name" value="{{$benefit['internal_name']}}" placeholder="Interný názov" class="form-control">
                        </div>
                    </div>
                    @if(isset($languages) && !empty($languages))
                        @foreach($languages as $language)
                            <div class="col-md-4 languages lang_{{$language['lang_code']}}">
                                <div class="form-group">
                                    <label>Slug v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
                                    <input type="text" value="{{isset($benefit['slug'][$language['lang_code']]) && !empty($benefit['slug'][$language['lang_code']]) ? $benefit['slug'][$language['lang_code']] : ''}}" name="slug[{{$language['lang_code']}}]" placeholder="Slug" class="form-control">
                                </div>
                            </div>
                        @endforeach
                    @endif
                    <legend></legend>
                    @if(isset($languages) && !empty($languages))
                        @foreach($languages as $language)
                            <div class="col-md-6 languages lang_{{$language['lang_code']}}">
                                <div class="form-group">
                                    <label>Krátky popis v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
                                    <input type="text" name="short_description[{{$language['lang_code']}}]"  value="{{isset($benefit['short_description'][$language['lang_code']]) && !empty($benefit['short_description'][$language['lang_code']]) ? $benefit['short_description'][$language['lang_code']] : ''}}" placeholder="Krátky popis" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6 languages lang_{{$language['lang_code']}}">
                                <div class="form-group">
                                    <label>Dlhý popis v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
                                    <textarea type="text"  name="long_description[{{$language['lang_code']}}]" class="form-control summernote">{{isset($benefit['long_description'][$language['lang_code']]) && !empty($benefit['long_description'][$language['lang_code']]) ? $benefit['long_description'][$language['lang_code']] : ''}}</textarea>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
                <legend></legend>
                <div class="row">
                    <div class="col-md-6">
                        <div class="imageWrapper" style="{{isset($benefit['image']) && !empty($benefit['image']) ? 'background-image: url("'.asset('images/benefits/'.$benefit['image']).'")' : ''}}"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <button type="button" class="btn btn-success w100 btn-changeimg">Pridať/zmeniť obrázok</button>
                            </div>
                            <div class="col-md-12">
                                <button type="button" class="btn btn-danger w100 mt10 btn-removeImage">Zmazať obrázok</button>
                            </div>
                            <input type="file" name="image" id="image" style="display:none;">
                        </div>
                    </div>
                    <input type="hidden" name="old_image" value="{{isset($benefit['image']) && !empty($benefit['image']) ? $benefit['image'] : ''}}">
                    <input type="hidden" name="remove_old_image" value="0">
                </div>
                <legend></legend>
                <button class="btn btn-primary btn-custom">Uložiť</button>
            </form>
        </div>
    </div>

    <script>
        $('.summernote').summernote();

        $('.btn-changeimg').on('click',function(){
            $('#image').click();
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.imageWrapper').css('background-image','url('+e.target.result+')');
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('.btn-removeImage').on('click',function(){
            $('#image').val('');
            $('.imageWrapper').removeAttr('style');
            $('.removeOldImage').val('1');
        });

        $('#image').on('change',function(e){
            e.preventDefault();
            e.stopPropagation();
            readURL(this);
            $('.removeOldImage').val('1');
        });

        $('#languageChanger').on('change',function(){
            $('.languages').hide();
            $('.lang_'+$(this).val()).show();
        });
    </script>
@endsection
