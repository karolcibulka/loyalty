<div class="table-responsive">
    @if(isset($benefits) && !empty($benefits))
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th style="text-align: center;">#</th>
                <th style="text-align: center;">Interný názov</th>
                <th style="text-align: center;">Akcia</th>
            </tr>
            </thead>
            <tbody>
            @foreach($benefits as $benefit)
                <tr>
                    <td style="text-align: center;">{{$benefit['id']}}</td>
                    <td style="text-align: center;">{{$benefit['internal_name']}}</td>
                    <td style="text-align: center;">
                        @can('benefit.edit')
                            <a href="{{route('benefits.edit',['benefit'=>$benefit['id']])}}"><span class="badge badge-primary" style="cursor:pointer;">Upraviť</span></a>
                        @endcan
                        @can('benefit.delete')
                            <form action="{{route('benefits.destroy',['benefit'=>$benefit['id']])}}" method="post" style="display:inline" id="formDestroy_{{$benefit['id']}}">
                                @csrf
                                {{method_field('delete')}}
                                <span class="badge badge-danger confirmDestroy" data-id="{{$benefit['id']}}" style="cursor:pointer;">Zmazať</span>
                            </form>
                        @endcan
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$benefits->links()}}
    @endif
</div>


<script>
    $('.confirmDestroy').on('click',function(){
        var id = $(this).data().id;
        Swal.fire({
            title:'Naozaj chcete zmazať tento benefit?',
            showCancelButton:true,
            cancelButtonText:'ODVOLAŤ',
            confirmButtonText:'ZMAZAŤ',
            confirmButtonColor:'darkred',
        }).then(function(response){
            if(response.value === true){
                $('#formDestroy_'+id).submit();
            }
        });
    });
</script>
