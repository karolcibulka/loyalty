@extends('crm::layouts.master')

@section('content')
    <div class="card">
        <div class="card-header">
            <h5>Nastavenia benefitov</h5>
        </div>
        <div class="card-body">
            <a href="{{route('benefits.create')}}" style="color:white" class="btn btn primary btn-custom">Vytvoriť benefit</a>
            <legend></legend>
            @include('crm::views.loyalty.benefits.partials.table')
        </div>
    </div>
@endsection
