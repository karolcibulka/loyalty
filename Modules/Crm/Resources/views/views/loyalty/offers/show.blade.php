@extends('crm::layouts.master')

@section('content')
    <div class="card">
        <div class="card-header">
            <h5>Aktuálne ponuky</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <a href="{{route('offers.create')}}" class="btn btn-primary btn-custom" style="color:white;">Vytvoriť novú ponuku</a>
            </div>
            <legend></legend>
            <div id="tableWrapper">
                @include('crm::views.loyalty.offers.partials.table')
            </div>
        </div>
    </div>
@endsection
