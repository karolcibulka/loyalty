<style>
    .changeActivityoffer{
        cursor: pointer;
    }
    .changeAddPoints{
        cursor:pointer;
    }
</style>

@if(isset($offers) && !empty($offers))
    <table id="offersTable" class="table  table-striped table-bordered" style="width:100%">
        <thead>
        <tr>
            <th style="text-align: center;">#</th>
            <th style="text-align: center;">Interný názov zľavy</th>
            <th style="text-align: center;">Počet</th>
            <th style="text-align: center;">Predané</th>
            <th style="text-align: center;">Cena</th>
            <th style="text-align: center;">Viditeľné od - do</th>
            <th style="text-align: center;">Aktívna</th>
            <th style="text-align: center;">Pridať body</th>
            <th style="text-align: center;">Akcie</th>
        </tr>
        </thead>
        <tbody>
        @foreach($offers as $offer)
            <tr>
                <td class="editContent" data-type="showName" data-id="{{$offer['id']}}" style="text-align: center;">{{$offer['id']}}</td>
                <td class="editContent" data-type="showName" data-id="{{$offer['id']}}" style="text-align: center;">{{$offer['internal_name']}}</td>
                <td class="editContent" data-type="showName" data-id="{{$offer['id']}}" style="text-align: center;">{{$offer['offers_restrict_count']}}</td>
                <td class="editContent" data-type="showName" data-id="{{$offer['id']}}" style="text-align: center;">{{$offer['available']}} x</td>
                <td class="editContent" data-type="showName" data-id="{{$offer['id']}}" style="text-align: center;">{{isset($offer['currency']) && !empty($offer['currency']) ? $offer['price'].' '.$offer['currency'] : $offer['price'].' €'}}</td>
                <td class="editContent" data-type="showName" data-id="{{$offer['id']}}" style="text-align: center;">{{date('d.m.Y',strtotime($offer['visible_from'])).' - '.date('d.m.Y',strtotime($offer['visible_to']))}}</td>
                <td class="editContent" data-type="showName" data-id="{{$offer['id']}}" style="text-align: center;">
                    @can('offer.edit')
                        <?=$offer['active'] == '1' ? '<span data-type="activity" class="changeActivityoffer" data-id="'.$offer['id'].'"> <i class="icon-check active" style="color:darkgreen"></i></span>' : '<span data-type="activity" class="changeActivityoffer" data-id="'.$offer['id'].'"><i class="icon-cross2 nonactive" style="color:darkred"></i></span>'?>
                    @endcan
                </td>
                <td style="text-align: center;" class="editContent" data-type="showName" data-id="{{$offer['id']}}">
                    @can('offer.edit')
                        <?=$offer['add_points'] == '1' ? '<span class="changeAddPoints" data-type="add_points" data-id="'.$offer['id'].'"> <i class="icon-check active" style="color:darkgreen"></i></span>' : '<span data-type="add_points" class="changeAddPoints" data-id="'.$offer['id'].'"><i class="icon-cross2 nonactive" style="color:darkred"></i></span>'?>
                    @endcan
                </td>
                <td style="text-align: center;">
                    @can('offer.edit')
                    <a href="{{route('offers.edit',['offer'=>$offer['id']])}}" class="editRow badge badge-primary" data-id="{{$offer['id']}}" style="cursor:pointer;margin-right:10px;">
                        Upraviť
                    </a>
                    @endcan
                    @can('offer.delete')
                        <form action="{{route('offers.destroy',['offer'=>$offer['id']])}}" method="POST" id="form_delete_{{$offer['id']}}" style="display: inline;">
                            @csrf
                            <input type="hidden" name="_method" value="DELETE">
                            <span class="deleteRow badge badge-danger" data-id="{{$offer['id']}}" style="cursor:pointer;">
                                    Vymazať
                                </span>
                        </form>
                    @endcan
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="row" style="margin-top:20px;">
        <div class="col-md-12">
            {{$offers->links()}}
        </div>
    </div>
@endif


<script>

    var getCheckedIcon = function(){
        var string = '<i class="icon-check active" style="color:darkgreen"></i></span>';
        return string
    }

    var getUncheckedIcon = function(){
        var string = '<i class="icon-cross2 nonactive" style="color:darkred"></i></span>';
        return string
    }

    var getSpinner = function(){
        var string = '<i class="spinner icon-spinner2" style="color:black"></i></span>';
        return string;
    }

    $('.deleteRow').on('click',function(){
        var id = $(this).data().id;
        Swal.fire({
            title:'Naozaj chcete zmazať túto ponuku?',
            showCancelButton:true,
            cancelButtonText:'ODVOLAŤ',
            confirmButtonText:'ZMAZAŤ',
            confirmButtonColor:'darkred',
        }).then(function(response){
            if(response.value === true){
                $('#form_delete_'+id).submit();
            }
        });
    });

    $('.changeActivityoffer,.changeAddPoints').on('click',function(){
        var $this = $(this);
        var value = '0';
        type = '';
        if($this.data().type === 'add_points'){
            type = 'add_points';
        }
        if($this.find('i').hasClass('icon-spinner2')){
            return;
        }
        else if($this.find('i').hasClass('active')){
            //console.log('som tu');return ;
            $this.html(getSpinner());
            value= '0';
            $.ajax({
                type: "POST",
                url: '{{route('changeActivityoffer')}}',
                data: {value:value,type:type,id:$this.data().id,_token:'{{csrf_token()}}'},
                dataType:'json',
                success:function(data){
                    if(data.status==='1'){
                        $this.html(getUncheckedIcon());
                    }
                }
            });
        }
        else{
            $this.html(getSpinner());
            value = 1;
            $.ajax({
                type: "POST",
                url: '{{route('changeActivityoffer')}}',
                data: {value:value,type:type,id:$this.data().id,_token:'{{csrf_token()}}'},
                dataType:'json',
                success:function(data){
                    if(data.status==='1'){
                        $this.html(getCheckedIcon());
                    }
                }
            });
        }
    });
</script>
