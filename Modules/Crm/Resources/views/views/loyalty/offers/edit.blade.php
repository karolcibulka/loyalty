@extends('crm::layouts.master')

@section('summernote')
    true
@endsection

@section('content')
    <style>
        .imageoffer{
            min-height:400px;
            background-repeat : no-repeat;
            background-size: cover;
            background-position: center;
        }
    </style>
    <div class="card" style="margin: 0 auto;">
        <div class="card-header">
            <div class="row">
                <div class="col-md-6">
                    <h5>Editácia zážitku</h5>
                </div>
                <div class="col-md-6">
                    <select name="" id="langChangerCurrent" class="form-control">
                        @if(isset($languages) && !empty($languages))
                            @foreach($languages as $language)
                                <option name="name[{{$language['lang_code']}}]" value="{{$language['lang_code']}}">@lang('global.'.$language['lang_name'])</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <legend></legend>
            <ul class="nav nav-pills nav-pills-bordered nav-pills-toolbar nav-justified">
                <li class="nav-item"><a href="#toolbar-justified-pill1" class="nav-link active" data-toggle="tab">Texty</a></li>
                <li class="nav-item"><a href="#toolbar-justified-pill2" class="nav-link" data-toggle="tab">Nastavenia</a></li>
                <li class="nav-item"><a href="#toolbar-justified-pill3" class="nav-link" data-toggle="tab">Obrázok</a></li>
            </ul>
        </div>
        <div class="card-body">
            <form action="{{route('offers.update',[$offer['id']])}}" method="post" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <div class="tab-pane fade active show" id="toolbar-justified-pill1">
                    <div class="row">
                        <div class="col-md-4">
                            @if(isset($languages) && !empty($languages))
                                @foreach($languages as $language)
                                    <div class="form-group languageRequired language_{{$language['lang_code']}}">
                                        <label> Názov v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
                                        <input type="text"  name="name[{{$language['lang_code']}}]" value="{{isset($offer['lang'][$language['lang_code']]['name']) && !empty($offer['lang'][$language['lang_code']]['name']) ? $offer['lang'][$language['lang_code']]['name'] : ''}}" class="form-control" placeholder="Názov v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}">
                                    </div>
                                @endforeach
                            @endif
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Interný názov</label>
                                <input type="text" name="internal_name" value="<?=$offer['internal_name']?>" class="form-control" placeholder="Interný názov">
                            </div>
                        </div>
                        @if(isset($languages) && !empty($languages))
                            @foreach($languages as $language)
                                <div class="col-md-4 languageRequired language_{{$language['lang_code']}}">
                                    <label>Slug v jazyku {{__('global.'.$language['lang_name'])}}</label>
                                    <input type="text" value="{{isset($offer['lang'][$language['lang_code']]['slug']) && !empty($offer['lang'][$language['lang_code']]['slug']) ? $offer['lang'][$language['lang_code']]['slug'] : ''}}" name="slug[{{$language['lang_code']}}]" placeholder="Slug" class="form-control">
                                </div>
                            @endforeach
                        @endif

                        <div class="col-md-12">
                            @if(isset($languages) && !empty($languages))
                                @foreach($languages as $language)
                                    <div class="form-group languageRequired language_{{$language['lang_code']}}">
                                        <label>Krátky popis v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
                                        <input name="short_description[{{$language['lang_code']}}]" value="{{isset($offer['lang'][$language['lang_code']]['short_description']) && !empty($offer['lang'][$language['lang_code']]['short_description']) ? $offer['lang'][$language['lang_code']]['short_description'] : ''}}" class="form-control" placeholder="Názov v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}">
                                    </div>
                                @endforeach
                            @endif
                        </div>
                        <div class="col-md-12">
                            @if(isset($languages) && !empty($languages))
                                @foreach($languages as $language)
                                    <div class="form-group languageRequired language_{{$language['lang_code']}}">
                                        <label>Dlhý popis v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
                                        <textarea class="summernote" name="long_description[{{$language['lang_code']}}]" placeholder="Názov v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}"><?=isset($offer['lang'][$language['lang_code']]['long_description']) && !empty($offer['lang'][$language['lang_code']]['long_description']) ? $offer['lang'][$language['lang_code']]['long_description'] : ''?></textarea>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="toolbar-justified-pill2">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Viditeľné od</label>
                                <div class="input-group-prepend date" id="datetimepicker1" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input"  name="showed_from" value="{{isset($offer['visible_from']) && !empty($offer['visible_from']) ? $offer['visible_from'] : date('d.m.Y')}}" data-target="#datetimepicker1" data-toggle="datetimepicker"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Viditeľné do</label>
                                <div class="input-group-prepend date" id="datetimepicker2" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" name="showed_to" value="{{isset($offer['visible_to']) && !empty($offer['visible_to']) ? $offer['visible_to'] : date('d.m.Y',strtotime('+ 1 month'))}}" data-target="#datetimepicker2" data-toggle="datetimepicker"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Zobraziť v ponuke zliav</label>
                            <select name="show_in_sales" id="showInSales" class="form-control">
                                <option {{isset($offer['show_in_sales']) && !empty($offer['show_in_sales']) && $offer['show_in_sales'] == '1' ? 'selected' : ''}} value="1">Áno</option>
                                <option {{isset($offer['show_in_sales']) && !empty($offer['show_in_sales']) && $offer['show_in_sales'] == '0' ? 'selected' : ''}} value="0">Nie</option>
                            </select>
                        </div>
                    </div>
                    <div class="row cloneWrapper">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Rezervovať od</label>
                                <div class="input-group-prepend date" id="datetimepicker3" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" name="reservation_from[]" value="{{isset($offer['from'][0]) && !empty($offer['from'][0]) ? $offer['from'][0] : date('d.m.Y')}}" data-target="#datetimepicker3" data-toggle="datetimepicker"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Rezervovať do</label>
                                <div class="input-group-prepend date" id="datetimepicker4" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" name="reservation_to[]" value="{{isset($offer['to'][0]) && !empty($offer['to'][0]) ? $offer['to'][0] : date('d.m.Y',strtotime('+ 1 month'))}}" data-target="#datetimepicker4" data-toggle="datetimepicker"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 operators">
                            <div class="form-group">
                                <label style="opacity:0;display:block"> xxxy</label>
                                <button type="button" class="btn btn-success addNewRow">+</button>
                                <button type="button" class="btn btn-danger removeRow" style="display:none;">-</button>
                            </div>

                        </div>
                    </div>
                    <div id="addedDates">
                        @if(isset($offer['from']) && !empty($offer['from']) && count($offer['from'])>1)
                            @foreach($offer['from'] as $key => $from)
                                @if($key>0)
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="input-group-prepend date" id="datetimepicker_{{$key}}_{{$key}}" data-target-input="nearest">
                                                    <input type="text" class="form-control datetimepicker-input" name="reservation_from[]" value="{{isset($offer['from'][$key]) && !empty($offer['from'][$key]) ? $offer['from'][$key] : date('d.m.Y')}}" data-target="#datetimepicker_{{$key}}_{{$key}}" data-toggle="datetimepicker"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="input-group-prepend date" id="datetimepicker_{{$key}}_{{$key+1}}" data-target-input="nearest">
                                                    <input type="text" class="form-control datetimepicker-input" name="reservation_to[]" value="{{isset($offer['to'][$key]) && !empty($offer['to'][$key]) ? $offer['to'][$key] : date('d.m.Y',strtotime('+ 1 month'))}}" data-target="#datetimepicker_{{$key}}_{{$key+1}}" data-toggle="datetimepicker"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 operators">
                                            <div class="form-group">
                                                <button type="button" class="btn btn-danger removeRow">-</button>
                                            </div>
                                        </div>
                                    </div>
                                    <script>
                                        $('#datetimepicker_{{$key}}_{{$key}},#datetimepicker_{{$key}}_{{$key+1}}').datetimepicker({
                                            format:"DD.MM.YYYY",
                                            minDate:new Date(),
                                            timePicker:false,
                                        });
                                    </script>
                                @endif
                            @endforeach
                        @endif
                    </div>
                    <legend></legend>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Cena v €</label>
                                <input type="number" class="form-control" name="price" value={{ $offer['price'] }} placeholder="napr.10">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>
                                    <input type="hidden" name="offersRestrict" value="0">
                                    <input type="checkbox" {{$offer['offers_restrict'] == '1' ? 'checked' : ''}} name="offers_restrict" value="1" class="offersRestrict">
                                    Obmedziť počet týchto zliav
                                </label>
                                <input type="number" name="offers_restrict_count" class="form-control offersRestrictor" value="{{$offer['offers_restrict_count']}}" placeholder="napr. 100" {{$offer['offers_restrict'] == '1' ? '' : 'disabled'}}>
                            </div>
                        </div>
                        <legend></legend>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Zo zariadení</label>
                            </div>
                            <div class="row">
                                @if(!empty($properties))
                                    @foreach($properties as $property)
                                        <div class="col-md-3">
                                            <label><input type="checkbox" name="properties[]" {{isset($offer['properties']) && !empty($offer['properties']) && in_array($property['id'],$offer['properties']) ? 'checked' : '' }} value="{{$property['id']}}"> {{$property['internal_name']}}</label>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="toolbar-justified-pill3">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="imageoffer" style="{{isset($offer['image']) && !empty($offer['image']) ? 'background-image:url("'.$offer['image'].'")' : ''}}"></div>
                        </div>
                        <input type="hidden" name="remove_image" class="removeImage" value="0">
                        <input type="hidden" name="old_image" value="{{$offer['img']}}">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-info imagePickerButton" style="width:100%;">Vybrať obrázok</button>
                                    <input type="file" name="image" class="imagePicker" style="display:none;">
                                </div>
                                <legend></legend>
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-success cropImageButton" style="width: 100%; {{isset($offer['img']) && !empty($offer['img']) ? '' : 'display:none;'}}">Orezať obrázok</button>
                                </div>
                                <legend></legend>
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-danger removePicture" style="width:100%;">Zmazať obrázok</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="cropper_used" value="0">
                    <input type="hidden" name="cropper_value" value="">
                </div>
                <legend></legend>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Aktivita <br>
                                <label class="switch">
                                    <input type="hidden" name="active" value="0">
                                    <input type="checkbox" {{$offer['active'] == '1' ? 'checked' : ''}} name="active" checked="" value="1">
                                    <span class="slider round"></span>
                                </label>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>
                                Pridať body za nákup <br>
                                <label class="switch">
                                    <input type="hidden" name="add_points" value="0">
                                    <input type="checkbox" {{$offer['add_points'] == '1' ? 'checked' : ''}} name="add_points" checked="" value="1">
                                    <span class="slider round"></span>
                                </label>
                            </label>
                        </div>
                    </div>
                </div>
                <button class="btn btn-primary btn-custom">Odoslať</button>
            </form>
        </div>
    </div>

    <div class="modal fade" id="cropper" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document" style="min-width: 80% !important;">
            <div class="modal-content">
                <div class="modal-body">
                   @include('crm::views.partials.cropper.cropper',['img'=>$offer['image']])
                </div>
            </div>
        </div>
    </div>

    <script>

        $('.removePicture').on('click',function(){
            $('.imageoffer').removeAttr('style');
            $('.cropImageButton').hide();
            $('[name="cropper_used"]').val('0');
            $('[name="cropper_value"]').val('');
            $('.removeImage').val('1');
        });

        var counter = 5;
        $(function () {
            $('#datetimepicker1,#datetimepicker2,#datetimepicker3,#datetimepicker4').datetimepicker({
                format:"DD.MM.YYYY",
                timePicker:false,
            });
        });

        function readURL(input) {
            if (input[0].files && input[0].files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.removeImage').val('0');
                    $('[name="cropper_used"]').val('0');
                    $('[name="cropper_value"]').val('');
                    $('#image').attr('src',e.target.result);
                    $('.imageoffer').css({'backgroundImage':'url("'+e.target.result+'")'});
                    $('.cropImageButton').show();
                }

                reader.readAsDataURL(input[0].files[0]);
            }
        }

        $('.cropImageButton').on('click',function(){
            $('#cropper').modal();
            setTimeout(function(){
                makeClickable()
            },500);
        });

        var makeClickable = function(){
            $(document).find('.aspectSixteenClickable').click();
        }

        $('.imagePicker').on('change',function(){
            readURL($(this));
        });

        $('.imagePickerButton').on('click',function(){
            var html = $(this).html();
            $('.imagePicker').click();
            $(this).html(html);
        });

        $('#langChangerCurrent').on('change',function(){
            var val = $(this).val();
            $('.languageRequired').hide();
            $('.language_'+val).show();
        });

        $(document).ready(function() {
            $('.summernote').summernote();
        });

        $('.addNewRow').on('click',function(){
            var clone = $('.cloneWrapper').clone();
            clone.removeClass('cloneWrapper');
            clone.find('label').remove();
            clone.find('.removeRow').show();

            clone.find('.addNewRow').hide();

            clone.find('#datetimepicker3').removeAttr('id').attr('id','datetimepicker'+(+counter)).find('input').removeAttr('data-target').attr('data-target','#datetimepicker'+(+(counter)));

            clone.find('#datetimepicker4').removeAttr('id').attr('id','datetimepicker'+(+(counter)+1)).find('input').removeAttr('data-target').attr('data-target','#datetimepicker'+(+(counter)+1));

            clone.find('#datetimepicker'+counter).datetimepicker({
                format:"DD.MM.YYYY",
                minDate:new Date(),
                timePicker:false,
            });

            clone.find('#datetimepicker'+((+counter)+1)).datetimepicker({
                format:"DD.MM.YYYY",
                minDate:new Date(),
                timePicker:false,
            });

            counter = counter + 2;

            $('#addedDates').prepend(clone);
        });

        $(document).on('click','.removeRow',function(){
            $(this).closest('.row').remove();
        });

        $('.offersRestrict').on('change',function(){
            if($(this).is(':checked')){
                $('.offersRestrictor').attr('disabled',false);
            }
            else{
                $('.offersRestrictor').attr('disabled',true);
            }
        });
    </script>
@endsection
