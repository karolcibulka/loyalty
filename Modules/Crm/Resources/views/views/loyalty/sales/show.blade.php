@extends('crm::layouts.master')

@section('content')
    <div class="card">
        <div class="card-header">
            <h5>Aktuálne zľavy</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <a href="{{route('sales.create')}}" class="btn btn-primary btn-custom" style="color:white;">Vytvoriť novú zľavu</a>
            </div>
            <legend></legend>
            <div id="tableWrapper">
                @include('crm::views.loyalty.sales.partials.table')
            </div>
        </div>
    </div>
@endsection
