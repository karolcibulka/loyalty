<style>
    .changeActivitySale{
        cursor: pointer;
    }
    .changeAddPointsSale{
        cursor:pointer;
    }
</style>

@if(isset($sales) && !empty($sales))
    <table id="salesTable" class="table  table-striped table-bordered" style="width:100%">
        <thead>
        <tr>
            <th style="text-align: center;">#</th>
            <th style="text-align: center;">Interný názov zľavy</th>
            <th style="text-align: center;">Počet</th>
            <th style="text-align: center;">Predané</th>
            <th style="text-align: center;">Cena</th>
            <th style="text-align: center;">Body</th>
            <th style="text-align: center;">Viditeľné od - do</th>
            <th style="text-align: center;">Aktívna</th>
            <th style="text-align: center;">Pridať body</th>
            <th style="text-align: center;">Akcie</th>
        </tr>
        </thead>
        <tbody>
            @foreach($sales as $sale)
                <tr>
                    <td class="editContent" data-type="showName" data-id="{{$sale['id']}}" style="text-align: center;">{{$sale['id']}}</td>
                    <td class="editContent" data-type="showName" data-id="{{$sale['id']}}" style="text-align: center;">{{$sale['internal_name']}}</td>
                    <td class="editContent" data-type="showName" data-id="{{$sale['id']}}" style="text-align: center;">{{$sale['sales_restrict_count']}}</td>
                    <td class="editContent" data-type="showName" data-id="{{$sale['id']}}" style="text-align: center;">{{$sale['available']}} x</td>
                    <td class="editContent" data-type="showName" data-id="{{$sale['id']}}" style="text-align: center;">{{isset($sale['currency']) && !empty($sale['currency']) ? $sale['price'].' '.$sale['currency'] : $sale['price'].' €'}}</td>
                    <td class="editContent" data-type="showName" data-id="{{$sale['id']}}" style="text-align: center;">{{isset($sale['points']) && !empty($sale['points']) ? $sale['points'].' bodov' : '0'.' bodov'}}</td>
                    <td class="editContent" data-type="showName" data-id="{{$sale['id']}}" style="text-align: center;">{{date('d.m.Y',strtotime($sale['visible_from'])).' - '.date('d.m.Y',strtotime($sale['visible_to']))}}</td>
                    <td class="editContent" data-type="showName" data-id="{{$sale['id']}}" style="text-align: center;">
                        @can('sale.edit')
                            <?=$sale['active'] == '1' ? '<span class="changeActivitySale" data-id="'.$sale['id'].'"> <i class="icon-check active" style="color:darkgreen"></i></span>' : '<span class="changeActivitySale" data-id="'.$sale['id'].'"><i class="icon-cross2 nonactive" style="color:darkred"></i></span>'?>
                        @endcan
                    </td>
                    <td class="editContent" data-type="showName" data-id="{{$sale['id']}}" style="text-align: center;">
                        @can('sale.edit')
                        {!! $sale['add_points'] == '1' ? '<span class="changeAddPointsSale" data-id="'.$sale['id'].'"> <i class="icon-check active" style="color:darkgreen"></i></span>' : '<span class="changeAddPointsSale" data-id="'.$sale['id'].'"><i class="icon-cross2 nonactive" style="color:darkred"></i></span>' !!}
                        @endcan
                    </td>
                    <td style="text-align: center;">
                        @can('sale.edit')
                            <a href="{{route('sales.edit',['sale'=>$sale['id']])}}" class="editRow badge badge-primary" data-id="{{$sale['id']}}" style="cursor:pointer;margin-right:10px;">
                                Upraviť
                            </a>
                        @endcan
                        @can('sale.delete')
                            <form action="{{route('sales.destroy',['sale'=>$sale['id']])}}" method="POST" id="form_delete_{{$sale['id']}}" style="display: inline;">
                                @csrf
                                <input type="hidden" name="_method" value="DELETE">
                                <span class="deleteRow badge badge-danger" data-id="{{$sale['id']}}" style="cursor:pointer;">
                                    Vymazať
                                </span>
                            </form>
                        @endcan
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="row" style="margin-top:20px;">
        <div class="col-md-12">
            {{$sales->links()}}
        </div>
    </div>
@endif


<script>

    var getCheckedIcon = function(){
        var string = '<i class="icon-check active" style="color:darkgreen"></i></span>';
        return string
    }

    var getUncheckedIcon = function(){
        var string = '<i class="icon-cross2 nonactive" style="color:darkred"></i></span>';
        return string
    }

    var getSpinner = function(){
        var string = '<i class="spinner icon-spinner2" style="color:black"></i></span>';
        return string;
    }

    $('.deleteRow').on('click',function(){
        var id = $(this).data().id;
        Swal.fire({
            title:'Naozaj chcete zmazať túto ponuku?',
            showCancelButton:true,
            cancelButtonText:'ODVOLAŤ',
            confirmButtonText:'ZMAZAŤ',
            confirmButtonColor:'darkred',
        }).then(function(response){
            if(response.value === true){
                $('#form_delete_'+id).submit();
            }
        });
    });

    $('.changeActivitySale,.changeAddPointsSale').on('click',function(){
        var $this = $(this);
        var addpoints = '0';

        if($this.hasClass('changeAddPointsSale')){
            addpoints = '1';
        }

        var value = '0';
        if($this.find('i').hasClass('icon-spinner2')){
            return;
        }
        else if($this.find('i').hasClass('active')){
            $this.html(getSpinner());
            value= '0';
            $.ajax({
            type: "POST",
            url: '{{route('changeActivitySale')}}',
            data: {addpoints:addpoints,value:value,id:$this.data().id,_token:'{{csrf_token()}}'},
            dataType:'json',
            success:function(data){
                if(data.status==='1'){
                    $this.html(getUncheckedIcon());
                }
            }
        });
        }
        else{
            $this.html(getSpinner());
            value = 1;
            $.ajax({
            type: "POST",
            url: '{{route('changeActivitySale')}}',
            data: {addpoints:addpoints,value:value,id:$this.data().id,_token:'{{csrf_token()}}'},
            dataType:'json',
            success:function(data){
                if(data.status==='1'){
                    $this.html(getCheckedIcon());
                }
            }
        });
        }
    });
</script>
