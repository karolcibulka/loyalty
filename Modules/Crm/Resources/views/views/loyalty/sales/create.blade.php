@extends('crm::layouts.master')

@section('summernote')
    true
@endsection

@section('content')
    <style>
        .imageSale{
            height:400px;
            background-repeat : no-repeat;
            background-size: cover;
            background-position: center;
        }
    </style>
    <div class="card" style="margin: 0 auto;">
        <div class="card-header">
            <div class="row">
                <div class="col-md-6">
                    <h5>Vytvorenie novej zľavy</h5>
                </div>
                <div class="col-md-6">
                    <select name="" id="langChangerCurrent" class="form-control">
                        @if(isset($languages) && !empty($languages))
                            @foreach($languages as $language)
                                <option name="name[{{$language['lang_code']}}]" value="{{$language['lang_code']}}">@lang('global.'.$language['lang_name'])</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <legend></legend>
            <ul class="nav nav-pills nav-pills-bordered nav-pills-toolbar nav-justified">
                <li class="nav-item"><a href="#toolbar-justified-pill1" class="nav-link active" data-toggle="tab">Texty</a></li>
                <li class="nav-item"><a href="#toolbar-justified-pill2" class="nav-link" data-toggle="tab">Nastavenia</a></li>
                <li class="nav-item"><a href="#toolbar-justified-pill3" class="nav-link" data-toggle="tab">Obrázok</a></li>
            </ul>
        </div>
        <div class="card-body">
            <form action="{{route('sales.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="tab-pane fade active show" id="toolbar-justified-pill1">
                    <div class="row">
                        <div class="col-md-4">
                            @if(isset($languages) && !empty($languages))
                                @foreach($languages as $language)
                                    <div class="form-group languageRequired language_{{$language['lang_code']}}">
                                        <label> Názov v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
                                        <input type="text"  name="name[{{$language['lang_code']}}]" class="form-control" placeholder="Názov v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}">
                                    </div>
                                @endforeach
                            @endif
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Interný názov</label>
                                <input type="text" name="internal_name" class="form-control" placeholder="Interný názov">
                            </div>
                        </div>
                        @if(isset($languages) && !empty($languages))
                            @foreach($languages as $language)
                                <div class="col-md-4 languageRequired language_{{$language['lang_code']}}">
                                    <label>Slug v jazyku {{__('global.'.$language['lang_name'])}}</label>
                                    <input type="text" name="slug[{{$language['lang_code']}}]" placeholder="Slug" class="form-control">
                                </div>
                            @endforeach
                        @endif

                        <div class="col-md-12">
                            @if(isset($languages) && !empty($languages))
                                @foreach($languages as $language)
                                    <div class="form-group languageRequired language_{{$language['lang_code']}}">
                                        <label>Krátky popis v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
                                        <input name="short_description[{{$language['lang_code']}}]" class="form-control" placeholder="Názov v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}">
                                    </div>
                                @endforeach
                            @endif
                        </div>
                        <div class="col-md-12">
                            @if(isset($languages) && !empty($languages))
                                @foreach($languages as $language)
                                    <div class="form-group languageRequired language_{{$language['lang_code']}}">
                                        <label>Dlhý popis v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
                                        <textarea class="summernote" name="long_description[{{$language['lang_code']}}]" placeholder="Názov v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}"></textarea>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="toolbar-justified-pill2">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Viditeľné od</label>
                                <div class="input-group-prepend date" id="datetimepicker1" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" name="showed_from" value="{{date('d.m.Y')}}" data-target="#datetimepicker1" data-toggle="datetimepicker"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Viditeľné do</label>
                                <div class="input-group-prepend date" id="datetimepicker2" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" name="showed_to" value="{{date('d.m.Y',strtotime('+ 1 month'))}}" data-target="#datetimepicker2" data-toggle="datetimepicker"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Viditeľné pre profil >=</label>
                            <select name="profile" id="" class="form-control">
                                <option value="">Pre všetky profily</option>
                            </select>
                        </div>
                    </div>
                    <div class="row cloneWrapper">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Rezervovať od</label>
                                <div class="input-group-prepend date" id="datetimepicker3" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" name="reservation_from[]" value="{{date('d.m.Y')}}" data-target="#datetimepicker3" data-toggle="datetimepicker"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Rezervovať do</label>
                                <div class="input-group-prepend date" id="datetimepicker4" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" name="reservation_to[]" value="{{date('d.m.Y',strtotime('+ 1 month'))}}" data-target="#datetimepicker4" data-toggle="datetimepicker"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 operators">
                            <div class="form-group">
                                <label style="opacity:0;display:block"> xxxy</label>
                                <button type="button" class="btn btn-success addNewRow">+</button>
                                <button type="button" class="btn btn-danger removeRow" style="display:none;">-</button>
                            </div>

                        </div>
                    </div>
                    <div id="addedDates">

                    </div>
                    <legend></legend>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Počet bodov</label>
                                <input type="number" class="form-control" name="points" placeholder="napr.10">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Cena v €</label>
                                <input type="number" class="form-control" name="price" placeholder="napr.10">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>
                                    <input type="hidden" name="sales_restrict" value="0">
                                    <input type="checkbox" name="sales_restrict" value="1" class="salesRestrict">
                                    Obmedziť počet týchto zliav
                                </label>
                                <input type="number" name="sales_restrict_count" class="form-control salesRestrictor" placeholder="napr. 100" disabled>
                            </div>
                        </div>
                        <legend></legend>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Zo zariadení</label>
                            </div>
                            <div class="row">
                                @if(!empty($properties))
                                    @foreach($properties as $property)
                                        <div class="col-md-3">
                                            <label><input type="checkbox" name="properties[]" value="{{$property['id']}}"> {{$property['internal_name']}}</label>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="toolbar-justified-pill3">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="imageSale"></div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-info imagePickerButton" style="width:100%;">Vybrať obrázok</button>
                                    <input type="file" name="image" class="imagePicker" style="display:none;">
                                </div>
                                <legend></legend>
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-success cropImageButton" style="width: 100%; {{isset($sale['img']) && !empty($sale['img']) ? '' : 'display:none;'}}">Orezať obrázok</button>
                                </div>
                                <legend></legend>
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-danger" style="width:100%;">Zmazať obrázok</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="cropper_used" value="0">
                    <input type="hidden" name="cropper_value" value="">
                </div>
                <legend></legend>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Aktívne <br>
                                <label class="switch">
                                    <input type="hidden" name="active" value="0">
                                    <input type="checkbox" name="active" value="1">
                                    <span class="slider round"></span>
                                </label>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Pridať body pri nákupe <br>
                                <label class="switch">
                                    <input type="hidden" name="add_points" value="0">
                                    <input type="checkbox" name="add_points" value="1">
                                    <span class="slider round"></span>
                                </label>
                            </label>
                        </div>
                    </div>
                </div>
                <button class="btn btn-primary btn-custom">Odoslať</button>
            </form>
        </div>
    </div>

    <div class="modal fade" id="cropper" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document" style="min-width: 80% !important;">
            <div class="modal-content">
                <div class="modal-body">
                    @include('crm::views.partials.cropper.cropper')
                </div>
            </div>
        </div>
    </div>

    <script>

        var counter = 5;
        $(function () {
            $('#datetimepicker1,#datetimepicker2,#datetimepicker3,#datetimepicker4').datetimepicker({
                format:"DD.MM.YYYY",
                timePicker:false,
            });
        });

        function readURL(input) {
            if (input[0].files && input[0].files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('[name="cropper_used"]').val('0');
                    $('[name="cropper_value"]').val('');
                    $('#image').attr('src',e.target.result);
                    $('.imageSale').css({'backgroundImage':'url("'+e.target.result+'")'});
                    $('.cropImageButton').show();

                }

                reader.readAsDataURL(input[0].files[0]);
            }
        }

        $('.cropImageButton').on('click',function(){
            $('#cropper').modal();
            setTimeout(function(){
                makeClickable()
            },500);
        });

        var makeClickable = function(){
            $(document).find('.aspectSixteenClickable').click();
        }


        $('.imagePicker').on('change',function(){
            readURL($(this));
        });

        $('.imagePickerButton').on('click',function(){
            var html = $(this).html();
           $('.imagePicker').click();
           $(this).html(html);
        });

        $('#langChangerCurrent').on('change',function(){
           var val = $(this).val();
           $('.languageRequired').hide();
           $('.language_'+val).show();
        });

        $(document).ready(function() {
            $('.summernote').summernote();
        });

        $('.addNewRow').on('click',function(){
            var clone = $('.cloneWrapper').clone();
            clone.removeClass('cloneWrapper');
            clone.find('label').remove();
            clone.find('.removeRow').show();

            clone.find('.addNewRow').hide();

            clone.find('#datetimepicker3').removeAttr('id').attr('id','datetimepicker'+(+counter)).find('input').removeAttr('data-target').attr('data-target','#datetimepicker'+(+(counter)));

            clone.find('#datetimepicker4').removeAttr('id').attr('id','datetimepicker'+(+(counter)+1)).find('input').removeAttr('data-target').attr('data-target','#datetimepicker'+(+(counter)+1));

            clone.find('#datetimepicker'+counter).datetimepicker({
                format:"DD.MM.YYYY",
                minDate:new Date(),
                timePicker:false,
            });

            clone.find('#datetimepicker'+((+counter)+1)).datetimepicker({
                format:"DD.MM.YYYY",
                minDate:new Date(),
                timePicker:false,
            });

            counter = counter + 2;

            //console.log(counter);

            $('#addedDates').prepend(clone);
        });

        $(document).on('click','.removeRow',function(){
            $(this).closest('.row').remove();
        });

        $('.salesRestrict').on('change',function(){
           if($(this).is(':checked')){
               $('.salesRestrictor').attr('disabled',false);
           }
           else{
               $('.salesRestrictor').attr('disabled',true);
           }
        });
    </script>
@endsection
