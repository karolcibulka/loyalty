@extends('crm::layouts.master')

@section('content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/nestable2/1.6.0/jquery.nestable.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/nestable2/1.6.0/jquery.nestable.min.js"></script>

    <style>
        .dd{
            max-width: 100%;
        }
        .dd-handle {
            height: 40px;
            padding-top:8px;
        }
        .dd-item>button {
            padding-top:8px !important;
        }
        .dd-item span{
            cursor:pointer;
        }
    </style>
    <div class="card">
        <div class="card-body">
            <a href="{{route('loyaltynavigations.create')}}" class="btn btn-own btn-custom btn-primary text-white">Pridať prvok do navigácie</a>
            <legend></legend>
            <div class="row">
                <div class="dd col-md-12" id="nestable">
                    <ol class="dd-list">
                        @if(isset($items) && !empty($items))
                            @foreach($items as $item)
                                <li class="dd-item" data-id="{{$item['id']}}">
                                    <div class="dd-handle">
                                        {{$item['internal_name']}}
                                    </div>
                                    <div style="position:absolute;top:10px;right:10px;height:100%;width:200px;">
                                            <span class="ml-auto" style="z-index:10000000;">
                                                <span style="margin-right:10px;">
                                                    @can('loyaltynavigation.edit')
                                                        {!! $item['active'] == '1' ? '<i class="icon-check"  data-id="'.$item['id'].'" style="color:darkgreen"></i>' : '<i class="icon-cross2"  data-id="'.$item['id'].'" style="color:darkred"></i>'!!}
                                                    @endcan
                                                </span>
                                                 @can('loyaltynavigation.edit')
                                                    <a href="{{route('loyaltynavigations.edit',[$item['id']])}}"><span class="badge badge-primary" data-id="{{$item['id']}}" >Upraviť</span></a>
                                                @endcan
                                                @can('loyaltynavigation.delete')
                                                 <form action="{{route('loyaltynavigations.destroy',$item['id'])}}" method="POST" id="form_delete_{{$item['id']}}" style="display: inline;">
                                                    @csrf
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <span class="deleteRow badge badge-danger" data-id="{{$item['id']}}" style="cursor:pointer;">
                                                            Vymazať
                                                        </span>
                                                </form>
                                                @endcan
                                             </span>
                                    </div>
                                    @if(isset($item['children']) && !empty($item['children']))
                                        <ol class="dd-list">
                                            @foreach($item['children'] as $children)
                                                <li class="dd-item" data-id="{{$children['id']}}">
                                                    <div class="dd-handle" style="position:relative;">
                                                        {{$children['internal_name']}}
                                                    </div>
                                                    <div style="position:absolute;top:10px;right:10px;height:100%;width:200px;">
                                                             <span class="ml-auto" style="z-index:10000000;">
                                                                  @can('loyaltynavigation.edit')
                                                                    <span class="changeActivity" style="margin-right:10px;">
                                                                        {!! $children['active'] == '1' ? '<i class="icon-check"  data-id="'.$children['id'].'" style="color:darkgreen"></i>' : '<i class="icon-cross2"  data-id="'.$children['id'].'" style="color:darkred"></i>'!!}
                                                                    </span>
                                                                 @endcan
                                                                  @can('loyaltynavigation.edit')
                                                                    <a href="{{route('loyaltynavigations.edit',[$children['id']])}}"><span class="badge badge-primary" data-id="{{$children['id']}}" >Upraviť</span></a>
                                                                  @endcan
                                                                  @can('loyaltynavigation.edit')
                                                                     <form action="{{route('loyaltynavigations.destroy',$children['id'])}}" method="POST" id="form_delete_{{$children['id']}}" style="display: inline;">
                                                                        @csrf
                                                                        <input type="hidden" name="_method" value="DELETE">
                                                                        <span class="deleteRow badge badge-danger" data-id="{{$children['id']}}" style="cursor:pointer;">
                                                                                Vymazať
                                                                        </span>
                                                                    </form>
                                                                  @endcan
                                                             </span>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ol>
                                    @endif
                                </li>
                            @endforeach
                        @endif
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('#nestable').nestable({
            maxDepth: 2
        });

        $('#nestable').on('change', function() {
            var ser =  $('#nestable').nestable('serialize');
            $.ajax({
                type: "POST",
                url: '{{route('navigationChangeOrder')}}',
                data: {json:ser,_token:'{{csrf_token()}}'},
                dataType:'json',
                success:function(data){
                    if(data.status==='1'){
                    }
                    else if(data.status==='2'){
                        //neni meno
                    }
                    else{
                        //nepreslo to
                    }
                }
            });
        });

        var getCheckedIcon = function(){
            var string = '<i class="icon-check active" style="color:darkgreen"></i></span>';
            return string
        }

        var getUncheckedIcon = function(){
            var string = '<i class="icon-cross2 nonactive" style="color:darkred"></i></span>';
            return string
        }

        var getSpinner = function(){
            var string = '<i class="spinner icon-spinner2" style="color:black"></i></span>';
            return string;
        }

        $('.deleteRow').on('click',function(){
            var id = $(this).data().id;
            Swal.fire({
                title:'Naozaj chcete zmazať túto kategóriu?',
                showCancelButton:true,
                cancelButtonText:'ODVOLAŤ',
                confirmButtonText:'ZMAZAŤ',
                confirmButtonColor:'darkred',
            }).then(function(response){
                if(response.value === true){
                    $('#form_delete_'+id).submit();
                }
            });
        });

        $('.changeActivity').on('click',function(){
            var $this = $(this);
            var value = '0';
            if($this.find('i').hasClass('icon-spinner2')){
                return;
            }
            else if($this.find('i').hasClass('active')){
                $this.html(getSpinner());
                value= '0';
                $.ajax({
                    type: "POST",
                    url: '{{route('changeActivityNavigationLoyalty')}}',
                    data: {value:value,id:$this.data().id,_token:'{{csrf_token()}}'},
                    dataType:'json',
                    success:function(data){
                        if(data.status==='1'){
                            $this.html(getUncheckedIcon());
                        }
                    }
                });
            }
            else{
                $this.html(getSpinner());
                value = 1;
                $.ajax({
                    type: "POST",
                    url: '{{route('changeActivityNavigationLoyalty')}}',
                    data: {value:value,id:$this.data().id,_token:'{{csrf_token()}}'},
                    dataType:'json',
                    success:function(data){
                        if(data.status==='1'){
                            $this.html(getCheckedIcon());
                        }
                    }
                });
            }
        });
    </script>
@endsection
