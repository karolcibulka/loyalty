@extends('crm::layouts.master')

@section('content')
    <style>
        .language{
            display:none;
        }
        .language-sk{
            display:block;
        }
        .type{
            display:none;
        }

        .type-{{$item['type']}}{
            display:block;
        }
    </style>
    <div class="row" style="margin-bottom:10px;">
        <div class="col-md-9"></div>
        <div class="col-md-3">
            <select id="language-changer" class="form-control">
                @if(isset($languages) && !empty($languages))
                    @foreach($languages as $language)
                        <option value="{{$language['lang_code']}}">{{__('global.'.$language['lang_name'])}}</option>
                    @endforeach
                @endif
            </select>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h5>Pridať nový prvok do navigácie</h5>
        </div>
        <div class="card-body">
            <form action="{{route('loyaltynavigations.update',[$item['id']])}}" method="post" enctype="multipart/form-data">
                {{method_field('PUT')}}
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Interný názov</label>
                            <input type="text" placeholder="Interný názov" value="{{$item['internal_name']}}" name="internal_name" class="form-control">
                        </div>
                    </div>
                    @if(isset($languages) && !empty($languages))
                        @foreach($languages as $language)
                            <div class="col-md-6 language language-{{$language['lang_code']}}">
                                <div class="form-group">
                                    <label>Názov v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
                                    <input type="text" placeholder="Názov v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}" value="{{isset($item['titles'][$language['lang_code']]) && !empty($item['titles'][$language['lang_code']]) ? $item['titles'][$language['lang_code']] : ''}}" name="title[{{$language['lang_code']}}]" class="form-control">
                                </div>
                            </div>
                        @endforeach
                    @endif
                    @if(isset($languages) && !empty($languages))
                        @foreach($languages as $language)
                            <div class="col-md-6 language language-{{$language['lang_code']}}">
                                <div class="form-group">
                                    <label>Slug v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
                                    <input type="text" placeholder="Slug v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}" value="{{isset($item['slugs'][$language['lang_code']]) && !empty($item['slugs'][$language['lang_code']]) ? $item['slugs'][$language['lang_code']] : ''}}" name="slug[{{$language['lang_code']}}]" class="form-control">
                                </div>
                            </div>
                        @endforeach
                    @endif
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Typ</label>
                            <select name="type" class="form-control" id="type-changer">
                                <option {{$item['type'] == 'placeholder' ? 'selected' : ''}} value="placeholder">Placeholder</option>
                                <option {{$item['type'] == 'link' ? 'selected' : ''}} value="link">Odkaz</option>
                                <option {{$item['type'] == 'subpage' ? 'selected' : ''}} value="subpage">Podstránka</option>
                                <option {{$item['type'] == 'file' ? 'selected' : ''}} value="file">Súbor</option>
                            </select>
                        </div>
                    </div>
                    <legend class="type type-link type-subpage type-file"></legend>
                    <div class="col-md-4 type type-link">
                        <div class="form-group">
                            <label>Odkaz</label>
                            <input type="text" name="href" value="{{$item['href']}}" class="form-control" placeholder="Odkaz">
                        </div>
                    </div>
                    <div class="col-md-4 type type-subpage">
                        <label>Podstránka</label>
                        <select name="subpage" class="form-control">
                            <option {{$item['subpage'] == '0' ? 'selected' : ''}} value="0">Default</option>
                            @if(isset($subpages) && !empty($subpages))
                                @foreach($subpages as $subpage)
                                    <option {{$item['subpage'] == $subpage['id'] ? 'selected' : ''}} value="{{$subpage['id']}}">{{$subpage['internal_name']}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>

                    <div class="col-md-4 type type-link type-subpage">
                        <div class="from-group">
                            <label>Otvoriť</label>
                            <select name="open" class="form-control">
                                <option {{$item['open'] == 'current' ? 'selected' : ''}} value="current">V aktuálnom okne</option>
                                <option {{$item['open'] == 'new' ? 'selected' : ''}} value="new">V novom okne</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 type type-file">
                        <div class="from-group">
                            <label>Súbor</label>
                            <input name="file" type="file" class="form-control">
                        </div>
                    </div>
                    @if(isset($item['file']) && !empty($item['file']) && $item['file']!=='')
                    <div class="col-md-4 type type-file">
                        <div class="form-group">
                            <label>Nahraný súbor</label>
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" value="{{$item['file']}}" class="form-control" disabled>
                                </div>
                                <div class="col-md-6">
                                    <a target="_blank" href="{{asset('files/'.$item['file'])}}" class="btn btn-primary">Súbor tu</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    <input type="hidden" name="oldFile" value="{{$item['file']}}">
                    <div class="col-md-4 type type-file">
                        <div class="from-group">
                            <label>Stiahnuť/otvoriť súbor</label>
                            <select name="download" class="form-control">
                                <option {{$item['download'] == '1' ? 'selected' : ''}} value="1">Stiahnuť</option>
                                <option {{$item['download'] == '0' ? 'selected' : ''}} value="0">Otvoriť</option>
                            </select>
                        </div>
                    </div>
                </div>

                <legend></legend>
                <div class="row">
                    <div class="col-md-12">
                        <button class="btn btn-primary btn-own btn-custom">Uložiť</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script>
        $('#language-changer').on('change',function(){
            $('.language').hide();
            $('.language-'+$(this).val()).show();
        });

        $('#type-changer').on('change',function(){
            $('.type').hide();
            $('.type-'+$(this).val()).show();
        });
    </script>
@endsection
