@extends('crm::layouts.master')

@section('content')
    <style>
        .language{
            display:none;
        }
        .language-sk{
            display:block;
        }
        .type{
            display:none;
        }
    </style>
    <div class="row" style="margin-bottom:10px;">
        <div class="col-md-9"></div>
        <div class="col-md-3">
            <select id="language-changer" class="form-control">
                @if(isset($languages) && !empty($languages))
                    @foreach($languages as $language)
                        <option value="{{$language['lang_code']}}">{{__('global.'.$language['lang_name'])}}</option>
                    @endforeach
                @endif
            </select>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h5>Pridať nový prvok do navigácie</h5>
        </div>
        <div class="card-body">
            <form action="{{route('loyaltynavigations.store')}}" method="post" enctype="multipart/form-data">
               @csrf
               <div class="row">
                   <div class="col-md-6">
                       <div class="form-group">
                           <label>Interný názov</label>
                           <input type="text" placeholder="Interný názov" name="internal_name" class="form-control">
                       </div>
                   </div>
                   @if(isset($languages) && !empty($languages))
                       @foreach($languages as $language)
                           <div class="col-md-6 language language-{{$language['lang_code']}}">
                               <div class="form-group">
                                   <label>Názov v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
                                   <input type="text" placeholder="Názov v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}" name="title[{{$language['lang_code']}}]" class="form-control">
                               </div>
                           </div>
                       @endforeach
                   @endif
                   @if(isset($languages) && !empty($languages))
                       @foreach($languages as $language)
                           <div class="col-md-6 language language-{{$language['lang_code']}}">
                               <div class="form-group">
                                   <label>Slug v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
                                   <input type="text" placeholder="Slug v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}" name="slug[{{$language['lang_code']}}]" class="form-control">
                               </div>
                           </div>
                       @endforeach
                   @endif
                   <div class="col-md-6">
                       <div class="form-group">
                           <label>Typ</label>
                           <select name="type" class="form-control" id="type-changer">
                               <option value="placeholder">Placeholder</option>
                               <option value="link">Odkaz</option>
                               <option value="subpage">Podstránka</option>
                               <option value="file">Súbor</option>
                           </select>
                       </div>
                   </div>
                   <legend class="type type-link type-subpage type-file"></legend>
                   <div class="col-md-4 type type-link">
                       <div class="form-group">
                           <label>Odkaz</label>
                           <input type="text" name="href" class="form-control" placeholder="Odkaz">
                       </div>
                   </div>
                   <div class="col-md-4 type type-subpage">
                       <label>Podstránka</label>
                       <select name="subpage" class="form-control">
                           <option value="0">Default</option>
                           @if(isset($subpages) && !empty($subpages))
                               @foreach($subpages as $subpage)
                                   <option value="{{$subpage['id']}}">{{$subpage['internal_name']}}</option>
                               @endforeach
                           @endif
                       </select>
                   </div>

                   <div class="col-md-4 type type-link type-subpage">
                       <div class="from-group">
                           <label>Otvoriť</label>
                           <select name="open" class="form-control">
                               <option value="current">V aktuálnom okne</option>
                               <option value="new">V novom okne</option>
                           </select>
                       </div>
                   </div>
                   <div class="col-md-4 type type-file">
                       <div class="from-group">
                           <label>Súbor</label>
                           <input name="file" type="file" class="form-control">
                       </div>
                   </div>
                   <div class="col-md-4 type type-file">
                       <div class="from-group">
                           <label>Stiahnuť/otvoriť súbor</label>
                           <select name="download" class="form-control">
                               <option value="1">Stiahnuť</option>
                               <option value="0">Otvoriť</option>
                           </select>
                       </div>
                   </div>
               </div>

                <legend></legend>
                <div class="row">
                    <div class="col-md-12">
                        <button class="btn btn-primary btn-own btn-custom">Uložiť</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script>
        $('#language-changer').on('change',function(){
           $('.language').hide();
           $('.language-'+$(this).val()).show();
        });

        $('#type-changer').on('change',function(){
            $('.type').hide();
            $('.type-'+$(this).val()).show();
        });
    </script>
@endsection
