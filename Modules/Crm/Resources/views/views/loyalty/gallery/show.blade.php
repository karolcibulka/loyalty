@extends('crm::layouts.master')
@section('content')
    <div class="card">
        <div class="card-body">
            <a href="{{route('galleries.create')}}" class="btn btn-primary btn-custom btn-own text-white">Vytvoriť novú galériu</a>
            <legend></legend>
            @include('crm::views.loyalty.gallery.partials.table')
        </div>

    </div>
@endsection
