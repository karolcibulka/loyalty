<style>
    .badge{
        cursor:pointer;
    }
    .changeActivity{
        cursor:pointer;
    }
</style>

<legend></legend>
<div class="row">
    <div class="dd col-md-12" id="nestable" style="min-width:100%;">
        <ol class="dd-list">
            @if(isset($images) && !empty($images))
                @foreach($images as $image)
                    <li class="dd-item" data-id="{{$image['image_id']}}">
                        <div class="dd-handle">
                            <div style="height:100%;width:150px;background-repeat: no-repeat;background-size: cover;background-position: center;background-image:url('{{asset('images/medium').'/'.$image['image']}}')"></div>
                        </div>
                        <div style="position:absolute;top:10px;right:10px;height:100%;width:200px;">
                            <span class="ml-auto" style="z-index:10000000;">
                                <span style="margin-right:10px;">
                                    @can('image.edit')
                                        <span class="changeActivity" data-id="{{$image['image_id']}}" style="margin-right:10px;">
                                            {!! $image['active'] == '1' ? '<i class="icon-check active" style="color:darkgreen"></i>' : '<i class="icon-cross2 nonactive" style="color:darkred"></i>'!!}
                                        </span>
                                    @endcan
                                </span>
                                @can('image.edit')
                                    <a href="{{route('images.edit',$image['image_id'])}}"><span class="badge badge-primary" data-id="{{$image['image_id']}}">Upraviť</span></a>
                                @endcan
                                @can('image.delete')
                                    <span class="badge badge-danger removeRow" data-id="{{$image['image_id']}}">Zmazať</span>
                                @endcan
                             </span>
                        </div>
                    </li>
                @endforeach
            @endif
        </ol>
    </div>
</div>

<script>
    $('#nestable').nestable({
        maxDepth: 1
    });

    $('.removeRow').on('click',function(){
       var _this = $(this);
       var id = _this.data().id;
        Swal.fire({
            title:'Naozaj chcete zmazať tento obrázok?',
            showCancelButton:true,
            cancelButtonText:'ODVOLAŤ',
            confirmButtonText:'ZMAZAŤ',
            confirmButtonColor:'darkred',
        }).then(function(response){
            if(response.value === true){
                $.ajax({
                    type:'post',
                    url:'{{route('galleries.removeImage')}}',
                    dataType:'json',
                    data:{_token:'{{csrf_token()}}',id:id},
                    success:function(data){
                        _this.closest('.dd-item').remove();
                    }
                });
            }
        });
    });


    $('#nestable').on('change', function() {
        var serialized =  $('#nestable').nestable('serialize');
        console.log(serialized);
        $.ajax({
            type: "POST",
            url: '{{route('galleryChangeOrder')}}',
            data: {json:serialized,galleryID:'{{$id}}',_token:'{{csrf_token()}}'},
            dataType:'json',
            success:function(data){
                if(data.status==='1'){

                }
                else if(data.status==='2'){
                    //neni meno
                }
                else{
                    //nepreslo to
                }
            }
        });
    });
</script>
