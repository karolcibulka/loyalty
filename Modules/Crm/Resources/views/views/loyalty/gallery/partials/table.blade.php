<style>
    .badge{
        cursor:pointer;
    }
    .activeChanger{
        cursor:pointer;
    }
</style>

@if(isset($galleries) && !empty($galleries))
    <table id="offersTable" class="table  table-striped table-bordered" style="width:100%">
        <thead>
        <tr>
            <th style="text-align: center;">#</th>
            <th style="text-align: center;">Interný názov galérie</th>
            <th style="text-align: center;">Aktivita</th>
            <th style="text-align: center;">Akcia</th>

        </tr>
        </thead>
        <tbody>
        @foreach($galleries as $gallery)
            <tr>
                <td class="editContent" data-type="showName" data-id="{{$gallery['id']}}" style="text-align: center;">{{$gallery['id']}}</td>
                <td class="editContent" data-type="showName" data-id="{{$gallery['id']}}" style="text-align: center;">{{$gallery['internal_name']}}</td>
                <td class="editContent" data-type="showName" data-id="{{$gallery['id']}}" style="text-align: center;">
                    @can('gallery.edit')
                        <span class="activeChanger" data-id="{{$gallery['id']}}">
                            {!! $gallery['active'] == '1' ? '<i class="icon-check active" style="color:darkgreen"></i>' : '<i class="icon-cross2 nonactive" style="color:darkred"></i>'!!}
                        </span>
                    @endcan
                </td>
                <td class="editContent" data-type="showName" data-id="{{$gallery['id']}}" style="text-align: center;">
                    @can('gallery.show')
                        <a href="{{route('galleries.show',[$gallery['id']])}}"><span class="badge badge-success">Galéria</span></a>
                    @endcan
                    @can('gallery.edit')
                        <a href="{{route('galleries.edit',[$gallery['id']])}}"><span class="badge badge-primary">Upraviť</span></a>
                    @endcan
                    @can('gallery.delete')
                        <form action="{{route('galleries.destroy',[$gallery['id']])}}" method="post" id="form_delete_{{$gallery['id']}}" style="display:inline-block">
                            {{method_field('DELETE')}}
                            @csrf
                        </form>
                       <span class="badge badge-danger deleteRow" data-id="{{$gallery['id']}}">Zmazať</span>
                    @endcan
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="row" style="margin-top:20px;">
        <div class="col-md-12">
            {{$galleries->links()}}
        </div>
    </div>
@endif

<script>

    var getCheckedIcon = function(){
        var string = '<i class="icon-check active" style="color:darkgreen"></i></span>';
        return string
    }

    var getUncheckedIcon = function(){
        var string = '<i class="icon-cross2 nonactive" style="color:darkred"></i></span>';
        return string
    }

    var getSpinner = function(){
        var string = '<i class="spinner icon-spinner2" style="color:black"></i></span>';
        return string;
    }

    $('.activeChanger').on('click',function(){
        var $this = $(this);
        var value = '0';

        if($this.find('i').hasClass('icon-spinner2')){
            return;
        }
        else if($this.find('i').hasClass('active')){
            $this.html(getSpinner());
            value= '0';
            $.ajax({
                type: "POST",
                url: '{{route('galleries.changeActivityGallery')}}',
                data: {value:value,id:$this.data().id,_token:'{{csrf_token()}}'},
                dataType:'json',
                success:function(data){
                    if(data.status==='1'){
                        $this.html(getUncheckedIcon());
                    }
                }
            });
        }
        else{
            $this.html(getSpinner());
            value = 1;
            $.ajax({
                type: "POST",
                url: '{{route('galleries.changeActivityGallery')}}',
                data: {value:value,id:$this.data().id,_token:'{{csrf_token()}}'},
                dataType:'json',
                success:function(data){
                    if(data.status==='1'){
                        $this.html(getCheckedIcon());
                    }
                }
            });
        }
    });


    $('.deleteRow').on('click',function(){
        var id = $(this).data().id;
        Swal.fire({
            title:'Naozaj chcete zmazať túto galériu?',
            showCancelButton:true,
            cancelButtonText:'ODVOLAŤ',
            confirmButtonText:'ZMAZAŤ',
            confirmButtonColor:'darkred',
        }).then(function(response){
            if(response.value === true){
                $('#form_delete_'+id).submit();
            }
        });
    });
</script>
