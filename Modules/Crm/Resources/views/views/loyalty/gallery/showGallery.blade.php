@extends('crm::layouts.master')

@section('content')

    <style>
        .dropzone .dz-default.dz-message:before {
            content: '\ea0e';
            font-family: icomoon;
            font-size: 4rem;
            display: inline-block;
            position: absolute;
            top: 0 !important;
            left: 50%;
            -webkit-transform: translateX(-50%);
            transform: translateX(-50%);
            line-height: 1;
            z-index: 2;
            color: #ccc;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }

        .dropzone .dz-default.dz-message>span {
            font-size: 1.0625rem;
            color: #777;
            display: block;
            margin-top: 5.25rem;
        }

        .dropzone .dz-default.dz-message {
            height: 7rem;
            width: 100%;
            position: absolute;
            top: 0;
            left: 0;
            text-align: center;
            opacity: 1;
            border-radius: .1875rem;
            transition: opacity ease-in-out .15s;
        }

        .dd-handle {
            display: block;
            min-height: 100px !important;
            margin: 5px 0;
            padding: 5px 10px;
            color: #333;
            text-decoration: none;
            font-weight: 700;
            border: 1px solid #ccc;
            background: #fafafa;
             border-radius: 0 !important;
            box-sizing: border-box;
        }

    </style>
<div class="card">
    <div class="card-body">
        <script src="https://rawgit.com/enyo/dropzone/master/dist/dropzone.js"></script>
        <link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/nestable2/1.6.0/jquery.nestable.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/nestable2/1.6.0/jquery.nestable.min.js"></script>
        <div class="row">
            <div class="col-md-3">
                <a href="{{route('galleries.index')}}" class="btn btn-primary btn-own btn-custom">Späť</a>
            </div>
        </div>
        @can('gallery.create')
        <legend></legend>
            {!! Form::open([ 'route' => [ 'galleries.imagestore',$id ], 'files' => true, 'enctype' => 'multipart/form-data', 'class' => 'dropzone', 'id' => 'image-upload' ]) !!}
            {!! Form::close() !!}
        @endcan
        <div id="imageWrapper" style="width:100%">
            @include('crm::views.loyalty.gallery.partials.imageWrapper')
        </div>
    </div>
</div>

    <script type="text/javascript">
        Dropzone.options.imageUpload = {
            maxFilesize: 100,
            acceptedFiles: ".jpeg,.jpg,.png",
            init: function () {
                this.on("success", function (response, controllerData) {
                    var _brm = controllerData;
                    $('#imageWrapper').html(_brm.view);
                });
            }
        };

        var getCheckedIcon = function(){
            var string = '<i class="icon-check active" style="color:darkgreen"></i></span>';
            return string
        }

        var getUncheckedIcon = function(){
            var string = '<i class="icon-cross2 nonactive" style="color:darkred"></i></span>';
            return string
        }

        var getSpinner = function(){
            var string = '<i class="spinner icon-spinner2" style="color:black"></i></span>';
            return string;
        }

        $(document).on('click','.changeActivity',function(event){
            event.preventDefault();
            event.stopPropagation();
            var $this = $(this);
            var value = '0';
            if ($this.find('i').hasClass('icon-spinner2')) {
                return;
            } else if ($this.find('i').hasClass('active')) {
                $this.html(getSpinner());
                value = '0';
                $.ajax({
                    type: "POST",
                    url: '{{route('galleries.changeActivity')}}',
                    data: {value: value, id: $this.data().id, _token: '{{csrf_token()}}'},
                    dataType: 'json',
                    success: function (data) {
                        if (data.status === '1') {
                            $this.html(getUncheckedIcon());
                        }
                    }
                });
            }
            else{
                $this.html(getSpinner());
                value = 1;
                $.ajax({
                    type: "POST",
                    url: '{{route('galleries.changeActivity')}}',
                    data: {value:value,id:$this.data().id,_token:'{{csrf_token()}}'},
                    dataType:'json',
                    success:function(data){
                        if(data.status==='1'){
                            $this.html(getCheckedIcon());
                        }
                    }
                });
            }
        });
    </script>

@endsection
