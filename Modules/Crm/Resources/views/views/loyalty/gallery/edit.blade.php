@extends('crm::layouts.master')
@section('summernote')
    1
@endsection
@section('content')
    <style>
        .languages{
            display:none;
        }

        .language-sk{
            display:block;
        }

    </style>
    <div class="row">
        <div class="col-md-9"></div>
        <div class="col-md-3">
            <select id="language-changer" class="form-control">
                @if(isset($languages) && !empty($languages))
                    @foreach($languages as $language)
                        <option value="{{$language['lang_code']}}">{{__('global.'.$language['lang_name'])}}</option>
                    @endforeach
                @endif
            </select>
        </div>
    </div>

    <div class="card" style="margin-top:10px;">
        <div class="card-header">
            <h5>Vytvorenie novej galérie</h5>
        </div>
        <div class="card-body">
            <form action="{{route('galleries.update',$id)}}" method="post">
                {{method_field('PUT')}}
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Interný názov</label>
                            <input type="text" name="internal_name" value="{{$gallery['internal_name']}}" placeholder="Interný názov" class="form-control">
                        </div>
                    </div>
                    @if(isset($languages) && !empty($languages))
                        @foreach($languages as $language)
                            <div class="col-md-6 languages language-{{$language['lang_code']}}">
                                <div class="form-group">
                                    <label>Nadpis v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
                                    <input type="text" name="title[{{$language['lang_code']}}]" value="{{isset($gallery['title'][$language['lang_code']]) && !empty($gallery['title'][$language['lang_code']]) ? $gallery['title'][$language['lang_code']] : ''}}" placeholder="Nadpis v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}" class="form-control">
                                </div>
                            </div>
                        @endforeach
                    @endif
                    @if(isset($languages) && !empty($languages))
                        @foreach($languages as $language)
                            <div class="col-md-6 languages language-{{$language['lang_code']}}">
                                <div class="form-group">
                                    <label>Krátky popis v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
                                    <input type="text" name="short_description[{{$language['lang_code']}}]" value="{{isset($gallery['short_description'][$language['lang_code']]) && !empty($gallery['short_description'][$language['lang_code']]) ? $gallery['short_description'][$language['lang_code']] : ''}}" placeholder="Krátky popis v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}" class="form-control">
                                </div>
                            </div>
                        @endforeach
                    @endif
                    @if(isset($languages) && !empty($languages))
                        @foreach($languages as $language)
                            <div class="col-md-6 languages language-{{$language['lang_code']}}">
                                <div class="form-group">
                                    <label>Dlhý popis v jazyku {{mb_strtolower(__('global.'.$language['lang_name']))}}</label>
                                    <textarea type="text" name="long_description[{{$language['lang_code']}}]" class="form-control summernote">{{isset($gallery['long_description'][$language['lang_code']]) && !empty($gallery['long_description'][$language['lang_code']]) ? $gallery['long_description'][$language['lang_code']] : ''}}</textarea>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
                <legend></legend>
                <button class="btn btn-primary btn-own btn-custom w100">Uložiť</button>
            </form>
        </div>
    </div>

    <script>

        $('.summernote').summernote();

        $('#language-changer').on('change',function(){
            $('.languages').hide();
            $('.language-'+$(this).val()).show();
        });
    </script>
@endsection
