<table id="rolesTable" class="table  table-striped table-bordered" style="width:100%">
    <thead>
    <tr>
        <th style="text-align: center;">Permission</th>
        <th style="text-align: center;">Akcia</th>
    </tr>
    </thead>
    <tbody>
    @if(isset($permissions) && !empty($permissions))
        @foreach($permissions as $permissionName => $permission)
            <tr>
                <td class="editContent" style="text-align: center;">{{$permissionName}}</td>
                <td style="text-align: center;">
                    <a href="{{route('permission.edit',['permission'=>$permission['id']])}}" class="editRow badge badge-primary" data-id="{{$permission['id']}}" style="cursor:pointer;margin-right:10px;">
                        Upraviť
                    </a>
                    <form action="{{route('permission.destroy',['permission'=>$permission['id']])}}" id="deleteField_{{$permission['id']}}" method="post" style="display:inline;">
                        {{ method_field('delete') }}
                        @csrf
                        <span class="deleteRow badge badge-danger" data-id="{{$permission['id']}}" style="cursor:pointer;">
                            Vymazať
                        </span>
                    </form>
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>

<script>
    $('.deleteRow').on('click',function(){
       var id = $(this).data().id;
        Swal.fire({
            title:'Naozaj chcete zmazať túto právomoc?',
            showCancelButton:true,
            cancelButtonText:'ODVOLAŤ',
            confirmButtonText:'ZMAZAŤ',
            confirmButtonColor:'darkred',
        }).then(function(response){
            if(response.value === true){
                $('#deleteField_'+id).submit();
            }
        });
    });
</script>
