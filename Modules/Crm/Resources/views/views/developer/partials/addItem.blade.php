<form action="{{route('store')}}" id="createForm" method="post">
    <div class="form-group">
        <label class="col-form-label" for="">Názov v menu</label>
        <input type="text" id="controllerName" class="form-control" value="{{(isset($item['show_name']) && !empty($item['show_name'])) ? $item['show_name'] : ''}}" name="controller_name">
    </div>
    <div class="form-group">
        <label class="col-form-label" for="">Typ</label>
        <select id="type" name="type" class="form-control">
            <option {{(isset($item['type']) && !empty($item['type'])) && $item['type']=='placeholder' ? 'selected' : ''}} value="placeholder">Placeholder</option>
            <option {{(isset($item['type']) && !empty($item['type'])) && $item['type']=='controller' ? 'selected' : ''}} value="controller">Controller</option>
            <option {{(isset($item['type']) && !empty($item['type'])) && $item['type']=='link' ? 'selected' : ''}} value="link">Link</option>
        </select>
    </div>
    <div class="form-group">
        <label for="" class="col-form-label">Navigácia</label>
        <select name="navigation" id="" class="form-control">
            <option {{(isset($item['navigation']) && !empty($item['navigation'])) && $item['navigation']=='crm' ? 'selected' : ''}} value="crm">CRM</option>
            <option {{(isset($item['navigation']) && !empty($item['navigation'])) && $item['navigation']=='loyalty' ? 'selected' : ''}} value="loyalty">Loyalty</option>
        </select>
    </div>
    <div id="controllerWrapper" class="form-group" style="{{(isset($item['type']) && !empty($item['type'])) && $item['type']=='controller' ? '' : 'display:none'}}">
        <label class="col-form-label" for="">Controller</label>
        <select id="" name="controller_id" class="form-control">
            @if(isset($controllers) && !empty($controllers))
                @foreach($controllers as $controller)
                    <option {{(isset($item['controller_id']) && !empty($item['controller_id']) && $item['controller_id']==$controller['id']) ? 'selected' : ''}} value="{{$controller['id']}}">{{$controller['show_name'].'  ['.$controller['name'].']'}}</option>
                @endforeach
            @endif
        </select>
    </div>
    <div id="parentWrapper" class="form-group" style="{{(isset($item['type']) && !empty($item['type'])) && $item['type']=='controller' ? '' : 'display:none'}}">
        <label class="col-form-label" for="">Rodič</label>
        <select id="" name="parent_id" class="form-control">
            <option value="">Ja som rodič!</option>
            @if(isset($parents) && !empty($parents))
                @foreach($parents as $parent)
                    <option {{(isset($item['parent']) && !empty($item['parent']) && $item['parent']==$parent['id']) ? 'selected' : ''}} value="{{$parent['id']}}">{{$parent['show_name']}}</option>
                @endforeach
            @endif
        </select>
    </div>
    <div id="uriWrapper" class="form-group" style="{{(isset($item['type']) && !empty($item['type'])) && $item['type']=='link' ? '' : 'display:none'}}">
        <label class="col-form-label" for="">URL</label>
        <input type="text" name="uri" value="{{(isset($item['uri']) && !empty($item['uri'])) ? $item['uri'] : ''}}" class="form-control">
    </div>
    <legend></legend>
    <div class="row">
        <div class="col-md-12" style="max-height:300px;overflow-y: scroll">
           @if(isset($icons) && !empty($icons))
               <div class="row">
                   @foreach($icons as $icon)
                        <div class="col-md-3">
                            <label for="">
                                <input name="icon" {{(isset($item['icon']) && !empty($item['icon']) && $item['icon']==$icon['value']) ? 'checked' : ''}} value="{{$icon['value']}}" type="radio">
                                <i class="icon-{{$icon['value']}}" style="padding-left:5px;"></i>
                            </label>
                        </div>
                    @endforeach
               </div>
           @endif
        </div>
    </div>

    <legend></legend>
    @if(isset($item['id']) && !empty($item['id']))
        <input type="hidden" name="id" value="{{$item['id']}}">
    @endif
    @csrf
    <div class="form-group">
        <button id="{{isset($item['id']) && !empty($item['id']) ? 'editButton' : 'createButton'}}" class="btn btn-primary btn-custom">{{(isset($item['id']) && !empty($item['id'])) ? 'Uložiť' : 'Odoslať'}}</button>
    </div>
</form>
