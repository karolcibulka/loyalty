@extends('crm::layouts.master')

@section('content')
    <div class="card" style="width:60%;margin:0 auto;">
        <div class="card-header">
            <h5>Vytvorenie nového api-kľúču</h5>
        </div>

        <div class="card-body">
            <form action="{{route('apideveloper.store')}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="" class="form-label">Názov api kľúču</label>
                    <input type="text" name="name" class="form-control" placeholder="Názov ec. api-gcci">
                </div>
                <legend></legend>
                <button class="btn btn-primary btn-custom">Vytvoriť nový api-kľúč</button>
            </form>
        </div>

    </div>
@endsection
