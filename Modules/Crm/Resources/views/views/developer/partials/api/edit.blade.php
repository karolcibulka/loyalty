@extends('crm::layouts.master')

@section('content')
    <div class="card" style="width:60%;margin:0 auto;">
        <div class="card-header">
            <h5>Editácia api-kľúču</h5>
        </div>

        <div class="card-body">
            <form action="{{route('apideveloper.update',['apideveloper'=>$api['id']])}}" method="post">
                {{method_field('PUT')}}
                @csrf
                <div class="form-group">
                    <label for="" class="form-label">Názov api kľúču</label>
                    <input type="text" name="name" class="form-control" value="{{$api['name']}}" placeholder="Názov ec. api-gcci">
                </div>
                <legend></legend>
                <button class="btn btn-primary btn-custom">Upraviť api-kľúč</button>
            </form>
        </div>
    </div>
@endsection
