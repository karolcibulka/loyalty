<table id="example" class="table  table-striped table-bordered" style="width:100%">
    <thead>
    <tr>
        <th style="text-align: center;">#</th>
        <th style="text-align: center;">Zobrazenie</th>
        <th style="text-align: center;">Názov</th>
        <th style="text-align: center;">Controller</th>
        <th style="text-align: center;">Funkcia</th>
        <th style="text-align: center;">Akcie</th>
    </tr>
    </thead>
    <tbody>
    @if(isset($controllers) && !empty($controllers))
    @foreach($controllers as $controller)
    <tr>
        <td style="text-align: center;">{{$controller['id']}}</td>
        <td class="editContent" data-type="showName" data-id="{{$controller['id']}}" style="text-align: center;">{{$controller['show_name']}}</td>
        <td class="editContent" data-type="name" data-id="{{$controller['id']}}" style="text-align: center;">{{$controller['name']}}</td>
        <td class="editContent" data-type="controller" data-id="{{$controller['id']}}" style="text-align:center;">{{$controller['controller']}}</td>
        <td class="editContent" data-type="function" data-id="{{$controller['id']}}" style="text-align: center;">{{$controller['function']}}</td>
        <td style="text-align: center;"><span class="editRow badge badge-primary" data-id="{{$controller['id']}}" style="cursor:pointer;margin-right:10px;">Edit</span><span class="deleteRow badge badge-danger" data-id="{{$controller['id']}}" style="cursor:pointer;">Delete</span></td>
    </tr>
    @endforeach
    @endif
    </tbody>
</table>

<script>

    $('.editRow').on('click',function(){
        var id = $(this).data().id;
        $.ajax({
            url:'{{route('editController')}}',
            method:'post',
            data:{id:id},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType:'json',
            success:function(data){
                if(data.status==='1'){
                   $('#formWrapper').html(data.view);
                }
            }
        });
    });

    $('.deleteRow').on('click',function(){
        var btn = $(this);
        Swal.fire({
            title:'Naozaj chcete zmazať tento controller?',
            showCancelButton:true,
            cancelButtonText:'ODVOLAŤ',
            confirmButtonText:'ZMAZAŤ',
            confirmButtonColor:'darkred',
        }).then(function(response){
            if(response.value === true){
                var id = btn.data().id;
                var row = btn.closest('tr');
                $.ajax({
                    url:'{{route('removeController')}}',
                    method:'post',
                    data:{id:id},
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType:'json',
                    success:function(data){
                        if(data.status==='1'){
                            row.remove();
                        }
                    }
                });
            }
        });
    });
</script>
