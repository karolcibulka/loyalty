@extends('crm::layouts.master')
@section('content')
    <div class="card" style="width:60%;margin:0 auto;">
        <div class="card-header">
            <h5>Úprava permissionu</h5>
        </div>
        <div class="card-body">
            <form action="{{route('permission.update',['permission'=>$id])}}" method="post">
                {{ method_field('PUT') }}
                <div class="form-group">
                    <label for="" class="form-label">Názov</label>
                    <input type="text" class="form-control" name="name" value="{{$name}}" placeholder="Názov fe. role">
                </div>
                @if(isset($permissions) && !empty($permissions))
                    <input type="hidden" name="ids" value="{{$permissions['ids']}}">
                @endif
                <input type="hidden" name="oldname" value="{{$name}}">
                @csrf
                <button class="btn btn-primary btn-custom">Uložiť</button>
            </form>
        </div>
    </div>
@endsection
