@extends('crm::layouts.master')
@section('content')
    <div class="card" style="width:60%;margin:0 auto;">
        <div class="card-header">
            <h5>Vytvorenie permissionu</h5>
        </div>
        <div class="card-body">
            <form action="{{route('permission.store')}}" method="post">
                <div class="form-group">
                    <label for="" class="form-label">Názov</label>
                    <input type="text" class="form-control" name="name" placeholder="Názov fe. role">
                </div>
                @csrf
                <button class="btn btn-primary btn-custom">Uložiť</button>
            </form>
        </div>
    </div>
@endsection
