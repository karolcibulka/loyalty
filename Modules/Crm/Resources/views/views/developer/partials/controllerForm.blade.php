@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{(isset($item) && !empty($item)) ? route('editItemController') : route('storeController')}}" method="post">
    <div class="row" style="margin-bottom:20px;">
        <div class="col-md-3">
            <div class="form-group">
                <label for="" class="form-label">Zobrazenie</label>
                <input type="text" class="form-control" name="show_name" value="{{isset($item['show_name']) && !empty($item['show_name']) ? $item['show_name'] : ''}}" placeholder="Zobrazenie">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="" class="form-label">Názov</label>
                <input type="text" placeholder="Názov" name="name" id="controllerName" value="{{isset($item['name']) && !empty($item['name']) ? $item['name'] : ''}}" class="form-control">
            </div>
        </div>
        @if (isset($item) && !empty($item))
            <input type="hidden" name="id" value="{{$item['id']}}">
        @endif
        @csrf
        <div class="col-md-3">
            <div class="form-group">
                <label for="" class="form-label">Controller</label>
                <input type="text" name="controller" id="controllerSelect" value="{{isset($item['controller']) && !empty($item['controller']) ? $item['controller'] : ''}}" class="form-control" placeholder="controller">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="" class="form-label" style="color:transparent">button</label>
                @if(isset($item) && !empty($item))
                    <div class="row">
                        <button class="btn btn-primary btn-custom" style="width:49%;">Upraviť</button>
                        <a class="btn btn-info createNew" style="width:49%;color:white">Nový</a>
                    </div>
                @else
                    <button class="btn btn-primary btn-custom">Uložiť</button>
                @endif
            </div>
        </div>
    </div>
</form>
