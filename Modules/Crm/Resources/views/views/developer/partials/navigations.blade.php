<style>
    p { line-height: 1.5em; }
    .small { color: #666; font-size: 0.875em; }
    .large { font-size: 1.25em; }
    /**
     * Nestable
     */
    .dd { position: relative; display: block; margin: 0; padding: 0; max-width: 600px; list-style: none; font-size: 13px; line-height: 20px; }
    .dd-list { display: block; position: relative; margin: 0; padding: 0; list-style: none; }
    .dd-list .dd-list { padding-left: 30px; }
    .dd-collapsed .dd-list { display: none; }
    .dd-item,
    .dd-empty,
    .dd-placeholder { display: block; position: relative; margin: 0; padding: 0; min-height: 20px; font-size: 13px; line-height: 20px; }
    .dd-handle { display: block; height: 30px; margin: 5px 0; padding: 5px 10px; color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
        background: #fafafa;
        background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
        background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
        background:         linear-gradient(top, #fafafa 0%, #eee 100%);
        -webkit-border-radius: 3px;
        border-radius: 3px;
        box-sizing: border-box; -moz-box-sizing: border-box;
    }
    .dd-handle:hover { color: #2ea8e5; background: #fff; }
    .dd-item > button { display: block; position: relative; cursor: pointer; float: left; width: 25px; height: 20px; margin: 5px 0; padding: 0; text-indent: 100%; white-space: nowrap; overflow: hidden; border: 0; background: transparent; font-size: 12px; line-height: 1; text-align: center; font-weight: bold; }
    .dd-item > button:before { content: '+'; display: block; position: absolute; width: 100%; text-align: center; text-indent: 0; }
    .dd-item > button[data-action="collapse"]:before { content: '-'; }
    .dd-placeholder,
    .dd-empty { margin: 5px 0; padding: 0; min-height: 30px; background: #f2fbff; border: 1px dashed #b6bcbf; box-sizing: border-box; -moz-box-sizing: border-box; }
    .dd-empty { border: 1px dashed #bbb; min-height: 100px; background-color: #e5e5e5;
        background-image: -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
        -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
        background-image:    -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
        -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
        background-image:         linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
        linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
        background-size: 60px 60px;
        background-position: 0 0, 30px 30px;
    }
    .dd-dragel { position: absolute; pointer-events: none; z-index: 9999; }
    .dd-dragel > .dd-item .dd-handle { margin-top: 0; }
    .dd-dragel .dd-handle {
        -webkit-box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
        box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
    }
    /**
     * Nestable Extras
     */
    .nestable-lists { display: block; clear: both; padding: 30px 0; width: 100%; border: 0; border-top: 2px solid #ddd; border-bottom: 2px solid #ddd; }
    #nestable-menu { padding: 0; margin: 20px 0; }
    #nestable-output,
    #nestable2-output { width: 100%; height: 7em; font-size: 0.75em; line-height: 1.333333em; font-family: Consolas, monospace; padding: 5px; box-sizing: border-box; -moz-box-sizing: border-box; }
    #nestable2 .dd-handle {
        color: #fff;
        border: 1px solid #999;
        background: #bbb;
        background: -webkit-linear-gradient(top, #bbb 0%, #999 100%);
        background:    -moz-linear-gradient(top, #bbb 0%, #999 100%);
        background:         linear-gradient(top, #bbb 0%, #999 100%);
    }
    #nestable2 .dd-handle:hover { background: #bbb; }
    #nestable2 .dd-item > button:before { color: #fff; }
    @media only screen and (min-width: 700px) {
        .dd { float: left; width: 48%; }
        .dd + .dd { margin-left: 2%; }
    }
    .dd-hover > .dd-handle { background: #2ea8e5 !important; }
    /**
     * Nestable Draggable Handles
     */
    .dd3-content { display: block; height: 30px; margin: 5px 0; padding: 5px 10px 5px 40px; color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
        background: #fafafa;
        background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
        background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
        background:         linear-gradient(top, #fafafa 0%, #eee 100%);
        -webkit-border-radius: 3px;
        border-radius: 3px;
        box-sizing: border-box; -moz-box-sizing: border-box;
    }
    .dd3-content:hover { color: #2ea8e5; background: #fff; }
    .dd-dragel > .dd3-item > .dd3-content { margin: 0; }
    .dd3-item > button { margin-left: 30px; }
    .dd3-handle { position: absolute; margin: 0; left: 0; top: 0; cursor: pointer; width: 30px; text-indent: 100%; white-space: nowrap; overflow: hidden;
        border: 1px solid #aaa;
        background: #ddd;
        background: -webkit-linear-gradient(top, #ddd 0%, #bbb 100%);
        background:    -moz-linear-gradient(top, #ddd 0%, #bbb 100%);
        background:         linear-gradient(top, #ddd 0%, #bbb 100%);
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
    }
    .dd3-handle:before { content: '≡'; display: block; position: absolute; left: 0; top: 3px; width: 100%; text-align: center; text-indent: 0; color: #fff; font-size: 20px; font-weight: normal; }
    .dd3-handle:hover { background: #ddd; }
    /**
     * Socialite
     */
    .socialite { display: block; float: left; height: 35px; }
</style>

<div class="card" style="padding:40px;">
    <div class="dd" id="nestable" style="min-width:100%">
        <ol class="dd-list">
            @if(isset($navigations) && !empty($navigations))
                @foreach($navigations as $navigation)
                    @if(isset($navigation['children']) && !empty($navigation['children']))
                        <li class="dd-item" data-id="{{$navigation['id']}}">
                            <div style="position:absolute;top:4px;right:10px;">
                                <span class="ml-auto" style="z-index:10000000;"><span class="editItem" data-id="{{$navigation['id']}}" style="cursor:pointer;padding-right:10px;">edit</span> | <span class="deleteMenuItem" data-id="{{$navigation['id']}}" style="cursor:pointer;padding-left:10px;">delete</span></span>
                            </div>
                            <div class="dd-handle"><div style="display:flex"><span>{{$navigation['show_name']}}</span></div></div>
                            <ol class="dd-list">
                                @foreach($navigation['children'] as $children)
                                    <li class="dd-item" data-id="{{$children['id']}}">
                                        <div style="position:absolute;top:4px;right:10px;">
                                            <span class="ml-auto" style="z-index:10000000;"><span class="editItem" data-id="{{$children['id']}}" style="cursor:pointer;padding-right:10px;">edit</span> | <span class="deleteMenuItem" data-id="{{$children['id']}}" style="cursor:pointer;padding-left:10px;">delete</span></span>
                                        </div>
                                        <div class="dd-handle">
                                            <div style="display:flex">
                                                <span>{{$children['show_name']}}</span>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ol>
                        </li>
                    @else
                    <li class="dd-item" data-id="{{$navigation['id']}}">
                        <div style="position:absolute;top:4px;right:10px;">
                            <span class="ml-auto" style="z-index:10000000;"><span class="editItem" data-id="{{$navigation['id']}}" style="cursor:pointer;padding-right:10px;">edit</span> | <span class="deleteMenuItem" data-id="{{$navigation['id']}}" style="cursor:pointer;padding-left:10px;">delete</span></span>
                        </div>
                        <div class="dd-handle"><div style="display:flex"><span>{{$navigation['show_name']}}</span></div></div>
                    </li>
                    @endif
                @endforeach
            @endif
        </ol>
    </div>
</div>

<div class="card" style="padding:40px;">
    <div class="dd" id="nestable2" style="min-width:100%">
        <ol class="dd-list">
            @if(isset($loyalties2) && !empty($loyalties2))
                @foreach($loyalties2 as $loyalty2)
                    @if(isset($loyalty2['children']) && !empty($loyalty2['children']))
                        <li class="dd-item" data-id="{{$loyalty2['id']}}">
                            <div style="position:absolute;top:4px;right:10px;">
                                <span class="ml-auto" style="z-index:10000000;"><span class="editItem" data-id="{{$loyalty2['id']}}" style="cursor:pointer;padding-right:10px;">edit</span> | <span class="deleteMenuItem" data-id="{{$loyalty2['id']}}" style="cursor:pointer;padding-left:10px;">delete</span></span>
                            </div>
                            <div class="dd-handle"><div style="display:flex"><span>{{$loyalty2['show_name']}}</span></div></div>
                            <ol class="dd-list">
                                @foreach($loyalty2['children'] as $children)
                                    <li class="dd-item" data-id="{{$children['id']}}">
                                        <div style="position:absolute;top:4px;right:10px;">
                                            <span class="ml-auto" style="z-index:10000000;"><span class="editItem" data-id="{{$children['id']}}" style="cursor:pointer;padding-right:10px;">edit</span> | <span class="deleteMenuItem" data-id="{{$children['id']}}" style="cursor:pointer;padding-left:10px;">delete</span></span>
                                        </div>
                                        <div class="dd-handle">
                                            <div style="display:flex">
                                                <span>{{$children['show_name']}}</span>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ol>
                        </li>
                    @else
                        <li class="dd-item" data-id="{{$loyalty2['id']}}">
                            <div style="position:absolute;top:4px;right:10px;">
                                <span class="ml-auto" style="z-index:10000000;"><span class="editItem" data-id="{{$loyalty2['id']}}" style="cursor:pointer;padding-right:10px;">edit</span> | <span class="deleteMenuItem" data-id="{{$navigation['id']}}" style="cursor:pointer;padding-left:10px;">delete</span></span>
                            </div>
                            <div class="dd-handle"><div style="display:flex"><span>{{$loyalty2['show_name']}}</span></div></div>
                        </li>
                    @endif
                @endforeach
            @endif
        </ol>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#nestable').nestable({
            maxDepth: 2
        });
        $('#nestable2').nestable({
            maxDepth: 2
        });
        $('#nestable').on('change', function() {
           var ser =  $('#nestable').nestable('serialize');
            $.ajax({
                type: "POST",
                url: '{{route('handleChangeOrder')}}',
                data: {json:ser},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType:'json',
                success:function(data){
                    if(data.status==='1'){
                        $('#navigationWrapper').html(data.view);
                        $('#type').val('placeholder');
                        $('#controllerName').val('');
                        $('#controllerWrapper').hide();
                        $('#parentWrapper').hide();
                        $('#uriWrapper').hide();

                    }
                    else if(data.status==='2'){
                        //neni meno
                    }
                    else{
                        //nepreslo to
                    }
                }
            });
        });
        $('#nestable2').on('change', function() {
            var ser =  $('#nestable2').nestable('serialize');
            $.ajax({
                type: "POST",
                url: '{{route('handleChangeOrder')}}',
                data: {json:ser},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType:'json',
                success:function(data){
                    if(data.status==='1'){
                        $('#navigationWrapper').html(data.view);
                        $('#type').val('placeholder');
                        $('#controllerName').val('');
                        $('#controllerWrapper').hide();
                        $('#parentWrapper').hide();
                        $('#uriWrapper').hide();

                    }
                    else if(data.status==='2'){
                        //neni meno
                    }
                    else{
                        //nepreslo to
                    }
                }
            });
        });
    });
</script>
