<table id="apiTable" class="table  table-striped table-bordered" style="width:100%">
    <thead>
    <tr>
        <th style="text-align: center;">id</th>
        <th style="text-align: center;">name</th>
        <th style="text-align: center;">api-key</th>
        <th style="text-align: center;">status</th>
        <th style="text-align: center;">akcia</th>
    </tr>
    </thead>
    <tbody>
    @if(isset($apis) && !empty($apis))
        @foreach($apis as $api)
            <tr>
                <td class="editContent" style="text-align: center;">{{$api['id']}}</td>
                <td class="editContent" style="text-align: center;">{{$api['name']}}</td>
                <td class="editContent" style="text-align: center;">{{$api['key']}}</td>
                <td class="editContent" style="text-align: center;"><?=($api['active']==1) ? '<span class="badge badge-success changeStatus" data-name="'.$api['name'].'" style="cursor:pointer;">Aktivovaný</span>' : '<span class="badge badge-warning changeStatus" data-name="'.$api['name'].'" style="cursor:pointer;">Deaktivovaný</span>'?></td>
                <td style="text-align: center;">
                    <a href="{{route('apideveloper.edit',['apideveloper'=>$api['id']])}}" class="editRow badge badge-primary" data-id="{{$api['id']}}" style="cursor:pointer;margin-right:10px;">
                        Upraviť
                    </a>
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>

<script>
    $('.deleteRow').on('click',function(){
        var id = $(this).data().id;
        var form = $('#deleteField_'+id);
        form.submit();
    });

    $(document).on('click','.changeStatus',function(){
        var name = $(this).data().name;
        var td = $(this).closest('td');
        $.ajax({
            type: "POST",
            url: '{{route('handleChangeApiStatus')}}',
            data: {name:name},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType:'json',
            success:function(data){
                if(data.status==='1'){
                    td.html('<span class="badge badge-warning changeStatus" data-name="'+name+'" style="cursor:pointer;">Deaktivovaný</span>');
                }
                else if(data.status==='2'){
                    td.html('<span class="badge badge-success changeStatus" data-name="'+name+'" style="cursor:pointer;">Aktivovaný</span>');
                }
            }
        });
    });
</script>