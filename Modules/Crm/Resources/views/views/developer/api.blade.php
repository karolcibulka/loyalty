@extends('crm::layouts.master')

@section('content')
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <div class="card">
        <div class="card-header">
            <h5>Api</h5>
        </div>
        <div class="card-body">
            <a href="{{route('apideveloper.create')}}" class="btn btn-primary btn-custom">Vytvoriť nový api-key</a>
            <legend></legend>
            @include('crm::views.developer.partials.apiTable')
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#apiTable').DataTable();
        } );
    </script>
@endsection
