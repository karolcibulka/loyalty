@extends('crm::layouts.master')

@section('content')
    <div class="row">
        <div class="col-md-2">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Navigácia - možnosti</h5>
                </div>
                <div class="card-body" id="navigationFormWrapper">
                    @include('crm::views.developer.partials.addItem')
                </div>
            </div>
        </div>
        <div class="col-md-10" id="navigationWrapper">
            @include('crm::views.developer.partials.navigations')
        </div>
    </div>

    <script>
        $(document).on('click','.editItem',function(){
            var id = $(this).data().id;
            $.ajax({
                type: "POST",
                url: '{{route('editItem')}}',
                data: {id:id},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType:'json',
                success:function(data){
                    if(data.status==='1'){
                        $('#navigationFormWrapper').html(data.view);
                    }
                }
            });
        });

        $(document).on('click','.deleteMenuItem',function(){
            if(confirm("Naozaj chcete odstrániť tento item?")){
            var id = $(this).data().id;
                $.ajax({
                    type: "POST",
                    url: '{{route('removeMenuItem')}}',
                    data: {id:id},
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType:'json',
                    success:function(data){
                        if(data.status==='1'){
                            $('#navigationWrapper').html(data.view);
                        }
                    }
                });
            }
        });

        $(document).on('click','#editButton',function(e){
            e.preventDefault();
            var formData = $('#createForm').serializeArray();
            $.ajax({
                type: "POST",
                url: '{{route('editProccess')}}',
                data: formData,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType:'json',
                success:function(data){
                    if(data.status==='1'){
                        $('#navigationWrapper').html(data.view);
                        $('#navigationFormWrapper').html(data.view2);
                        $('#type').val('placeholder');
                        $('#controllerName').val('');
                        $('#controllerWrapper').hide();
                        $('#parentWrapper').hide();
                        $('#uriWrapper').hide();
                        $('#editButton').html('Uložiť')
                    }
                    else if(data.status==='2'){
                        //neni meno
                    }
                    else{
                        //nepreslo to
                    }
                }
            })
        });

        $(document).on('click','#createButton',function(e){
            e.preventDefault();
            var formData = $('#createForm').serializeArray();
            $.ajax({
                type: "POST",
                url: '{{route('store')}}',
                data: formData,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType:'json',
                success:function(data){
                    if(data.status==='1'){
                        $('#navigationWrapper').html(data.view);
                        $('#type').val('placeholder');
                        $('#controllerName').val('');
                        $('#controllerWrapper').hide();
                        $('#parentWrapper').hide();
                        $('#uriWrapper').hide();
                        $('#createButton').html('Uložiť')
                    }
                    else if(data.status==='2'){
                        //neni meno
                    }
                    else{
                        //nepreslo to
                    }
                }
            })
        });

        $('#type').on('change',function(){
           if($(this).val()==='controller'){
               $('#controllerWrapper').show();
               $('#parentWrapper').show();
               $('#uriWrapper').hide();
           }
           else{
               if($(this).val()==='link'){
                   $('#uriWrapper').show();
                   $('#controllerWrapper').hide();
                   $('#parentWrapper').show();
               }
               else{
                   $('#uriWrapper').hide();
                   $('#controllerWrapper').hide();
                   $('#parentWrapper').hide();
               }
           }
        });

    </script>


@endsection
