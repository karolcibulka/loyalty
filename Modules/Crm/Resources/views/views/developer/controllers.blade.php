@extends('crm::layouts.master')

@section('content')
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>


    <div class="card">
        <div class="card-header">
            <h5>Controllers</h5>
        </div>
        <div class="card-body">
            <div id="formWrapper">
                @include('crm::views.developer.partials.controllerForm')
            </div>
            <div class="row" style="">
                <div class="col-md-12" style="text-align:center;">
                    <span style="">Route::get('<strong class="controllerName"></strong>','<strong class="controllerChoosed"></strong>@index')->name('<strong class="controllerName"></strong>')</span>
                </div>
            </div>
            <legend></legend>
            <div id="tableWrapper">
                @include('crm::views.developer.partials.controllerTable')
            </div>
        </div>
    </div>
    <script>
        $(document).on('input','#controllerName',function(){
           $('.controllerName').html($(this).val());
        });
        $(document).on('input','#controllerSelect',function(){
           $('.controllerChoosed').html($(this).val());
        });
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>

    <script>
        $(document).on('click','.createNew',function(){
            window.location.href="{{route('controllers')}}";
        });
    </script>
@endsection

