@extends('crm::layouts.master')
@section('content')
<div class="card">
    <div class="card-header">
        <h5>Permissions</h5>
    </div>
    <div class="card-body">
        <a href="{{route('permission.create')}}" class="btn btn-primary btn-custom">Vytvoriť permission</a>
        <legend></legend>
        @include('crm::views.developer.partials.permissionsTable')
    </div>
</div>
@endsection
