@extends('crm::layouts.app')

@section('content')

    <div class="content d-flex justify-content-center align-items-center">

        <!-- Login form -->
        <div class="card mb-0" style="width: 400px; position: absolute;top: 50%;-ms-transform: translateY(-50%); transform: translateY(-50%);">
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="card-body">
                    <div class="text-center mb-3">
                        <i class="icon-reading icon-2x text-slate-300 border-slate-300 border-3 rounded-round p-3 mb-3 mt-1"></i>
                        <h5 class="mb-0">Login to your account</h5>
                        <span class="d-block text-muted">Enter your credentials below</span>
                    </div>

                    <div class="form-group form-group-feedback form-group-feedback-left">
                        <input type="email" name="email" class="form-control" placeholder="{{ __('E-Mail Address') }}">
                        <div class="form-control-feedback">
                            <i class="icon-user text-muted"></i>
                        </div>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>

                    <div class="form-group form-group-feedback form-group-feedback-left">
                        <input type="password" name="password" class="form-control" placeholder="{{ __('Password') }}">
                        <div class="form-control-feedback">
                            <i class="icon-lock2 text-muted"></i>
                        </div>
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block"> {{ __('Login') }} <i class="icon-circle-right2 ml-2"></i></button>
                    </div>
                </div>
            </form>
        </div>

        <!-- /login form -->

    </div>
@endsection
