<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4> <span class="font-weight-semibold">{{Request::segment(3) ? Request::segment(3) : Request::segment(2)}}</span> - {{Request::segment(3) ? Request::segment(3).'.'.Request::segment(3) : Request::segment(2).'.'.Request::segment(2)}}</h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>Dashboard</a>
                <span class="breadcrumb-item active">{{Request::segment(3) ? Request::segment(3) : Request::segment(2)}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
