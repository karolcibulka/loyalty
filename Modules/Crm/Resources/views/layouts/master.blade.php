<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{(isset($title) && !empty($title)) ? $title : 'CRM'}}</title>

    <!-- Global stylesheets -->
    <link href="{{asset('assets/global_assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/bootstrap_limitless.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/components.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/colors.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/layout.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">


    <link href="{{asset('assets/css/bootstrap-datetimepicker.css')}}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="{{asset('assets/global_assets/js/main/jquery.min.js')}}"></script>
    <script src="{{asset('assets/global_assets/js/main/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('assets/global_assets/js/plugins/loaders/blockui.min.js')}}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="{{asset('assets/global_assets/js/plugins/visualization/d3/d3.min.js')}}"></script>
    <script src="{{asset('assets/global_assets/js/plugins/visualization/c3/c3.min.js')}}"></script>
    <script src="{{asset('assets/global_assets/js/plugins/visualization/d3/d3_tooltip.js')}}"></script>
    <script src="{{asset('assets/global_assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
    <script src="{{asset('assets/global_assets/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
    <script src="{{asset('assets/global_assets/js/plugins/ui/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/global_assets/js/plugins/pickers/daterangepicker.js')}}"></script>
    <script src="{{asset('assets/js/nestable.js')}}"></script>

    <script src="{{asset('assets/js/app.js')}}"></script>
    <script src="{{asset('assets/global_assets/js/demo_pages/charts/c3/c3_advanced.js')}}"></script>
    <script src="{{asset('assets/global_assets/js/demo_pages/dashboard.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>


    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" />

    @hasSection('summernote')
        <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote.min.js"></script>
    @endif
    <!-- /theme JS files -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />

</head>

<body>

<style>
    div.bootstrap-datetimepicker-widget.dropdown-menu.widget.usetwentyfour.bottom{
        background-color:white !important;
        width:480px !important;
    }
    .datepicker{
        background-color:white !important;
        width:480px !important;
    }

    .languageRequired{
        display:none;
    }
    .language_sk{
        display:block;
    }
    .fade:not(.show) {
        display:none;
    }
    div.bootstrap-datetimepicker-widget.dropdown-menu.usetwentyfour.bottom{
        min-width:500px !important;
    }
</style>

<!-- Main navbar -->

<div class="navbar navbar-expand-md navbar-dark">
    <div class="navbar-brand" style="position:relative">
       <div style="position:absolute;top:9px;left:0;font-size:22px;">
           <a href="{{route('home')}}" class="text-white"><strong>CRM</strong> <small style="font-size: 12px;">TRAVELDATA</small></a>
       </div>
    </div>
    <div class="d-md-none">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
            <i class="icon-tree5"></i>
        </button>
        <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
            <i class="icon-paragraph-justify3"></i>
        </button>
    </div>

    <div class="collapse navbar-collapse" id="navbar-mobile">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                    <i class="icon-paragraph-justify3"></i>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown dropdown-user">
                <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
                    <span><img src="{{asset('assets/flags/'.Session::get('lang').'.svg')}}" width="18px" alt=""> &nbsp; @lang('global.lang.'.Session::get('lang'))</span>
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                    @if(isset($languages) && !empty($languages))
                        @foreach($languages as $language)
                            <a href="#" data-lang="{{$language['lang_code']}}" class="changeLang dropdown-item"><img src="{{asset('assets/flags/'.$language['lang_code'].'.svg')}}" alt=""> @lang('global.'.$language['lang_name'])</a>
                        @endforeach
                    @endif
                </div>
            </li>
        </ul>

        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown dropdown-user">
                <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
                    @auth
                        <span>{{Auth::user()->name}}</span>
                    @endauth
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                    <form action="{{ route('logout') }}" method="post">
                        @csrf
                    <button class="dropdown-item"><i class="icon-switch2"></i> Logout</button>
                    </form>
                </div>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->


<!-- Page content -->
<div class="page-content">

    <!-- Main sidebar -->
    <div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

        <!-- Sidebar mobile toggler -->
        <div class="sidebar-mobile-toggler text-center">
            <a href="#" class="sidebar-mobile-main-toggle">
                <i class="icon-arrow-left8"></i>
            </a>
            Navigation
            <a href="#" class="sidebar-mobile-expand">
                <i class="icon-screen-full"></i>
                <i class="icon-screen-normal"></i>
            </a>
        </div>
        <!-- /sidebar mobile toggler -->


        <!-- Sidebar content -->
        <div class="sidebar-content">

            <!-- User menu -->

            <!-- /user menu -->


            <!-- Main navigation -->
            <div class="card card-sidebar-mobile">
                @auth
                    @include('crm::layouts.navigation')
                @endauth
            </div>
            <!-- /main navigation -->

        </div>
        <!-- /sidebar content -->

    </div>
    <!-- /main sidebar -->


    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Page header -->
        @include('crm::layouts.breadcrumb')
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">
            @yield('content')
        </div>

    </div>
    <!-- /main content -->

</div>
<script>
    $('.changeLang').on('click',function(){
      var lang =  $(this).data().lang;
        $.ajax({
            type: "POST",
            url: '{{route('changeLang')}}',
            data: {lang:lang},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType:'json',
            success:function(data){
                if(data.status==='1'){
                    window.location.reload();
                }
            }
        });
    });

    $('.btn-custom').on('click',function(){
       $(this).html('<i class="icon-spinner2 spinner"></i>');
    });

</script>
</body>
</html>
