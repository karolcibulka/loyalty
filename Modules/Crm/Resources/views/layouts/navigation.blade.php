
<ul class="nav nav-sidebar" data-nav-type="accordion">
    <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Hlavná ponuka</div> <i class="icon-menu" title="Hlavná ponuka"></i></li>
    <li class="nav-item">
        <a href="{{route('home')}}" class="nav-link {{'home' == Request::segment(2) || empty(Request::segment(3)) ? 'active' : ''}}">
            <i class="icon-home4"></i>
            <span>
                Dashboard
            </span>
        </a>
    </li>

    <!-- Main -->
    <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">CRM</div> <i class="icon-menu" title="CRM"></i></li>

    @if(isset($navs) && !empty($navs))
        @foreach($navs as $nav)
            @if(isset($nav['children']) && !empty($nav['children']))
                @php
                    $arr = array();
                    foreach($nav['children'] as $ch){
                        $arr[] = $ch['name'];
                        $arr[] = $ch['name'].'s';
                        $substr = substr($ch['name'], 0, -1);
                        $arr[] = $substr.'ies';
                    }
                @endphp

                <li class="nav-item nav-item-submenu {{(in_array(Request::segment(3),$arr)) ? 'nav-item-open' : ''}}">
                    <a href="#" class="nav-link"><i class="icon-{{(isset($nav['icon']) && !empty($nav['icon'])) ? $nav['icon'] : 'copy'}}"></i> <span>{{$nav['navigation_name']}}</span></a>

                    <ul class="nav nav-group-sub" data-submenu-title="{{$nav['navigation_name']}}" style="{{(in_array(Request::segment(3),$arr)) ? 'display:block;' : 'display:none;'}}">
                        @foreach($nav['children'] as $children)
                            @can($children['name'].'.show')
                                <li class="nav-item">
                                    <a href="{{route($children['name'])}}" class="nav-link {{$children['name'] == Request::segment(3) || $children['name'].'s' == Request::segment(1) ? 'active' : ''}}">
                                        <i class="icon-{{(isset($children['icon']) && !empty($children['icon'])) ? $children['icon'] : 'copy'}}"></i>
                                        {{$children['navigation_name']}}
                                    </a>
                                </li>
                            @endcan
                        @endforeach
                    </ul>
                </li>
            @else
                @if($nav['type']=='placeholder')
                @else
                    @can($nav['name'].'.show')
                        <li class="nav-item">
                            <a href="{{route($nav['name'])}}" class="nav-link {{$nav['name'] === Request::segment(3) || $nav['name'].'s' == Request::segment(3) || $nav['name'].'ies' == Request::segment(3) || substr($nav['name'], 0, -1).'ies' == Request::segment(3) ? 'active' : ''}}">
                                <i class="icon-{{$nav['icon']}}"></i>
                                <span>{{$nav['navigation_name']}}</span>
                            </a>
                        </li>
                    @endcan
                @endif
            @endif
        @endforeach
    @endif

    <!-- /main -->
    <!-- Loyalty-->
    @if(isset($loyalties) && !empty($loyalties))
        <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Loyalty zóna</div> <i class="icon-menu" title="Loyalty zóna"></i></li>
        @foreach($loyalties as $loyalty)
            @if(isset($loyalty['children']) && !empty($loyalty['children']))
                @php
                    $arr2 = array();
                    foreach($loyalty['children'] as $ch){
                        $arr2[] = $ch['name'];
                        $arr2[] = $ch['name'].'s';
                        $substr = substr($ch['name'], 0, -1);
                        $arr2[] = $substr.'ies';
                    }
                @endphp
                <li class="nav-item nav-item-submenu {{(in_array(Request::segment(3),$arr2)) ? 'nav-item-open' : ''}}">
                    <a href="#" class="nav-link"><i class="icon-{{(isset($loyalty['icon']) && !empty($loyalty['icon'])) ? $loyalty['icon'] : 'copy'}}"></i> <span>{{$loyalty['navigation_name']}}</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="{{$loyalty['navigation_name']}}" style="{{(in_array(Request::segment(3),$arr2)) ? 'display:block;' : 'display:none;'}}">
                        @foreach($loyalty['children'] as $loyaltyChildren)
                            @can($loyaltyChildren['name'].'.show')
                                <li class="nav-item">
                                    <a href="{{route($loyaltyChildren['name'])}}" class="nav-link {{$loyaltyChildren['name'] == Request::segment(3) || $loyaltyChildren['name'].'s' == Request::segment(2) ? 'active' : ''}}">
                                        <i class="icon-{{(isset($loyaltyChildren['icon']) && !empty($loyaltyChildren['icon'])) ? $loyaltyChildren['icon'] : 'copy'}}"></i>
                                        {{$loyaltyChildren['navigation_name']}}
                                    </a>
                                </li>
                            @endcan
                        @endforeach
                    </ul>
                </li>
            @else
                @if($loyalty['type']=='placeholder')
                @else
                    @can($loyalty['name'].'.show')
                        <li class="nav-item">
                            <a href="{{route($loyalty['name'])}}" class="nav-link {{$nav['name'] === Request::segment(3) || $nav['name'].'s' == Request::segment(3) || $nav['name'].'ies' == Request::segment(2) || substr($nav['name'], 0, -1).'ies' == Request::segment(3) ? 'active' : ''}}">
                                <i class="icon-{{$loyalty['icon']}}"></i>
                                <span>{{$loyalty['navigation_name']}}</span>
                            </a>
                        </li>
                    @endcan
                @endif
            @endif
        @endforeach
    @endif
    <!-- End loyalty-->


    @if(in_array(Auth::user()->email,$dev))
        @if(isset($developers) && !empty($developers))
            @php
                foreach($developers as $developer){
                $array[] = $developer['name'];
            }
            @endphp
            <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Developerská zóna</div> <i class="icon-menu" title="Developerská zóna"></i></li>
            <li class="nav-item nav-item-submenu {{in_array(Request::segment(3),$array) ? 'nav-item-open' : ''}}">
                <a href="#" class="nav-link"><i class="icon-cogs"></i> <span>Nastavenia</span></a>
                <ul class="nav nav-group-sub" data-submenu-title="Layouts" style=" {{in_array(Request::segment(3),$array) ? 'display:block;' : 'display:none;'}}">
                    @foreach($developers as $developer)
                        <li class="nav-item">
                            <a href="{{route($developer['name'])}}" class="nav-link {{Request::segment(3)==$developer['name'] ? 'active' : ''}}">
                                <i class="icon-cog3"></i>
                                {{$developer['show_name']}}
                            </a>
                        </li>
                    @endforeach
                    <li class="nav-item"><a href="#" class="nav-link disabled">Other stuff <span class="badge bg-transparent align-self-center ml-auto">Coming soon</span></a></li>
                </ul>
            </li>
        @endif
    @endif

</ul>
