@extends('crm::layouts.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @if(Session::has('permission'))
            <div class="col-md-12" style="margin-bottom:30px;">
                <div class="alert alert-danger text-center">
                    <strong>Nemáte právo navštíviť túto stránku!</strong><br>
                   {!! Session::get('permission') !!}
                </div>
            </div>
        @endif
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
