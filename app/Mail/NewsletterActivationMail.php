<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewsletterActivationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $link;
    public $deactivationLink;


    public function __construct($link,$deactivationLink)
    {
        $this->link = $link;
        $this->deactivationLink = $deactivationLink;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('loyalty::emails.activationNewsletter')->subject('Aktivácia newsletterov');
    }
}
