<?php

namespace App\Models\External;

use Illuminate\Database\Eloquent\Model;

class EmailHistory extends Model
{
    protected $fillable = ['response'];
}
