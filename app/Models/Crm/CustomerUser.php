<?php

namespace App\Models\Crm;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class CustomerUser extends Authenticatable
{
    use Notifiable;

    protected $guard = 'customer';

    protected $fillable = [
       'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
