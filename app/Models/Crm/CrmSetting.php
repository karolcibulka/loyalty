<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Model;

class CrmSetting extends Model
{
    protected $fillable = ['driver','name','host','port','username','password','address','mailchimp'];
}
