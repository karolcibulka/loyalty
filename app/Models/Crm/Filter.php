<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Model;

class Filter extends Model
{
    protected $fillable = ['internal_name','sql','raw','description','deleted','active','show_in'];
}
