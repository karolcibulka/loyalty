<?php

namespace App\Models\Loyalty;

use Illuminate\Database\Eloquent\Model;

class LoyaltyNavigation extends Model
{
    protected $fillable = ['order'];
}
