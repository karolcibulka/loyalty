<?php


namespace App\Libraries;


use App\Models\Crm\Customer;
use App\Models\Crm\CustomerGroup;
use App\Models\Crm\CustomerUser;
use App\Models\Crm\Point;
use App\Models\Loyalty\LoyaltySetting;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CustomerLibrary
{

    public function __construct()
    {
    }

    public function getOrCreateCustomer($email,$last_name){
        $sluggedLastName = Str::slug($last_name,'');
        $customerUser = Customer::where('email','=',$email)->where('encrypted_last_name','=',$sluggedLastName)->first();
        if(!is_null($customerUser)){
            return $customerUser['id'];
        }
        else{

            $cardNumberFormat = $this->getCardNumberFormat();
            $cardNumber = $this->getCardNumber($cardNumberFormat);

            $createCustomerData = array(
                'group_id' => $this->getDefaultCustomerGroup(),
                'encrypted_last_name' => $sluggedLastName,
                'email' => $email,
                'token' => Str::random(40),
                'created_at' => date('Y-m-d H:i:s'),
                'is_registered' => '0',
                'card_number' => $cardNumber,
            );

            $customerID = Customer::insertGetId($createCustomerData);
            $resPoints = Point::insert(array('customer_id'=>$customerID,'points'=>'0','created_at'=>date('Y-m-d H:i:s')));


            return $customerID;
        }
    }

    public function getCardNumberFormat(){
        $defaultCardNumberFormat = 'RRMMDDCCCC';
        $settings = LoyaltySetting::first();
        if(isset($settings) && !empty($settings)){
            if(isset($settings['card_number_format']) && !empty($settings['card_number_format'])){
                return $settings['card_number_format'];
            }
            else{
                return $defaultCardNumberFormat;
            }
        }
        else{
            return $defaultCardNumberFormat;
        }
    }

    public function getCardNumber($format){

        $cardNumber = $format;

        $cardNumber = str_replace('RRRR',date('Y'),$cardNumber);
        $cardNumber = str_replace('RR',date('y'),$cardNumber);
        $cardNumber = str_replace('MM',date('m'),$cardNumber);
        $cardNumber = str_replace('DD',date('d'),$cardNumber);

        $cCount = substr_count($cardNumber, 'C');

        $cString = '';
        for($i = 0;$i<$cCount;$i++){
            $cString .= 'C';
        }

        //dd(date('Y-m-d'));
        $q = Customer::select(DB::raw('COUNT(id) as count'));
        if(strpos($format,'RRRR') !== false || strpos($format,'RR') !== false ){
            $q->whereYear('created_at','=',date('Y'));
        }
        if( strpos($format,'MM') !== false ){
            $q->whereMonth('created_at','=',date('m'));
        }
        if(strpos($format,'DD') !== false){
            $q->whereDay('created_at','=',date('d'));
        }

        $customersResult = $q->first();

        if($cString!=''){
            $cardNumber = str_replace($cString,str_pad($customersResult['count'],$cCount,'0',STR_PAD_LEFT),$cardNumber);
        }

        return $cardNumber;
    }

    public function updateCustomersCardNumber($format){
        $customers = Customer::all();
        if(isset($customers) && !empty($customers)){
            foreach($customers as $customer){
                Customer::where('id',$customer['id'])
                    ->update(array('card_number'=>$this->getCardNumber($format)));
            }
        }
        return true;
    }

    public function getDefaultCustomerGroup(){
        $group = CustomerGroup::where('is_default','=','1')->first();
        return $group['id'];
    }

}
