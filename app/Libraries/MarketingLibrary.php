<?php


namespace App\Libraries;

use App\Mail\NewsletterActivationMail;
use App\Models\Crm\CrmSetting;
use DrewM\MailChimp\MailChimp;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Mail;
use Newsletter;

class MarketingLibrary
{

    public function __construct()
    {
    }

    public function getAllSubscribers(){
        $subscribers = Newsletter::getMembers();

        return $subscribers;
    }

    public function makePrettyArrayOfMembers($members){
        $response = array();

        if(isset($members) && !empty($members)){
            if(isset($members['members']) && !empty($members['members'])){
                foreach($members['members'] as $member){
                    if($member['status'] != 'unsubscribed'){
                        $response['emails'][$member['id']] = $member['email_address'];
                    }
                    $response['members'][$member['email_address']]['email'] = $member['email_address'];
                    $response['members'][$member['email_address']]['last_name'] = $member['merge_fields']['LNAME'];
                    $response['members'][$member['email_address']]['first_name'] = $member['merge_fields']['FNAME'];
                    $response['members'][$member['email_address']]['stats']['opened'] = $member['stats']['avg_open_rate'];
                    $response['members'][$member['email_address']]['stats']['clicked'] = $member['stats']['avg_click_rate'];
                    $response['members'][$member['email_address']]['vip'] = $member['vip'];
                    $response['members'][$member['email_address']]['source'] = $member['source'];
                    $response['members'][$member['email_address']]['id'] = $member['id'];
                }
            }
        }

        return $response;
    }

    public function addCustomerToMailChimp($customer){
        $crmSettings = CrmSetting::first();
        if($crmSettings['mailchimp'] == '1') {
            Newsletter::subscribeOrUpdate($customer['email'], ['FNAME' => $customer['first_name'], 'LNAME' => $customer['last_name']]);
        }
    }

    public function removeCustomerFromMailChimp($customer){

        $crmSettings = CrmSetting::first();
        if($crmSettings['mailchimp'] == '1'){
            Newsletter::unsubscribe($customer['email']);
        }
    }

    public function updateAllCustomers($customers,$status){
        $post = array();

        if(!empty($customers)){
            foreach($customers as $key => $customer){
                $post['members'][$key]['email_address'] = $customer['email'];
                $post['members'][$key]['status'] = $status;
                $post['members'][$key]['email_type'] = 'html';
                $post['members'][$key]['merge_fields']['FNAME'] = $customer['first_name'];
                $post['members'][$key]['merge_fields']['LNAME'] = $customer['last_name'];
                $post['update_existing']=true;
            }
        }

        $client = new Client();
        $response = $client->request('POST',$this->getApi()->getApiEndpoint().'/lists/'.env('MAILCHIMP_LIST_ID'),[
            'headers'=>[
                'Authorization:'=> env('MAILCHIMP_APIKEY'),
                'Content-Type' => 'application/json',
            ],
            'body' => json_encode((object)$post)
        ]);

        return $response->getStatusCode() == 200 ? true : false;

    }

    public function getApi(){
       return Newsletter::getApi();
    }

    public function sendNewsletterActivationEmail($customer){
        $link = route('newsletterActivation',$customer['token']);
        $deactivationLink = route('newsletterDeactivation',$customer['token']);
        Mail::to($customer['email'])->send(new NewsletterActivationMail($link,$deactivationLink));
    }
}
