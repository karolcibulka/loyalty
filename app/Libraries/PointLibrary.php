<?php

namespace App\Libraries;

use App\Models\Crm\Customer;
use App\Models\Crm\LoyaltyAccountSetting;
use App\Models\Crm\Point;
use App\Models\Crm\PointHistory;
use Illuminate\Support\Facades\Session;

class PointLibrary {

    public $customer;
    public $basket;
    public $group_id;
    public $reservation_id;

    public function __construct($customer,$basketPrice,$reservation_id)
    {
        $this->customer = $customer;
        $this->basket = $basketPrice;
        $this->reservation_id = $reservation_id;
    }

    public function checkCustomerProfile(){
        if(isset($this->customer['group_id']) && !empty($this->customer['group_id'])){
            $this->group_id = $this->customer['group_id'];
            return true;
        }
        else{
            return false;
        }
    }

    public function checkAddProfiles(){

        $add_points = 0;
        $price  = 0;

        if(!empty($this->basket)){
            foreach($this->basket as $basket_item){
                if($basket_item['add_points'] == '1'){
                    $price += $basket_item['price'];
                }
            }
        }

        $profiles = LoyaltyAccountSetting::where(function($query){
            $query->where('date_required','=','0');
            $query->where('active','=','1');
            $query->where('deleted','=','0');
            $query->where('group_id','=',$this->group_id);
            $query->where('loyalty_account_setting_type','=','ADD');
        })
            ->orWhere(function($query){
                $query->where('date_required','=','1');
                $query->where('date_from','<=',date('Y-m-d H:i:s'));
                $query->where('date_to','>=',date('Y-m-d H:i:s'));
                $query->where('active','=','1');
                $query->where('deleted','=','0');
                $query->where('group_id','=',$this->group_id);
                $query->where('loyalty_account_setting_type','=','ADD');
            })
            ->join('loyalty_account_setting_groups as lasg','lasg.loyalty_account_setting_id','=','loyalty_account_settings.id')
            ->get();

        if(isset($profiles) && !empty($profiles)){
            foreach($profiles as $profile){
                if($profile['extra_key'] == 'RATIO'){
                    $add_points += $price * $profile['extra_value'];
                }
                elseif($profile['extra_key'] == 'POINTS'){
                    $add_points += $profile['extra_value'];
                }
            }

        }

        if($add_points>0){
            $newPoints = $this->customer['points'] + $add_points;

            $points = array(
                'customer_id' => $this->customer['id'],
                'points' => $add_points,
                'reason' => 'Bonus rezervácia č.'.$this->reservation_id,
                'created_at' => date('Y-m-d H:i:s',strtotime('+ 10 seconds')),
                'type' => '+',
                'old_points' => $this->customer['points'],
                'new_points' => $newPoints,
                'reservation_id' => $this->reservation_id,
            );

            PointHistory::insert($points);

            $pointRecord = Point::where('customer_id','=',$this->customer['id'])->first();
            if(!is_null($pointRecord)){
                Point::where('customer_id','=',$this->customer['id'])->update(array('points'=>$newPoints,'updated_at'=>date('Y-m-d H:i:s')));
            }
            else{
                Point::insert(array('customer_id'=>$this->customer['id'],'points'=>$newPoints,'created_at'=>date('Y-m-d H:i:s')));
            }


            $customer = Customer::where('id','=',$this->customer['id'])->leftJoin('points','points.customer_id','=','customers.id')->get()->first();
            Session::put('customer_session',$customer);


        }

        return $add_points;
    }
}
