<?php


namespace App\Libraries;

use App\Models\System\Log;

class LogLibrary
{
    public $controller;
    public $action;
    public $user_id;
    public $api_id;
    public $api;
    public $request;
    public $old_data;

    public function __construct($controller, $action, $user_id = null, $api = '0', $api_id = null, $request = null, $old_data = null)
    {
        $this->controller = $controller;
        $this->action     = $action;
        $this->user_id    = $user_id;
        $this->api_id     = $api_id;
        $this->api        = $api;
        $this->old_data   = $old_data;
        $this->request    = $request;
    }

    public function storeLog()
    {
        $insertLog = array(
            'controller' => $this->controller,
            'action' => $this->action,
            'user_id' => $this->user_id,
            'api' => $this->api,
            'api_id' => $this->api_id,
            'request' => $this->request,
            'old_data' => $this->old_data,
            'created_at' => date('Y-m-d H:i:s'),
        );

        Log::insert($insertLog);
    }
}
