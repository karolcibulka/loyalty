<?php

namespace App\Providers;

use App\Models\Crm\CrmSetting;
use Illuminate\Mail\MailServiceProvider;
use Illuminate\Mail\TransportManager;
use Illuminate\Support\Facades\Config;


class CustomMailServiceProvider extends MailServiceProvider
{

    protected function registerSwiftTransport(){

        if ($settings = CrmSetting::first()) {
            if (isset($settings['driver']) && !empty($settings['driver']) && $settings['driver'] != 'default') {
                $driver = $settings['driver'];
                $conf   = [
                    'driver' => $driver,
                    'host' => isset($settings['host']) && !empty($settings['host']) ? $settings['host'] : env('MAIL_HOST'),
                    'port' => isset($settings['port']) && !empty($settings['port']) ? $settings['port'] : env('MAIL_PORT'),
                    'from' => [
                        'address' => isset($settings['address']) && !empty($settings['address']) ? $settings['address'] : Config::get('mail.from.address'),
                        'name' => isset($settings['name']) && !empty($settings['name']) ? $settings['name'] : Config::get('mail.from.name'),
                    ],
                    'username' => isset($settings['username']) && !empty($settings['username']) ? $settings['username'] : env('MAIL_USERNAME'),
                    'password' => isset($settings['password']) && !empty($settings['password']) ? $settings['password'] : env('MAIL_PASSWORD')
                ];

                Config::set('mail', $conf);
                $this->app['config']['mail'] = $conf;
            }
        }

        $this->app->singleton('swift.transport', function ($app){
            return new TransportManager($app);
        });
    }
}
