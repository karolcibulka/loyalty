<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     */

    public function render($request, Exception $exception)
    {
        //dd($exception);
        //$host = $request->header('host');


        if(strpos(Request::server('REQUEST_URI'),'dashboard')!==false){
            return parent::render($request, $exception);
        }
        elseif($this->isHttpException($exception))
        {
            switch ($exception->getStatusCode())
            {
                case 500:
                    //return redirect()->guest(Config::get('app.locale_prefix'));
                    break;
                case 429:
                    Session::flash('throttleResponse',$exception->getHeaders());
                    return  redirect()->route('throttleLogin');
                    break;
                default:
                    return $this->renderHttpException($exception);
                    break;
            }
        }
        else
        {
            return parent::render($request, $exception);
        }
    }
}
